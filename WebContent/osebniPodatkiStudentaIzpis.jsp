<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, si.fri.estudent.jpa.*" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	String[][] tabela=(String[][])request.getAttribute("tabela");
	String vpisna=(String)request.getAttribute("vpisna");
	String[][][] vpisi=(String[][][])request.getAttribute("vpisi");
%>

<h1>OSEBNI PODATKI ŠTUDENTA</h1>
<table class="tabele" style="width: 400px; margin-left: 255px;">
	<% for(String[] vrstica:tabela){ %>
		<tr>
			<th style="width: 180px"><%=vrstica[0] %></th>
			<td><%= vrstica[1] %></td>
		</tr>
	<%} %>	
</table>
<br />
<br />

<%if(vpisi.length > 0){ %>
	<h1>VPISI</h1>
<% } %>


<% for(String[][] tab:vpisi){%>
	<table class="tabele" style="width: 400px; margin-left: 255px;">
	<% for(String[] vrstica:tab){ %>
		<tr>
			<th style="width: 180px"><%= vrstica[0] %></th>
			<td><%= vrstica[1] %></td>
		</tr>
	<% } %>
	</table>		
	<br />
<% } %>


<a href="OsebniPodatkiStudentaServlet?nacinIzpisa=tiskanje&vpisnaStevilka=<%= vpisna %>">Izpis za tiskanje </a>