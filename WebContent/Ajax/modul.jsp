<%@page import="si.fri.estudent.jpa.Modul"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
List<Modul> moduli = null;

if (request.getAttribute("moduli") != null) {
	moduli = (List<Modul>)request.getAttribute("moduli");
}
if (moduli != null && moduli.size() > 0) { %>
	<option value='0'>-- Izberi modul --</option>

	<%
	for (Modul m : moduli) { %>
		<option value='<%= m.getIdModul() %>'><%= m.getNazivModul() %></option>
	<% }
} else { %>
	<option value='0' disabled='disabled'>Ta kombinacija študija nima modulov</option>
<% } %>