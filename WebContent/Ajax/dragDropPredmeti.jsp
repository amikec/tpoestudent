<%@page import="si.fri.estudent.jpa.Predmet"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<br />
<div id="drag">
<%
	if (request.getAttribute("neIzbraniPredmeti") != null) {
		List<Predmet> neIzbraniPredmeti = (List<Predmet>)request.getAttribute("neIzbraniPredmeti");
		
		
		for (Predmet p : neIzbraniPredmeti) { %>
			<div id='<%= p.getIdPredmeta() %>'><%= p.getSifraPredmeta() + " &nbsp;-- " + p.getNazivPredmet() %></div><%
		}
	}
%>
</div>
<div id="drop">
<%
	String inputHiddenIdPredmetov = "";

	if (request.getAttribute("izbraniPredmeti") != null) {
		List<Predmet> izbraniPredmeti = (List<Predmet>)request.getAttribute("izbraniPredmeti");
		
		
		for (Predmet p : izbraniPredmeti) { %>
			<div id='<%= p.getIdPredmeta() %>'><%= p.getSifraPredmeta() + " &nbsp;-- " + p.getNazivPredmet() %></div><%
			inputHiddenIdPredmetov += p.getIdPredmeta() + ",";
		}
		
		if (inputHiddenIdPredmetov.length() > 0) {
			inputHiddenIdPredmetov = inputHiddenIdPredmetov.substring(0, inputHiddenIdPredmetov.length() - 1);
		}
	}
%>
</div>
<input type="hidden" name="stareIzbire" value='<%= inputHiddenIdPredmetov %>' />
<input type="hidden" name="izbire" value='<%= inputHiddenIdPredmetov %>' />
<script type="text/javascript">
	initializeDragDrop();
</script>