<%@page import="si.fri.estudent.jpa.Studijska_smer"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
List<Studijska_smer> studijskeSmeri = null;

if (request.getAttribute("studijskeSmeri") != null) {
	studijskeSmeri = (List<Studijska_smer>)request.getAttribute("studijskeSmeri");
}
if (studijskeSmeri != null && studijskeSmeri.size() > 0) { %>
	<option value='0'>-- Izberi študijsko smer --</option>
	
	<%
	for (Studijska_smer ss : studijskeSmeri) { %>
	<option value='<%= ss.getIdStudijskaSmer() %>'><%= ss.getKraticaStudijskaSmer() + " - " + ss.getNazivStudijskaSmer() %></option>
<% }
} else { %>
	<option value='0' disabled='disabled'>Ta kombinacija študija nima študijskih smeri</option>
<% } %>