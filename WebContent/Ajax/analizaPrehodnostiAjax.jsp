<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, si.fri.estudent.jpa.*, si.fri.estudent.utilities.SkupenProgram" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<% 	
	String prvoLeto = (String)request.getAttribute("leto1");
	String drugoLeto = (String)request.getAttribute("leto2");
	String[][] tabela=(String[][])request.getAttribute("tabela");	
%>

<table class="tabele" style="margin: 0px auto">
	<tr >
		<td colspan="2"></td>
		<th>Število vpisanih</th>
		<th>Odstotek</th>
	</tr>
	<tr>
		<td><%= prvoLeto %></td>
		<td><%= tabela[0][0] %></td>
		<td style="text-align: center"><%= tabela[0][1] %></td>
		<td style="text-align: center"><%= tabela[0][2] %></td>
	</tr>
	<tr>
		<td rowspan="3"><%= drugoLeto %></td>
		<td><%= tabela[1][0] %></td>
		<td style="text-align: center"><%= tabela[1][1] %></td>
		<td style="text-align: center"><%= tabela[1][2] %></td>
	</tr>
	<tr>
		<td><%= tabela[2][0] %></td>
		<td style="text-align: center"><%= tabela[2][1] %></td>
		<td style="text-align: center"><%= tabela[2][2] %></td>
	</tr>
	<tr>
		<td><%= tabela[3][0] %></td>
		<td style="text-align: center"><%= tabela[3][1] %></td>
		<td style="text-align: center"><%= tabela[3][2] %></td>
	</tr>
	
</table>
