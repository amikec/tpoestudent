<%@page import="si.fri.estudent.jpa.Predmet"%>
<%@page import="java.text.DateFormat"%>
<%@page import="si.fri.estudent.jpa.Izpitni_rok"%>
<%@page import="si.fri.estudent.jpa.Ocena"%>
<%@page import="java.util.List"%>
<%@page import="si.fri.estudent.jpa.Kombinacije_izvajalcev"%>
<%@page import="si.fri.estudent.utilities.Constants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
List<double[]> podatki1 = null;
List<double[]> podatki2 = null;

List<Kombinacije_izvajalcev> ki = null;
List<Predmet> predmetki = null;
Kombinacije_izvajalcev k = null;
Ocena o = null;
String nacin = null;
String od = null;
String dox = null;

if (request.getAttribute("izvajalci") != null) {
	ki = (List<Kombinacije_izvajalcev>)request.getAttribute("izvajalci");
}
if (request.getAttribute("nacin") != null) {
	nacin = request.getAttribute("nacin").toString();
}
if (request.getAttribute("podatki1") != null) {
	podatki1 = (List<double[]>)request.getAttribute("podatki1");
}
if (request.getAttribute("podatki2") != null) {
	podatki2 = (List<double[]>)request.getAttribute("podatki2");
}
if (request.getAttribute("od") != null) {
	od = request.getAttribute("od").toString();
}
if (request.getAttribute("dox") != null) {
	dox = request.getAttribute("dox").toString();
}

%><h4>Prikaz za obdobje od <%= od %> do <%= dox %></h4><%

if (ki != null && ki.size() > 0) { 
	if(nacin.equals(Constants.OCENA_IZPIT) || nacin.equals(Constants.OCENA_IZPIT_VAJE)){
	%><h3>POLAGANJA IZPITOV</h3><%
	}
	else if(nacin.equals(Constants.OCENA_VAJE) || nacin.equals(Constants.OCENA_IZPIT_VAJE)){
		%><h3>POLAGANJA VAJ</h3><%
	}
	else if(nacin.equals(Constants.OCENA_KONCNA)){
		%><h3>KONČNA POLAGANJA</h3><%
	}%>

		<table id="studentovaOcene" class="tabele" style="width: 820px; margin-left: 45px; margin-bottom: 10px">
			<thead>
				<tr>
					<th style="width: 100px;">Predavatelj(i)</th>
					<th style="width: 100px;">Št. ocen</th>
					<th style="width: 100px;">Št. poz. ocen</th>
					<th style="width: 100px;">Pov. št. pol.</th>
					<th style="width: 100px;">Povp. ocena</th>
					<th style="width: 100px;">Povp. poz. ocena</th>
				</tr>
			</thead>

	<% 
	
	for(int i = 0; i < ki.size(); i++){
	
		double[] podatki = podatki1.get(i);
		
		String izvajalci = "";
		
		k = ki.get(i);
		
		izvajalci += k.getOsebje1().getOseba().getPriimek();
		
		if(k.getOsebje2() != null)
			izvajalci += ", " + k.getOsebje2().getOseba().getPriimek();
		
		if(k.getOsebje3() != null)
			izvajalci += ", " + k.getOsebje3().getOseba().getPriimek();
		
		%>

			<tr>		
					<td><%= izvajalci %></td>			
					<td><%= podatki[0] %></td>
					<td><%= podatki[1] %></td>
					<td><%= podatki[2] %></td>
					<td><%= podatki[3] %></td>
					<td><%= podatki[4] %></td>
			</tr>
		
			<%
	} 
	
	double t1 = 0;
	double t2 = 0;
	double t3 = 0;
	double t4 = 0;
	double t5 = 0;
	
	double[] podatki;
	
	for(int i = 0; i < podatki1.size(); i++){
		podatki = podatki1.get(i);
		
		t1 += podatki[0];
		t2 += podatki[1];
		t3 += podatki[2];
		t4 += podatki[3];
		t5 += podatki[4];
	}
	
	t3 = t3 / (double) podatki1.size();
	t4 = t4 / (double) podatki1.size();
	t5 = t5 / (double) podatki1.size();
	
	%>
	<tr>
	<td>Skupaj</td>			
		<td><%= t1 %></td>
		<td><%= t2 %></td>
		<td><%= t3 %></td>
		<td><%= t4 %></td>
		<td><%= t5 %></td>
	</tr>
	</table>
<%  
	if(nacin.equals(Constants.OCENA_IZPIT_VAJE) ){
 %>
 	<h3>POLAGANJA VAJ</h3>
 		<table id="studentovaOcene" class="tabele" style="width: 820px; margin-left: 45px; margin-bottom: 10px">
			<thead>
				<tr>
					<th style="width: 100px;">Predavatelj(i)</th>
					<th style="width: 100px;">Št. ocen</th>
					<th style="width: 100px;">Št. poz. ocen</th>
					<th style="width: 100px;">Pov. št. pol.</th>
					<th style="width: 100px;">Povp. ocena</th>
					<th style="width: 100px;">Povp. poz. ocena</th>
				</tr>
			</thead>

	<% 
	
	for(int i = 0; i < ki.size(); i++){
		
		podatki = podatki2.get(i);
		
		String izvajalci = "";
		
		k = ki.get(i);
		
		izvajalci += k.getOsebje1().getOseba().getPriimek();
		
		if(k.getOsebje2() != null)
			izvajalci += ", " + k.getOsebje2().getOseba().getPriimek();
		
		if(k.getOsebje3() != null)
			izvajalci += ", " + k.getOsebje3().getOseba().getPriimek();
		
		%>

			<tr>		
					<td><%= izvajalci %></td>			
					<td><%= podatki[0] %></td>
					<td><%= podatki[1] %></td>
					<td><%= podatki[2] %></td>
					<td><%= podatki[3] %></td>
					<td><%= podatki[4] %></td>
			</tr>
		
			<%
	} 
	
	 t1 = 0;
	 t2 = 0;
	 t3 = 0;
	 t4 = 0;
	 t5 = 0;
		for(int i = 0; i < podatki2.size(); i++){
			podatki = podatki2.get(i);
			
			t1 += podatki[0];
			t2 += podatki[1];
			t3 += podatki[2];
			t4 += podatki[3];
			t5 += podatki[4];
		}
	
	t3 = t3 / (double) podatki2.size();
	t4 = t4 / (double) podatki2.size();
	t5 = t5 / (double) podatki2.size();
	
	%>
	<tr>
		<td>Skupaj</td>			
		<td><%= t1 %></td>
		<td><%= t2 %></td>
		<td><%= t3 %></td>
		<td><%= t4 %></td>
		<td><%= t5 %></td>
	</tr>
	</table>
 <%
	}	
} %>
<script type="text/javascript">
	$('.tabele td').css('text-align', 'center');
</script>