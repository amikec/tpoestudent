<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, si.fri.estudent.jpa.*, si.fri.estudent.utilities.SkupenProgram" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<% 	
	String[][] tabela = (String[][])request.getAttribute("tabela");
	int stVrstic=tabela.length;
	int stStolpcev=tabela[0].length;
%>

<table class="tabele" style="margin: 0px auto">
	<tr >
		<% for(int i=0;i<stStolpcev;i++){ %>
			<th><%=  tabela[0][i] %></th>			
		<% } %>
	</tr>	
	
	<% for(int i=1;i<stVrstic;i++){ %>
		<tr>
			<%for(int j=0;j<stStolpcev;j++){ %>
				<td><%= tabela[i][j] %></td>			
			<% } %>
		</tr>
	<% } %>
	
</table>
