<%@page import="si.fri.estudent.jpa.Kombinacije_izvajalcev"%>
<%@page import="si.fri.estudent.jpa.Predmet_izvajalec"%>
<%@page import="si.fri.estudent.jpa.Predmet"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
List<Predmet_izvajalec> predmetIzvajalci = null;
Kombinacije_izvajalcev kombinacija = null;
String imeIzvajalec = "";

if (request.getAttribute("predmetIzvajalci") != null) {
	predmetIzvajalci = (List<Predmet_izvajalec>)request.getAttribute("predmetIzvajalci");
}
if (predmetIzvajalci != null && predmetIzvajalci.size() > 0) { %>
	<option value='0'>-- Izberi predmet --</option>
	
	<%
	for (Predmet_izvajalec pi : predmetIzvajalci) { 
		kombinacija = pi.getKombinacijeIzvajalcev();
		
		imeIzvajalec = "";
		if (kombinacija.getOsebje1() != null) {
			imeIzvajalec += kombinacija.getOsebje1().getOseba().getPriimek();
		}
		if (kombinacija.getOsebje2() != null) {
			imeIzvajalec += ", " + kombinacija.getOsebje2().getOseba().getPriimek();
		}
		if (kombinacija.getOsebje3() != null) {
			imeIzvajalec += ", " + kombinacija.getOsebje3().getOseba().getPriimek();
		}
		
		%>
		<option value='<%= pi.getIdPredmetIzvajalca() %>'><%= pi.getPredmet().getSifraPredmeta() + " - " + pi.getPredmet().getNazivPredmet() + " (" + imeIzvajalec + ")" %></option><%
	}
} else { %>
	<option value='0' disabled='disabled'>Ta kombinacija študija nima predmetov</option>
<% } %>