<%@page import="si.fri.estudent.jpa.Studijski_program"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
List<Studijski_program> studijskiProgrami = null;

if (request.getAttribute("studijskiProgrami") != null) {
	studijskiProgrami = (List<Studijski_program>)request.getAttribute("studijskiProgrami");
}
if (studijskiProgrami != null && studijskiProgrami.size() > 0) { %>
	<option value='0'>-- Izberi študijski program --</option>
	
	<%
	for (Studijski_program sp : studijskiProgrami) { %>
		<option value='<%= sp.getIdStudijskiProgram() %>'><%= sp.getKraticaStudijskiProgram() + " - " + sp.getNazivStudijskiProgram() %></option>
	<% }
} else { %>
	<option value='0' disabled='disabled'>Ta kombinacija študija nima študijskih programov</option>
<% } %>