<%@page import="si.fri.estudent.jpa.Vrsta_studija"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
List<Vrsta_studija> vrsteStudija = null;

if (request.getAttribute("vrsteStudija") != null) {
	vrsteStudija = (List<Vrsta_studija>)request.getAttribute("vrsteStudija");
}
if (vrsteStudija != null && vrsteStudija.size() > 0) { %>
	<option value='0'>-- Izberi vrsto študija --</option>
	
	<%
	for (Vrsta_studija vs : vrsteStudija) { %>
		<option value='<%= vs.getIdVrstaStudija() %>'><%= vs.getKraticaVrstaStudija() + " - " + vs.getNazivVrstaStudija() %></option>
	<% }
} else { %>
	<option value='0' disabled='disabled'>Ta profesor trenutno ne uči na nobeni vrsti študija</option>
<% } %>