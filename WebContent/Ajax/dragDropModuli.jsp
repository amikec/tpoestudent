<%@page import="si.fri.estudent.jpa.Modul"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<br />
<div id="drag">
<%
	if (request.getAttribute("neIzbraniModuli") != null) {
		List<Modul> neIzbraniModuli = (List<Modul>)request.getAttribute("neIzbraniModuli");
		
		for (Modul m : neIzbraniModuli) { %>
			<div id='<%= m.getIdModul() %>'><%= m.getNazivModul() %></div><%
		}
	}
%>
</div>
<div id="drop">
<%
	String inputHiddenIdModulov = "";

	if (request.getAttribute("izbraniModuli") != null) {
		List<Modul> izbraniModuli = (List<Modul>)request.getAttribute("izbraniModuli");
		
		
		for (Modul m : izbraniModuli) { %>
			<div id='<%= m.getIdModul() %>'><%= m.getNazivModul() %></div><%
				inputHiddenIdModulov += m.getIdModul() + ",";
		}
		
		if (inputHiddenIdModulov.length() > 0) {
			inputHiddenIdModulov = inputHiddenIdModulov.substring(0, inputHiddenIdModulov.length() - 1);
		}
	}
%>
</div>
<input type="hidden" name="stareIzbire" value='<%= inputHiddenIdModulov %>' />
<input type="hidden" name="izbire" value='<%= inputHiddenIdModulov %>' />
<input type="hidden" name="izbirniPredmetnikModulDragDrop" value="modul" />
<script type="text/javascript">
	initializeDragDrop();
</script>