<%@page import="si.fri.estudent.jpa.Predmet"%>
<%@page import="java.text.DateFormat"%>
<%@page import="si.fri.estudent.jpa.Izpitni_rok"%>
<%@page import="si.fri.estudent.jpa.Ocena"%>
<%@page import="java.util.List"%>
<%@page import="si.fri.estudent.utilities.Constants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
List<Izpitni_rok> izpitniRoki = null;
List<Ocena> ocene = null;
List<Integer> ponavljanjaPolaganja = null;
Izpitni_rok ir = null;
Ocena o = null;
String nacin = null;

if (request.getAttribute("izpitniRoki") != null) {
	izpitniRoki = (List<Izpitni_rok>)request.getAttribute("izpitniRoki");
	if (izpitniRoki.size() > 0) {
		nacin = izpitniRoki.get(0).getPredmet().getNacinOcenjevanja();
	}
}
if (request.getAttribute("ocene") != null) {
	ocene = (List<Ocena>)request.getAttribute("ocene");
}
if (request.getAttribute("polaganjaPonavljanja") != null) {
	ponavljanjaPolaganja = (List<Integer>)request.getAttribute("polaganjaPonavljanja");
}

if (izpitniRoki != null && izpitniRoki.size() > 0 && ocene != null && ocene.size() > 0 && ponavljanjaPolaganja != null && ponavljanjaPolaganja.size() > 0) { %>
	<table id="studentovaOcene" class="tabele" style="width: 100%">
		<thead>
			<tr>
				<th>TOČKE IZPIT</th>
				<th>TOČKE BONUS</th>
				<th>DATUM IZPIT</th>
				<th>ŠT. POLAGANJA</th>
				<th>ŠT. POLAGANJA V LETU</th>
				
			   <%
				if(nacin.equals(Constants.OCENA_IZPIT)){
					out.print("<th style='width: 120px;'>Ocena izpit</th>");
				}else if(nacin.equals(Constants.OCENA_IZPIT_VAJE)){
					out.print("<th style='width: 120px;'>Ocena izpit</th>");
					out.print("<th style='width: 120px;'>Ocena vaje</th>");
				}else if(nacin.equals(Constants.OCENA_KONCNA)){
					out.print("<th style='width: 120px;'>Končna</th>");
				}else if(nacin.equals(Constants.OCENA_VAJE)){
					out.print("<th style='width: 120px;'>Ocena vaje</th>");
				}
				%>
			</tr>
		</thead>
		<%
		for (int i = 0; i < izpitniRoki.size(); i++) {
			ir = izpitniRoki.get(i);
			o = ocene.get(i);
			
			String opravljanja = "";
			opravljanja = String.format("%d", o.getStPolaganja());
			/*
			if (ponavljanjaPolaganja.get(i) > 0) {
				opravljanja = String.format("%d - %d", o.getStPolaganja(), ponavljanjaPolaganja.get(i));
			} else {
				opravljanja = String.format("%d", o.getStPolaganja());
			}*/%>
			<tr>
				<td><%= o.getOcena() %></td>
				<td><%= o.getOcenaBonus() %></td>
				<td><%= DateFormat.getInstance().format(ir.getDatumIzpit()).toString() %></td>
				<td><%= opravljanja %></td>
				<td><%= o.getStPolaganjaLetos() %></td>
				<%
				if(nacin.equals(Constants.OCENA_IZPIT)) {
					if (o.getOcenaIzpit() > 0) {
						out.print("<td>" + o.getOcenaIzpit() + "</td>");
					} else {
						out.print("<td>Ocena še ni vpisana</td>");
					}
				} else if(nacin.equals(Constants.OCENA_IZPIT_VAJE)) {
					if (o.getOcenaIzpit() > 0) {
						out.print("<td>" + o.getOcenaIzpit() + "</td>");
					} else {
						out.print("<td>Ocena še ni vpisana</td>");
					}
					if (o.getOcenaVaje() > 0) {
						out.print("<td>" + o.getOcenaVaje() + "</td>");
					} else {
						out.print("<td>Ocena še ni vpisana</td>");
					}
				} else if(nacin.equals(Constants.OCENA_KONCNA)) {
					if (o.getOcenaKoncna() > 0) {
						out.print("<td>" + o.getOcenaKoncna() + "</td>");
					} else {
						out.print("<td>Ocena še ni vpisana</td>");
					}
				} else if(nacin.equals(Constants.OCENA_VAJE)) {
					if (o.getOcenaVaje() > 0) {
						out.print("<td>" + o.getOcenaVaje() + "</td>");
					} else {
						out.print("<td>Ocena še ni vpisana</td>");
					}
				}
				%>
			</tr>
		<% } %>
	</table>
<% } else { %>
	<h3 style="text-align: center;">Za ta predmet ni ocen</h3>
<% } %>