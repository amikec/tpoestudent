<%@page import="si.fri.estudent.jpa.Predmet"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
List<Predmet> predmeti = null;

if (request.getAttribute("predmeti") != null) {
	predmeti = (List<Predmet>)request.getAttribute("predmeti");
}
if (predmeti != null && predmeti.size() > 0) { %>
	<option value='0'>-- Izberi predmet --</option>
	
	<%
	for (Predmet p : predmeti) { %>
		<option value='<%= p.getIdPredmeta() %>'><%= p.getNazivPredmet() %></option>
	<% }
} else { %>
	<option value='0' disabled='disabled'></option>
<% } %>