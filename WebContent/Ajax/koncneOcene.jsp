<%@page import="si.fri.estudent.jpa.Izpitni_rok"%>
<%@page import="si.fri.estudent.jpa.Predmetnik"%>
<%@page import="java.util.Calendar"%>
<%@page import="si.fri.estudent.jpa.Ocena"%>
<%@page import="si.fri.estudent.jpa.Predmet"%>
<%@page import="si.fri.estudent.jpa.Oseba"%>
<%@page import="si.fri.estudent.jpa.Student"%>
<%@page import="si.fri.estudent.utilities.Constants"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%!
private String write(boolean izpis, String name, int ocena, Calendar now, Calendar rok) {
	if(izpis) {
		return "<td>" + ocena + "</td>";
	} else {
		if(now.after(rok)) {
			return "<td><input style='width: 110px' type='text' name='" + name + "' value='" + ocena + "'/></td>";
		} else {
			return "<td>Izpit še ni potekal</td>";
		}
	}
}
%>

<%
Predmet predmet = null;
Boolean izpis = false;
List<Student> studenti = null;
List<Ocena> ocene = null;
List<Ocena> koncneOcene = null;
List<Ocena> izpitOcene = null;
List<Ocena> vajeOcene = null;
List<int[]> stOpravljanjStudenti = null;
List<Integer> polaganjaPonavljanja = null;
Izpitni_rok izpitniRok = null;
String uspesnostPosodabljanja = null;
Calendar now = null;
Calendar rok = null;

if(request.getParameter("izpis") != null){
	izpis = Boolean.parseBoolean(request.getParameter("izpis"));
}
if (request.getAttribute("predmet") != null){
	predmet = (Predmet)request.getAttribute("predmet");
}
if (request.getAttribute("studenti") != null) {
	studenti = (List<Student>)request.getAttribute("studenti");
}
if(request.getAttribute("ocene") != null){
	ocene = (List<Ocena>)request.getAttribute("ocene");
}
if (request.getAttribute("stOpravljanjStudenti") != null) {
	stOpravljanjStudenti = (List<int[]>)request.getAttribute("stOpravljanjStudenti");
}
if (request.getAttribute("uspesnostPosodabljanja") != null) {
	uspesnostPosodabljanja = (String)request.getAttribute("uspesnostPosodabljanja");
}
if (request.getAttribute("polaganjaPonavljanja") != null) {
	polaganjaPonavljanja = (List<Integer>)request.getAttribute("polaganjaPonavljanja");
}
if (request.getAttribute("izpitniRok") != null) {
	izpitniRok = (Izpitni_rok)request.getAttribute("izpitniRok");
}

now = Calendar.getInstance();
rok = Calendar.getInstance();
rok.setTime(izpitniRok.getDatumIzpit());

if (studenti != null && studenti.size() > 0) { %>
	<h3>Seznam študentov</h3>
	<input type="hidden" id="nacinOcen" name="nacinOcen" value="<%= predmet.getNacinOcenjevanja() %>" />
	<table id="koncneOcene" class="tabele" style="width: 900px; margin-left: 5px; margin-bottom: 10px">
		<thead>
			<tr>
				<th>Zaporedna št</th>
				<th>Vpisna št</th>
				<th>Priimek in ime</th>
				<th>Zaporedna polaganja</th>
				<th style="width: 95px;">Št. polaganja</th>
				<%
				if(predmet.getNacinOcenjevanja().equals(Constants.OCENA_IZPIT)){
					out.print("<th style='width: 110px;'>Ocena izpit</th>");
				}else if(predmet.getNacinOcenjevanja().equals(Constants.OCENA_IZPIT_VAJE)){
					out.print("<th style='width: 110px;'>Ocena izpit</th>");
					out.print("<th style='width: 110px;'>Ocena vaje</th>");
				}else if(predmet.getNacinOcenjevanja().equals(Constants.OCENA_KONCNA)){
					out.print("<th style='width: 110px;'>Končna</th>");
				}else if(predmet.getNacinOcenjevanja().equals(Constants.OCENA_VAJE)){
					out.print("<th style='width: 110px;'>Ocena vaje</th>");
				}
				%>
				<th style="width: 80px;">Točke izpita</th>
				<th style="width: 85px;">Bonus točke</th>
			</tr>
		</thead>
		
		<%
		int [] oceneTabela = new int[10];
		for (int i = 0; i < oceneTabela.length; i++) {
			oceneTabela[i] = i + 1;
		}
		
		String imeStudent = null;
		int [] stOpravljanj = null;
		Student student = null;
		Oseba oseba = null;
		Ocena ocena;
		Predmetnik predmetnik;
		int ponavPolag = 0;
		
		for (int i = 0; i < studenti.size(); i++) {
			student = studenti.get(i);
			oseba = student.getOseba();
			ocena = ocene.get(i);
			
			ponavPolag = polaganjaPonavljanja.get(i);

			stOpravljanj = stOpravljanjStudenti.get(i);
			
			if (oseba.getPriimekDekliski() == null || oseba.getPriimekDekliski().equals("")) {
				imeStudent = oseba.getPriimek() + " " + oseba.getIme();
			} else {
				imeStudent = oseba.getPriimek() + " " + oseba.getPriimekDekliski() + " " + oseba.getIme();
			}
			
			String opravljanja = "";
			
			if (ponavPolag > 0) {
				opravljanja = String.format("%d - %d", stOpravljanj[0], ponavPolag);
			} else {
				opravljanja = String.format("%d", stOpravljanj[0]);
			}
			
			%>
			<tr>
				<input name="ocena" id="ocena" type="hidden" value="<%= ocena.getIdOcena() %>" />
				<td><%= i + 1 %></td>
				<td name="vpisnaStevilka"><%= student.getVpisnaSt() %></td>
				<td><%= imeStudent %></td>
				<td><%= opravljanja %></td>
				<td><%= ocena.getStPolaganja() %></td>
				<%
				if(predmet.getNacinOcenjevanja().equals(Constants.OCENA_IZPIT)) {
					out.print(write(izpis, "oceneIzpit", ocena.getOcenaIzpit(), now, rok));
				}else if(predmet.getNacinOcenjevanja().equals(Constants.OCENA_IZPIT_VAJE)) {
					out.print(write(izpis, "oceneIzpit", ocena.getOcenaIzpit(), now, rok));
					out.print(write(izpis, "oceneVaje", ocena.getOcenaVaje(), now, rok));
				}else if(predmet.getNacinOcenjevanja().equals(Constants.OCENA_KONCNA)) {
					out.print(write(izpis, "oceneKoncne", ocena.getOcenaKoncna(), now, rok));
				}else if(predmet.getNacinOcenjevanja().equals(Constants.OCENA_VAJE)) {
					out.print(write(izpis, "oceneVaje", ocena.getOcenaVaje(), now, rok));
				}
				%>
				<td name="izpitTocke"><%= ocena.getOcena() %></td>
				<td name="bonusTocke"><%= ocena.getOcenaBonus() %></td>
			</tr>
		<% } %>
	</table>
	<%
	if (uspesnostPosodabljanja != null) { %>
		<p class="success" id="uspesnostPosodabljanja" style="margin: 0px; padding: 0px;"><%= uspesnostPosodabljanja %></p>
	<% } %>
<% } else { %>
	<h3>Ta predmet nima prijavljenih študentov</h3>
<% } %>
<script type="text/javascript">
	$('select[name=ocena]').css("padding", "4px");
	$('select[name=ocena]').parent().css("padding", "0px");
	$('select[name=ocena]').parent().css("margin", "0px");
	$('select[name=ocenaBon]').css("padding", "4px");
	$('select[name=ocenaBon]').parent().css("padding", "0px");
	$('select[name=ocenaBon]').parent().css("margin", "0px");
	
	$('#posodobiOcene').show();
	<% if (now.after(rok)) { %>
		$('#posodobiKoncneOcene').show();
	<% } else { %>
		$('#posodobiKoncneOcene').hide();
	<% } %>
</script>