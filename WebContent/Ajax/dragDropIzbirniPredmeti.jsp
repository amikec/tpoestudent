<%@page import="si.fri.estudent.jpa.Predmetnik_letnika"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<br />
<div id="drag">
<%
	if (request.getAttribute("neizbraniIzbirni") != null) {
		List<Predmetnik_letnika> neizbraniIzbirni = (List<Predmetnik_letnika>)request.getAttribute("neizbraniIzbirni");
		
		
		for (Predmetnik_letnika pl : neizbraniIzbirni) { %>
			<div id='<%= pl.getIdPredmetnikLetnika() %>'><%= pl.getPredmet().getSifraPredmeta() + " &nbsp;-- " + pl.getPredmet().getNazivPredmet() %></div><%
		}
	}
%>
</div>
<div id="drop">
<%
	String inputHiddenIdPredmetov = "";

	if (request.getAttribute("izbraniIzbirni") != null) {
		List<Predmetnik_letnika> izbraniIzbirni = (List<Predmetnik_letnika>)request.getAttribute("izbraniIzbirni");
		
		
		for (Predmetnik_letnika pl : izbraniIzbirni) { %>
			<div id='<%= pl.getIdPredmetnikLetnika() %>'><%= pl.getPredmet().getSifraPredmeta() + " &nbsp;-- " + pl.getPredmet().getNazivPredmet() %></div><%
			inputHiddenIdPredmetov += pl.getIdPredmetnikLetnika() + ",";
		}
		
		if (inputHiddenIdPredmetov.length() > 0) {
			inputHiddenIdPredmetov = inputHiddenIdPredmetov.substring(0, inputHiddenIdPredmetov.length() - 1);
		}
	}
%>
</div>
<input type="hidden" name="stareIzbire" value='<%= inputHiddenIdPredmetov %>' />
<input type="hidden" name="izbire" value='<%= inputHiddenIdPredmetov %>' />
<input type="hidden" name="izbirniPredmetnikModulDragDrop" value="predmeti" />
<script type="text/javascript">
	initializeDragDrop();
</script>