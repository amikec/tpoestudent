<%@page import="si.fri.estudent.jpa.Izpitni_rok"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
List<Izpitni_rok> izpitniRoki = null;

if (request.getAttribute("izpitniRoki") != null) {
	izpitniRoki = (List<Izpitni_rok>)request.getAttribute("izpitniRoki");
}
if (izpitniRoki != null && izpitniRoki.size() > 0) { %>
	<option value='0'>-- Izberi izpitni rok --</option>
	
	<%
	for (Izpitni_rok ir : izpitniRoki) { %>
		<option value='<%= ir.getIdRok() %>'><%= ir.getDatumIzpit() %></option>
	<% }
} else { %>
	<option value='0' disabled='disabled'>Ta kombinacija študija nima izpitnih rokov</option>
<% } %>