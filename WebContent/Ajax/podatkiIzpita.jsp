<%@page import="si.fri.estudent.jpa.Predmet"%>
<%@page import="si.fri.estudent.jpa.Letnik"%>
<%@page import="si.fri.estudent.jpa.Ocena"%>
<%@page import="si.fri.estudent.jpa.Izpitni_rok"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
Predmet predmet = null;
String nacinOcenjevanja = "";
if (request.getAttribute("predmet") != null) {
	predmet = (Predmet)request.getAttribute("predmet");
	nacinOcenjevanja = predmet.getNacinOcenjevanja();
}

Izpitni_rok ir = null;
int predlaganoStPolaganja = 0;
int idOdprtePrijave = -1;
Ocena oc = null;
String datum = "";
String cas = "";

if(request.getAttribute("odprtaPrijava") != null ){
	oc = (Ocena) request.getAttribute("odprtaPrijava");
	predlaganoStPolaganja = oc.getStPolaganja() + 1;
	idOdprtePrijave = oc.getIdOcena();
}

if(request.getAttribute("izpitniRokPrijave") != null ){
	ir = (Izpitni_rok) request.getAttribute("izpitniRokPrijave");
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
	datum = dateFormat.format(ir.getDatumIzpit());
	dateFormat = new SimpleDateFormat("HH:mm");
	cas = dateFormat.format(ir.getDatumIzpit());
}

if(request.getAttribute("predlaganStPolaganja") != null )
	predlaganoStPolaganja = Integer.parseInt(request.getAttribute("predlaganStPolaganja").toString());

%>

<tr>
	<td colspan="3">
		<input type="hidden" name="idOdprtePrijave" value="<%= idOdprtePrijave %>" /> 
	</td>
</tr>
<tr>
	<th>Datum opravljanja</th>
	<td>
		<input type="text" name="datumVnosPrijavnica" readonly="readonly" value="<%= datum %>" <%
			if (idOdprtePrijave != -1) {
				out.print("disabled='disabled'");
			}
		%>/>
	</td>
	<td class="error"></td>
</tr>

<% if (nacinOcenjevanja.equals("IV") || nacinOcenjevanja.equals("I")) { %>
<tr>
	<th>Ocena izpit</th>
	<td>
		<input type="text" name="koncnaOcenaIzpitPrijavnica" />
	</td>
	<td class="error"></td>
</tr>
<% } 
if (nacinOcenjevanja.equals("IV") || nacinOcenjevanja.equals("V")) { %>
<tr>
	<th>Ocena vaj</th>
	<td>
		<input type="text" name="koncnaOcenaVajePrijavnica" />
	</td>
	<td class="error"></td>
</tr>
<% }
if (nacinOcenjevanja.equals("K")) { %>
<tr>
	<th>Končna ocena</th>
	<td>
		<input type="text" name="koncnaOcenaPrijavnica" />
	</td>
	<td class="error"></td>
</tr>
<% } %>
<tr>
	<th>Številka polaganja</th>
	<td>
		<input type="text" name="stPolaganjaPrijavnica" value="<%= predlaganoStPolaganja %>" <%
			if (idOdprtePrijave != -1) {
				out.print("disabled='disabled'");
			}
		%>/>
	</td>
	<td class="error"></td>
</tr>
<tr>
	<td colspan="2" class="gumbi"><div><input type='submit' value='SHRANI' /></div></td>
	<td></td>
</tr>
<script type="text/javascript">
	$("input[name=datumVnosPrijavnica]").datepicker({
		dateFormat: 'dd.mm.yy'       
	});
	$('input[name=casVnosPrijavnica]').timepicker({});
</script>