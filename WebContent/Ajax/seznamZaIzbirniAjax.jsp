<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, si.fri.estudent.jpa.*, si.fri.estudent.utilities.SkupenProgram" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<% 	
	String[][] podatki = (String[][])request.getAttribute("podatki");
	int st=podatki.length;
%>

<table class="tabele" style="margin: 0px auto">
	<tr >
		<th>Ime</th>
		<th>Priimek</th>
		<th>Vpisna</th>
	</tr>
	
	<%for(int i=0;i<st;i++){ %>
		<tr>
			<td><%= podatki[i][0] %></td>
			<td><%= podatki[i][1] %></td>
			<td><%= podatki[i][2] %></td>
		</tr>
	<% } %>
</table>