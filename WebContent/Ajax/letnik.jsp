<%@page import="si.fri.estudent.jpa.Letnik"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
List<Letnik> letniki = null;

if (request.getAttribute("letniki") != null) {
	letniki = (List<Letnik>)request.getAttribute("letniki");
}
if (letniki != null && letniki.size() > 0) { %>
	<option value='0'>-- Izberi letnik --</option>
	
	<%
	for (Letnik l : letniki) { %>
		<option value='<%= l.getIdLetnik() %>'><%= l.getNazivLetnik() %></option>
	<% }
} else { %>
	<option value='0' disabled='disabled'>Ta kombinacija študija nima letnikov</option>
<% } %>