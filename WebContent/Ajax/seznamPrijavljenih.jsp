<%@page import="si.fri.estudent.jpa.Izpitni_rok"%>
<%@page import="java.util.Calendar"%>
<%@page import="si.fri.estudent.jpa.Ocena"%>
<%@page import="si.fri.estudent.jpa.Oseba"%>
<%@page import="si.fri.estudent.jpa.Student"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<%
Izpitni_rok izpitniRok = null;
List<Student> studenti = null;
List<Ocena> ocene = null;
List<int[]> stOpravljanjStudenti = null;
String uspesnostPosodabljanja = null;
List<Integer> ponavljanjaPolaganja = null;
Calendar now = null;
Calendar rok = null;

if (request.getAttribute("ir") != null) {
	izpitniRok = (Izpitni_rok)request.getAttribute("ir");
}
if (request.getAttribute("studenti") != null) {
	studenti = (List<Student>)request.getAttribute("studenti");
}
if (request.getAttribute("ocene") != null) {
	ocene = (List<Ocena>)request.getAttribute("ocene");
}
if (request.getAttribute("stOpravljanjStudenti") != null) {
	stOpravljanjStudenti = (List<int[]>)request.getAttribute("stOpravljanjStudenti");
}
if (request.getAttribute("uspesnostPosodabljanja") != null) {
	uspesnostPosodabljanja = (String)request.getAttribute("uspesnostPosodabljanja");
}
if (request.getAttribute("polaganjaPonavljanja") != null) {
	ponavljanjaPolaganja = (List<Integer>)request.getAttribute("polaganjaPonavljanja");
}

boolean izpis = false;
if(request.getParameter("izpis") != null)
		izpis = Boolean.parseBoolean(request.getParameter("izpis").toString());

now = Calendar.getInstance();
rok = Calendar.getInstance();
rok.setTime(izpitniRok.getDatumIzpit());

if (studenti != null && studenti.size() > 0 && stOpravljanjStudenti != null && stOpravljanjStudenti.size() > 0 && izpitniRok != null) { %>
	<h3>Seznam prijavljenih študentov</h3>
	<table id="seznamPrijavljenih" class="tabele" style="width: 820px; margin-left: 45px; margin-bottom: 10px">
		<thead>
			<tr>
				<th style="width: 100px;">Zaporedna št</th>
				<th style="width: 90px;">Vpisna št</th>
				<th style="width: 200px;">Priimek in ime</th>
				<th style="width: 150px;">Zaporedna polaganja</th>
				<th style="width: 150px;">Št. polaganja</th>
				
				<% if(!izpis){ %>
				<th style="width: 110px;">Točke</th>
				<th style="width: 110px;">Bonus točke</th>
				<%} %>
			</tr>
		</thead>
		
		<%
		int [] oceneTabela = new int[10];
		for (int i = 0; i < oceneTabela.length; i++) {
			oceneTabela[i] = i + 1;
		}
		
		String imeStudent = null;
		int [] stOpravljanj = null;
		Student student = null;
		Oseba oseba = null;
		Ocena ocena;
		
		for (int i = 0; i < studenti.size(); i++) {
			student = studenti.get(i);
			oseba = student.getOseba();
			ocena = ocene.get(i);
			stOpravljanj = stOpravljanjStudenti.get(i);
			
			if (oseba.getPriimekDekliski() == null || oseba.getPriimekDekliski().equals("")) {
				imeStudent = oseba.getPriimek() + " " + oseba.getIme();
			} else {
				imeStudent = oseba.getPriimek() + " " + oseba.getPriimekDekliski() + " " + oseba.getIme();
			}
			
			String opravljanja = "";
			
			if (ponavljanjaPolaganja.get(i) > 0) {
				opravljanja = String.format("%d - %d", stOpravljanj[1], ponavljanjaPolaganja.get(i));
			} else {
				opravljanja = String.format("%d", stOpravljanj[1]);
			}
			
			%>
			<tr>
				<td><%= i + 1 %></td>
				<td name="vpisnaStevilka"><%= student.getVpisnaSt() %></td>
				<td><%= imeStudent %></td>
				<td><%= opravljanja %></td>

				<td><%= ocena.getStPolaganja() %>				

				<% if(now.after(rok)) { 								
						String oc = "";
						if(ocena.getOcena() == -1.0) {
							oc = "VP";
						} else {
							oc = Double.toString(ocena.getOcena());
						}
						
						
						if(!izpis){ %>
							<td><input name='ocena' style="width: 110px;" type='text' id='ocena' value='<%= oc %>'/></td>
							<td><input name='ocenaBon' style="width: 110px;" type='text' id='ocenaBon' value='<%= ocena.getOcenaBonus() %>'/></td>
						
					<% } %>
							
				<% } else { %>
					<td>Izpit še ni potekal</td>
					<td></td>
				<% } %>
			</tr>
		<% } %>
	</table>
	<%
		if (uspesnostPosodabljanja != null) { %>
			<p class="success" id="uspesnostPosodabljanja" style="margin: 0px; padding: 0px;"><%= uspesnostPosodabljanja %></p>
	<% } %>
	
<% } else { %>
	<h3>Na ta izpitni rok ni prijavljenih študentov</h3>
<% } %>
<script type="text/javascript">
	$('select[name=ocena]').css("padding", "4px");
	$('select[name=ocena]').parent().css("padding", "0px");
	$('select[name=ocena]').parent().css("margin", "0px");
	$('select[name=ocenaBon]').css("padding", "4px");
	$('select[name=ocenaBon]').parent().css("padding", "0px");
	$('select[name=ocenaBon]').parent().css("margin", "0px");
	<% if (now.after(rok)) { %>
		$('#posodobiOcene').show();
	<% } else { %>
		$('#posodobiOcene').hide();
	<% } %>
</script>