<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, si.fri.estudent.jpa.*, si.fri.estudent.utilities.SkupenProgram" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<h1>Izpis kartotečnega lista</h1>

<% List<SkupenProgram> skupenProgram=(List<SkupenProgram>)request.getAttribute("skupenProgram"); %>
	
<form action='KartotecniListServlet' method='post' class="forme" style="width: 750px; margin-left: 170px;">
	<table>				
		<tr>
			<td>1 </td>
			<th>Varianta izpisa:</th>
			<td><select name="varianta"><option value="vsa">Vsa polaganja</option>
			                            <option value="zadnja">Samo zadnja polaganja</option>
			    </select></td>
			<td ></td>
		</tr>
		
		<% if(skupenProgram.size()>1){%>
			<tr>
			<td>2 </td>
			<th>Izberi program:</th>
			<td><select name="program">
			<option value="0">Vsi programi</option>
			<%
				int i=1;
				for(SkupenProgram sp:skupenProgram){%>
					<option value="<%=i%>"><%=sp.getStudijskiProgram().getNazivStudijskiProgram()+" "
					+sp.getVrstaStudija().getNazivVrstaStudija()%></option>
				<%i++;}
			%></select></td>
			<td ></td>
		</tr>
			
		<% } %>
		<tr>
			<td colspan="3" class="gumbi"><div><input type='submit' value='POIZVEDI' /></div></td>
			<td style="backgorund-color: red"></td>
		</tr>
	</table>
</form>
<script type="text/javascript">	
	$('.error').css('width', '200px'); /* kakšen del širine forme nej zavzamejo errorji */	
</script>