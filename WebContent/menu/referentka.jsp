<%@page import="com.sun.corba.se.impl.orbutil.closure.Constant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="si.fri.estudent.utilities.*" %>

<li><a href="index.jsp?stran=domov">Domov</a></li>
<li>
	Izpiti <span class="arrow"></span>
	<ul>
		<li class="first"><a href="index.jsp?stran=iskanjeStudenta&posredujNa=PrijavaNaIzpitAjaxServlet">Prijava študenta na izpit</a></li>
		<li><a href="index.jsp?stran=iskanjeStudenta&posredujNa=OdjavaIzpitServlet">Odjava študenta z izpita</a></li>
		<li><a href="VnosIzpitnegaRokaServlet">Vnos izpitnega roka</a></li>
		<li><a href="SpremembaIzpitnegaRokaServlet">Sprememba izpitnega roka</a></li>
		<li><a href="PregledPrijavljenihKandidatovServlet">Vnos rezultatov pisnega dela izpita</a></li>
		<li><a href="PregledPrijavljenihKandidatovServlet?izpis=1987">Pregled prijavljenih kandidatov</a></li>
		<li><a href="SeznamZaIzbirniInit">Seznam za izbirni predmet</a></li>
		<li><a href="index.jsp?stran=iskanjeStudenta&posredujNa=VnosOcenePrijavnicaServlet">Vnos ocene s prijavnice</a></li>
	</ul>
</li>
<li>
	Študenti <span class="arrow"></span>
	<ul>
		<li class="first"><a href="VpisniListServlet">Vpisni list</a></li>
		<li><a href="index.jsp?stran=iskanjeStudenta&posredujNa=OsebniPodatkiStudentaServlet">Osebni podatki študenta</a></li>
		<li><a href="index.jsp?stran=iskanjeStudenta&posredujNa=KartotecniListInitServlet">Kartotecni list</a></li>
		<li><a href="index.jsp?stran=iskanjeStudenta&posredujNa=IzbirniPredmetiServlet">Izbirni Predmeti</a></li>
		<li><a href="VnosKoncnihOcenServlet">Vnos končnih ocen</a></li>
		<li><a href="VnosKoncnihOcenServlet?izpis=true">Izpis končnih ocen</a></li>	
		<li><a href="SeznamVpisanihInitServlet">Seznam vpisanih</a></li>		
	</ul>
</li>
<li><a href="odjavaServlet">Odjava</a></li>