<%@page import="com.sun.corba.se.impl.orbutil.closure.Constant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<li><a href="index.jsp?stran=domov">Domov</a></li>
<li>
	Izpiti <span class="arrow"></span>
	<ul>
		<li class="first"><a href="index.jsp?stran=prijavaIzpitStudent">Prijava na izpit</a></li>
		<li><a href="index.jsp?stran=odjavaIzpitStudent">Odjava z izpita</a></li>
	</ul>
</li>
<li>
	Vzdrževanje <span class="arrow"></span>
	<ul>
		<li><a href="index.jsp?stran=sifranti">Vzdrževanje šifrantov</a></li>
		<li><a href="VzdrzevanjePredmetnikaServlet">Predmetniki in Moduli</a></li>
		<li><a href="VzdrzevanjeIzvajalcevServlet">Vzdrževanje podatkov o izvajalcih</a></li>
	</ul>
</li>
<li><a href="odjavaServlet">Odjava</a></li>