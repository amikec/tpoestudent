<%@page import="com.sun.corba.se.impl.orbutil.closure.Constant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="si.fri.estudent.utilities.*" %>
<li><a href="index.jsp?stran=domov">Domov</a></li>
<li>
	Izpiti <span class="arrow"></span>
	<ul>
		<li class="first"><a href="PrijavaNaIzpitAjaxServlet">Prijava na izpit</a></li>
		<li><a href="OdjavaIzpitServlet">Odjava z izpita</a></li>
		<li><a href="PregledRazpisanihRokovStudentAjaxServlet">Pregled razpisanih rokov</a></li>
	</ul>
</li>
<li>
	Študent <span class="arrow"></span>
	<ul>
		<li class="first"><a href="KartotecniListInitServlet">Kartotečni list</a></li>
	</ul>
</li>
<li><a href="odjavaServlet">Odjava</a></li>