<%@page import="si.fri.estudent.jpa.Predmet, si.fri.estudent.jpa.Osebje, si.fri.estudent.jpa.Predmet_izvajalec, java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript">	
	$(document).ready(function(){
		$('#idOsebja').submit(function() { // Pred submitom preverimo, da nismo izbrali več kot 3 predavatelje
		   	var izbrani = $('input[name=izbire]').val();
		   	if (izbrani.split(',').length > 3) {
		    	$('.success').html("<span class='error'>Maksimalno število predavateljev je 3</span>");
		    	return false;  
		  	}else if (izbrani == "") {
		    	$('.success').html("<span class='error'>Niste izbrali predavatelja</span>");
		    	return false;   
		  	}else {
		   		return true;
		  	}
		 });
	});
</script>
<h1>UREDI IZVAJACA ZA PREDMET</h1>
<form action="VzdrzevanjeIzvajalcevUrejanjeServlet?id=<% Predmet_izvajalec prd = (Predmet_izvajalec)request.getAttribute("predmetIzvajalec"); out.print(prd.getIdPredmetIzvajalca());%>" method='POST' class="forme" id="idOsebja" style="width: 450px; margin-left: 220px;">
	<table>
		<tr>
			<td colspan="3" style="text-align: center" class="success"></td>
		</tr>
		<%if(request.getAttribute("uspeh") != null) {%>
			<tr>
				<td colspan="3" style="text-align: center" class="success"><%= request.getAttribute("uspeh") %></td>
			</tr>
		<% } %>
		<tr>
			<th>Predmet</th>
			<td>
				<select name="predmet" id="predmet">
					<%if(request.getAttribute("predmetIzvajalec") != null) {
						Predmet_izvajalec piz = (Predmet_izvajalec)request.getAttribute("predmetIzvajalec");%>
						<option value='<%=piz.getPredmet().getIdPredmeta()%>'><%=piz.getPredmet().getNazivPredmet() %></option><%
						if(request.getAttribute("predmeti") != null) {
							List<Predmet> predmeti = (List<Predmet>)request.getAttribute("predmeti");
							for(Predmet p : predmeti) { 
								if(piz.getPredmet().getIdPredmeta() != p.getIdPredmeta()) {%>
									<option value='<%=p.getIdPredmeta()%>'><%=p.getNazivPredmet() %></option>								
						<%		}
							}
						}
					} else {%>
						<option value="-1">--izberite predmet--</option>
					<%
						if(request.getAttribute("predmeti") != null) {
							List<Predmet> predmeti = (List<Predmet>)request.getAttribute("predmeti");
							for(Predmet p : predmeti) { %>
								<option value='<%=p.getIdPredmeta()%>'><%=p.getNazivPredmet() %></option>								
						<%	} 
						}
					}
					%>
				</select>
			</td>
		</tr>
		<tr>		
			<td colspan="2" class="dragDrop">
				<div id="drag">
				<%
					if (request.getAttribute("osebje") != null) {
						List<Osebje> osebje = (List<Osebje>)request.getAttribute("osebje");
						
						
						for (Osebje o : osebje) { %>
							<div id='<%= o.getIdOsebje() %>'><%= o.getOseba().getPriimek() + " " + o.getOseba().getIme() %></div><%
						}
					}
				%>
				</div>
				<div id="drop">
				<%
					String inputHiddenIdPredmetov = "";
				
					if (request.getAttribute("predmetIzvajalec") != null) {
						Predmet_izvajalec pi = (Predmet_izvajalec)request.getAttribute("predmetIzvajalec");
				%>
						<div id='<%= pi.getKombinacijeIzvajalcev().getOsebje1().getIdOsebje() %>'><%= pi.getKombinacijeIzvajalcev().getOsebje1().getOseba().getPriimek() + " " + pi.getKombinacijeIzvajalcev().getOsebje1().getOseba().getIme() %></div><%
						inputHiddenIdPredmetov += pi.getKombinacijeIzvajalcev().getOsebje1().getIdOsebje() + ",";
						
						if(pi.getKombinacijeIzvajalcev().getOsebje2() != null) {%>
							<div id='<%= pi.getKombinacijeIzvajalcev().getOsebje2().getIdOsebje() %>'><%= pi.getKombinacijeIzvajalcev().getOsebje2().getOseba().getPriimek() + " " + pi.getKombinacijeIzvajalcev().getOsebje2().getOseba().getIme() %></div><%
							inputHiddenIdPredmetov += pi.getKombinacijeIzvajalcev().getOsebje2().getIdOsebje() + ",";	
						}
						if(pi.getKombinacijeIzvajalcev().getOsebje3() != null) {%>
							<div id='<%= pi.getKombinacijeIzvajalcev().getOsebje3().getIdOsebje() %>'><%= pi.getKombinacijeIzvajalcev().getOsebje3().getOseba().getPriimek() + " " + pi.getKombinacijeIzvajalcev().getOsebje3().getOseba().getIme() %></div><%
							inputHiddenIdPredmetov += pi.getKombinacijeIzvajalcev().getOsebje3().getIdOsebje() + ",";
						}
						
						if (inputHiddenIdPredmetov.length() > 0) {
							inputHiddenIdPredmetov = inputHiddenIdPredmetov.substring(0, inputHiddenIdPredmetov.length() - 1);
						}
					}
				%>
				</div>
				<input type="hidden" name="stareIzbire" value='<%= inputHiddenIdPredmetov %>' />
				<input type="hidden" name="izbire" value='<%= inputHiddenIdPredmetov %>' />
				<script type="text/javascript">
					initializeDragDrop();
				</script>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="gumbi"><div><input type='submit' value='POSODOBI' /></div></td>
			<td></td>
		</tr>
	</table>
</form>