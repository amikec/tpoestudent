<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="si.fri.estudent.jpa.*" %>
<%@ page import="java.util.*" %>
<%@ page import="si.fri.estudent.utilities.*" %> 
<h1>Prijava na izpit</h1>

<link type="text/css" href="JavaScript/jQuery/UI-datepicker/css/custom-theme/jquery-ui-1.8.18.custom.css" rel="stylesheet" />	
<link type="text/css" href="JavaScript/jQuery/UI-datepicker/css/custom-theme/jquery-ui-timepicker.css" rel="stylesheet" />
<script type="text/javascript" src="JavaScript/jQuery/UI-datepicker/js/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="JavaScript/jQuery/UI-datepicker/js/jquery-ui-timepicker.js"></script>
<form action="" method="POST" class="forme" style="width: 420px; margin-left: 245px;" >
<table>
<%
	
	Vloga_oseba vloga = (Vloga_oseba) session.getAttribute("vloga");
	
	if(vloga.getIdVlogaOseba() == Constants.VLOGA_REFERENTKA){
		
		Student student = (Student) session.getAttribute("studentIzIskanja");
		Oseba oseba = student.getOseba();
%>

	<tr>
		<td>Študent:</td><td> <%= oseba.getIme() + " " + oseba.getPriimek() %></td>
	</tr>
<% 	} %>


	<tr>
		<th>Študij</th>
		<td>
			<select id="prijavaNaIzpitAjaxRequest" name="prijavaNaIzpitAjaxRequest">
				<option value='0' ></option>
				<% 
					if(request.getAttribute("leta") != null){
						List<Vpisni_list> leta = (List<Vpisni_list>)request.getAttribute("leta");
						
						for(int i = 0; i < leta.size(); i++){

							Vpisni_list vl = leta.get(i);
							Letnik l = vl.getLetnik();
							Studij st = l.getStudij();
							
							String s =  l.getNazivLetnik() + ". letnik - " + 
										st.getVrstaStudija().getKraticaVrstaStudija() + " - " + 
										st.getStudijskiProgram().getNazivStudijskiProgram() + " - " +
										vl.getStudijskoLeto();
						%>
							<option value='<%= vl.getIdVpisni() %>'><%= s %></option>
						<% 	
						}
					}
				%>
			</select>
		</td>
	</tr>
	<tr><td><input type='hidden' id='idVpisni' value='' /></td></tr>
	<tr>
		<th>Predmeti</th>
		<td>
			<select id="izpisPredmetov" name="predmeti">
			
			</select>
		</td>
	</tr>
	<tr>
		<th>Izpitni rok</th>
		<td>
			<select id="izpisRokov" name="izpisRokov">
			
			</select>
		</td>
	</tr>
</table>
<table id="prikazRokov" name="prikazRokov">

</table>
</form>
<div id="#dialog"></div>
<script type="text/javascript">
	$('.forme th').css('width', '80px'); /* kakšen del širine forme nej zavzamejo <th> */
	
</script>