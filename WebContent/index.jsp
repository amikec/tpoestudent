<%@page import="si.fri.estudent.jpa.Vloga_oseba"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by Free CSS Templates
http://www.freecsstemplates.org
Released for free under a Creative Commons Attribution 3.0 License

Name       : Reinstated
Description: A two-column, fixed-width design with dark color scheme.
Version    : 1.0
Released   : 20120326
-->
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>E Študent fri</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		
		<link href="http://fonts.googleapis.com/css?family=Oswald" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="JavaScript/jQuery/UI-datepicker/css/custom-theme/jquery-ui-1.8.18.custom.css" />
		<link rel="stylesheet" type="text/css" href="style.css" />
		
		<script type="text/javascript" src="JavaScript/jQuery/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="JavaScript/jQuery/UI-datepicker/js/jquery-ui-1.8.18.custom.min.js"></script>
		<script type="text/javascript" src="JavaScript/jQuery/jquery.dropotron-1.0.js"></script>
		<script type="text/javascript" src="JavaScript/init.js"></script>
		<script type="text/javascript" src="JavaScript/validations.js"></script>
		<script type="text/javascript" src="JavaScript/dragDrop.js"></script>
		<script type="text/javascript" src="JavaScript/autocomplete.js"></script>
		<script type="text/javascript" src="JavaScript/Ajax/ajax.js"></script>
	</head>
	<body>
		<div id="wrapper">
			<div id="splash">
				<img src="images/logo.gif" alt="" />
			</div>
			<div id="menu">
				<ul>
					<%
					//session.setAttribute("uporabnik", "bla"); // samo začasno, dokler ne bomo dodajali v sejo
					//session.invalidate();
					if(session.getAttribute("vloga") != null) {  
						Vloga_oseba vloga = (Vloga_oseba) session.getAttribute("vloga");
						String s = "menu/" + vloga.getNazivVlogaOseba() + ".jsp";
						%>
						<jsp:include page='<%= s %>' />
					<% } else { %>
						<li></li>
					<% } %>
				</ul>
				<br class="clearfix" />
			</div>
			<div id="page">
				<%-- Vstavljanje drugih .jsp --%>
				<% 

				String stran = request.getParameter("stran");
				
				if(session.getAttribute("uporabnik") != null) {
					if(stran == null || stran.equals("login")) { %>
						<jsp:include page="domov.jsp" />; <% // default stran če smo prijavljeni
					}
					else { 
						stran += ".jsp";
					%>
						<jsp:include page='<%= stran %>' /> <% // iskana stran če smo prijavljeni
					}
				}
				else { %>
					<jsp:include page='login.jsp' /> <% // če nismo prijavljeni
				} %>
			</div>
		</div>
		<div id="footer">&copy; Copyright 2012 E-student.si - All Rights Reserved
		</div>
	</body>
</html>