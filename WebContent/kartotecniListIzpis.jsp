<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, si.fri.estudent.jpa.*" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%	
	ArrayList<String> imenaProgramov=(ArrayList<String>)request.getAttribute("imenaProgramov");
	ArrayList<String[][]> naslovneTabele=(ArrayList<String[][]>)request.getAttribute("naslovneTabele");
	ArrayList<String[][]> vsebinskeTabele=(ArrayList<String[][]>)request.getAttribute("vsebinskeTabele");
	ArrayList<String[][]> zakljucneTabele=(ArrayList<String[][]>)request.getAttribute("zakljucneTabele");
%>
<style type="text/css">
	.zgornja{
		width:100%;
		border:0px;		
		background-color: gray;
		border-bottom:2px solid white;
	}
	.zgornja th{
		font-weight:bold;
	}
	.srednja {
		width:100%;
		border:0px;		
		background-color: gray;
		border-bottom:2px solid white;
	}
	.srednja th{
		font-weight:bold;
	}
	.spodnja{
		width:100%;
		border:0px;		
		background-color: gray;
	}
	.spodnja th{
		font-weight:bold;
	}
</style>

<h1>Kartotečni list</h1>

<%  
	if(naslovneTabele!=null && vsebinskeTabele!=null && zakljucneTabele!=null){
		int st=naslovneTabele.size();
		
		for(int i=0;i<st;i++){ %>		
			<h3><%= imenaProgramov.get(i)  %></h3>
			<table class="tabele zgornja">
				<%
				String[][] nasTab=naslovneTabele.get(i);
				 %>
					<tr>						
							<th style="padding-left:10px;padding-right:10px;"><%= nasTab[0][0]%></th>
							<td style="padding-left:10px;padding-right:10px;"><%= nasTab[0][1]%></td>					
							<th style="padding-left:10px;padding-right:10px;"><%= nasTab[1][0]%></th>	
							<td style="padding-left:10px;padding-right:10px;"><%= nasTab[1][1]%></td>				
					</tr>
					<tr>						
							<th style="padding-left:10px;padding-right:10px;"><%= nasTab[2][0]%></th>
							<td style="padding-left:10px;padding-right:10px;"><%= nasTab[2][1]%></td>							
							<th style="padding-left:10px;padding-right:10px;"><%= nasTab[3][0]%></th>	
							<td style="padding-left:10px;padding-right:10px;"><%= nasTab[3][1]%></td>
					</tr>
					<tr>		
							<th style="padding-left:10px;padding-right:10px;"><%= nasTab[4][0]%></th>		
							<td style="padding-left:10px;padding-right:10px;"><%= nasTab[4][1]%></td>
							<th style="padding-left:10px;padding-right:10px;">Skupina:</th>		
							<td style="padding-left:10px;padding-right:10px;">Ljubljana</td>		
					</tr>						
			</table>	
			
			<table class="tabele srednja">
				<tr>
					<th style="padding-left:10px;padding-right:10px;">Šifra</th>
					<th style="padding-left:10px;padding-right:10px;">Predmet</th>
					<th style="padding-left:10px;padding-right:10px;">KT/U</th> 
					<th style="padding-left:10px;padding-right:10px;">Predavatelj(i)</th> 
					<th style="padding-left:10px;padding-right:10px;">Datum</th> 
					<th style="padding-left:10px;padding-right:10px;">Ocena</th> 
					<th style="padding-left:10px;padding-right:10px;">Št. polaganj</th> 
					
				</tr>
				<%
				String[][] vseTab=vsebinskeTabele.get(i);
				for(String[] vrstica:vseTab){ %>
					<tr>						
							<td style="padding-left:10px;padding-right:10px;text-align:center;"><%= vrstica[0] %></td>
							<td style="padding-left:10px;padding-right:10px;"><%= vrstica[1] %></td>
							<td style="padding-left:10px;padding-right:10px;text-align:center;"><%= vrstica[2] %></td>
							<td style="padding-left:10px;padding-right:10px;"><%= vrstica[3] %></td>
							<td style="padding-left:10px;padding-right:10px;"><%= vrstica[4] %></td>
							<td style="padding-left:10px;padding-right:10px;text-align:center;"><%= vrstica[5] %></td>
							<td style="padding-left:10px;padding-right:10px;text-align:center;"><% if(vrstica[6]!=""){
								out.write(vrstica[6]);
							}else{
								out.write("");
							} %></td>
							
					</tr>
				<%} %>		
			</table>
			
			<table class="tabele spodnja">
				<%
				String[][] zakTab=zakljucneTabele.get(i);
				%>
					<tr>						
						<th style="padding-left:10px;padding-right:10px;"><%= zakTab[0][0] %></th>
						<td style="padding-left:10px;padding-right:10px;"><%= zakTab[0][1] %></td>
						
						<th style="padding-left:10px;padding-right:10px;"><%= zakTab[1][0] %></th>
						<td style="padding-left:10px;padding-right:10px;"><%= zakTab[1][1] %></td>
					</tr>
					<tr>						
						<th style="padding-left:10px;padding-right:10px;"><%= zakTab[2][0] %></th>
						<td style="padding-left:10px;padding-right:10px;"><%= zakTab[2][1] %></td>
						
						<th style="padding-left:10px;padding-right:10px;"><%= zakTab[3][0] %></th>
						<td style="padding-left:10px;padding-right:10px;"><%= zakTab[3][1] %></td>
					</tr>	
						
			</table>
			
			<br /><br />			
		<%}
	}
%>

<a href="KartotecniListServlet?vpisnaStevilka=<%= "vpisna" %>">Izpis za tiskanje </a>