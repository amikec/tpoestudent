<%@page import="si.fri.estudent.jpa.Studijska_smer"%>
<%@page import="si.fri.estudent.jpa.Studijski_program"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
boolean error = false;

if(request.getAttribute("error") != null) {
	error = true;
}
%>

<h1>VPISNI LIST 2011/2012</h1>
<form action='VpisniListServlet' method='post' class="forme" style="width: 750px; margin-left: 175px;">
	<table>
		<% if (request.getAttribute("uspesnostVnosa") != null) { %>
			<tr>
				<td colspan="3" class="success"><%= (String)request.getAttribute("uspesnostVnosa") %></td>
				<td></td>
			</tr>
		<% } %>
		<tr>
			<td>1</td>
			<th>Vpisna številka*</th>
			<td>
				<input type="text" name="vpisnaStevilka" maxlength="8" style="width: 180px"
					<%
					if (request.getParameter("vpisnaStevilka") != null && error) {
						out.print("value='" + request.getParameter("vpisnaStevilka").toString() + "'");
					} %>
				/>
				<div class="gumbi" style="padding-top: 1px; float: right;">
					<input type="button" id="nov" value="NOV" />
					<input type="button" id="obstojeci" value="OBSTOJEČI" />
				</div>
			</td>
			<td class="error">
				<%
				if (request.getAttribute("errorVpisnaStevilka") != null) {
					out.print(request.getAttribute("errorVpisnaStevilka").toString());
				} %>
			</td>
		</tr>
		<tr>
			<td>2</td>
			<th>Priimek*</th>
			<td>
				<input type="text" name="priimek"
					<%
					if (request.getParameter("priimek") != null && error) {
						out.print("value='" + request.getParameter("priimek").toString() + "'");
					} %>
				/>
			</td>
			<td class="error">
				<%
				if (request.getAttribute("errorPriimek") != null) {
					out.print(request.getAttribute("errorPriimek").toString());
				} %>
			</td>
		</tr>
		<tr>
			<td></td>
			<th>Dekliški priimek</th>
			<td>
				<input type="text" name="dekliskiPriimek"
					<%
					if (request.getParameter("dekliskiPriimek") != null && error) {
						out.print("value='" + request.getParameter("dekliskiPriimek").toString() + "'");
					} %>
				/>
			</td>
			<td class="error">
				<%
				if (request.getAttribute("errorDekliskiPriimek") != null) {
					out.print(request.getAttribute("errorDekliskiPriimek").toString());
				} %>
			</td>
		</tr>
		<tr>
			<td></td>
			<th>Ime*</th>
			<td>
				<input type="text" name="ime"
					<%
					if (request.getParameter("ime") != null && error) {
						out.print("value='" + request.getParameter("ime").toString() + "'");
					} %>
				/>
			</td>
			<td class="error">
				<%
				if (request.getAttribute("errorIme") != null) {
					out.print(request.getAttribute("errorIme").toString());
				} %>
			</td>
		</tr>
		<tr>
			<td>3</td>
			<th>Enotna matična številka občana*</th>
			<td>
				<input type="text" name="emso" maxlength="13"
					<%
					if (request.getParameter("emso") != null && error) {
						out.print("value='" + request.getParameter("emso").toString() + "'");
					} %>
				/>
			</td>
			<td class="error">
				<%
				if (request.getAttribute("errorEmso") != null) {
					out.print(request.getAttribute("errorEmso").toString());
				} %>
			</td>
		</tr>
		<tr>
			<td></td>
			<th>Davčna številka*</th>
			<td>
				<input type="text" name="davcnaStevilka" maxlength="8"
					<%
					if (request.getParameter("davcnaStevilka") != null && error) {
						out.print("value='" + request.getParameter("davcnaStevilka").toString() + "'");
					} %>
				/>
			</td>
			<td class="error">
				<%
				if (request.getAttribute("errorDavcnaStevilka") != null) {
					out.print(request.getAttribute("errorDavcnaStevilka").toString());
				} %>
			</td>
		</tr>
		<tr>
			<td>3a</td>
			<th>Študent s posebnimi potrebami</th>
			<td>
				<table>
					<tr>
						<td><input type="checkbox" id="1" name="posebnePotrebe" value="1" />A - slepi/slabovidni</td>
						<td><input type="checkbox" id="2" name="posebnePotrebe" value="2" />B - gluhi/naglušni</td>
					</tr>
					<tr>
						<td><input type="checkbox" id="3" name="posebnePotrebe" value="3" />C - gibalno ovirani/invalidi</td>
						<td><input type="checkbox" id="4" name="posebnePotrebe" value="4" />D - drugo</td>
					</tr>
				</table>
			</td>
			<td class="error">
				<%
				if (request.getAttribute("errorPosebnePotrebe") != null) {
					out.print(request.getAttribute("errorPosebnePotrebe").toString());
				} %>
			</td>
		</tr>
		<tr>
			<th colspan="4" class="odsek">NASLOV STALNEGA BIVALIŠČA</th>
		</tr>
		<tr>
			<td>4</td>
			<th>Ulica (naselje) in hišna številka*</th>
			<td>
				<input type="text" name="ulicaS"
					<%
					if (request.getParameter("ulicaS") != null && error) {
						out.print("value='" + request.getParameter("ulicaS").toString() + "'");
					} %>
				/>
			</td>
			<td class="error">
				<%
				if (request.getAttribute("errorUlicaS") != null) {
					out.print(request.getAttribute("errorUlicaS").toString());
				} %>
			</td>
		</tr>
		<tr>
			<td>5</td>
			<th>Pošta*</th>
			<td>
				<input type="text" name="postaS"
					<%
					if (request.getParameter("postaS") != null && error) {
						out.print("value='" + request.getParameter("postaS").toString() + "'");
					} %>
				/>
				<input type="hidden" name="idPostaS"
					<%
					if (request.getParameter("idPostaS") != null && error) {
						out.print("value='" + request.getParameter("idPostaS").toString() + "'");
					} %>
				/>
			</td>
			<td class="error">
				<%
				if (request.getAttribute("errorPostaS") != null) {
					out.print(request.getAttribute("errorPostaS").toString());
				} %>
			</td>
		</tr>
		<tr>
			<td></td>
			<th>Poštna številka*</th>
			<td>
				<input type="text" name="postnaStevilkaS" readonly="readonly"
					<%
					if (request.getParameter("postnaStevilkaS") != null && error) {
						out.print("value='" + request.getParameter("postnaStevilkaS").toString() + "'");
					} %>
				/>
			</td>
			<td class="error">
				<%
				if (request.getAttribute("errorPostnaStevilkaS") != null) {
					out.print(request.getAttribute("errorPostnaStevilkaS").toString());
				} %>
			</td>
		</tr>
		<tr>
			<td>6</td>
			<th>Občina*</th>
			<td>
				<input type="text" name="obcinaS"
					<%
					if (request.getParameter("obcinaS") != null && error) {
						out.print("value='" + request.getParameter("obcinaS").toString() + "'");
					} %>
				/>
				<input type="hidden" name="idObcinaS"
					<%
					if (request.getParameter("idObcinaS") != null && error) {
						out.print("value='" + request.getParameter("idObcinaS").toString() + "'");
					} %>
				/>
			</td>
			<td class="error">
				<%
				if (request.getAttribute("errorObcinaS") != null) {
					out.print(request.getAttribute("errorObcinaS").toString());
				} %>
			</td>
		</tr>
		<tr>
			<td></td>
			<th>Država*</th>
			<td>
				<input type="text" name="drzava"
					<%
					if (request.getParameter("drzava") != null && error) {
						out.print("value='" + request.getParameter("drzava").toString() + "'");
					} else {
						out.print("value='Slovenija'");
					}%>
				/>
				<input type="hidden" name="idDrzava"
					<%
					if (request.getParameter("idDrzava") != null && error) {
						out.print("value='" + request.getParameter("idDrzava").toString() + "'");
					} else {
						out.print("value='201'");
					}%>
				/>
			</td>
			<td class="error">
				<%
				if (request.getAttribute("errorDrzava") != null) {
					out.print(request.getAttribute("errorDrzava").toString());
				} %>
			</td>
		</tr>
		<tr>
			<td></td>
			<th>Telefonska številka*</th>
			<td>
				<input type="text" name="telefon" maxlength="12"
					<%
					if (request.getParameter("telefon") != null && error) {
						out.print("value='" + request.getParameter("telefon").toString() + "'");
					} %>
				/>
			</td>
			<td class="error">
				<%
				if (request.getAttribute("errorTelefon") != null) {
					out.print(request.getAttribute("errorTelefon").toString());
				} %>
			</td>
		</tr>
		<tr>
			<td></td>
			<th>Prenosni telefon*</th>
			<td>
				<input type="text" name="prenosni"
					<%
					if (request.getParameter("prenosni") != null && error) {
						out.print("value='" + request.getParameter("prenosni").toString() + "'");
					} %>
				/>
			</td>
			<td class="error">
				<%
				if (request.getAttribute("errorPrenosni") != null) {
					out.print(request.getAttribute("errorPrenosni").toString());
				} %>
			</td>
		</tr>
		<tr>
			<td></td>
			<th>Elektronska pošta*</th>
			<td>
				<input type="text" name="email"
					<%
					if (request.getParameter("email") != null && error) {
						out.print("value='" + request.getParameter("email").toString() + "'");
					} %>
				/>
			</td>
			<td class="error">
				<%
				if (request.getAttribute("errorEmail") != null) {
					out.print(request.getAttribute("errorEmail").toString());
				} %>
			</td>
		</tr>
		<tr>
			<th colspan="4" class="odsek">NASLOV ZAČASNEGA BIVALIŠČA</th>
		</tr>
		<tr>
			<td>7</td>
			<th>Ulica (naselje) in hišna številka</th>
			<td>
				<input type="text" name="ulicaZ"
					<%
					if (request.getParameter("ulicaZ") != null && error) {
						out.print("value='" + request.getParameter("ulicaZ").toString() + "'");
					} %>
				/>
			</td>
			<td class="error">
				<%
				if (request.getAttribute("errorUlicaZ") != null) {
					out.print(request.getAttribute("errorUlicaZ").toString());
				} %>
			</td>
		</tr>
		<tr>
			<td>8</td>
			<th>Pošta</th>
			<td>
				<input type="text" name="postaZ"
					<%
					if (request.getParameter("postaZ") != null && error) {
						out.print("value='" + request.getParameter("postaZ").toString() + "'");
					} %>
				/>
				<input type="hidden" name="idPostaZ"
					<%
					if (request.getParameter("idPostaZ") != null && error) {
						out.print("value='" + request.getParameter("idPostaZ").toString() + "'");
					} %>
				/>
			</td>
			<td class="error">
				<%
				if (request.getAttribute("errorPostaZ") != null) {
					out.print(request.getAttribute("errorPostaZ").toString());
				} %>
			</td>
		</tr>
		<tr>
			<td></td>
			<th>Poštna številka</th>
			<td>
				<input type="text" name="postnaStevilkaZ" readonly="readonly"
					<%
					if (request.getParameter("postnaStevilkaZ") != null && error) {
						out.print("value='" + request.getParameter("postnaStevilkaZ").toString() + "'");
					} %>
				/>
			</td>
			<td class="error">
				<%
				if (request.getAttribute("errorPostnaStevilkaZ") != null) {
					out.print(request.getAttribute("errorPostnaStevilkaZ").toString());
				} %>
			</td>
		</tr>
		<tr>
			<td>9</td>
			<th>Občina</th>
			<td>
				<input type="text" name="obcinaZ"
					<%
					if (request.getParameter("obcinaZ") != null && error) {
						out.print("value='" + request.getParameter("obcinaZ").toString() + "'");
					} %>
				/>
				<input type="hidden" name="idObcinaZ"
					<%
					if (request.getParameter("idObcinaZ") != null && error) {
						out.print("value='" + request.getParameter("idObcinaZ").toString() + "'");
					} %>
				/>
			</td>
			<td class="error">
				<%
				if (request.getAttribute("errorObcinaZ") != null) {
					out.print(request.getAttribute("errorObcinaZ").toString());
				} %>
			</td>
		</tr>
		<tr>
			<th colspan="4" class="odsek">PODATKI O VPISU</th>
		</tr>
		<tr>
			<td>10</td>
			<th>Visokošolski zavod članica</th>
			<td><input type="text" name="visokosolskiZavod" readonly="readonly" value="163 - Fakulteta za računalništvo in informatiko"/></td>
			<td class="error"></td>
		</tr>
		<tr>
			<td>11</td>
			<th>Študijski program</th>
			<td>
				<select name="studijskiProgramVpisniList">
					<%
					if (request.getAttribute("studijskiProgrami") != null) {
						List<Studijski_program> studijskiProgrami = (List<Studijski_program>)request.getAttribute("studijskiProgrami");
						for (Studijski_program program : studijskiProgrami) { %>
							<option value="<%= program.getIdStudijskiProgram() %>"><%= program.getNazivStudijskiProgram() %></option><% 
						}
					}%>
				</select>
			</td>
			<td class="error"></td>
		</tr>
		<tr>
			<td>12</td>
			<th>Smer in izbirna skupina</th>
			<td>
				<select name="smerSkupina">
					<%
					if (request.getAttribute("studijskeSmeri") != null) {
						List<Studijska_smer> studijskiProgrami = (List<Studijska_smer>)request.getAttribute("studijskeSmeri");
						for (Studijska_smer smer : studijskiProgrami) { %>
							<option value="<%= smer.getIdStudijskaSmer() %>"><%= smer.getNazivStudijskaSmer() %></option><% 
						}
					}%>
				</select>
			</td>
			<td class="error"></td>
		</tr>
		<tr>
			<td>13</td>
			<th>Skupina</th>
			<td><input type="text" name="skupina" /></td>
			<td class="error"></td>
		</tr>
		<tr>
			<td>14</td>
			<th>Študijsko leto</th>
			<td>
				<select name="studijskoLeto">
					<option value="2009/2010">2009/2010</option>
					<option value="2010/2011">2010/2011</option>
					<option value="2011/2012">2011/2012</option>
					<option value="2012/2013">2012/2013</option>
					<option value="2013/2014">2013/2014</option>
				</select>
			</td>
			<td class="error"></td>
		</tr>
		<tr>
			<td>15</td>
			<th>Vrsta študija</th>
			<td>
				<table>
					<tr>
						<th colspan="2">Prva stopnja:</th>
					</tr>
					<tr>
						<td style="width: 50%"><input type="radio" name="vrstaStudija" value="1" checked="checked" /> J - visokošolski strokovni</td>
						<td><input type="radio" name="vrstaStudija" value="2" /> K - univerzitetni</td>
					</tr>
					<tr>
						<th colspan="2">Druga stopnja:</th>
					</tr>
					<tr>
						<td style="width: 50%"><input type="radio" name="vrstaStudija" value="3" /> L - magistrski</td>
						<td><input type="radio" name="vrstaStudija" value="4" /> N - enovit magistrski</td>
					</tr>
					<tr>
						<th colspan="2">Tretja stopnja:</th>
					</tr>
					<tr>
						<td style="width: 50%"><input type="radio" name="vrstaStudija" value="5" /> M - doktorski</td>
					</tr>
				</table>
				<hr />
				<table>
					<tr>
						<td style="width: 50%"><input type="radio" name="vrstaStudija" value="6" /> B - visokošolski strokovni</td>
						<td><input type="radio" name="vrstaStudija" value="7" /> C - univerzitetni</td>
					</tr>
					<tr>
						<td style="width: 50%"><input type="radio" name="vrstaStudija" value="8" /> E - specialistični</td>
						<td><input type="radio" name="vrstaStudija" value="9" /> F - magistrski</td>
					</tr>
					<tr>
						<td style="width: 50%"><input type="radio" name="vrstaStudija" value="10" /> G - doktorski</td>
						<td><input type="radio" name="vrstaStudija" value="11" /> H - enovit doktorski</td>
					</tr>
				</table>
			</td>
			<td class="error"></td>
		</tr>
		<tr>
			<td>16</td>
			<th>Letnik študija</th>
			<td>
				<table>
					<tr>
						<td><input type="radio" name="letnikStudija" value="1" checked="checked" /> 1</td>
						<td><input type="radio" name="letnikStudija" value="2" /> 2</td>
						<td><input type="radio" name="letnikStudija" value="3" /> 3</td>
						<td><input type="radio" name="letnikStudija" value="4" /> 4</td>
						<td><input type="radio" name="letnikStudija" value="5" /> 5</td>
						<td><input type="radio" name="letnikStudija" value="6" /> 6</td>
					</tr>
					<tr>
						<td colspan="6"><input type="radio" name="letnikStudija" value="7 - absolvent visokošolskega študija" /> 7 - absolvent visokošolskega študija</td>
					</tr>
					<tr>
						<td colspan="6"><input type="radio" name="letnikStudija" value="8 - absolvent univerzitetnega študija" /> 8 - absolvent univerzitetnega študija</td>
					</tr>
					<tr>
						<td colspan="6"><input type="radio" name="letnikStudija" value="9 - absolvent magisterskega študija" /> 9 - absolvent magisterskega študija</td>
					</tr>
				</table>
			</td>
			<td class="error"></td>
		</tr>
		<tr>
			<td>17</td>
			<th>Način študija</th>
			<td>
				<table>
					<tr>
						<td style="width: 60px"><input type="radio" name="nacinStudija" value="R" checked="checked" /> 1 - redni</td>
						<td><input type="radio" name="nacinStudija" value="I" /> 3 - izredni</td>
					</tr>
				</table>
			</td>
			<td class="error"></td>
		</tr>
		<tr>
			<td></td>
			<th>Študij na daljavo</th>
			<td>
				<table>
					<tr>
						<td style="width: 60px"><input type="radio" name="studijNaDaljavo" value="1" /> 1 - da</td>
						<td><input type="radio" name="studijNaDaljavo" value="0" checked="checked" /> 2 - ne</td>
					</tr>
				</table>
			</td>
			<td class="error"></td>
		</tr>
		<tr>
			<td>18</td>
			<th>Vrsta vpisa</th>
			<td>
				<table>
					<tr>
						<td style="width: 50%"><input type="radio" name="vrstaVpisa" value="1" checked="checked" /> V1 - prvi vpis v letnik</td>
						<td><input type="radio" name="vrstaVpisa" value="2" /> V2 - ponavljanje letnika</td>
					</tr>
					<tr>
						<td style="width: 50%"><input type="radio" name="vrstaVpisa" value="3" /> V3 - nadaljevanje letnika</td>
						<td><input type="radio" name="vrstaVpisa" value="4" /> AB - absolvent</td>
					</tr>
				</table>
			</td>
			<td class="error"></td>
		</tr>
		<tr>
			<td colspan="3" class="gumbi"><div><input type='submit' value='POŠLJI' /></div></td>
			<td></td>
		</tr>
	</table>
</form>
<script type="text/javascript">
	$('input[type=radio]').addClass("izbire"); /* To je obvezno, če imate radiobtn ali pa chkbx, če ne so vsi rdbtn in chkbx široki 100% + majo grd stil + vsakič je treba... */
	$('input[type=checkbox]').addClass("izbire"); /* ...pisat class za vsak input + niso poravnani. Poleg tega je treba vse chkbx in rdbtn dajat v gnezdeno tabelo. */
	$('.forme th').css('width', '188px'); /* kakšen del širine forme nej zavzamejo <th> */
	$('.error').css('width', '200px'); /* kakšen del širine forme nej zavzamejo errorji */
	/* Ostal plac je naštiman, da ga zafilajo polja (text, select, textarea, ...) */
</script>