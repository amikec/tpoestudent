<%@page import="java.util.List"%>
<%@page import="si.fri.estudent.jpa.Predmet_izvajalec" %> 
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<h1> VZDRŽEVANJE PODTKOV O IZVAJALCIH </h1>

<div class="gumbi" style="margin-left:647px">
	<a href="VzdrzevanjeIzvajalcevDodajServlet">Dodaj izvajalca za predmet</a>
</div>
<%if(request.getAttribute("uspeh") != null) { %>
	<p class="success" style="text-align: center;"><%=request.getAttribute("uspeh")%></p>
<%} else {
	out.print("<br />");
}%>
<table class="tabele" style="width: 700px; margin-left: 100px;">
	<thead>
		<tr>
			<th>Šifra predmeta</th>
			<th>Naziv predmeta</th>
			<th>Kombinacija izvajalcev</th>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<% 
		if(request.getAttribute("predmetIzvajalec") != null) {
			List<Predmet_izvajalec> predmetIzvajalec = (List<Predmet_izvajalec>)request.getAttribute("predmetIzvajalec");
			for(Predmet_izvajalec p : predmetIzvajalec) { %>
				<tr>
					<td><%= p.getPredmet().getSifraPredmeta() %></td>
					<td><%= p.getPredmet().getNazivPredmet() %></td>
					<td>
					<% String komb = p.getKombinacijeIzvajalcev().getOsebje1().getOseba().getIme() + " " + p.getKombinacijeIzvajalcev().getOsebje1().getOseba().getPriimek(); 
						if(p.getKombinacijeIzvajalcev().getOsebje2() != null) {
							komb +=  ", " + p.getKombinacijeIzvajalcev().getOsebje2().getOseba().getIme() + " " + p.getKombinacijeIzvajalcev().getOsebje2().getOseba().getPriimek();
						}
						if(p.getKombinacijeIzvajalcev().getOsebje3() != null) {
							komb += ", " + p.getKombinacijeIzvajalcev().getOsebje3().getOseba().getIme() + " " + p.getKombinacijeIzvajalcev().getOsebje3().getOseba().getPriimek();
						}
						out.print(komb);
					%> 
					</td>
					<td><a href='VzdrzevanjeIzvajalcevUrejanjeServlet?id=<%=p.getIdPredmetIzvajalca()%>'>Uredi</a></td>
					<td><a href='VzdrzevanjeIzvajalcevServlet?id=<%=p.getIdPredmetIzvajalca()%>'>Izbriši</a></td>
				</tr>
			<%}
		}
	%>
</table>

