<%@page import="si.fri.estudent.jpa.Vrsta_studija"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<h1>VZDRŽEVANJE PREDMETNIKOV IN MODULOV</h1>
<form action="VzdrzevanjePredmetnikaServlet" method="post" class="forme" style="width: 650px; margin-left: 130px">
	<table>
		<% if (request.getAttribute("error") != null) { %>
			<tr>
				<td colspan="2" class="success"><%= (String)request.getAttribute("error") %></td>
			</tr>
		<% } %>
		<tr>
			<th>Vrsta študija</th>
			<td>
				<select name="vrstaStudija">
					<option value='0'>-- Izberi vrsto študija --</option>
					<%
					if (request.getAttribute("vrsteStudija") != null) {
						List<Vrsta_studija> vrsteStudija = (List<Vrsta_studija>)request.getAttribute("vrsteStudija");
						
						for (Vrsta_studija vs : vrsteStudija) { %>
							<option value='<%= vs.getIdVrstaStudija() %>'><%= vs.getKraticaVrstaStudija() + " - " + vs.getNazivVrstaStudija() %></option>
						<% }
					} %>
				</select>
			</td>
		</tr>
		<tr>
			<th>Študijski program</th>
			<td>
				<select name="studijskiProgram">
				</select>
			</td>
		</tr>
		<tr>
			<th>Studijska smer</th>
			<td>
				<select name="studijskaSmer">
				</select>
			</td>
		</tr>
		<tr>
			<th>Študijsko leto</th>
			<td>
				<select name="studijskoLeto" disabled="disabled">
					<option value="0">-- Izberite študijsko leto --</option>
					<option value="2009/2010">2009/2010</option>
					<option value="2010/2011">2010/2011</option>
					<option value="2011/2012">2011/2012</option>
					<option value="2012/2013">2012/2013</option>
					<option value="2013/2014">2013/2014</option>
				</select>
			</td>
		</tr>
		<tr>
			<th>Letnik</th>
			<td>
				<select name="letnik">
				</select>
			</td>
		</tr>
		<tr>
			<th>Predmetnik / Modul</th>
			<td>
				<select name="predmetnikModul" disabled="disabled">
					<option value="0">-- Izberite predmetnik ali modul --</option>
					<option value="predmetnik">Predmetnik</option>
					<option value="modul">Modul</option>
				</select>
			</td>
		</tr>
		<tr id="predmetnikVrstica">
			<th>Vrsta predmetnika</th>
			<td>
				<select name="vrstaPredmetnika">
					<option value="0">-- Izberite vrsto predmetnika --</option>
					<option value="obvezni">Obvezni predmetnik</option>
					<option value="neobvezni">Neobvezni predmetnik</option>
				</select>
			</td>
		</tr>
		<tr id="modulVrstica">
			<th>Modul</th>
			<td>
				<select name="modul">
				</select>
			</td>
		</tr>
		<tbody class="showHidePredmetnikDragDrop">
			<tr>
				<td colspan="2" class="dragDrop">
					
				</td>
			</tr>
			<tr>
				<td colspan="2" class="gumbi"><div><input type='submit' value='SHRANI' /></div></td>
			</tr>
		</tbody>
	</table>
</form>
<script type="text/javascript">
	$('.forme th').css('width', '130px');
	$('.showHidePredmetnikDragDrop').hide();
	$('#predmetnikVrstica').hide();
	$('#modulVrstica').hide();
</script>