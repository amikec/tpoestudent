<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, si.fri.estudent.jpa.*, si.fri.estudent.utilities.SkupenProgram" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">



<% 	String sifrantIme=(String)request.getAttribute("sifrantIme");
	String sifrant=(String)request.getAttribute("sifrant");
	String[] naslovna=(String[])request.getAttribute("naslovna");
	String[][] vsebina=(String[][])request.getAttribute("vsebina");
	int stTextov=vsebina[0].length-2;
	
	if(sifrant.equals("predavatelj")){
		out.write("<head><style type='text/css'> td input{  width: 110px;  }  </style></head>");
	}
%>


<script type="text/javascript">	
	var sifrantS="<%= sifrant %>";
	var stTextov=<%= vsebina[0].length-2%>;
	$(document).ready(init());
	function init(){	
		$(".izbrisi").unbind();
		$(".izbrisi").click(function(){			
			var answer = confirm("Izbirši?");
			var id=this.id;
			
			if(answer){				
				$.post('SifrantiAjaxServlet', podatki = {sifrant: ("izbrisi"+sifrantS), id: this.id }, function(responseData) {
									
					if(responseData=="OK"){
						$("tr#"+id).remove();
						init();
					}else{
						$("tr#"+id+ " .error").html(responseData);
					}
				});
			}
			
		});
		$(".spremenljivTekst").unbind();
		$(".spremenljivTekst").keydown(function(){			
			$(this).css('background-color', '#66FFFF');			
		});
		$(".status").unbind();
		$(".status").change(function(){			
			$(this).css('background-color', '#66FFFF');			
		});
		$("tr").unbind();
		$("tr").keydown(function(){						
			$("tr#"+this.id+ " .gumbi .shrani").css('visibility', 'visible');	
		});
		
		$("tr").change(function(){	
			//alert("spremenjen tr "+this.id);
			$("tr#"+this.id+ " .gumbi .shrani").css('visibility', 'visible');	
		});
		
		$(".shrani").unbind();
		$(".shrani").click(function(){
			
			var id=this.id;			
			<% 
			String izpis="";
			for(int i=1;i<stTextov+1;i++){				
				izpis+="var text"+i+" =$('tr#'+this.id+ ' .t"+i+"').val();";								
			}
			out.write(izpis);
			%>
			
			//var text1=$('tr#'+this.id+ ' .t1').val();
			//alert("text1 vsebina "+text1);
			//var text1=$("tr#"+this.id+ " .t1").val();
			var status=$("tr#"+this.id+ " .status").val();
			//alert(this.id+" "+sifrantS+ (stTextov+10)+" "+text1+" "+status);
				
			var nasel=0;
			var sifra=text1;
			
			$('#mytable tr.vseb').each(function() {
				
				var sifX=$('tr#'+this.id+' input.t1').val();
				
				if(sifX.toLowerCase()==sifra.toLowerCase()){
										
					nasel++;
				}
			});
			if(nasel>1){
				
				$("tr#"+id+" .error").html("Ta sifra ze obstaja");
			}else{
			
				$.post('SifrantiAjaxServlet', podatki = {sifrant: ("shrani"+sifrantS), id: this.id 
					<% 
					izpis="";
					for(int i=1;i<stTextov+1;i++){
						izpis+=", text"+i+" :text"+i+"";
					}
					out.write(izpis);
					%>
					
					,status:status}, function(responseData) {
						//alert(responseData.substring(0,6));
					if(responseData=="OK"){
						//alert("ok"+id);
						<% 
						izpis="";
						for(int i=1;i<stTextov+1;i++){
							izpis+="$('tr#'+id+ ' .t"+i+"').css('background-color', '#FFFFFF');";
						}
						out.write(izpis);
						%>
								
						$("tr#"+id+ " .status").css('background-color', '#FFFFFF');
						$("tr#"+id+ " .gumbi .shrani").css('visibility', 'hidden');						
						$("tr#"+id+ " .error").html("");
						init();
					}else if(responseData.substring(0,6) =="Napaka"){
						//alert("napaka");
						$("tr#"+id+ " .error").html(responseData);
					}
						
				});
			}
					
		});
		$("#dodajButton").unbind();
		$("#dodajButton").click(function(){
			<% 
			izpis="";
			for(int i=1;i<stTextov+1;i++){				
				izpis+="var text"+i+"=$('tr#dodaj .t"+i+"').val();";								
			}
			out.write(izpis);
			%>
			
			var status=$("tr#dodaj .statusDodaj").val();			
			
			var nasel=0;
			var sifra=text1;
			$('#mytable tr.vseb').each(function() {
				var sifX=$('tr#'+this.id+' input.t1').val();
				if(sifX.toLowerCase()==sifra.toLowerCase()){
					nasel=1;
				}
			});
			if(nasel==1){
				$("tr#dodaj .error").html("Ta sifra ze obstaja");
			}else{
			
				$.post('SifrantiAjaxServlet', podatki = {sifrant: ("dodaj"+sifrantS) 
						<% 
						izpis="";
						for(int i=1;i<stTextov+1;i++){
							izpis+=", text"+i+" :text"+i+"";
						}
						out.write(izpis);
						%>					
					
					,status:status}, function(responseData) {
					//alert(responseData.substring(0,6));
					if(responseData.substring(0,6) !="Napaka"){
						<% 
						izpis="";
						for(int i=1;i<stTextov+1;i++){
							izpis+="$('tr#dodaj #t"+i+"').val('');";
						}
						out.write(izpis);
						%>						
						$("tr#dodaj .statusDodaj").val("1");
						$("tr#dodaj .error").html("");
						
						
						$("tr#dodaj").before(responseData);
						init();
					}else if(responseData.substring(0,6) =="Napaka"){
						//alert("napaka");
						$("tr#dodaj .error").html(responseData);
					}
						
				});
			}
				
			
		});
		$("#iskanjeButton").unbind();
		$("#iskanjeButton").click(function(){
			var iskanjeS=$("#iskanjeTekst").val();		
			var xx=0;
			if(iskanjeS!=""){				
				$('#mytable tr.vseb').each(function() {
					xx++;
					var nasel=0;
					var i=1;					
					$('tr#'+this.id+' input.spremenljivTekst').each(function(){
						if(this.value.toLowerCase().indexOf(iskanjeS.toLowerCase())===0){
							nasel=1;
						}
					});
					if(nasel==1){						
						$('tr#'+this.id).show();						
					}else{
						$('tr#'+this.id).hide();
					}
					
				});
				
			}else{
				$('#mytable tr.vseb').each(function() {										
					$('tr#'+this.id).show();							
				});
			}			
		});
	}
	
	
	
</script>
<h1><%= sifrantIme %></h1>

<div class="gumbi" id="vrh">
	<a href="#dodaj">Dodaj</a> 
	
	<input type="text" id="iskanjeTekst" 
	value="<%	if(request.getAttribute("iskanje")!=null){
					out.write((String)request.getAttribute("iskanje"));
				} %>" style="position:absolute;right:130px;" />
	<input type="button" id="iskanjeButton" value="Iskanje" style="position:absolute;right:60px;" />
</div>
<table id="mytable">
	<tr>
		<th>Id</th>
		<% for(String x:naslovna){ %>
			<th><%= x %></th>
		<%} %>
		<th>Veljaven</th>
	</tr>
	<%
	int st=vsebina[0].length;
	for(String[] vrstica:vsebina){ %>		
		<tr id="<%= vrstica[0]%>" class="vseb" >
			<td><%= vrstica[0]%></td>
			<% 	
				for(int i=1;i<st-1;i++){%>
					<td><input type="text" class="t<%= i%> spremenljivTekst" value="<%= vrstica[i] %>" id="t<%= i%>"/> </td>
			<% } %>
			<td>
				<select name="program" class="status">
					<option value="1" <% if(vrstica[st-1].equals("1")){out.write("selected='selected'");}%> >Da</option>
					<option value="0" <% if(vrstica[st-1].equals("0")){out.write("selected='selected'");}%> >Ne</option>
				</select>	
			</td>			
			
			<td class="gumbi" style="padding-top:0px;"><input type="button" class="izbrisi" value="Izbrisi" id="<%= vrstica[0]%>" style="padding-top:0px;" /></td>
			<td class="gumbi" style="padding-top:0px; "><input type="button" class="shrani" value="Shrani" id="<%= vrstica[0]%>" style="padding-top:0px;visibility:hidden;" /></td>
			<td class="error"></td>
		</tr>		
	<%} %>
	
	<tr id="dodaj">
			<td></td>
			<% 	
				for(int i=1;i<st-1;i++){%>
					<td><input type="text" class="t<%= i%>" value="" id="t<%= i%>"/> </td>
			<% } %>			
			
			<td>
				<select name="program" class="statusDodaj">
					<option value="1">Da</option>
					<option value="0">Ne</option>
				</select>	
			</td>
			<td class="gumbi" style="padding-top:0px; "><input type="button" class="dodajButton" value="Dodaj" id="dodajButton" style="padding-top:0px;" /></td>
			<td class="error"></td>
		</tr>
</table>


<div class="gumbi"><a href="#vrh">Na vrh</a></div>