<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="si.fri.estudent.jpa.*" %>
<%@ page import="java.util.*" %>
<%@ page import="si.fri.estudent.utilities.*" %>
<h1>Odjava z izpita</h1>

<link type="text/css" href="JavaScript/jQuery/UI-datepicker/css/custom-theme/jquery-ui-1.8.18.custom.css" rel="stylesheet" />	
<link type="text/css" href="JavaScript/jQuery/UI-datepicker/css/custom-theme/jquery-ui-timepicker.css" rel="stylesheet" />
<script type="text/javascript" src="JavaScript/jQuery/UI-datepicker/js/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="JavaScript/jQuery/UI-datepicker/js/jquery-ui-timepicker.js"></script>
</head>
<body>

<%
	
	List<Izpitni_rok> roki = (List<Izpitni_rok>)request.getAttribute("roki");
	if(roki == null)
		roki = new LinkedList<Izpitni_rok>();
	Predmet predmet;
	LinkedList<Oseba> izvajalci = new LinkedList<Oseba>();
	Kombinacije_izvajalcev kombinacije;
	Oseba izvajalec1;
	Oseba izvajalec2;
	Oseba izvajalec3;
	boolean referentka = false;

	Vloga_oseba vloga = (Vloga_oseba) session.getAttribute("vloga");
	if(vloga.getIdVlogaOseba() == Constants.VLOGA_REFERENTKA)
		referentka = true;

	if(referentka == true){
		Student student = (Student) session.getAttribute("studentIzIskanja");
		Oseba oseba = student.getOseba();
%>
<table>
	<tr>
		<td>Študent: <%= oseba.getIme() + " " + oseba.getPriimek() %></td>
	</tr>
</table>
<% 	} %>
<table class="tabele" style="margin-left: 150px;">
	<tr>
		<th>Predmet</th><th>Datum</th><th>Izvajalci</th><th></th>
		
		<%
		String izpis = "";
		
		for(Izpitni_rok rok : roki){
			
			izvajalec1 = null;
			izvajalec2 = null;
			izvajalec3 = null;
			
			predmet = rok.getPredmet();
			kombinacije = rok.getPredmetIzvajalec().getKombinacijeIzvajalcev();
			
			izvajalec1 = kombinacije.getOsebje1().getOseba();
			if(kombinacije.getOsebje2() != null)
				izvajalec2 = kombinacije.getOsebje2().getOseba();
			if(kombinacije.getOsebje3() != null)
				izvajalec3 = kombinacije.getOsebje3().getOseba();
			
			izpis = "<tr><td>" + predmet.getNazivPredmet() + "</td>" +
			               "<td>" + Utilities.getDatumIzpita(rok.getDatumIzpit()) + "</td>" + 
						   "<td>" + izvajalec1.getIme() + " " + izvajalec1.getPriimek() + "</td>";
						   
			if(!IzpitValidator.moznaPrijavaNaIzpit(rok.getDatumIzpit()) || referentka)
				izpis += "<td class='gumbi'><a href='OdjavaIzpitServlet?mem=" + rok.getIdRok() + "'>Odjava</a></td></tr>";
			else
				izpis += "<td>**</td></tr>";
					
			if(izvajalec2 != null)
				izpis += "<tr><td></td><td></td><td>" + izvajalec2.getIme() + "" + izvajalec2.getPriimek() + "</td><td></td></tr>";
				
			if(izvajalec3 != null)
				izpis += "<tr><td></td><td></td><td>" + izvajalec3.getIme() + "" + izvajalec3.getPriimek() + "</td><td></td></tr>";
			
			out.print(izpis);
		}
		
		if(!referentka)
			out.print("<tr><td colspan='4'>** Rok za odjavo potekel.</td></tr>");
		
		%>
	</tr>
</table>
</body>
</html>
<script type="text/javascript">
	$('.tabele th').css('width', '150px'); /* kakšen del širine forme nej zavzamejo <th> */
	$('.tabele td').css('padding-left', '10px');
	$('.tabele td').css('padding-right', '10px');
</script>