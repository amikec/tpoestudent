<%@page import="si.fri.estudent.jpa.Vrsta_studija"%>
<%@page import="si.fri.estudent.jpa.Student"%>
<%@page import="si.fri.estudent.jpa.Oseba"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
String imeStudent = null;
Student student = null;
Oseba oseba = null;
List<Vrsta_studija> vrsteStudija = null;

if(request.getAttribute("student") != null && request.getAttribute("oseba") != null){
	student = (Student)request.getAttribute("student");
	oseba = (Oseba)request.getAttribute("oseba");
	
	if (oseba.getPriimekDekliski() == null || oseba.getPriimekDekliski().equals("")) {
		imeStudent = oseba.getIme() + " " + oseba.getPriimek() + " (" + student.getVpisnaSt() + ")";
	} else {
		imeStudent = oseba.getIme() + " " + oseba.getPriimekDekliski() + " " + oseba.getPriimek() + " (" + student.getVpisnaSt() + ")";
	}
} else {
	imeStudent = "Iskani študent ne obstaja";
}

if (request.getAttribute("vrsteStudija") != null) {
	vrsteStudija = (List<Vrsta_studija>)request.getAttribute("vrsteStudija");
}
%>

<h1>VPOGLED V ŠTUDENTOVE OCENE</h1>
<form action="#" method="post" class="forme" style="width: 700px; margin-left: 105px;">
	<input type="hidden" name="idStudent" value='<%= student.getIdStudent() %>' />
	<table>
		<tr>
			<td colspan="2" class="success">
				<% if (request.getAttribute("errorText") != null) { %>
					<%= (String)request.getAttribute("errorText") %>
				<% } %>
			</td>
		</tr>
		<tr>
			<th>Študent</th>
			<td><input type="text" name="imeStudent" readonly="readonly" value='<%= imeStudent %>' /></td>
		</tr>
		<tr>
			<th>Vrsta študija</th>
			<td>
				<select name="vrstaStudijaStOcene">
					<%
					if (vrsteStudija.size() > 0) { %>
						<option value='0'>-- Izberi vrsto študija --</option>
						<%
						
						for (Vrsta_studija vs : vrsteStudija) { %>
							<option value='<%= vs.getIdVrstaStudija() %>'><%= vs.getKraticaVrstaStudija() + " - " + vs.getNazivVrstaStudija() %></option>
						<% }
					} %>
				</select>
			</td>
		</tr>
		<tr>
			<th>Študijski program</th>
			<td>
				<select name="studijskiProgramStOcene">
				</select>
			</td>
		</tr>
		<tr>
			<th>Studijska smer</th>
			<td>
				<select name="studijskaSmerStOcene">
				</select>
			</td>
		</tr>
		<tr>
			<th>Predmet</th>
			<td>
				<select name="predmetStOcene">
				</select>
			</td>
		</tr>
	</table>
</form>
<div id="showHideStOcene" style="width: 700px; margin-left: 105px;"></div>
<div id="natisniStudentoveOcene" style="width: 700px; margin-left: 105px;">
	<div class="gumbi" style="text-align: right"><a href="VpogledStudentoveOceneAjaxServlet?tiskanje=tiskanje">IZPIS ZA TISKANJE</a></div>
</div>
<script type="text/javascript">
	$('.forme th').css('width', '120px');
	$('studentoveOceneStOcene').hide();
	$('#natisniStudentoveOcene').hide();
</script>