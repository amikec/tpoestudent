<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, si.fri.estudent.jpa.*, si.fri.estudent.utilities.SkupenProgram" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<h1>Šifranti</h1>

<script type="text/javascript">	
	function posljiAjax(sifrantS) {
		$.post('SifrantiAjaxServlet', podatki = {sifrant: sifrantS}, function(responseData) {
			$('#ajaxIzpis').html(responseData);
		});
		
	}

	$(document).ready(function(){		
		$(".sifra").click(function(){
			posljiAjax(this.id);
		});
	});
	
	
</script>
<div class="gumbi">
<input type="button" id="obcina" class="sifra" value="Šifranti občin"/>
<input type="button" id="drzava" class="sifra" value="Šifranti držav"/>
<input type="button" id="posta" class="sifra" value="Šifranti pošt"/>
<input type="button" id="programInSmer" class="sifra" value="Šifranti programov in smeri"/>
<input type="button" id="predmet" class="sifra" value="Šifranti predmetov"/>
<input type="button" id="predavatelj" class="sifra" value="Šifranti predavateljev"/>
</div>
<br />
<br />
<div id="ajaxIzpis"></div>

