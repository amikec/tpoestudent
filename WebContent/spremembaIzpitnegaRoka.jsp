<%@ page import="java.util.*" import="si.fri.estudent.jpa.Predmet" import="si.fri.estudent.jpa.Oseba" import="si.fri.estudent.jpa.Izpitni_rok" import="si.fri.estudent.jpa.Vloga_oseba" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<link type="text/css" href="JavaScript/jQuery/UI-datepicker/css/custom-theme/jquery-ui-timepicker.css" rel="stylesheet" />

<script type="text/javascript" src="JavaScript/jQuery/UI-datepicker/js/jquery-ui-timepicker.js"></script>

<script type="text/javascript">
	$(function(){
		
		$("input[name=datumIz]").datepicker({
	    	dateFormat: 'dd.mm.yy'	    		
		});
		$('input[name=cas]').timepicker({});
		
		$('#urejanje').hide();
		
		$('#vnosIzpitnegaRokaAjaxRequest option[value=<% if(request.getAttribute("predmet") != null) out.print(request.getAttribute("predmet")); %>]').prop('selected', true);
		
		<% 
		Vloga_oseba vloga = (Vloga_oseba)session.getAttribute("vloga");
		if(vloga.getNazivVlogaOseba().compareTo("ucitelj") != 0) { %>
				
			// ajax z JSON na servlet
		    $('#spremembaIzpitnegaRokaAjaxRequest').change(function() {
		    	var sifra = $('#spremembaIzpitnegaRokaAjaxRequest').val();
		    	$.post('VnosIzpitnegaRokaAjaxServlet', podatki = {sifraPredmeta: sifra}, function(responseData) {
		        	var json = jQuery.parseJSON(responseData); // Metoda parsa responseData v json objekt
		        	
		        	var kombNiz = "";
		        	var predavatelj = "<option>-- izberite izvajalca --</option>";
		        	$('#spremembaIzpitnegaRokaAjaxResponse').find('option').remove();
		        	for(var i = 0; i < json.listPredavatelji.length; i++) { 
		        		kombNiz = json.idKombinacije[i];        		
		        		predavatelj += "<option value="+ kombNiz + ">" + json.listPredavatelji[i] + "</option>";
		        	}
		        	$('#spremembaIzpitnegaRokaAjaxResponse').append(predavatelj);
		        });
		    	$('#uspeh').html('');
		    });
			
		
			// To se da izboljšati
			$(function() {
		    	var sifra = $('#spremembaIzpitnegaRokaAjaxRequest').val();
		    	$.post('VnosIzpitnegaRokaAjaxServlet', podatki = {sifraPredmeta: sifra}, function(responseData) {
		        	var json = jQuery.parseJSON(responseData); // Metoda parsa responseData v json objekt
		        	
		        	var kombNiz = "";
		        	var predavatelj = "<option>-- izberite izvajalca --</option>";
		        	$('#spremembaIzpitnegaRokaAjaxResponse').find('option').remove();
		        	
		        	<%if(request.getAttribute("predavatelj") != null) {%>
			        	for(var i = 0; i < json.listPredavatelji.length; i++) {
			        		kombNiz = json.idKombinacije[i];        		
			        		if(kombNiz == <%= request.getAttribute("predavatelj") %>)
			        			predavatelj += "<option value="+ kombNiz + ">" + json.listPredavatelji[i] + "</option>";
			        	}
		        	<%
		        	}
		        	%>
		        	for(var i = 0; i < json.listPredavatelji.length; i++) { 
		        		kombNiz = json.idKombinacije[i]; 
		        		<%if(request.getAttribute("predavatelj") != null) { %>
		        			if(kombNiz == <%= request.getAttribute("predavatelj") %>) {}
		        			else {
		        				predavatelj += "<option value="+ kombNiz + ">" + json.listPredavatelji[i] + "</option>";
		        			}
		        		<%	
		        		} else {
		        		%>
		        			predavatelj += "<option value="+ kombNiz + ">" + json.listPredavatelji[i] + "</option>";
		        		<%
		        		}
		        		%>
		        	}
		        	$('#spremembaIzpitnegaRokaAjaxResponse').append(predavatelj);
		        });
		    	$('#uspeh').html('');
	    	});
		<%
		} else {%>	
			$('#spremembaIzpitnegaRokaAjaxRequest').change(function() {
		    	var sifra = $('#spremembaIzpitnegaRokaAjaxRequest').val();
		    	$.post('VnosIzpitnegaRokaAjaxServlet', podatki = {sifraPredmeta: sifra}, function(responseData) {
		        	var json = jQuery.parseJSON(responseData); // Metoda parsa responseData v json objekt
		        	
		        	var kombNiz = "";
		        	var predavatelj = "<option>-- izberite izvajalca --</option>";
		        	$('#spremembaIzpitnegaRokaAjaxResponse').find('option').remove();
		        	
		        	<%  Oseba uporabnik = (Oseba)session.getAttribute("uporabnik"); %>
			        	for(var i = 0; i < json.listPredavatelji.length; i++) {
			        		kombNiz = json.idKombinacije[i];        		
			        		if(!(json.listPredavatelji[i].indexOf(<%= "'" + uporabnik.getIme() + " " + uporabnik.getPriimek() + "'" %>) == -1)) {
			        			predavatelj += "<option value='"+ kombNiz + "'>" + json.listPredavatelji[i] + "</option>";
			        		}
			        	}

		        	$('#spremembaIzpitnegaRokaAjaxResponse').append(predavatelj);
		        });
		    	$('#uspeh').html('');
	    	});
			
			$(function() {
				var sifra = $('#spremembaIzpitnegaRokaAjaxRequest').val();
		    	$.post('VnosIzpitnegaRokaAjaxServlet', podatki = {sifraPredmeta: sifra}, function(responseData) {
		        	var json = jQuery.parseJSON(responseData); // Metoda parsa responseData v json objekt
		        	
		        	var kombNiz = "";
		        	var predavatelj = "<option>-- izberite izvajalca --</option>";
		        	$('#spremembaIzpitnegaRokaAjaxResponse').find('option').remove();
		        	
		        	<%  uporabnik = (Oseba)session.getAttribute("uporabnik"); %>
			        	for(var i = 0; i < json.listPredavatelji.length; i++) {
			        		kombNiz = json.idKombinacije[i];        		
			        		if(!(json.listPredavatelji[i].indexOf(<%= "'" + uporabnik.getIme() + " " + uporabnik.getPriimek() + "'" %>) == -1)) {
			        			predavatelj += "<option value="+ kombNiz + ">" + json.listPredavatelji[i] + "</option>";
			        		}
			        	}
	
		        	$('#spremembaIzpitnegaRokaAjaxResponse').append(predavatelj);
		        });
		    	$('#uspeh').html('');
			});
			
		<%	
		}	
		%>
		
		
		// za polnjenje izpitnih rokov
		$('#spremembaIzpitnegaRokaAjaxResponse').change(function() {
			var sifraKombinacije = $('#spremembaIzpitnegaRokaAjaxResponse').val();
			var sifraPredmeta = $('#spremembaIzpitnegaRokaAjaxRequest').val();
	    	$.post('SpremembaIzpitnegaRokaAjaxServlet', podatki = {sifraPredmeta: sifraPredmeta, sifaKombinacije: sifraKombinacije}, function(responseData) {
	        	var json = jQuery.parseJSON(responseData); // Metoda parsa responseData v json objekt

	        	$('#izpitniRokAjaxResponse').find('option').remove();
	        	
	        	var izpiti = "<option>-- izberite izpitni rok --</option>";
	        	
	        	for(var i = 0; i < json.listIzpitniRoki.length; i++) {   
	        		izpiti += "<option value="+ json.idIzpitnegaRoka[i] + ">" + json.listIzpitniRoki[i] + "</option>";		
	        	}
				
	        	$('#izpitniRokAjaxResponse').append(izpiti);
	        });
	    	$('#uspeh').html('');
		});

		// dialog
		var $dialog = 	$('<div></div>')
						.dialog({
							autoOpen: false,
							title: "Pozor",
							buttons: {
								'Da': 	function() {
											obdelajDialog();
										},
								'Ne': 	function() {
											$dialog.dialog("close");
										}
							},
							width: 500,
							height: 200
						});
		
		var $dialog2 = 	$('<div></div>')
						.dialog({
							autoOpen: false,
							title: "Pozor",
							buttons: {
								'Ok': 	function() {
											$dialog2.dialog("close");
										}
							},
							width: 500,
							height: 200
						});
		
		// obdelava dialoga v primeru, da uporabnik pritisne da
		function obdelajDialog() {
			$dialog.dialog("close");
			var submitValue = $('input[name=submitValue]').val();
			var idRoka = $('#izpitniRokAjaxResponse').val();
			if(submitValue == 1) {
	    		$.post('SpremembaIzpitnegaRokaServlet', podatki = {izpitniRok : idRoka, submitValue : submitValue}, function(responseData) {
	    			var json = jQuery.parseJSON(responseData);
	    			$('#uspeh').html(json.uspeh);
	    		});
			} else {
				$('#izbira').hide();
				$('#urejanje').show();
	    		$.post('UrejanjeIzpitnegaRokaServlet', podatki = {izpitniRok : idRoka, posodabljanje : 'no'}, function(responseData) {
	    			var json = jQuery.parseJSON(responseData);
	    			
	    			$('input[name=datumIz]').val(json.datum);
	    			$('input[name=cas]').val(json.cas);
	    			$('input[name=predavalnica]').val(json.predavalnica);
	    			$('input[name=maksStPrijav]').val(json.maksStPrijav);
	    			$('input[name=mozneTocke]').val(json.mozneTocke);
	    			$('input[name=mejaZaPozitivno]').val(json.mejaZaPozitivno);
	    			$('input[name=idIzpitnegaRoka]').val(idRoka);

	    		});
	    		
			}
		};
		
		// če gre za bisanje izpitnega roka
		$('#izbrisiShowDialog').click(function(){
			
			var idRoka = $('#izpitniRokAjaxResponse').val();
	    	$.post('SpremembaIzpitnegaRokaAjaxServlet?dialog=0', podatki = {idIzpitnegaRoka: idRoka}, function(responseData) {
	        	var json = jQuery.parseJSON(responseData); // Metoda parsa responseData v json objekt     
	        	
	        	if(json.stPrijav == -2) {
	        		$dialog2.text("Za izbran rok že obstajajo ocene. Brisanje ni možno.").dialog('open');
	        	} else 
        	// za izbran izpitni rok obstaja več prijav
				if(json.stPrijav > 0) {
					$dialog.text("Za izbran izpitni rok obstajajo prijave. Jih želite izbrisati?").dialog('open');
				} else {
					$('input[name=submitValue]').val(1);
					obdelajDialog();
				}
	        
	        });
		});
		
		// če gre za urejanje izpitnega roka
		$('#urediShowDialog').click(function(){
			var idRoka = $('#izpitniRokAjaxResponse').val();
	    	$.post('SpremembaIzpitnegaRokaAjaxServlet?dialog=0', podatki = {idIzpitnegaRoka: idRoka}, function(responseData) {
	        	var json = jQuery.parseJSON(responseData); // Metoda parsa responseData v json objekt     

	        	if(json.stPrijav == -2) {
	        		$dialog2.text("Za izbran rok že obstajajo ocene. Urejanje ni možno.").dialog('open');
	        	} else 
	        	// za izbran izpitni rok obstaja več prijav za katere ni ocene
				if(json.stPrijav > 0) {
        			$dialog.text("Za izbran izpitni rok obstajajo prijave. Po urejanju bodo prijave popravljene. Želite nadaljevati?").dialog('open');
				} else {
					obdelajDialog();
				}
	    	});
	    });
		
	});	
	
	function setSubmit(value) {
		$('input[name=submitValue]').val(value);
	}
	
	function posodobi(value) {
		$('input[name=posodabljanje]').val(value);
	}
	
	function nazaj(value) {	
		$('#izbira').show();
		$('#urejanje').hide();
	
	}
	
	
</script>
<div id='izbira'>
	<h1>SPREMEMBA IZPITNEGA ROKA</h1>
	<form action="SpremembaIzpitnegaRokaServlet" method="POST" class="forme" style="width: 700px; margin-left: 180px;" >
		
		<table>
			<tr>
				<td colspan="2" class="success" id='uspeh'></td>
				<td></td>
			</tr>
			<% if(request.getAttribute("uspeh") != null) { %>
				<tr>
					<td colspan="2" class="success"><%= request.getAttribute("uspeh") %></td>
					<td></td>
				</tr>
			<%
			}
			%>
			<tr>
				<th>Predmet</th>
				<td>
					<select id="spremembaIzpitnegaRokaAjaxRequest" name="predmet">
						<option value="-1">-- izberite predmet --</option>
						<% 
							if(request.getAttribute("predmeti") != null) {
								List<Predmet> predmeti = (List<Predmet>)request.getAttribute("predmeti");
								
								for(Predmet predmet : predmeti) { %>
									<option value=<%= predmet.getSifraPredmeta() %>><%= predmet.getNazivPredmet() %></option>
								<% 
								}
							}	
						%>
					</select>
				</td>
				<td class="error"></td>
			</tr>
			<tr>
				<th>Predavatelj</th>
				<td>
					<select id="spremembaIzpitnegaRokaAjaxResponse" name="predavatelj" >
					</select>
				</td>
				<td class="error"></td>
			</tr>
			<tr>
				<th>Izpitni rok</th>
				<td>
					<select id="izpitniRokAjaxResponse" name="izpitniRok">
						<option>-- izberite izpitni rok --</option>
					</select>
				</td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2" class="gumbi"><div><input type='button' id='izbrisiShowDialog' value='IZBRIŠI' onclick='setSubmit(1)' /><input type='button' id='urediShowDialog' value='UREDI' onclick='setSubmit(0)' /></div></td>
				<td style="backgorund-color: red"></td>
			</tr>
		</table>
		<input type="hidden" name='submitValue' />
	</form>
	<div id='dialog'></div>		
	<script type="text/javascript">
		$('.forme th').css('width', '120px'); /* kakšen del širine forme nej zavzamejo <th> */
		$('.error').css('width', '180px'); /* kakšen del širine forme nej zavzamejo errorji */
		/* Ostal plac je naštiman, da ga zafilajo polja (text, select, textarea, ...) */
	</script>
</div>
<div id='urejanje'>
	<h1>UREJANJE IZPITNEGA ROKA</h1>
	<form action="UrejanjeIzpitnegaRokaServlet" method='POST' class="forme" style="width: 700px; margin-left: 180px;">
		<table>
			<% if(request.getAttribute("uspeh") != null) { %>
				<tr>
					<td colspan="2" class="success"><%= request.getAttribute("uspeh") %></td>
					<td></td>
				</tr>
			<%
			}
			%>
			<tr>
				<th>Datum izpita</th>
				<td><input type="text" name="datumIz" value="<% if(request.getAttribute("datum") != null) out.print(request.getAttribute("datum")); %>"/></td>
				<td class="error">
					<%
					if (request.getAttribute("errorDatum") != null) {
						out.print(request.getAttribute("errorDatum").toString());
					} %>
				</td>
			</tr>
			<tr>
				<th>Čas izpita</th>
				<td><input type="text" name="cas" value="<% if(request.getAttribute("cas") != null) out.print(request.getAttribute("cas")); else out.print("08:00"); %>" /></td>
				<td class="error">
					<%
					if (request.getAttribute("errorCas") != null) {
						out.print(request.getAttribute("errorCas").toString());
					} %>
				</td>
			<tr>
			<tr>
				<th>Predavalnica</th>
				<td><input type="text" name="predavalnica" value="<% if(request.getAttribute("predavalnica") != null) out.print(request.getAttribute("predavalnica")); %>" /></td>
				<td class="error">
					<%
					if (request.getAttribute("errorPredavalnica") != null) {
						out.print(request.getAttribute("errorPredavalnica").toString());
					} %>
				</td>
			</tr>
			<tr>
				<th>Maks. št. prijav</th>
				<td><input type="text" name="maksStPrijav" value="<% if(request.getAttribute("maksStPrijav") != null) out.print(request.getAttribute("maksStPrijav")); %>" /></td>
				<td class="error">
					<%
					if (request.getAttribute("errorMaksStPrijav") != null) {
						out.print(request.getAttribute("errorMaksStPrijav").toString());
					} %>
				</td>
			</tr>
			<tr>
				<th>Možne točke</th>
				<td><input type="text" name="mozneTocke" value="<% if(request.getAttribute("mozneTocke") != null) out.print(request.getAttribute("mozneTocke")); %>" /></td>
				<td class="error">
					<%
					if (request.getAttribute("errorMozneTocke") != null) {
						out.print(request.getAttribute("errorMozneTocke").toString());
					} %>
				</td>
			</tr>
			<tr>
				<th>Meja za pozitivno</th>
				<td><input type="text" name="mejaZaPozitivno" value="<% if(request.getAttribute("mejaZaPozitivno") != null) out.print(request.getAttribute("mejaZaPozitivno")); %>" /></td>
				<td class="error">
					<%
					if (request.getAttribute("errorMejaZaPozitivno") != null) {
						out.print(request.getAttribute("errorMejaZaPozitivno").toString());
					} %>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="gumbi"><div><input type='button' value='NAZAJ' onclick='nazaj("no")'/><input type='submit' value='POSODOBI' onclick='posodobi(yes)'/></div></td>
				<td style="backgorund-color: red"></td>
			</tr>
			<tr>
				<td><input type='hidden' name='posodabljanje' value='yes' /></td>
				<td><input type='hidden' name='idIzpitnegaRoka' /></td>
			</tr>
		</table>
	</form>
	<script type="text/javascript">
		$('.forme th').css('width', '120px'); /* kakšen del širine forme nej zavzamejo <th> */
		$('.error').css('width', '180px'); /* kakšen del širine forme nej zavzamejo errorji */
		/* Ostal plac je naštiman, da ga zafilajo polja (text, select, textarea, ...) */
	</script>
</div>