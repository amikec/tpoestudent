<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="si.fri.estudent.jpa.*" %>
<%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<h1>Analiza uspešnosti pri predmetu</h1>
<link type="text/css" href="JavaScript/jQuery/UI-datepicker/css/custom-theme/jquery-ui-timepicker.css" rel="stylesheet" />
<script type="text/javascript" src="JavaScript/jQuery/UI-datepicker/js/jquery-ui-timepicker.js"></script>

<form action="AnalizaUspesnostiAjaxServlet" method="POST" class="forme"  >
<table style="width: 400px; margin-left: 255px;">
	<tr>
		<th>Študijsko leto</th>
		<td>
			<select name="studijskoLeto">
				<option value='0' ></option>
				<option value='2009/2010' >2009/2010</option>
				<option value='2010/2011' >2010/2011</option>
				<option value='2011/2012' >2011/2012</option>
			</select>
		</td>
	</tr>
	<tr>
		<th rowspan="2" valign="middle">Obdobje od</th>
			<th><input type="text" name="od" id="datumAnalizaOd" readonly='readonly'/></th>
			<th>do</th>
	</tr><tr>
			<td><input type="text" name="dox" id="datumAnalizaDo" readonly='readonly'/></td>
	</tr>
	<tr>
		<th>Predmet</th>
			<td>
				<select name="predmet" id="predmet">
				</select>
			</td>
			<td></td>
		</tr>
</table>
</form>
<div id="izpisAnalizeUspesnosti" name="izpisAnalizeUspesnosti">
</div>
<script type="text/javascript">

	
	$('.forme th').css('width', '100px'); /* kakšen del širine forme nej zavzamejo <th> */
	/* Ostal plac je naštiman, da ga zafilajo polja (text, select, textarea, ...) */
		$("#datumAnalizaOd").datepicker({
		dateFormat: 'dd.mm.yy'       
	});
	
		$("#datumAnalizaDo").datepicker({
			dateFormat: 'dd.mm.yy'       
		});
</script>
</script>