<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, si.fri.estudent.jpa.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">	
	
	stLet=2;

	$(document).ready(function(){
		$("#divTisk").hide();
		$("#poizvedi").click(function(){
			poizvedi();
		});
		
		$(".izbira").change(function(){
			$("#vsebina").html("");
			$("#divTisk").hide();
		});
		
		$("#stLet").click(function(){
			spremeniStLet();
		});
		spremeniStLet();
		
	});

	function poizvedi(){
		$("#vsebina").html("");
		if(stLet==1){
			$.post('SeznamZaIzbirniServlet', podatki = {
					predmet : $("#predmeti option:selected").val(),
					studijskoLeto :  $("#studijskaLeta1 option:selected").val()				
			}, function(responseData) {
				$("#vsebina").html(responseData);	
				$("#divTisk").show();
			});
		}else if(stLet==2){
			$.post('SeznamZaIzbirniServlet', podatki = {
					predmet : $("#predmeti option:selected").val(),
					studijskoLeto :  $("#studijskaLeta2 option:selected").val()				
			}, function(responseData) {
				$("#vsebina").html(responseData);	
				$("#divTisk").show();
			});
		}
	}
	
	
	function spremeniStLet(){		
		if(stLet==1){				
			$("#enoLeto").hide();
			$("#dveLeti").show();
			stLet=2;
		}else if(stLet==2){
			$("#dveLeti").hide();
			$("#enoLeto").show();
			stLet=1;
		}
	};
	
	
</script>

<h1>Seznam za izbirni predmet</h1>

<%	String[] predmeti=(String[])request.getAttribute("predmeti");
	String[] leta=(String[])request.getAttribute("leta");	
	String[] leta2=(String[])request.getAttribute("leta2");	
%>

<div class="forme">
	<table>
		<tr>
			<td>Izbirni predmet</td>
			<td>
				<select id="predmeti" class="izbira">
					<% for(String predmet: predmeti){
						int i=0;%>
						<option value="<%= predmet%>">
							<%= predmet %>
						</option>
					<% } %>
				</select>
			</td>
			<td></td>
		</tr>
		
		<tr>
			<td>Študijsko leto</td>
			<td>
				<span id="enoLeto">
					<select id="studijskaLeta1" class="izbira" >
						<% for(String studijskoLeto: leta){
							int i=0;%>
							<option value="<%= studijskoLeto%>">
								<%= studijskoLeto %>
							</option>
						<% } %>
					</select>
				</span>
				<span id="dveLeti">
					<select id="studijskaLeta2" class="izbira" >
						<% for(String studijskoLeto: leta2){
							int i=0;%>
							<option value="<%= studijskoLeto%>">
								<%= studijskoLeto %>
							</option>
						<% } %>
					</select>
				</span>
			</td>
			<td class="gumbi" style="padding-top: 0px"><button id="stLet" >Spremeni stevilo let</button></td>	
		</tr>
		
		<tr>
			<td></td>
			<td class="gumbi"><button id="poizvedi" >Poizvedi</button></td>	
			<td></td>		
		</tr>	
	</table>
</div>
<br />
<div id="vsebina" style="min-height:150px" >

</div>
<br />
<div id="divTisk"><a href="SeznamZaIzbirniServlet">Natisni</a></div>