<%@page import="si.fri.estudent.jpa.Predmet, si.fri.estudent.jpa.Osebje, java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">	
	$(document).ready(function(){
		$('#idOsebja').submit(function() { // Pred submitom preverimo, da nismo izbrali več kot 3 predavatelje
		   	var izbrani = $('input[name=izbire]').val();
		   	if (izbrani.split(',').length > 3) {
		    	$('.success').html("<span class='error'>Maksimalno število predavateljev je 3</span>");
		    	return false;  
		  	}else if (izbrani == "") {
		    	$('.success').html("<span class='error'>Niste izbrali predavatelja</span>");
		    	return false;   
		  	}else {
		   		return true;
		  	}
		 });
	});
</script>
<h1>DODAJ IZVAJACA ZA PREDMET</h1>
<form action="VzdrzevanjeIzvajalcevDodajServlet" method='POST' class="forme" id="idOsebja" style="width: 450px; margin-left: 220px;">
	<table>
		<tr>
			<td colspan="3" style="text-align: center" class="success"></td>
		</tr>
		<%if(request.getAttribute("uspeh") != null) {%>
			<tr>
				<td colspan="3" style="text-align: center" class="success"><%= request.getAttribute("uspeh") %></td>
			</tr>
		<% } %>
		<tr>
			<th>Predmet</th>
			<td>
				<select name="predmet">
					<option value="-1">--izberite predmet--</option>
					<%
						if(request.getAttribute("predmeti") != null) {
							List<Predmet> predmeti = (List<Predmet>)request.getAttribute("predmeti");
							for(Predmet p : predmeti) { %>
								<option value='<%=p.getIdPredmeta()%>'><%=p.getNazivPredmet() %></option>								
						<%	} 
						}
					%>
				</select>
			</td>
		</tr>
		<tr>		
			<td colspan="2" class="dragDrop">
				<div id="drag">
				<%
					if (request.getAttribute("osebje") != null) {
						List<Osebje> osebje = (List<Osebje>)request.getAttribute("osebje");
						
						
						for (Osebje o : osebje) { %>
							<div id='<%= o.getIdOsebje() %>'><%= o.getOseba().getPriimek() + " " + o.getOseba().getIme() %></div><%
						}
					}
				%>
				</div>
				<div id="drop">
				<%
					String inputHiddenIdPredmetov = "";
				
				%>
				</div>
				<input type="hidden" name="izbire" value='<%= inputHiddenIdPredmetov %>' />
				<script type="text/javascript">
					initializeDragDrop();
				</script>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="gumbi"><div><input type='submit' value='POŠLJI' /></div></td>
			<td></td>
		</tr>
	</table>
</form>