<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="JavaScript/jQuery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="JavaScript/jQuery/UI-datepicker/js/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="JavaScript/jQuery/jquery.dropotron-1.0.js"></script>

<script type="text/javascript">	
	$(document).ready(function(){
		
		
		<%  if(request.getAttribute("nacin")!=null){
				String nacin=(String)request.getAttribute("nacin");
				if(nacin.equals("imeInPriimek")){
					out.print("$('#button1').hide();$('#imeInPriimekDel').addClass('forme');$('#vpisnaDel').hide()");
				}else if(nacin.equals("vpisna")){
					out.print("$('#button2').hide();$('#vpisnaDel').addClass('forme');$('#imeInPriimekDel').hide()");
				}
			}else{
				out.print("$('#button1').hide();$('#imeInPriimekDel').addClass('forme');$('#vpisnaDel').hide()");
			}
		%>
		
		
		
		});
	
	function posljiAjax()
	{
		var imeS = $('#imeS').val();
		var priimekS = $('#priimekS').val();
		$.post('IskanjeStudentaAjaxServlet', podatki = {ime: imeS, priimek: priimekS}, function(responseData) {
			$('#ajaxIzpis').html(responseData);
		});
	}
	
</script>

<%
	//shrani v sejo kam naj posreduje vpisno st studenta
	String posredujNa=request.getParameter("posredujNa");
	if(posredujNa!=null){
		session.setAttribute("posredujNa", posredujNa);
	}
%>

<h1>Iskanje študenta</h1>

<div id="napakaDiv">
		<%  if(request.getAttribute("napaka")!=null){
		String napaka=(String)request.getAttribute("napaka");
		%>
		<span class="error" style="font-size:15px;"><%=napaka %></span>
		<%}	%>
</div>
	
<div class="gumbi" >
		<a onclick="$('#vpisnaDel').removeClass('forme');$('#imeInPriimekDel').addClass('forme');
			$('#imeInPriimekDel').show();$('#button1').toggle();$('#button2').toggle();$('#vpisnaDel').hide();
			$('#napakaDiv').html('');$('#ajaxIzpis').html('');" 
			id="button1">Iskanje z imenom in priimkom</a>	
		
		<a onclick="$('#vpisnaDel').addClass('forme');$('#imeInPriimekDel').removeClass('forme');$('#vpisnaDel').show();
			$('#button2').toggle();$('#button1').toggle();$('#imeInPriimekDel').hide();
			$('#napakaDiv').html('');$('#ajaxIzpis').html('');"
			id="button2">Iskanje z vpisno številko</a>	
	</div>
<br />	
<hr />	
<form style="width: 750px; margin-left: 170px;" id="imeInPriimekDel">
	<table>	
		<tr><td colspan="4">Iskanje z imenom in priimkom<input type="hidden" name="nacinIskanja" value="imeInPriimek" /></td></tr>
		<tr>
			<td></td>
			<th>Ime:</th>
			<td><input type="text" name="imeS" id="imeS" maxlength="10" /></td>
			<td class="error" style="color: red"></td>
		</tr>
		<tr>
			<td>  </td>
			<th>Priimek:</th>
			<td><input type="text" name="priimekS" id="priimekS" maxlength="10" /></td>
			<td class="error" style="color: red"></td>
		</tr>
		<tr>
			<td colspan="3" class="gumbi"><div><input id="gumbImePriimek" type="submit" onclick="posljiAjax();return false;" value="Poizvedi" /></div></td>
			<td style="backgorund-color: red"></td>
		</tr>
	</table>
</form>		
	
	
<form action='IskanjeStudentaServlet' method='post' style="width: 750px; margin-left: 170px;" id="vpisnaDel">	
	<table>		
		<tr><td colspan="4">Iskanje z vpisno številko<input type="hidden" name="nacinIskanja" value="vpisna" /></td></tr>
		<tr>
			<td></td>
			<th>Vpisna številka:</th>
			<td><input type="text" name="student" maxlength="8" /></td>
			<td class="error" style="color: red"></td>
		</tr>
		<tr>
			<td colspan="3" class="gumbi"><div><input type='submit' value='POIZVEDI' /></div></td>
			<td style="backgorund-color: red"></td>
		</tr>
	</table>
</form>

<br />
<div id="ajaxIzpis" style='text-align:left;margin-left: 300px;font-family: Oswald, sans-serif;'>
	
</div>

<script type="text/javascript">	
	$('.error').css('width', '200px'); /* kakšen del širine forme nej zavzamejo errorji */	
</script>