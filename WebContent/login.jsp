<%@page import="com.sun.corba.se.impl.orbutil.closure.Constant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="si.fri.estudent.utilities.*" %>
<%
	String status = "";

	if(request.getAttribute("status") != null)
		status = request.getAttribute("status").toString();
	
	if(status.equals(Constants.STATUS_SUCCESS)){
		
	}
%>


<form action="LoginManagerServlet" method="post" class="forme" style="width: 330px; margin-left: 290px;">
	<table>
		<tr>
			<th>Uporabniško ime</th>
			<td><input type="text" name="username" /></td>
		</tr>
		<tr>
			<th>Geslo</th>
			<td><input type="password" name="password" /></td>
		</tr>
		<tr>
			<td colspan="2" class="gumbi"><div><input type='submit' value='PRIJAVA' /></div></td>
		</tr>
		<tr>
			<td colspan="2"><%= status %></td>
		</tr>
	</table>
</form>
<script type="text/javascript">
	$('.forme th').css('width', '105px'); /* kakšen del širine forme nej zavzamejo <th> */
</script>