$(document).ready(function(){
	// Pri vseh ajax metodah .ajax, .get, .post, .load so podatki opcijski
	
	// ajax z JSON na servlet
    $('#gumb').click(function() {
    	// $.post('ime servleta', 'podatki', function(objekt odgovor) {koda, ki se izvede, ko dobimo rezultat} => pošiljanje ajax zahteve
    	$.post('VpisniListAjaxServlet', podatki = {ime: "Janez", priimek: "Novak", predmeti: ["Predmet1", "Predmet2", "Predmet3"]}, function(responseData) {
        	var json = jQuery.parseJSON(responseData); // Metoda parsa responseData v json objekt
        	
        	$('#odg').html(json.ime + " " + json.priimek + "<br />"); // Vrednosti normalnih atributov dobimo kot json.key (key, ki smo ga določili v klicanem servletu)
        	
        	$('#odg').append("Odgovor pridobljen z each: ");
        	$.each(json.predmeti, function(index, value) { // Atribute z več podatki lahko dobimo ven z zanko each...
        		$('#odg').append(value + "; ");
        	});
        	
        	$('#odg').append("<br />Odgovor pridobljen s for: ");
        	for(var i = 0; i < json.predmeti.length; i++) { // ... ali pa old school z zanko
        		$('#odg').append(json.predmeti[i] + "; ");
        	}
        });
    });
    
    // ajax na JSP
    $('#gumb2').click(function() {
    	$('#ddl').load('Ajax/test.jsp');
    });
    
    // MOJA KODA, KI SE NE TIČE ZGORNJEGA TEMPLATA
    
    $('#nov').click(function() {
    	$.post('VpisniListAjaxServlet', {funkcija: "nov"}, function(responseData) {
        	var json = jQuery.parseJSON(responseData);
        	$('input[name=vpisnaStevilka]').val(json.vpisnaStevilka);
        	$('input[name=priimek]').val("");
        	$('input[name=dekliskiPriimek]').val("");
        	$('input[name=ime]').val("");
        	$('input[name=emso]').val("");
        	$('input[name=davcnaStevilka]').val("");
        	
        	$.each($('input[name=posebnePotrebe]'), function(index, value) {
        		$(this).prop("checked", false);
        	});
        	
        	$('input[name=ulicaS]').val("");
        	$('input[name=postaS]').val("");
        	$('input[name=idPostaS]').val("");
        	$('input[name=postnaStevilkaS]').val("");
        	$('input[name=obcinaS]').val("");
        	$('input[name=idObcinaS]').val("");
        	$('input[name=telefon]').val("");
        	$('input[name=prenosni]').val("");
        	$('input[name=email]').val("");
    	
        	$('input[name=ulicaZ]').val("");
        	$('input[name=postaZ]').val("");
        	$('input[name=idPostaZ]').val("");
        	$('input[name=postnaStevilkaZ]').val("");
        	$('input[name=obcinaZ]').val("");
        	$('input[name=idObcinaZ]').val("");
        });
    });
    
    $('#obstojeci').click(function() {
    	$.post('VpisniListAjaxServlet', {funkcija: "obstojeci", vpisnaStevilka: $('input[name=vpisnaStevilka]').val()}, function(responseData) {
        	var json = jQuery.parseJSON(responseData);
        	
        	$('input[name=priimek]').val(json.priimek);
        	$('input[name=dekliskiPriimek]').val(json.dekliskiPriimek);
        	$('input[name=ime]').val(json.ime);
        	$('input[name=emso]').val(json.emso);
        	$('input[name=davcnaStevilka]').val(json.davcnaStevilka);
        	$('input[name=davcnaStevilka]');
        	
        	$.each(json.posebnePotrebe, function(index, value) {
        		$('#' + value).prop("checked", true);
        	});
        	
        	$('input[name=ulicaS]').val(json.ulicaS);
        	$('input[name=postaS]').val(json.postaS);
        	$('input[name=idPostaS]').val(json.idPostaS);
        	$('input[name=postnaStevilkaS]').val(json.postnaStevilkaS);
        	$('input[name=obcinaS]').val(json.obcinaS);
        	$('input[name=idObcinaS]').val(json.idObcinaS);
        	$('input[name=drzava]').val(json.drzava);
        	$('input[name=idDrzava]').val(json.idDrzava);
        	$('input[name=telefon]').val(json.telefon);
        	$('input[name=prenosni]').val(json.prenosni);
        	$('input[name=email]').val(json.email);
        	
	        if(json.niZacasnegaNaslova !=  "true"){
	        	$('input[name=ulicaZ]').val(json.ulicaZ);
	        	$('input[name=postaZ]').val(json.postaZ);
	        	$('input[name=idPostaZ]').val(json.idPostaZ);
	        	$('input[name=postnaStevilkaZ]').val(json.postnaStevilkaZ);
	        	$('input[name=obcinaZ]').val(json.obcinaZ);
	        	$('input[name=idObcinaZ]').val(json.idObcinaZ);
        	}
        });
    });
});