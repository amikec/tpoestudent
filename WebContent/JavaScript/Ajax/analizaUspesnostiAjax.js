$(document).ready(function(){
    	
    $('select[name=studijskoLeto]').change(function() {
    	
    	var studijskoLeto = $('select[name=studijskoLeto]').find(":selected").val();
    	
    	$.post('AnalizaUspesnostiAjaxServlet', podatki = {studLeto: studijskoLeto}, function(responseData) {
    		
        	var json = jQuery.parseJSON(responseData); // Metoda parsa responseData v json objekt

    		//$('#izpisRokov').text("");
    		//$('#prikazRokov').text("");
        	$('#predmet').html("");
        	$('#izpisAnalizeUspesnosti').html("");
        	
            $('#predmet').append("<option value='0'></option>");
        	
        	for(var i = 0; i < json.predmeti.length; i++) { 
        		$('#predmet').append("<option value='" + json.idPredmeti[i] + "'>" + json.predmeti[i] + " (" + json.sifrePredmetov[i] +  ")  " +   "</option>");
        	}
        });
    });
 $('select[name=predmet]').change(function() {
    	
    	var predmet = $('select[name=predmet]').find(":selected").val();
    	var studijskoLeto = $('input[name=studijskoLeto]').find(":selected").val();
    	var od = $('input[name=od]').val();
    	var dox = $('input[name=dox]').val();

    	$.post('AnalizaUspesnostiAjaxServlet', podatki = {studLeto: studijskoLeto, predmet: predmet, od: od, dox: dox }, function(responseData) {
        	
    		$('#izpisAnalizeUspesnosti').html(responseData);
        	
        });
    });
});