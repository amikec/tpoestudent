$(document).ready(function() {
	$('input[name=profesor]').change(function() { // Ko izbrišemo ime profesorja in polja PROFESOR (Ajax se izvede na autocomletu)
		var pr = $('input[name=profesor]').val();
		
    	if (pr == '' || pr.search(/^[A-ZČĆŽŠĐ][a-zčćžšđ]{1,44}[\ ][A-ZČĆŽŠĐ][a-zčćžšđ]{1,44}[\ ]?[A-Z]?[a-zČĆŽŠĐčćžšđ]{0,44}$/) != 0) {
    		$('input[name=idProfesor]').val('0');
    		$('select[name=vrstaStudijaKand]').html('');
    		$('select[name=studijskiProgramKand]').html('');
        	$('select[name=studijskaSmerKand]').html('');
        	$("select[name=studijskoLetoKand]").val('0');
        	$('select[name=studijskoLetoKand]').attr("disabled", true);
        	$('select[name=letnikKand]').html('');
        	$('select[name=predmetKand]').html('');
        	$('select[name=izpitniRokKand]').html('');
        	$('#seznamPrijavljenih').html('');
        	$('.gumbi').hide();
    	}
    });
	
	$('select[name=vrstaStudijaKand]').change(function() { // Ko izberemo VRSTO ŠTUDIJA
		var idPr = $('input[name=idProfesor]').val();
    	var vs = $('select[name=vrstaStudijaKand]').find(":selected").val();
    	
    	if (vs != 0) {
	    	$.post('PregledPrijavljenihKandidatovAjaxServlet', {idProfesor: idPr, vrstaStudija: vs}, function(responseData) {
	        	$('select[name=studijskiProgramKand]').html(responseData);
	        });
    	} else {
    		$('select[name=studijskiProgramKand]').html('');
    	}
    	
    	$('select[name=studijskaSmerKand]').html('');
    	$("select[name=studijskoLetoKand]").val('0');
    	$('select[name=studijskoLetoKand]').attr("disabled", true);
    	$('select[name=letnikKand]').html('');
    	$('select[name=predmetKand]').html('');
    	$('select[name=izpitniRokKand]').html('');
    	$('#seznamPrijavljenih').html('');
    	$('.gumbi').hide();
    });
	
	$('select[name=studijskiProgramKand]').change(function() { // Ko izberemo ŠTUDIJSKI PROGRAM
		var idPr = $('input[name=idProfesor]').val();
    	var vs = $('select[name=vrstaStudijaKand]').find(":selected").val();
    	var sp = $('select[name=studijskiProgramKand]').find(":selected").val();
    	
    	if (sp != 0) {
	    	$.post('PregledPrijavljenihKandidatovAjaxServlet', {idProfesor: idPr, vrstaStudija: vs, studijskiProgram: sp}, function(responseData) {
	    		$('select[name=studijskaSmerKand]').html(responseData);
	        });
    	} else {
    		$('select[name=studijskaSmerKand]').html('');
    	}
    	
    	$("select[name=studijskoLetoKand]").val('0');
    	$('select[name=studijskoLetoKand]').attr("disabled", true);
    	$('select[name=letnikKand]').html('');
    	$('select[name=predmetKand]').html('');
    	$('select[name=izpitniRokKand]').html('');
    	$('#seznamPrijavljenih').html('');
    	$('.gumbi').hide();
    });
	
	$('select[name=studijskaSmerKand]').change(function() { // Ko izberemo ŠTUDIJSKA SMER
    	var ss = $('select[name=studijskaSmerKand]').find(":selected").val();

    	if (ss != 0) {
        	$('select[name=studijskoLetoKand]').attr("disabled", false);
    	} else {
        	$('select[name=studijskoLetoKand]').attr("disabled", true);
    	}
    	
    	$("select[name=studijskoLetoKand]").val('0');
    	$('select[name=letnikKand]').html('');
    	$('select[name=predmetKand]').html('');
    	$('select[name=izpitniRokKand]').html('');
    	$('#seznamPrijavljenih').html('');
    	$('.gumbi').hide();
    });
	
	$('select[name=studijskoLetoKand]').change(function() { // Ko izberemo ŠTUDIJSKO LETO
		var idPr = $('input[name=idProfesor]').val();
    	var vs = $('select[name=vrstaStudijaKand]').find(":selected").val();
    	var sp = $('select[name=studijskiProgramKand]').find(":selected").val();
    	var ss = $('select[name=studijskaSmerKand]').find(":selected").val();
    	var sl = $('select[name=studijskoLetoKand]').find(":selected").val();
    	
    	if (sl != 0) {
	    	$.post('PregledPrijavljenihKandidatovAjaxServlet', {idProfesor: idPr, vrstaStudija: vs, studijskiProgram: sp, studijskaSmer: ss, studijskoLeto: sl}, function(responseData) {
	    		$('select[name=letnikKand]').html(responseData);
	        });
    	} else {
    		$('select[name=letnikKand]').html('');
    	}
    	
    	$('select[name=predmetKand]').html('');
    	$('select[name=izpitniRokKand]').html('');
    	$('#seznamPrijavljenih').html('');
    	$('.gumbi').hide();
    });
	
	$('select[name=letnikKand]').change(function() { // Ko izberemo LETNIK
		var idPr = $('input[name=idProfesor]').val();
    	var l = $('select[name=letnikKand]').find(":selected").val();
    	var koncneOcene = $('input[name=konc]').val();
    	    	
    	if (l != 0) {
	    	$.post('PregledPrijavljenihKandidatovAjaxServlet', {idProfesor: idPr, letnik: l}, function(responseData) {
	    		$('select[name=predmetKand]').html(responseData);
	        });
    	} else {
    		$('select[name=predmetKand]').html('');
    	}
    	
    	$('select[name=izpitniRokKand]').html('');
    	$('#seznamPrijavljenih').html('');
    	$('.gumbi').hide();
    });
	
	$('select[name=predmetKand]').change(function() { // Ko izberemo PREDMET
		var idPr = $('input[name=idProfesor]').val();
    	var p = $('select[name=predmetKand]').find(":selected").val();
    	var koncneOcene = $('input[name=konc]').val();
    	    	
    	if (p != 0) {
	    	$.post('PregledPrijavljenihKandidatovAjaxServlet', {idProfesor: idPr, predmet: p}, function(responseData) {
	    		if(koncneOcene == "true")
	    			$('select[name=izpitniRokKoncne]').html(responseData);
	    		else
	    			$('select[name=izpitniRokKand]').html(responseData);
	        });
    	} else {
    		if(koncneOcene == "true")
    			$('select[name=izpitniRokKoncne]').html('');
    		else
    			$('select[name=izpitniRokKand]').html('');
    	}
    	
    	$('#seznamPrijavljenih').html('');
    	$('.gumbi').hide();
    });
	
	$('select[name=izpitniRokKand]').change(function() { // Ko izberemo IZPITNI ROK
    	var ir = $('select[name=izpitniRokKand]').find(":selected").val();
    	var izpis = $('#izpis').val();
    	var izpis2 = $('#izpisOcena').val();
    	var l = $('select[name=letnikKand]').find(":selected").val();
    	
    	if (ir != 0) {
	    	$.post('PregledPrijavljenihKandidatovAjaxServlet', {izpisOcena:izpis2, letnik:l, izpitniRok: ir, izpis: izpis}, function(responseData) {
	    		$('#seznamPrijavljenih').html(responseData);
	    		
	    		if (responseData.search(/seznamPrijavljenih/) != -1) {
	    			$('.gumbi').show();
	    		} else {
	    			$('.gumbi').hide();
	    		}
	        });
    	} else {
    		$('#seznamPrijavljenih').html('');
    		$('.gumbi').hide();
    	}
    	
    	$('#tiskanje').attr("href", "PregledPrijavljenihKandidatovAjaxServlet?letnik="+l+"&izpis="+ izpis +"&izpitniRok=" + ir);
    });
	
	$('#posodobiOcene').click(function() { // Ko izberemo POSODOBI OCENE
		var ir = $('select[name=izpitniRokKand]').find(":selected").val();
		var l = $('select[name=letnikKand]').find(":selected").val();
		var vpisne = new Array();
		var oceneStudenti = new Array();
		var bonusTocke = new Array();
		
		$('input[name=ocenaBon]').each(function(index, value) {
			bonusTocke.push($(this).val());
		});
		
		$('td[name=vpisnaStevilka]').each(function(index, value) {
			vpisne.push($(this).html());
    	});
		
		$('input[name=ocena]').each(function(index, value) {
			oceneStudenti.push($(this).val());
    	});
		
		$.post('PregledPrijavljenihKandidatovAjaxServlet', {letnik:l, izpitniRok: ir, vpisneStevilke: vpisne, ocene: oceneStudenti, bonusTocke:bonusTocke}, function(responseData) {
			$('#seznamPrijavljenih').html(responseData);
		});
    });
});