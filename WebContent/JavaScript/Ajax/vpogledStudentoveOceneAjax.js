function isNumber (o) {
	return ! isNaN (o-0);
}

$(document).ready(function() {
    $('select[name=vrstaStudijaStOcene]').change(function() { // Ko izberemo VRSTO ŠTUDIJA
    	var is = $('input[name=idStudent]').val();
    	var vs = $('select[name=vrstaStudijaStOcene]').find(":selected").val();
    	
    	if (vs != 0) {
	    	$.post('VpogledStudentoveOceneAjaxServlet', {idStudent: is, vrstaStudija: vs}, function(responseData) {
	        	$('select[name=studijskiProgramStOcene]').html(responseData);
	        });
    	} else {
    		$('select[name=studijskiProgramStOcene]').html('');
    	}
    	
    	$('select[name=studijskaSmerStOcene]').html('');
    	$('select[name=predmetStOcene]').html('');
    	$('#showHideStOcene').hide();
    	$('#natisniStudentoveOcene').hide();
    });
    
    $('select[name=studijskiProgramStOcene]').change(function() { // Ko izberemo ŠTUDIJSKI PROGRAM
    	var is = $('input[name=idStudent]').val();
    	var vs = $('select[name=vrstaStudijaStOcene]').find(":selected").val();
    	var sp = $('select[name=studijskiProgramStOcene]').find(":selected").val();
    	
    	if (sp != 0) {
	    	$.post('VpogledStudentoveOceneAjaxServlet', {idStudent: is, vrstaStudija: vs, studijskiProgram: sp}, function(responseData) {
	    		$('select[name=studijskaSmerStOcene]').html(responseData);
	        });
    	} else {
    		$('select[name=studijskaSmerStOcene]').html('');
    	}
    	
    	$('select[name=predmetStOcene]').html('');
    	$('#showHideStOcene').hide();
    	$('#natisniStudentoveOcene').hide();
    });
    
    $('select[name=studijskaSmerStOcene]').change(function() { // Ko izberemo ŠTUDIJSKO SMER
    	var is = $('input[name=idStudent]').val();
    	var vs = $('select[name=vrstaStudijaStOcene]').find(":selected").val();
    	var sp = $('select[name=studijskiProgramStOcene]').find(":selected").val();
    	var ss = $('select[name=studijskaSmerStOcene]').find(":selected").val();
    	
    	if (ss != 0) {
	    	$.post('VpogledStudentoveOceneAjaxServlet', {idStudent: is, vrstaStudija: vs, studijskiProgram: sp, studijskaSmer: ss}, function(responseData) {
	    		$('select[name=predmetStOcene]').html(responseData);
	        });
    	} else {
    		$('select[name=predmetStOcene]').html('');
    	}
    	
    	$('#showHideStOcene').hide();
    	$('#natisniStudentoveOcene').hide();
    });
    
    $('select[name=predmetStOcene]').change(function() {// Ko izberemo PREDMET
    	var is = $('input[name=idStudent]').val();
    	var vs = $('select[name=vrstaStudijaStOcene]').find(":selected").val();
    	var sp = $('select[name=studijskiProgramStOcene]').find(":selected").val();
    	var ss = $('select[name=studijskaSmerStOcene]').find(":selected").val();
    	var p = $('select[name=predmetStOcene]').find(":selected").val();
    	
    	if (p != 0) {
    		$.post('VpogledStudentoveOceneAjaxServlet', {idStudent: is, vrstaStudija: vs, studijskiProgram: sp, studijskaSmer: ss, predmet: p}, function(responseData) {
	    		$('#showHideStOcene').html(responseData);
	    		$('#showHideStOcene').show();
	    		
	    		if (responseData.search(/studentovaOcene/) != -1) { // Če dobimo zadetke, prikažemo tudi gumb za tiskanje
	    			$('#natisniStudentoveOcene').hide();
		    		$('#natisniStudentoveOcene div a').attr("href", "VpogledStudentoveOceneAjaxServlet?idStudent=" + is + "&vrstaStudija=" + vs + "&studijskiProgram=" + sp + "&studijskaSmer=" + ss + "&predmet=" + p);
	    		} else {
	    			$('#natisniStudentoveOcene').hide();
	    		}
	        });
    	} else {
    		$('#showHideStOcene').hide();
    	}
    });
});