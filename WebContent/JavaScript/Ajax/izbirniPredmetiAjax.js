$(document).ready(function(){
	
    $('select[name=vrstaStudijaIzbirniP]').change(function() { // Ko izberemo VRSTO ŠTUDIJA
    	var is = $('input[name=idStudent]').val();
    	var vs = $('select[name=vrstaStudijaIzbirniP]').find(":selected").val();
    	
    	if (vs != 0) {
	    	$.post('IzbirniPredmetiAjaxServlet', {idStudent: is, vrstaStudija: vs}, function(responseData) {
	        	$('select[name=studijskiProgramIzbirniP]').html(responseData);
	        });
    	} else {
    		$('select[name=studijskiProgramIzbirniP]').html('');
    	}
    	
    	$('select[name=studijskaSmerIzbirniP]').html('');
    	$('select[name=letnik]').html('');
    	$("select[name=izbirniPredmetnikModul]").val('0');
    	$('select[name=izbirniPredmetnikModul]').attr("disabled", true);
    	$('.showHideDragDrop').hide();
    });
    
    $('select[name=studijskiProgramIzbirniP]').change(function() { // Ko izberemo ŠTUDIJSKI PROGRAM
    	var is = $('input[name=idStudent]').val();
    	var vs = $('select[name=vrstaStudijaIzbirniP]').find(":selected").val();
    	var sp = $('select[name=studijskiProgramIzbirniP]').find(":selected").val();
    	
    	if (sp != 0) {
	    	$.post('IzbirniPredmetiAjaxServlet', {idStudent: is, vrstaStudija: vs, studijskiProgram: sp}, function(responseData) {
	    		$('select[name=studijskaSmerIzbirniP]').html(responseData);
	        });
    	} else {
    		$('select[name=studijskaSmerIzbirniP]').html('');
    	}
    	
    	$('select[name=letnikIzbirniP]').html('');
    	$("select[name=izbirniPredmetnikModul]").val('0');
    	$('select[name=izbirniPredmetnikModul]').attr("disabled", true);
		$('.showHideDragDrop').hide();
    });
    
    $('select[name=studijskaSmerIzbirniP]').change(function() { // Ko izberemo ŠTUDIJSKO SMER
    	var is = $('input[name=idStudent]').val();
    	var vs = $('select[name=vrstaStudijaIzbirniP]').find(":selected").val();
    	var sp = $('select[name=studijskiProgramIzbirniP]').find(":selected").val();
    	var ss = $('select[name=studijskaSmerIzbirniP]').find(":selected").val();
    	
    	if (ss != 0) {
	    	$.post('IzbirniPredmetiAjaxServlet', {idStudent: is, vrstaStudija: vs, studijskiProgram: sp, studijskaSmer: ss}, function(responseData) {
	    		$('select[name=letnikIzbirniP]').html(responseData);
	        });
    	} else {
    		$('select[name=letnikIzbirniP]').html('');
    	}
    	
    	$("select[name=izbirniPredmetnikModul]").val('0');
    	$('select[name=izbirniPredmetnikModul]').attr("disabled", true);
    	$('.showHideDragDrop').hide();
    });
    
    $('select[name=letnikIzbirniP]').change(function() { // Ko izberemo LETNIK
    	var l = $('select[name=letnikIzbirniP]').find(":selected").val();

    	if (l != 0) {
        	$('select[name=izbirniPredmetnikModul]').attr("disabled", false);
    	} else {
        	$('select[name=izbirniPredmetnikModul]').attr("disabled", true);
    	}
    	
    	$("select[name=izbirniPredmetnikModul]").val('0');
    	$('.showHideDragDrop').hide();
    });
    
    $('select[name=izbirniPredmetnikModul]').change(function() { // Ko izberemo IZBIRNI PREDMETNIK / MODUL
    	var is = $('input[name=idStudent]').val();
    	var l = $('select[name=letnikIzbirniP]').find(":selected").val();
    	var pm = $('select[name=izbirniPredmetnikModul]').find(":selected").val();

    	if (pm != 0) {
	    	$.post('IzbirniPredmetiAjaxServlet', {idStudent: is, letnik: l, izbirniPredmetnikModul: pm}, function(responseData) {
	    		$('.dragDrop').html(responseData);
	        });
	    	
	    	$('.showHideDragDrop').show();
    	} else {
    		$('.showHideDragDrop').hide();
    	}
    });
    
    $('#idIzbirniPredmeti').submit(function() { // Pred submitom preverimo, da nismo izbrali več kot 2 modula
    	if ($('input[name=izbirniPredmetnikModulDragDrop]').val() == 'modul') {
			var izbraniModuli = $('input[name=izbire]').val();
			
			if (izbraniModuli.split(',').length > 2) {
				$('.success').html("<span class='error'>Največje število modulov je 2</span>");
				return false;
			}
		}
    	
    	return true;
    });
});