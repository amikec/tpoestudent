$(document).ready(function(){
	
    $('select[name=vrstaStudija]').change(function() { // Ko izberemo VRSTO ŠTUDIJA
    	var vs = $('select[name=vrstaStudija]').find(":selected").val();
    	
    	if (vs != 0) {
	    	$.post('VzdrzevanjePredmetnikaAjaxServlet', {vrstaStudija: vs}, function(responseData) {
	        	$('select[name=studijskiProgram]').html(responseData);
	        });
    	} else {
    		$('select[name=studijskiProgram]').html('');
    	}
    	
    	$('select[name=studijskaSmer]').html('');
    	$("select[name=studijskoLeto]").val('0');
    	$('select[name=studijskoLeto]').attr("disabled", true);
    	$('select[name=letnik]').html('');
    	$("select[name=predmetnikModul]").val('0');
    	$('select[name=predmetnikModul]').attr("disabled", true);
    	$('#predmetnikVrstica').hide();
		$('#modulVrstica').hide();
    	$('.showHidePredmetnikDragDrop').hide();
    });
    
    $('select[name=studijskiProgram]').change(function() { // Ko izberemo ŠTUDIJSKI PROGRAM
    	var vs = $('select[name=vrstaStudija]').find(":selected").val();
    	var sp = $('select[name=studijskiProgram]').find(":selected").val();
    	
    	if (sp != 0) {
	    	$.post('VzdrzevanjePredmetnikaAjaxServlet', {vrstaStudija: vs, studijskiProgram: sp}, function(responseData) {
	    		$('select[name=studijskaSmer]').html(responseData);
	        });
    	} else {
    		$('select[name=studijskaSmer]').html('');
    	}
    	
    	$("select[name=studijskoLeto]").val('0');
    	$('select[name=studijskoLeto]').attr("disabled", true);
    	$('select[name=letnik]').html('');
    	$("select[name=predmetnikModul]").val('0');
    	$('select[name=predmetnikModul]').attr("disabled", true);
    	$('#predmetnikVrstica').hide();
		$('#modulVrstica').hide();
    	$('.showHidePredmetnikDragDrop').hide();
    });
    
    $('select[name=studijskaSmer]').change(function() { // Ko izberemo ŠTUDIJSKA SMER
    	var ss = $('select[name=studijskaSmer]').find(":selected").val();

    	if (ss != 0) {
        	$('select[name=studijskoLeto]').attr("disabled", false);
    	} else {
        	$('select[name=studijskoLeto]').attr("disabled", true);
    	}
    	
    	$('select[name=letnik]').html('');
    	$("select[name=studijskoLeto]").val('0');
    	$("select[name=predmetnikModul]").val('0');
    	$('select[name=predmetnikModul]').attr("disabled", true);
    	$('#predmetnikVrstica').hide();
		$('#modulVrstica').hide();
    	$('.showHidePredmetnikDragDrop').hide();
    });
    
    $('select[name=studijskoLeto]').change(function() { // Ko izberemo ŠTUDIJSKO LETO
    	var vs = $('select[name=vrstaStudija]').find(":selected").val();
    	var sp = $('select[name=studijskiProgram]').find(":selected").val();
    	var ss = $('select[name=studijskaSmer]').find(":selected").val();
    	var sl = $('select[name=studijskoLeto]').find(":selected").val();
    	
    	if (sl != 0) {
	    	$.post('VzdrzevanjePredmetnikaAjaxServlet', {vrstaStudija: vs, studijskiProgram: sp, studijskaSmer: ss, studijskoLeto: sl}, function(responseData) {
	    		$('select[name=letnik]').html(responseData);
	        });
    	} else {
    		$('select[name=letnik]').html('');
    	}
    	
    	$("select[name=predmetnikModul]").val('0');
    	$('select[name=predmetnikModul]').attr("disabled", true);
    	$('#predmetnikVrstica').hide();
		$('#modulVrstica').hide();
    	$('.showHidePredmetnikDragDrop').hide();
    });
    
    $('select[name=letnik]').change(function() { // Ko izberemo LETNIK
    	var l = $('#letnik').find(":selected").val();

    	if (l != 0) {
        	$('select[name=predmetnikModul]').attr("disabled", false);
    	} else {
        	$('select[name=predmetnikModul]').attr("disabled", true);
    	}
    	
    	$("select[name=predmetnikModul]").val('0');
    	$('#predmetnikVrstica').hide();
		$('#modulVrstica').hide();
    	$('.showHidePredmetnikDragDrop').hide();
    });
    
    $('select[name=predmetnikModul]').change(function() { // Ko izberemo PREDMETNIK / MODUL
    	var pm = $('select[name=predmetnikModul]').find(":selected").val();

    	$("select[name=vrstaPredmetnika]").val('0');
    	$('select[name=modul]').html('');
    	if (pm == 0) {
    		$('#predmetnikVrstica').hide();
    		$('#modulVrstica').hide();
    	} else if (pm == "predmetnik") {
    		$('#predmetnikVrstica').show();
    		$('#modulVrstica').hide();
    	} else if (pm == "modul") {
    		$.post('VzdrzevanjePredmetnikaAjaxServlet', {predmetnikModul: pm}, function(responseData) {
    			$('select[name=modul]').html(responseData);
    		});
    		
    		$('#modulVrstica').show();
    		$('#predmetnikVrstica').hide();
    	}
    	
    	$('.showHidePredmetnikDragDrop').hide();
    });
    
    $('select[name=vrstaPredmetnika]').change(function() { // Ko izberemo VRSTA PREDMETNIKA
    	var l = $('select[name=letnik]').find(":selected").val();
    	var pm = $('select[name=predmetnikModul]').find(":selected").val();
    	var vp = $('select[name=vrstaPredmetnika]').find(":selected").val();

    	if (vp != 0) {
	    	$.post('VzdrzevanjePredmetnikaAjaxServlet', {letnik: l, predmetnikModul: pm, vrstaPredmetnika: vp}, function(responseData) {
	    		$('.dragDrop').html(responseData);
	        });
	    	
	    	$('.showHidePredmetnikDragDrop').show();
    	} else {
    		$('.showHidePredmetnikDragDrop').hide();
    	}
    });
    
    $('select[name=modul]').change(function() {
    	var l = $('select[name=letnik]').find(":selected").val();
    	var pm = $('select[name=predmetnikModul]').find(":selected").val();
    	var m = $('select[name=modul]').find(":selected").val();
    	
    	if (m != 0) {
	    	$.post('VzdrzevanjePredmetnikaAjaxServlet', {letnik: l, predmetnikModul: pm, modul: m}, function(responseData) {
	    		$('.dragDrop').html(responseData);
	        });
	    	
	    	$('.showHidePredmetnikDragDrop').show();
    	} else {
    		$('.showHidePredmetnikDragDrop').hide();
    	}
    });
});