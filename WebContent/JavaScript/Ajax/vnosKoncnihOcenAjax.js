$(document).ready(function(){	

	$('select[name=izpitniRokKoncne]').change(function() {
				
		var idPr = $('input[name=idProfesor]').val();
    	var p = $('select[name=predmetKand]').find(":selected").val();
    	var l = $('select[name=letnikKand]').find(":selected").val();
    	var idRok = $('select[name=izpitniRokKoncne]').find(":selected").val();
    	var izpis = $('input[name=izpis]').val();
    	
    	
    	
    	if (p != 0) {
	    	$.post('VnosKoncnihOcenAjaxServlet', {izpis:izpis, izpitniRok:idRok, profesor: idPr, predmet: p, letnik:l }, function(responseData) {
	    		$('#postavkeStudentov').html(responseData);
	    		
	    		if (responseData.search(/koncneOcene/) != -1) {
	    			$('.gumbi').show();
	    			
	    		} else {
	    			$('.gumbi').hide();
	    		}
	        });
	    		    	
    	} else {
    		$('#postavkeStudentov').html('');
    		$('.gumbi').hide();
    	}
    	
    	$('#tiskanje').attr("href", "VnosKoncnihOcenAjaxServlet?izpitniRok=" + idRok + "&izpis="+ izpis +"&predmet=" + p + "&letnik=" + l + "&tiskanje=true" + "&profesor=" + idPr);
    });
	
	
	$('#posodobiKoncneOcene').click(function() { // Ko izberemo POSODOBI OCENE
		
		var vpisne = new Array();
		var oceneKoncne = new Array();
		var oceneVaje = new Array();
		var oceneIzpit = new Array();
		var ocene = new Array();
		var idRok = $('select[name=izpitniRokKoncne]').find(":selected").val();
		var nacinOcen = $('input[name=nacinOcen]').val();
		var idPr = $('input[name=idProfesor]').val();
    	var p = $('select[name=predmetKand]').find(":selected").val();
    	var l = $('select[name=letnikKand]').find(":selected").val();
		
		if(nacinOcen == "K"){
			$('input[name=oceneKoncne]').each(function(index, value) {
				oceneKoncne.push($(this).val());
			});
		}
		
		if(nacinOcen == "I" || nacinOcen == "IV"){
			$('input[name=oceneIzpit]').each(function(index, value) {
				oceneIzpit.push($(this).val());
			});
		}
		
		if(nacinOcen == "IV" || nacinOcen == "V"){
			$('input[name=oceneVaje]').each(function(index, value) {
				oceneVaje.push($(this).val());
    		});
		}
		
		$('td[name=vpisnaStevilka]').each(function(index, value) {
			vpisne.push($(this).html());
    	});

		$('input[name=ocena]').each(function(index, value) {
			ocene.push($(this).val());
    	});
		
		$.post('VnosKoncnihOcenAjaxServlet', {izpitniRok:idRok, profesor: idPr, predmet: p, letnik:l, ocene:ocene, vpisne:vpisne, nacinOcen:nacinOcen, oceneIzpit:oceneIzpit, oceneVaje:oceneVaje, oceneKoncne:oceneKoncne}, function(responseData) {
			$('#postavkeStudentov').html(responseData);
		});
    });
});
	