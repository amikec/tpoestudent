function includeJavaScript(jsFile) // Includanje javascript fileja
{
	document.write('<script type="text/javascript" src="' + jsFile + '"></script>'); 
}

//------------------ INLCUDI AJAX ------------------

includeJavaScript("JavaScript/Ajax/vnosKoncnihOcenAjax.js");
includeJavaScript("JavaScript/Ajax/vpisniListAjax.js");
includeJavaScript("JavaScript/Ajax/vnosIzpitnegaRokaAjax.js");
includeJavaScript("JavaScript/Ajax/prijavaNaIzpitAjax.js");
includeJavaScript("JavaScript/Ajax/pregledRazpisanihRokovStudentAjax.js");
includeJavaScript("JavaScript/Ajax/vzdrzevanjePredmetnika.js");
includeJavaScript("JavaScript/Ajax/pregledPrijavljenihKandidatovAjax.js");
includeJavaScript("JavaScript/Ajax/izbirniPredmetiAjax.js");
includeJavaScript("JavaScript/Ajax/analizaUspesnostiAjax.js");
includeJavaScript("JavaScript/Ajax/vnosOcenePrijavnicaAjax.js");
includeJavaScript("JavaScript/Ajax/vpogledStudentoveOceneAjax.js");