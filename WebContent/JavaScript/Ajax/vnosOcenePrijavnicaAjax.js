function isNumber (o) {
	return ! isNaN (o-0);
}

$(document).ready(function() {
    $('select[name=vrstaStudijaVnosPrijavnica]').change(function() { // Ko izberemo VRSTO ŠTUDIJA
    	var is = $('input[name=idStudent]').val();
    	var vs = $('select[name=vrstaStudijaVnosPrijavnica]').find(":selected").val();
    	
    	if (vs != 0) {
	    	$.post('VnosOcenePrijavnicaAjaxServlet', {idStudent: is, vrstaStudija: vs}, function(responseData) {
	        	$('select[name=studijskiProgramVnosPrijavnica]').html(responseData);
	        });
    	} else {
    		$('select[name=studijskiProgramVnosPrijavnica]').html('');
    	}
    	
    	$('select[name=studijskaSmerVnosPrijavnica]').html('');
    	$('select[name=letnikVnosPrijavnica]').html('');
    	$('select[name=predmetVnosPrijavnica]').html('');
    	$('#showHidePodatkiIzpita').hide();
    });
    
    $('select[name=studijskiProgramVnosPrijavnica]').change(function() { // Ko izberemo ŠTUDIJSKI PROGRAM
    	var is = $('input[name=idStudent]').val();
    	var vs = $('select[name=vrstaStudijaVnosPrijavnica]').find(":selected").val();
    	var sp = $('select[name=studijskiProgramVnosPrijavnica]').find(":selected").val();
    	
    	if (sp != 0) {
	    	$.post('VnosOcenePrijavnicaAjaxServlet', {idStudent: is, vrstaStudija: vs, studijskiProgram: sp}, function(responseData) {
	    		$('select[name=studijskaSmerVnosPrijavnica]').html(responseData);
	        });
    	} else {
    		$('select[name=studijskaSmerVnosPrijavnica]').html('');
    	}
    	
    	$('select[name=letnikVnosPrijavnica]').html('');
    	$('select[name=predmetVnosPrijavnica]').html('');
    	$('#showHidePodatkiIzpita').hide();
    });
    
    $('select[name=studijskaSmerVnosPrijavnica]').change(function() { // Ko izberemo ŠTUDIJSKO SMER
    	var is = $('input[name=idStudent]').val();
    	var vs = $('select[name=vrstaStudijaVnosPrijavnica]').find(":selected").val();
    	var sp = $('select[name=studijskiProgramVnosPrijavnica]').find(":selected").val();
    	var ss = $('select[name=studijskaSmerVnosPrijavnica]').find(":selected").val();
    	
    	if (ss != 0) {
	    	$.post('VnosOcenePrijavnicaAjaxServlet', {idStudent: is, vrstaStudija: vs, studijskiProgram: sp, studijskaSmer: ss}, function(responseData) {
	    		$('select[name=letnikVnosPrijavnica]').html(responseData);
	        });
    	} else {
    		$('select[name=letnikVnosPrijavnica]').html('');
    	}
    	
    	$('select[name=predmetVnosPrijavnica]').html('');
    	$('#showHidePodatkiIzpita').hide();
    });
    
    $('select[name=letnikVnosPrijavnica]').change(function() { // Ko izberemo LETNIK
    	var is = $('input[name=idStudent]').val();
    	var l = $('select[name=letnikVnosPrijavnica]').find(":selected").val();

    	if (l != 0) {
    		$.post('VnosOcenePrijavnicaAjaxServlet', {idStudent: is, letnik: l}, function(responseData) {
	    		$('select[name=predmetVnosPrijavnica]').html(responseData);
	        });
    	} else {
    		$('select[name=predmetVnosPrijavnica]').html('');
    	}
    	
    	$('#showHidePodatkiIzpita').hide();
    });
    
    $('select[name=predmetVnosPrijavnica]').change(function() {// Ko izberemo PREDMET
    	var p = $('select[name=predmetVnosPrijavnica]').find(":selected").val();
    	var is = $('input[name=idStudent]').val();
    	
    	if (p != 0) {
    		$.post('VnosOcenePrijavnicaAjaxServlet', {idStudent: is, predmet: p}, function(responseData) {
	    		$('#showHidePodatkiIzpita').html(responseData);
	        });
    		$('#showHidePodatkiIzpita').show();
    	} else {
    		$('#showHidePodatkiIzpita').hide();
    	}
    });
    
    $('#idVnosPrijavnica').submit(function() { // Pred submitom validiramo input type text-e
    	var submit = true;
    	var datum = $('input[name=datumVnosPrijavnica]');
    	var cas = $('input[name=casVnosPrijavnica]');
    	var tockeIzpita = $('input[name=tockeIzpitaVnosPrijavnica]');
    	var dodatneTocke = $('input[name=dodatneTockeVnosPrijavnica]');
    	var ocenaIzpit = $('input[name=koncnaOcenaIzpitPrijavnica]');
    	var ocenaVaj = $('input[name=koncnaOcenaVajePrijavnica]');
    	var koncnaOcena = $('input[name=koncnaOcenaPrijavnica]');
    	var steviloPolaganj = $('input[name=stPolaganjaPrijavnica]');
    	
		if (testError(datum, datum.val() != "", 'Neveljaven datum')) {
			submit = false;
		}
		if (ocenaIzpit.length > 0) {
			if (testError(ocenaIzpit, ocenaIzpit.val().search(/^([1-9]{1}|10)$/) == 0, 'Neveljavna ocena izpita')) {
				submit = false;
			}
		}
		if (ocenaVaj.length > 0) {
			if (testError(ocenaVaj, ocenaVaj.val().search(/^([1-9]{1}|10)$/) == 0, 'Neveljavna ocena vaj')) {
				submit = false;
			}
		}
		if (koncnaOcena.length > 0) {
			if (testError(koncnaOcena, koncnaOcena.val().search(/^([1-9]{1}|10)$/) == 0, 'Neveljavna končna ocena')) {
				submit = false;
			}
		}
		if (testError(steviloPolaganj, steviloPolaganj.val().search(/^[0-9]{1,44}$/) == 0 && steviloPolaganj.val() > 0, 'Neveljavena številka polaganja')) {
			submit = false;
		}
		
		if (submit) {
			$('input[name=datumVnosPrijavnica]').removeAttr('disabled');
			$('input[name=stPolaganjaPrijavnica]').removeAttr('disabled');
		}
    	
    	return submit;
    });
});