$(document).ready(function(){
	
	// ajax z JSON na servlet
    $('#pregledRazpisanihRokovStudentAjaxRequest').change(function() {
    	
    	var pod = $('#pregledRazpisanihRokovStudentAjaxRequest').find(":selected").val();

    	$.post('PregledRazpisanihRokovStudentAjaxServlet', podatki = {studijskoLeto: pod}, function(responseData) {
        	var json = jQuery.parseJSON(responseData); // Metoda parsa responseData v json objekt
        	
        	$('#izpisRazpisanihRokov').text("");
        	$('#izpisRazpisanihRokov').append("<form action='PregledRazpisanihRokovAjaxServlet' method='post'>");
        	$('#izpisRazpisanihRokov').append("<table>");
        	if(json.predmeti[0] != null) {  
       		$('#izpisRazpisanihRokov').append("<tr><th><b>" + json.headerPredmet + "</b></th><th><b>" + json.headerDatum  + "</b></th><th><b>" + json.headerUra  + "</b></th><th><b>"   
        				+ json.headerPredavalnica +"</b></th><th><b>" + json.headerIzvajalec  + "</b></th></tr>");
        	}
        	var count = 0;
        	for(var i = 0; i < json.datumi.length; i++) { 
        		for(var j = 0; j < json.stevci[i]; j++) {
		           	$('#izpisRazpisanihRokov').append("<tr><td>" + json.predmeti[i] + "</td><td>" + json.datumi[i] + "</td><td>" + json.ure[i] + "</td><td>" +  json.predavalnice[i] +  "</td><td>" +  json.kombinacijeIzvajalcev[i+count] + "</td></tr>");
		           	$('#izpisRazpisanihRokov').append("<tr><td colspan='6'><hr /></td><td></td></tr>");
		           	count++;
	        		}
        		count = 0;
        	}
        	if(json.predmeti[0] == null) {
        		$('#izpisRazpisanihRokov').append("<tr><td>Ni nobenih razpisanih rokov.</td></tr>");  
        	}
        	$('#izpisRazpisanihRokov').append("</table>");
        	$('#izpisRazpisanihRokov').append("</form>");

        });
    });  
});