$(document).ready(function(){
	
	var cancel = function() {
		$dialog.dialog("close");
	};
	
	var text = "";

    var obdelajPrijavo = function() {
    	
    	$dialog.dialog("close");
    	
    	var idRok = $('#idRok').val();
    	var izvajalecId = $('#izvajalecId').val();
    	var idPredmeta = $('#predmetId').val();
    	var prijava = $('#prijava').val();
    	var pod = $('#prijavaNaIzpitAjaxRequest').find(":selected").val();
    	
    	$.post('PrijavaNaIzpitAjaxServlet', podatki = {idVpisni3:pod, idRoka: idRok, izvajalecId: izvajalecId, idPredmeta:idPredmeta, prijava:prijava}, function(responseData) {
        	var json = jQuery.parseJSON(responseData); 
        	
        	if(json.uspelo == "true")
        		$('#err').text("Akcija uspela.");
        	else
        		$('#err').text("Akcija ni uspela.");
        	
        	$('#btnPrijava').toggle();
        	/*
        	if(json.akcija == "prijava"){
        		$('#btnPrijava').text("Odjavi");
        		$('#prijava').val("false");
        	}else{
        		$('#btnPrijava').text("Prijavi");
        		$('#prijava').val("true");
        	}*/
    	});
    };
    
	var $dialog = $('<div></div>')
	.dialog({
		autoOpen: false,
		title: "Pozor",
		buttons: {
			'Da': obdelajPrijavo,
			'Ne': cancel
		},
		width: 500,
		height: 200
	});


	// ajax z JSON na servlet
    $('#prijavaNaIzpitAjaxRequest').change(function() {
    	
    	var pod = $('#prijavaNaIzpitAjaxRequest').find(":selected").val();
    	$('#idVpisni').val(pod);

    	$.post('PrijavaNaIzpitAjaxServlet', podatki = {idVpisni: pod}, function(responseData) {
        	var json = jQuery.parseJSON(responseData); 
        	
        	$('#izpisRokov').text("");
        	$('#prikazRokov').text("");
        	$('#izpisPredmetov').text("");
        	$('#izpisPredmetov').append("<option value='0'></option>");
        	for(var i = 0; i < json.predmeti.length; i++) { 
        		$('#izpisPredmetov').append("<option value='" + json.pi[i] + "'>" + json.predmeti[i] + " - " +  json.sifre[i] + "(" + json.izvajalci[i] + ")</option>");
        	}
        });
    });
    
    $('#izpisPredmetov').change(function() {
    	
    	var pod = $('#izpisPredmetov').find(":selected").val();
    	var pod2 = $('#prijavaNaIzpitAjaxRequest').find(":selected").val();
    	$.post('PrijavaNaIzpitAjaxServlet', podatki = {pi: pod, idVpisni2: pod2}, function(responseData) {
    		
        	var json = jQuery.parseJSON(responseData); // Metoda parsa responseData v json objekt

    		$('#izpisRokov').text("");
    		$('#prikazRokov').text("");
        	$('#izpisRokov').append("<option value='0'></option>");
        	
        	for(var i = 0; i < json.datumi.length; i++) { 
        		$('#izpisRokov').append("<option value='" + json.idRoki[i] + "'>" + json.datumi[i] + "</option>");
        	}
        });
    });

    
    $('#izpisRokov').change(function() {
    	var pod = $('#izpisRokov').find(":selected").val();
    	var idVpisni = $('#idVpisni').val();
    	
    	$.post('PrijavaNaIzpitAjaxServlet', podatki = {idRoka: pod, idVpisni2:idVpisni}, function(responseData) {
        	var json = jQuery.parseJSON(responseData); 
        	
        	var prijavlja = json.prijavlja;
        	
        	$('#prikazRokov').text("");
        	$('#prikazRokov').append("<tr><th>Predmet: </th><td>" + json.predmet + 			 "</td></tr>");
        	$('#prikazRokov').append("<tr><th colspan='2'>Izvajalec:"+ 						 "</th></tr>");
        	
        	for(var i = 0; i < json.izvajalci.length; i++){
        		$('#prikazRokov').append("<tr><td></td><td>" + json.izvajalci[i] + 			 "</td></tr>");	
        	}
        	
        	$('#prikazRokov').append("<tr><th>Datum: "       + "</th><td>" + json.datum + 				 "</td></tr>");
        	$('#prikazRokov').append("<tr><th>Predavalnica:" + "</th><td>" + json.predavalnica + "</td></tr>");
        	
        	var polaganja = json.polaganjaSkupno;
        	
        	if(json.polaganjaPonavljanje > 0)
        		polaganja += " - " + json.polaganjaPonavljanje;
        		
        	$('#prikazRokov').append("<tr><th>Št. polaganj:" + "</th><td>" + polaganja + "</td></tr>");
        
        	if(json.prijava.length == 0 && (json.napaka2.length < 1 || prijavlja == "referentka")){
        		$('#prikazRokov').append("<tr><td></td> <td class='gumbi'><a id='btnPrijava'>Prijava</a></td></tr>");
        		$('#btnPrijava').bind('click', function() {
        			showDialog();
        		});
        		$('#prikazRokov').append("<tr><td></td> <td><input type='hidden' value='true' name='prijava' id='prijava' />	 </td></tr>");
        	}
        	else if(json.prijava.length > 0 && (json.napaka2.length < 1  || prijavlja == "referentka")){
        		//$('#prikazRokov').append("<tr><td></td> <td class='gumbi'><a id='btnPrijava'>Odjava</a> </td></tr>");
        		/*$('#btnPrijava').bind('click', function() {
        			showDialog();
        		});*/
        		$('#prikazRokov').append("<tr><td></td> <td><input type='hidden' value='false' name='prijava' id='prijava' />	 </td></tr>");
        		$('#prikazRokov').append("<tr><td id='err' colspan='2' class='error'>Na ta rok ste že prijavljeni. </td></tr>");
        	}
        	
        	$('#prikazRokov').append("<tr><td id='err' colspan='2' class='error'>" + json.napaka2 + "</td></tr>");

        	
        	$('#prikazRokov').append("<tr><td colspan='2'><input type='hidden' value='" + json.idRok + "' name='idRok' id='idRok' /></td></tr>");
        	$('#prikazRokov').append("<tr><td colspan='2'><input type='hidden' value='" + json.izvajalecId + "' name='izvajalecId' id='izvajalecId' /></td></tr>");
        	$('#prikazRokov').append("<tr><td colspan='2'><input type='hidden' value='" + json.predmetId + "' name='predmetId' id='predmetId' /></td></tr>");
        	$('#prikazRokov').append("<tr><td colspan='2'><input type='hidden' value='" + json.placljivo +	 "' name='placljivo' id='placljivo' /></td></tr>");
        	$('#prikazRokov').append("<tr><td colspan='2'><input type='hidden' value='" + json.placljivo +	 "' name='placljivo' id='placljivo' /></td></tr>");
        	
        });
    });
    
	function showDialog(){
		
		var placljivo = $('#placljivo').val();
		var prijava = $('#prijava').val();
		
		if(placljivo.length > 0 && prijava == "true")
			text = placljivo + "\n Izpit je plačljiv. Ali želite nadaljevati?";
		else if(prijava == "true")
			text = "Ali ste prepričani, da se želite prijaviti na izpit?";
		else if(prijava == "false")
			text = "Ali ste prepričani, da se želite odjaviti z izpita?";
		
		$dialog.text(text).dialog('open');
	};	
});
