$(document).ready(function(){
	////////////////////////////////// VPISNI LIST //////////////////////////////////
	$("input[name=drzava]").autocomplete({
        source: function(request, response) {
            $.post("VpisniListAutoServlet", {query: $('input[name=drzava]').val(), funkcija: "drzava"}, function(json) {
                    response(json);
            }, "json");
        },
        select: function(request, response) {
			$('input[name=idDrzava]').val(response.item.id);
			$('input[name=drzava]').css('background-color', '#F7F7F7'); // Da pobriše izpise od validacije. Obvezno!
			$('input[name=drzava]').parent().siblings('.error').html(''); // Da pobriše izpise od validacije. Obvezno!
		}
	});
	
	$("input[name=postaS]").autocomplete({
        source: function(request, response) {
            $.post("VpisniListAutoServlet", {query: $('input[name=postaS]').val(), funkcija: "posta"}, function(json) {
                    response(json);
            }, "json");
        },
        select: function(request, response) {
			$('input[name=idPostaS]').val(response.item.id);
			$('input[name=postnaStevilkaS]').val(response.item.postnaStevilka);
			$('input[name=postaS]').css('background-color', '#F7F7F7'); // Da pobriše izpise od validacije. Obvezno!
			$('input[name=postaS]').parent().siblings('.error').html(''); // Da pobriše izpise od validacije. Obvezno!
		}
	});
	
	$("input[name=postaZ]").autocomplete({
        source: function(request, response) {
            $.post("VpisniListAutoServlet", {query: $('input[name=postaZ]').val(), funkcija: "posta"}, function(json) {
                    response(json);
            }, "json");
        },
        select: function(request, response) {
			$('input[name=idPostaZ]').val(response.item.id);
			$('input[name=postnaStevilkaZ]').val(response.item.postnaStevilka);
			$('input[name=postaZ]').css('background-color', '#F7F7F7'); // Da pobriše izpise od validacije. Obvezno!
			$('input[name=postaZ]').parent().siblings('.error').html(''); // Da pobriše izpise od validacije. Obvezno!
		}
	});
	
	$("input[name=obcinaS]").autocomplete({
        source: function(request, response) {
            $.post("VpisniListAutoServlet", {query: $('input[name=obcinaS]').val(), funkcija: "obcine"}, function(json) {
                    response(json);
            }, "json");
        },
        select: function(request, response) {
			$('input[name=idObcinaS]').val(response.item.id);
			$('input[name=obcinaS]').css('background-color', '#F7F7F7'); // Da pobriše izpise od validacije. Obvezno!
			$('input[name=obcinaS]').parent().siblings('.error').html(''); // Da pobriše izpise od validacije. Obvezno!
		}
	});
	
	$("input[name=obcinaZ]").autocomplete({
        source: function(request, response) {
            $.post("VpisniListAutoServlet", {query: $('input[name=obcinaZ]').val(), funkcija: "obcine"}, function(json) {
                    response(json);
            }, "json");
        },
        select: function(request, response) {
			$('input[name=idObcinaZ]').val(response.item.id);
			$('input[name=obcinaZ]').css('background-color', '#F7F7F7'); // Da pobriše izpise od validacije. Obvezno!
			$('input[name=obcinaZ]').parent().siblings('.error').html(''); // Da pobriše izpise od validacije. Obvezno!
		}
	});
	
	////////////////////////////////// PREGLED PRIJAVLJENIH KANDIDATOV //////////////////////////////////	
	$("input[name=profesor]").autocomplete({
        source: function(request, response) {
            $.post("PregledPrijavljenihKandidatovAutoServlet", {query: $('input[name=profesor]').val()}, function(json) {
                    response(json);
            }, "json");
        },
        select: function(request, response) {
			$('input[name=idProfesor]').val(response.item.id);
			
	    	$.post('PregledPrijavljenihKandidatovAjaxServlet', {idProfesor: $('input[name=idProfesor]').val()}, function(responseData) {
	        	$('select[name=vrstaStudijaKand]').html(responseData);
	        });
	    	
			$('input[name=profesor]').css('background-color', '#F7F7F7'); // Da pobriše izpise od validacije. Obvezno!
			$('input[name=profesor]').parent().siblings('.error').html(''); // Da pobriše izpise od validacije. Obvezno!
		}
	});
});