$(document).ready(function(){
	initializeDragDrop()
});

function initializeDragDrop() {
	$('#drag div').draggable({ helper: "clone", revert: "invalid" }); // $('selektor div-a iz katerega dragamo + div')... -> '+' ne dat zraven :D
	$('#drop div').draggable({ helper: "clone", revert: "invalid" }); // ...da je še drop draggable
    $('#drag, #drop').droppable({ drop: function(event, ui) { // $('selektor div-a iz katerega dragamo, selektor div-a v katerega dropamo')...
        $(this).append(ui.draggable);
        $('input[name=izbire]').val(vrniIzbiro($('#drop div'))); // $('selektor input hidden, kamor skranjujemo izbire').val(vrniIzbiro($('selektor div-a v katerega dropamo + div')));
	}});
}

function vrniIzbiro(elm) { // Metoda da v string id vseh izbir in jih loči z ','
    var str = '';
    elm.each(function(){
        if($(this).attr('id')) {
            str += $(this).attr('id') + ",";
        }
    });
    
    if(str.length) {
        return str.substring(0,str.length-1);
    } else {
    	return '';
    }
}