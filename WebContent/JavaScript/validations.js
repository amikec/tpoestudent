// Tukaj so napisane validacije za forme:
// * Vpisni list
// * Vnos izpitnega roka

// Najprej preverite, katera polja so na teh obrazcih, da se ne bojo validacije ponavljale.
// Če je validacija že napisana, poskrbte samo za to, da ima input text isto ime, kot ga ima
// tukaj selektor validacije. Validacija bo potem avtomatsko poklicana. Če dodajate nove validacije,
// dodajte gor alineo, ki pove, za kateri obrazec ste dodali validacije.

function testError(element, test, errorText) { // Funkcija, ki glede na test izpiše error ali ga pa pobriše, če je test OK
	if (test) {
		element.css('background-color', '#F7F7F7');
		element.parent().siblings('.error').html('');
		return 0;
	} else  { // error
		element.css('background-color', '#ffdada');
		element.parent().siblings('.error').hide();
		element.parent().siblings('.error').html(errorText);
		element.parent().siblings('.error').fadeIn(300);
		return 1;
	}
}

//-------------------------------------- DODATNE FUNKCIJE ZA VALIDACIJO ----------------------------------------

function setCharAt(str, idx, chr) { // Zamenja znak v stringu
	return str.substr(0, idx) + chr + str.substr(idx + 1);
};

function sumaEMSO(emso) { // Izračun sume v EMŠU. Narejeno po navodilih na strani RS.
	var sum = 0;
	for (var i = 0; i < 12; i++) {
		sum += parseInt(emso[i]) * (7 - (i % 6));
	}
	return sum;
}

function subStringToInt(str, from, to) { // Pretvori število v str na mestih from - to v int
	var stevilo = 0;
	var faktor = 1;
	for (var i = to; i >= from; i--) {
		stevilo += parseInt(str[i]) * faktor;
		faktor *= 10;
	}
	return stevilo;
}

function insertIntToString(str, stevilo, from, to) { // Vstavi stevilo v str na mesto from - to. Če števk v številu zmanjka, na začetek fila 0
	for ( ; to >= from; to--) {
		if (stevilo > 1) {
			str = setCharAt(str, to, (stevilo % 10).toString());
			stevilo = Math.floor(stevilo / 10);
		} else { // Če zmankja števk v številu, dajemo za začetek 0
			str = setCharAt(str, to, '0');
		}
		
	}
	return str;
}


//------------------------------------------------ VALIDACIJE --------------------------------------------------

$(document).ready(function() {
	//--------------------- NA BLUR ---------------------------
	$('input[name=predavalnica]').blur(function() {
		testError($(this), $(this).val() == '' || $(this).val().search(/^[0-9A-ZČĆŽŠĐ\-]{1,10}$/) == 0, "Nevaljavna predavalnica"); 
	});
	$('input[name=datum]').blur(function() {
		testError($(this), $(this).val() == '' || $(this).val().search(/^(0[1-9]|[12][0-9]|3[01]).(0[1-9]|1[012]).(19|20)\d\d$/) == 0, 'Neveljaven datum (dd.mm.yyyy)');
	});
	$('input[name=cas]').blur(function() {
		testError($(this), $(this).val() == '' || $(this).val().search(/^([01][0-9]|2[0-3]):([0-5][0-9])$/) == 0, 'Neveljaven čas (hh:mm)');
	});
	$('input[name=vpisnaStevilka]').blur(function() {
		testError($(this), $(this).val() == '' || $(this).val().search(/^63\d{6}$/) == 0, 'Neveljavna vpisna številka');
	});
	$('input[name=priimek]').blur(function() {
		testError($(this), $(this).val() == '' || $(this).val().search(/^[A-ZČĆŽŠĐ][a-zčćžšđ]{1,44}$/) == 0, 'Neveljaven priimek');
	});
	$('input[name=dekliskiPriimek]').blur(function() {
		testError($(this), $(this).val() == '' || $(this).val().search(/^[A-ZČĆŽŠĐ][a-zčćžšđ]{1,44}$/) == 0, 'Neveljaven dekliški priimek');
	});
	$('input[name=ime]').blur(function() {
		testError($(this), $(this).val() == '' || $(this).val().search(/^[A-ZČĆŽŠĐ][a-zčćžšđ]{1,44}$/) == 0, 'Neveljaveno ime');
	});
	$('input[name=emso]').blur(function() {
		if ($('input[name=idDrzava]').length > 0 && $('input[name=idDrzava]').val() == "201") {
			if (!testError($(this), $(this).val() == '' || $(this).val().search(/^(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[012])(9[1-9][0-9]|00[1-9]|0[1-9][0-9])50[0-9]{4}$/) == 0, 'Neveljavena enotna matična številka občana')) {
				var emso = $(this).val(); // Če je regex OK, je treba še poračunat zadnjo 13. kontrolno številko
				var sum = sumaEMSO(emso);
				var kontrolnaStevilka;
				var ostanek = sum % 11;
							
				if (ostanek == 1) { // Če je ostanek 1, je treba izračunat novo zaporedno številko in spet izračunat ostanek - TO NI OBVEZNO ZA VALIDACIJO, FURST NE KOMPLICERA TOLK
					var novaZapStevilka = strStringToInt(emso, 9, 11) + 1;
					var novEmso = insertNoInArray(emso, novaZapStevilka, 9, 11);
					sum = sumaEMSO(novEmso);
					ostanek = sum % 11;
					alert(ostanek);
				}
				
				if(ostanek == 0) {
					kontrolnaStevilka = 0;
				} else {
					kontrolnaStevilka = 11 - ostanek;
				}
				testError($(this), $(this).val() == '' || kontrolnaStevilka == parseInt($(this).val()[12]), 'Neveljavena enotna matična številka občana');
			}
		} else {
			testError($(this), $(this).val() == '' || $(this).val().search(/^(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[012])(19[1-9][0-9]|200[1-9]|20[1-9][0-9])00000$/) == 0, 'Neveljaven EMŠO za tujce');
		}
	});
	$('input[name=davcnaStevilka]').blur(function() {
		testError($(this), $(this).val() == '' || $(this).val().search(/^[0-9]{8}$/) == 0, 'Neveljavena davčna številka');
	});
	$('input[name=ulicaS]').blur(function() {
		testError($(this), $(this).val() == '' || $(this).val().search(/^[A-ZČĆŽŠĐ][A-ZČĆŽŠĐa-z0-9čćžšđ\ \.]{1,44}$/) == 0, 'Neveljavena stalna ulica');
	});
	$('input[name=postaS]').blur(function() {
		testError($(this), $(this).val() == '' || $(this).val().search(/^[A-ZČĆŽŠĐ][A-ZČĆŽŠĐa-zčćžšđ\.\-,\ ]{1,44}$/) == 0, 'Neveljavena stalna pošta');
	});
	$('input[name=postnaStevilkaS]').blur(function() {
		testError($(this), $(this).val() == '' || $(this).val().search(/^[1-9][0-9]{3,9}$/) == 0, 'Neveljavena stalna poštna številka');
	});
	$('input[name=obcinaS]').blur(function() {
		testError($(this), $(this).val() == '' || $(this).val().search(/^[A-ZČĆŽŠĐ][A-ZČĆŽŠĐa-zčćžšđ\.\-,\ \/]{1,44}$/) == 0, 'Neveljavena stalna občina');
	});
	$('input[name=drzava]').blur(function() {
		testError($(this), $(this).val() == '' || $(this).val().search(/^[A-ZČĆŽŠĐ][A-ZČĆŽŠĐa-zčćžšđ ]{1,44}$/) == 0, 'Neveljavena država');
	});
	$('input[name=telefon]').blur(function() {
		testError($(this), $(this).val() == '' || $(this).val().search(/^(0[1-7])?[\ \/]?[0-9]{3}[\-\ ]?[0-9]{2}[\-\ ]?[0-9]{2}$/) == 0, 'Neveljavena telefonska številka');
	});
	$('input[name=prenosni]').blur(function() {
		testError($(this), $(this).val() == '' || $(this).val().search(/^[0-9]{3}[\ \/\-]?[0-9]{3}[\-\ ]?[0-9]{3}[\-\ ]?$/) == 0, 'Neveljaven prenosni telefon');
	});
	$('input[name=email]').blur(function() {
		testError($(this), $(this).val() == '' || $(this).val().search(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\.+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/) == 0, 'Neveljaven elektronski naslov');
	});
	$('input[name=ulicaZ]').blur(function() {
		if (!testError($(this), $(this).val() == '' || $(this).val().search(/^[A-ZČĆŽŠĐ][A-ZČĆŽŠĐa-z0-9čćžšđ\ \.]{1,44}$/) == 0, 'Neveljavena začasna ulica')) {
			if ($('input[name=ulicaZ]').val() == "") {
				$('input[name=idUlicaZ]').val("");
			}
		}
	});
	$('input[name=postaZ]').blur(function() {
		if (!testError($(this), $(this).val() == '' || $(this).val().search(/^[A-ZČĆŽŠĐ][A-ZČĆŽŠĐa-zčćžšđ\.\-,\ ]{1,44}$/) == 0, 'Neveljavena začasna pošta')) {
			if ($('input[name=postaZ]').val() == "") {
				$('input[name=idPostaZ]').val("");
				$('input[name=postnaStevilkaZ]').val("");
			}
		}
	});
	$('input[name=postnaStevilkaZ]').blur(function() {
		testError($(this), $(this).val() == '' || $(this).val().search(/^[1-9][0-9]{3,9}$/) == 0, 'Neveljavena začasna poštna številka');
	});
	$('input[name=obcinaZ]').blur(function() {
		if (!testError($(this), $(this).val() == '' || $(this).val().search(/^[A-ZČĆŽŠĐ][A-ZČĆŽŠĐa-zčćžšđ\.\-,\ \/]{1,44}$/) == 0, 'Neveljavena začasna občina')) {
			if ($('input[name=obcinaZ]').val() == "") {
				$('input[name=idObcinaZ]').val("");
			}
		}
	});
	$('input[name=profesor]').blur(function() {
		testError($(this), $(this).val() == '' || $(this).val().search(/^[A-ZČĆŽŠĐ][a-zčćžšđ]{1,44}[\ ][A-ZČĆŽŠĐ][a-zčćžšđ]{1,44}[\ ]?[A-Z]?[a-zČĆŽŠĐčćžšđ]{0,44}$/) == 0, 'Neveljavno ime profesorja');
	});
	
	//--------------------- NA SUBMIT --------------------------- skoraj isto kot odzgoraj, več ali manj copy paste
	$('.forme').submit(function() {
		var napaka = false;
		if ($('input[name=predavalnica]').length > 0) {
			if (testError($('input[name=predavalnica]'), $('input[name=predavalnica]').val().search(/^[0-9A-ZČĆŽŠĐ\-]{1,10}$/) == 0, 'Neveljavna predavalnica')) {
				napaka = true;
			}
		}
		if ($('input[name=datum]').length > 0) {
			if (testError($('input[name=datum]'), $('input[name=datum]').val().search(/^(0[1-9]|[12][0-9]|3[01]).(0[1-9]|1[012]).(19|20)\d\d$/) == 0, 'Neveljaven datum')) {
				napaka = true;
			}
		}
		if ($('input[name=cas]').length > 0) {
			if (testError($('input[name=cas]'), $('input[name=cas]').val().search(/^([01][0-9]|2[0-3]):([0-5][0-9])$/) == 0, 'Neveljaven čas (hh:mm)')) {
				napaka = true;
			}
		}
		if ($('input[name=vpisnaStevilka]').length > 0) {
			if (testError($('input[name=vpisnaStevilka]'), $('input[name=vpisnaStevilka]').val().search(/^63\d{6}$/) == 0, 'Neveljavna vpisna številka')) {
				napaka = true;
			}
		}
		if ($('input[name=priimek]').length > 0) {
			if (testError($('input[name=priimek]'), $('input[name=priimek]').val().search(/^[A-ZČĆŽŠĐ][a-zčćžšđ]{1,44}$/) == 0, 'Neveljaven priimek')) {
				napaka = true;
			}
		}
		if ($('input[name=dekliskiPriimek]').length > 0) {
			if (testError($('input[name=dekliskiPriimek]'), $('input[name=dekliskiPriimek]').val() == '' || $('input[name=dekliskiPriimek]').val().search(/^[A-ZČĆŽŠĐ][a-zčćžšđ]{1,44}$/) == 0, 'Neveljaven dekliški priimek')) {
				napaka = true;
			}
		}
		if ($('input[name=ime]').length > 0) {
			if (testError($('input[name=ime]'), $('input[name=ime]').val().search(/^[A-ZČĆŽŠĐ][a-zčćžšđ]{1,44}$/) == 0, 'Neveljaveno ime')) {
				napaka = true;
			}
		}
		if ($('input[name=emso]').length > 0) {
			if ($('input[name=idDrzava]').length > 0 && $('input[name=idDrzava]').val() == "201") {
				if (testError($('input[name=emso]'), $('input[name=emso]').val().search(/^(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[012])(9[1-9][0-9]|00[1-9]|0[1-9][0-9])50[0-9]{4}$/) == 0, 'Neveljavena enotna matična številka občana')) {
					napaka = true;
				} else {
					var emso = $('input[name=emso]').val(); // Če je regex OK, je treba še poračunat zadnjo 13. kontrolno številko
					var sum = sumaEMSO(emso);
					var kontrolnaStevilka;
					var ostanek = sum % 11;
								
					if (ostanek == 1) { // Če je ostanek 1, je treba izračunat novo zaporedno številko in spet izračunat ostanek
						var novaZapStevilka = strStringToInt(emso, 9, 11) + 1;
						var novEmso = insertNoInArray(emso, novaZapStevilka, 9, 11);
						sum = sumaEMSO(novEmso);
						ostanek = sum % 11;
						alert(ostanek);
					}
					
					if(ostanek == 0) {
						kontrolnaStevilka = 0;
					} else {
						kontrolnaStevilka = 11 - ostanek;
					}
					if (testError($('input[name=emso]'), kontrolnaStevilka == parseInt($('input[name=emso]').val()[12]), 'Neveljavena enotna matična številka občana')) {
						napaka = true;
					}
				}
			}
			else {
				testError($(this), $(this).val() == '' || $(this).val().search(/^(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[012])(19[1-9][0-9]|200[1-9]|20[1-9][0-9])00000$/) == 0, 'Neveljaven EMŠO za tujce');
			}
		}
		if ($('input[name=davcnaStevilka]').length > 0) {
			if (testError($('input[name=davcnaStevilka]'), $('input[name=davcnaStevilka]').val().search(/^[0-9]{8}$/) == 0, 'Neveljavena davčna številka')) {
				napaka = true;
			}
		}
		if ($('input[name=ulicaS]').length > 0) {
			if (testError($('input[name=ulicaS]'), $('input[name=ulicaS]').val().search(/^[A-ZČĆŽŠĐ][A-ZČĆŽŠĐa-z0-9čćžšđ\ \.]{1,44}$/) == 0, 'Neveljavena stalna ulica')) {
				napaka = true;
			}
		}
		if ($('input[name=postaS]').length > 0) {
			if (testError($('input[name=postaS]'), $('input[name=postaS]').val().search(/^[A-ZČĆŽŠĐ][A-ZČĆŽŠĐa-zčćžšđ\.\-,\ ]{1,44}$/) == 0, 'Neveljavena stalna pošta')) {
				napaka = true;
			}
		}
		if ($('input[name=postnaStevilkaS]').length > 0) {
			if (testError($('input[name=postnaStevilkaS]'), $('input[name=postnaStevilkaS]').val().search(/^[1-9][0-9]{3,9}$/) == 0, 'Neveljavena stalna poštna številka')) {
				napaka = true;
			}
		}
		if ($('input[name=obcinaS]').length > 0) {
			if (testError($('input[name=obcinaS]'), $('input[name=obcinaS]').val().search(/^[A-ZČĆŽŠĐ][A-ZČĆŽŠĐa-zčćžšđ\.\-,\ \/]{1,44}$/) == 0, 'Neveljavena stalna občina')) {
				napaka = true;
			}
		}
		if ($('input[name=drzava]').length > 0) {
			if (testError($('input[name=drzava]'), $('input[name=drzava]').val().search(/^[A-ZČĆŽŠĐ][A-ZČĆŽŠĐa-zčćžšđ ]{1,44}$/) == 0, 'Neveljavena država')) {
				napaka = true;
			}
		}
		if ($('input[name=telefon]').length > 0) {
			if (testError($('input[name=telefon]'), $('input[name=telefon]').val().search(/^(0[1-7])?[\ \/]?[0-9]{3}[\-\ ]?[0-9]{2}[\-\ ]?[0-9]{2}$/) == 0, 'Neveljavena telefonska številka')) {
				napaka = true;
			}
		}
		if ($('input[name=prenosni]').length > 0) {
			if (testError($('input[name=prenosni]'), $('input[name=prenosni]').val().search(/^[0-9]{3}[\ \/\-]?[0-9]{3}[\-\ ]?[0-9]{3}[\-\ ]?$/) == 0, 'Neveljaven prenosni telefon')) {
				napaka = true;
			}
		}
		if ($('input[name=email]').length > 0) {
			if (testError($('input[name=email]'), $('input[name=email]').val().search(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\.+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/) == 0, 'Neveljaven elektronski naslov')) {
				napaka = true;
			}
		}
		if ($('input[name=ulicaZ]').length > 0) {
			if (testError($('input[name=ulicaZ]'), $('input[name=ulicaZ]').val() == '' || $('input[name=ulicaZ]').val().search(/^[A-ZČĆŽŠĐ][A-ZČĆŽŠĐa-z0-9čćžšđ\ \.]{1,44}$/) == 0, 'Neveljavena začasna ulica')) {
				napaka = true;
			}
		}
		if ($('input[name=postaZ]').length > 0) {
			if (testError($('input[name=postaZ]'),$('input[name=postaZ]').val() == '' || $('input[name=postaZ]').val().search(/^[A-ZČĆŽŠĐ][A-ZČĆŽŠĐa-zčćžšđ\.\-,\ ]{1,44}$/) == 0, 'Neveljavena začasna pošta')) {
				napaka = true;
			}
		}
		if ($('input[name=postnaStevilkaZ]').length > 0) {
			if (testError($('input[name=postnaStevilkaZ]'), $('input[name=postnaStevilkaZ]').val() == '' || $('input[name=postnaStevilkaZ]').val().search(/^[1-9][0-9]{3,9}$/) == 0, 'Neveljavena začasna poštna številka')) {
				napaka = true;
			}
		}
		if ($('input[name=obcinaZ]').length > 0) {
			if (testError($('input[name=obcinaZ]'), $('input[name=obcinaZ]').val() == '' || $('input[name=obcinaZ]').val().search(/^[A-ZČĆŽŠĐ][A-ZČĆŽŠĐa-zčćžšđ\.\-,\ \/]{1,44}$/) == 0, 'Neveljavena začasna občina')) {
				napaka = true;
			}
		}
		
		if (napaka) {
			return false;
		} else {
			return true;
		}
	});
});