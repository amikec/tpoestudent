<%@page import="si.fri.estudent.jpa.Oseba"%>
<%@page import="si.fri.estudent.jpa.Student"%>
<%@page import="si.fri.estudent.jpa.Vrsta_studija"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<link type="text/css" href="JavaScript/jQuery/UI-datepicker/css/custom-theme/jquery-ui-timepicker.css" rel="stylesheet" />
<script type="text/javascript" src="JavaScript/jQuery/UI-datepicker/js/jquery-ui-timepicker.js"></script>

<%
String imeStudent = null;
Student student = null;
Oseba oseba = null;
List<Vrsta_studija> vrsteStudija = null;

if(request.getAttribute("student") != null && request.getAttribute("oseba") != null){
	student = (Student)request.getAttribute("student");
	oseba = (Oseba)request.getAttribute("oseba");
	
	if (oseba.getPriimekDekliski() == null || oseba.getPriimekDekliski().equals("")) {
		imeStudent = oseba.getIme() + " " + oseba.getPriimek() + " (" + student.getVpisnaSt() + ")";
	} else {
		imeStudent = oseba.getIme() + " " + oseba.getPriimekDekliski() + " " + oseba.getPriimek() + " (" + student.getVpisnaSt() + ")";
	}
} else {
	imeStudent = "Iskani študent ne obstaja";
}

if (request.getAttribute("vrsteStudija") != null) {
	vrsteStudija = (List<Vrsta_studija>)request.getAttribute("vrsteStudija");
}
%>

<h1>VNOS OCENE IZPITA S PRIJAVNICE</h1>
<form id="idVnosPrijavnica" action="VnosOcenePrijavnicaServlet" method="post" class="forme" style="width: 800px; margin-left: 130px;">
	<input type="hidden" name="idStudent" value='<%= student.getIdStudent() %>' />
	<table>
		<tr>
			<td colspan="2" class="success">
				<% if (request.getAttribute("errorText") != null) { %>
					<%= (String)request.getAttribute("errorText") %>
				<% } %>
			</td>
			<td></td>
		</tr>
		<tr>
			<th>Študent</th>
			<td><input type="text" name="imeStudent" readonly="readonly" value='<%= imeStudent %>' /></td>
			<td class="error"></td>
		</tr>
		<tr>
			<th>Vrsta študija</th>
			<td>
				<select name="vrstaStudijaVnosPrijavnica">
					<%
					if (vrsteStudija.size() > 0) { %>
						<option value='0'>-- Izberi vrsto študija --</option>
						<%
						
						for (Vrsta_studija vs : vrsteStudija) { %>
							<option value='<%= vs.getIdVrstaStudija() %>'><%= vs.getKraticaVrstaStudija() + " - " + vs.getNazivVrstaStudija() %></option>
						<% }
					} %>
				</select>
			</td>
			<td class="error"></td>
		</tr>
		<tr>
			<th>Študijski program</th>
			<td>
				<select name="studijskiProgramVnosPrijavnica">
				</select>
			</td>
			<td class="error"></td>
		</tr>
		<tr>
			<th>Studijska smer</th>
			<td>
				<select name="studijskaSmerVnosPrijavnica">
				</select>
			</td>
			<td class="error"></td>
		</tr>
		<tr>
			<th>Letnik</th>
			<td>
				<select name="letnikVnosPrijavnica">
				</select>
			</td>
			<td class="error"></td>
		</tr>
		<tr>
			<th>Predmet</th>
			<td>
				<select name="predmetVnosPrijavnica">
				</select>
			</td>
			<td class="error"></td>
		</tr>
		<tbody id="showHidePodatkiIzpita"></tbody>
	</table>
</form>
<script type="text/javascript">
	$('.forme th').css('width', '145px');
	$('#showHidePodatkiIzpita').hide();
	$('.error').css('width', '150px');
</script>