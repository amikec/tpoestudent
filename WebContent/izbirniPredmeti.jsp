<%@page import="si.fri.estudent.jpa.Oseba"%>
<%@page import="si.fri.estudent.jpa.Student"%>
<%@page import="si.fri.estudent.jpa.Vrsta_studija"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
String imeStudent = null;
Student student = null;
Oseba oseba = null;
List<Vrsta_studija> vrsteStudija = null;

if(request.getAttribute("student") != null && request.getAttribute("oseba") != null){
	student = (Student)request.getAttribute("student");
	oseba = (Oseba)request.getAttribute("oseba");
	
	if (oseba.getPriimekDekliski() == null || oseba.getPriimekDekliski().equals("")) {
		imeStudent = oseba.getIme() + " " + oseba.getPriimek() + " (" + student.getVpisnaSt() + ")";
	} else {
		imeStudent = oseba.getIme() + " " + oseba.getPriimekDekliski() + " " + oseba.getPriimek() + " (" + student.getVpisnaSt() + ")";
	}
} else {
	imeStudent = "Iskani študent ne obstaja";
}

if (request.getAttribute("vrsteStudija") != null) {
	vrsteStudija = (List<Vrsta_studija>)request.getAttribute("vrsteStudija");
}
%>

<h1>IZBIRNI PREDMETI</h1>
<form id="idIzbirniPredmeti" action="IzbirniPredmetiServlet" method="post" class="forme" style="width: 650px; margin-left: 130px;">
	<input type="hidden" name="idStudent" value='<%= student.getIdStudent() %>' />
	<table>
		<tr>
			<td colspan="2" class="success">
				<% if (request.getAttribute("error") != null) { %>
					<%= (String)request.getAttribute("error") %>
				<% } %>
			</td>
		</tr>
		<tr>
			<th>Študent</th>
			<td><input type="text" name="imeStudent" readonly="readonly" value='<%= imeStudent %>' /></td>
		</tr>
		<tr>
			<th>Vrsta študija</th>
			<td>
				<select name="vrstaStudijaIzbirniP">
					<%
					if (vrsteStudija.size() > 0) { %>
						<option value='0'>-- Izberi vrsto študija --</option>
						<%
						
						for (Vrsta_studija vs : vrsteStudija) { %>
							<option value='<%= vs.getIdVrstaStudija() %>'><%= vs.getKraticaVrstaStudija() + " - " + vs.getNazivVrstaStudija() %></option>
						<% }
					} %>
				</select>
			</td>
		</tr>
		<tr>
			<th>Študijski program</th>
			<td>
				<select name="studijskiProgramIzbirniP">
				</select>
			</td>
		</tr>
		<tr>
			<th>Studijska smer</th>
			<td>
				<select name="studijskaSmerIzbirniP">
				</select>
			</td>
		</tr>
		<tr>
			<th>Letnik</th>
			<td>
				<select name="letnikIzbirniP">
				</select>
			</td>
		</tr>
		<tr>
			<th>Izbirni predmet / Modul</th>
			<td>
				<select name="izbirniPredmetnikModul" disabled="disabled">
					<option value="0">-- Izberite izbirni predmetnik ali modul --</option>
					<option value="izbirniPredmetnik">Izbirni predmetnik</option>
					<option value="modul">Modul</option>
				</select>
			</td>
		</tr>
		<tbody class="showHideDragDrop">
			<tr>
				<td colspan="2" class="dragDrop">
					
				</td>
			</tr>
			<tr>
				<td colspan="2" class="gumbi"><div><input type='submit' value='SHRANI' /></div></td>
			</tr>
		</tbody>
	</table>
</form>
<script type="text/javascript">
	$('.forme th').css('width', '145px');
	$('.showHideDragDrop').hide();
</script>