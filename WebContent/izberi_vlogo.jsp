<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ page import="si.fri.estudent.utilities.*" %>
<%@ page import="si.fri.estudent.jpa.*" %>
<%@ page import="java.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Izbira uporabniške vloge</title>
</head>
<body>
	<table>
	<tr><td colspan="2">
		Prosim izberite uporabniško vlogo, s katero se želite vpisati.
	</td></tr>
	<% 
	
	List<Vloga_oseba> vloge = (List<Vloga_oseba>) session.getAttribute("vloge");
	
	for(Vloga_oseba vloga : vloge){
		
		String vlogaNaziv = vloga.getNazivVlogaOseba();
		
		if(vlogaNaziv.equals("student"))
			vlogaNaziv = "študent";
		else if(vlogaNaziv.equals("ucitelj"))
			vlogaNaziv = "učitelj";
		
		out.print("<tr><td>-</td><td><a href='LoginManagerServlet?mem="+vloga.getIdVlogaOseba()+"'>" + vlogaNaziv + "</a></td></tr>");
	}
	%>
	</table>
</body>
</html>