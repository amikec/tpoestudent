<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="si.fri.estudent.jpa.*" %>
<%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<h1>Pregled razpisanih rokov</h1>

<link type="text/css" href="JavaScript/jQuery/UI-datepicker/css/custom-theme/jquery-ui-1.8.18.custom.css" rel="stylesheet" />	
<link type="text/css" href="JavaScript/jQuery/UI-datepicker/css/custom-theme/jquery-ui-timepicker.css" rel="stylesheet" />
<script type="text/javascript" src="JavaScript/jQuery/UI-datepicker/js/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="JavaScript/jQuery/UI-datepicker/js/jquery-ui-timepicker.js"></script>

<form action="PregledRazpisanihRokovStudentAjaxServlet" method="POST" class="forme" style="width: 400px; margin-left: 255px;" >
<table>
	<tr>
		<th>Študijsko leto</th>
		<td>
			<select id="pregledRazpisanihRokovStudentAjaxRequest" name="studijskoLeto">
				<option value='2011/2012' >2011/2012</option>
				<option value='2012/2013' >2012/2013</option>
			</select>
		</td>
	</tr>
</table>
<table id="izpisRazpisanihRokov" name="izpisRazpisanihRokov">
</table>
</form>

<div class="gumbi"><a href="PregledRazpisanihRokovStudentAjaxServlet?tiskanje=tiskanje">Tiskanje</a></div>

<script type="text/javascript">

	
	$('.forme th').css('width', '100px'); /* kakšen del širine forme nej zavzamejo <th> */
	/* Ostal plac je naštiman, da ga zafilajo polja (text, select, textarea, ...) */
	
</script>