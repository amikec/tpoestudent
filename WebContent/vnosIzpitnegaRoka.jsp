<%@page import="si.fri.estudent.jpa.Vloga_oseba"%>
<%@ page import="java.util.*" import="si.fri.estudent.jpa.Predmet" import="si.fri.estudent.jpa.Oseba" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<link type="text/css" href="JavaScript/jQuery/UI-datepicker/css/custom-theme/jquery-ui-timepicker.css" rel="stylesheet" />

<script type="text/javascript" src="JavaScript/jQuery/UI-datepicker/js/jquery-ui-timepicker.js"></script>

<script type="text/javascript">
	$(function(){
		$("input[name=datum]").datepicker({
	    	dateFormat: 'dd.mm.yy'	    		
		});
		$('input[name=cas]').timepicker({});
		
		$('#vnosIzpitnegaRokaAjaxRequest option[value=<% if(request.getAttribute("predmet") != null) out.print(request.getAttribute("predmet")); %>]').prop('selected', true);
		
		<% 
		Vloga_oseba vloga = (Vloga_oseba)session.getAttribute("vloga");
		if(vloga.getNazivVlogaOseba().compareTo("ucitelj") != 0) { %>
				
			// ajax z JSON na servlet
		    $('#vnosIzpitnegaRokaAjaxRequest').change(function() {
		    	var sifra = $('#vnosIzpitnegaRokaAjaxRequest').val();
		    	$.post('VnosIzpitnegaRokaAjaxServlet', podatki = {sifraPredmeta: sifra}, function(responseData) {
		        	var json = jQuery.parseJSON(responseData); // Metoda parsa responseData v json objekt
		        	
		        	var kombNiz = "";
		        	var predavatelj = "";
		        	$('#vnosIzpitnegaRokaAjaxResponse').find('option').remove();
		        	for(var i = 0; i < json.listPredavatelji.length; i++) { 
		        		kombNiz = json.idKombinacije[i];        		
		        		predavatelj += "<option value="+ kombNiz + ">" + json.listPredavatelji[i] + "</option>";
		        	}
		        	$('#vnosIzpitnegaRokaAjaxResponse').append(predavatelj);
		        });
		    });
			
		
			// To se da izboljšati
			$(function() {
		    	var sifra = $('#vnosIzpitnegaRokaAjaxRequest').val();
		    	$.post('VnosIzpitnegaRokaAjaxServlet', podatki = {sifraPredmeta: sifra}, function(responseData) {
		        	var json = jQuery.parseJSON(responseData); // Metoda parsa responseData v json objekt
		        	
		        	var kombNiz = "";
		        	var predavatelj = "";
		        	$('#vnosIzpitnegaRokaAjaxResponse').find('option').remove();
		        	
		        	<%if(request.getAttribute("predavatelj") != null) {%>
			        	for(var i = 0; i < json.listPredavatelji.length; i++) {
			        		kombNiz = json.idKombinacije[i];        		
			        		if(kombNiz == <%= request.getAttribute("predavatelj") %>)
			        			predavatelj += "<option value="+ kombNiz + ">" + json.listPredavatelji[i] + "</option>";
			        	}
		        	<%
		        	}
		        	%>
		        	for(var i = 0; i < json.listPredavatelji.length; i++) { 
		        		kombNiz = json.idKombinacije[i]; 
		        		<%if(request.getAttribute("predavatelj") != null) { %>
		        			if(kombNiz == <%= request.getAttribute("predavatelj") %>) {} 
		        			else {
		        				predavatelj += "<option value="+ kombNiz + ">" + json.listPredavatelji[i] + "</option>";
		        			}
		        		<%	
		        		} else {
		        		%>
		        			predavatelj += "<option value="+ kombNiz + ">" + json.listPredavatelji[i] + "</option>";
		        		<%
		        		}
		        		%>
		        	}
		        	$('#vnosIzpitnegaRokaAjaxResponse').append(predavatelj);
		        });
	    	});
		<%
		} else {%>	
			$('#vnosIzpitnegaRokaAjaxRequest').change(function() {
		    	var sifra = $('#vnosIzpitnegaRokaAjaxRequest').val();
		    	$.post('VnosIzpitnegaRokaAjaxServlet', podatki = {sifraPredmeta: sifra}, function(responseData) {
		        	var json = jQuery.parseJSON(responseData); // Metoda parsa responseData v json objekt
		        	
		        	var kombNiz = "";
		        	var predavatelj = "";
		        	$('#vnosIzpitnegaRokaAjaxResponse').find('option').remove();
		        	
		        	<%  Oseba uporabnik = (Oseba)session.getAttribute("uporabnik"); %>
			        	for(var i = 0; i < json.listPredavatelji.length; i++) {
			        		kombNiz = json.idKombinacije[i];        		
			        		if(!(json.listPredavatelji[i].indexOf(<%= "'" + uporabnik.getIme() + " " + uporabnik.getPriimek() + "'" %>) == -1)) {
			        			predavatelj += "<option value="+ kombNiz + ">" + json.listPredavatelji[i] + "</option>";
			        		}
			        	}

		        	$('#vnosIzpitnegaRokaAjaxResponse').append(predavatelj);
		        });
	    	});
			
			$(function() {
				var sifra = $('#vnosIzpitnegaRokaAjaxRequest').val();
		    	$.post('VnosIzpitnegaRokaAjaxServlet', podatki = {sifraPredmeta: sifra}, function(responseData) {
		        	var json = jQuery.parseJSON(responseData); // Metoda parsa responseData v json objekt
		        	
		        	var kombNiz = "";
		        	var predavatelj = "";
		        	$('#vnosIzpitnegaRokaAjaxResponse').find('option').remove();
		        	
		        	<%  uporabnik = (Oseba)session.getAttribute("uporabnik"); %>
			        	for(var i = 0; i < json.listPredavatelji.length; i++) {
			        		kombNiz = json.idKombinacije[i];        		
			        		if(!(json.listPredavatelji[i].indexOf(<%= "'" + uporabnik.getIme() + " " + uporabnik.getPriimek() + "'" %>) == -1)) {
			        			predavatelj += "<option value="+ kombNiz + ">" + json.listPredavatelji[i] + "</option>";
			        		}
			        	}
	
		        	$('#vnosIzpitnegaRokaAjaxResponse').append(predavatelj);
		        });
			});
			
		<%	
		}	
		%>
		
	});
	
</script>

<h1>VNOS IZPITNEGA ROKA</h1>
<form action="VnosIzpitnegaRokaServlet" method="POST" class="forme" style="width: 700px; margin-left: 180px;" >
	
	<table>
		<% if(request.getAttribute("uspeh") != null) { %>
			<tr>
				<td colspan="2" class="success"><%= request.getAttribute("uspeh") %></td>
				<td></td>
			</tr>
		<%
		}
		%>
		<tr>
			<th>Predmet</th>
			<td>
				<select id="vnosIzpitnegaRokaAjaxRequest" name="predmet">
					<option value="-1">-- izberite predmet --</option>
					<% 
						if(request.getAttribute("predmeti") != null) {
							List<Predmet> predmeti = (List<Predmet>)request.getAttribute("predmeti");
							
							for(Predmet predmet : predmeti) { %>
								<option value=<%= predmet.getSifraPredmeta() %>><%= predmet.getNazivPredmet() %></option>
							<% 
							}
						}	
					%>
				</select>
			</td>
			<td class="error"></td>
		</tr>
		<tr>
			<th>Predavatelj</th>
			<td>
				<select id="vnosIzpitnegaRokaAjaxResponse" name="predavatelj" >
				</select>
			</td>
			<td class="error"></td>
		</tr>
		
		<tr>
			<th>Datum izpita</th>
			<td><input type="text" name="datum" value="<% if(request.getAttribute("datum") != null) out.print(request.getAttribute("datum")); %>"/></td>
			<td class="error">
				<%
				if (request.getAttribute("errorDatum") != null) {
					out.print(request.getAttribute("errorDatum").toString());
				} %>
			</td>
		</tr>
		<tr>
			<th>Čas izpita</th>
			<td><input type="text" name="cas" value="<% if(request.getAttribute("cas") != null) out.print(request.getAttribute("cas")); else out.print("08:00"); %>" /></td>
			<td class="error">
				<%
				if (request.getAttribute("errorCas") != null) {
					out.print(request.getAttribute("errorCas").toString());
				} %>
			</td>
		<tr>
		<tr>
			<th>Predavalnica</th>
			<td><input type="text" name="predavalnica" value="<% if(request.getAttribute("predavalnica") != null) out.print(request.getAttribute("predavalnica")); %>" /></td>
			<td class="error">
				<%
				if (request.getAttribute("errorPredavalnica") != null) {
					out.print(request.getAttribute("errorPredavalnica").toString());
				} %>
			</td>
		</tr>
		<tr>
			<th>Maks. št. prijav</th>
			<td><input type="text" name="maksStPrijav" value="<% if(request.getAttribute("maksStPrijav") != null) out.print(request.getAttribute("maksStPrijav")); %>" /></td>
			<td class="error">
				<%
				if (request.getAttribute("errorMaksStPrijav") != null) {
					out.print(request.getAttribute("errorMaksStPrijav").toString());
				} %>
			</td>
		</tr>
		<tr>
			<th>Možne točke</th>
			<td><input type="text" name="mozneTocke" value="<% if(request.getAttribute("mozneTocke") != null) out.print(request.getAttribute("mozneTocke")); %>" /></td>
			<td class="error">
				<%
				if (request.getAttribute("errorMozneTocke") != null) {
					out.print(request.getAttribute("errorMozneTocke").toString());
				} %>
			</td>
		</tr>
		<tr>
			<th>Meja za pozitivno</th>
			<td><input type="text" name="mejaZaPozitivno" value="<% if(request.getAttribute("mejaZaPozitivno") != null) out.print(request.getAttribute("mejaZaPozitivno")); %>" /></td>
			<td class="error">
				<%
				if (request.getAttribute("errorMejaZaPozitivno") != null) {
					out.print(request.getAttribute("errorMejaZaPozitivno").toString());
				} %>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="gumbi"><div><input type='submit' value='POŠLJI' /></div></td>
			<td style="backgorund-color: red"></td>
		</tr>
	</table>
</form>
<script type="text/javascript">
	$('.forme th').css('width', '120px'); /* kakšen del širine forme nej zavzamejo <th> */
	$('.error').css('width', '180px'); /* kakšen del širine forme nej zavzamejo errorji */
	/* Ostal plac je naštiman, da ga zafilajo polja (text, select, textarea, ...) */
</script>
