<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, si.fri.estudent.jpa.*, si.fri.estudent.utilities.SkupenProgram" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">	
	


	$(document).ready(function(){
		$("#divTisk").hide();
		$("#poizvedi").click(function(){
			poizvedi();
		});
		
		$(".izbira").change(function(){
			$("#vsebina").html("");
			$("#divTisk").hide();
		});
		
		
	});

	function poizvedi(){
		$("#vsebina").html("");
		$.post('SeznamVpisanihServlet', podatki = {
				letnik : $("#letniki option:selected").val(),
				studijskoLeto :  $("#studijskaLeta option:selected").val(),
				studijskiProgram :  $("#programiInSmeri option:selected").val(),
				nacinStudija :  $("#nacinStudija option:selected").val(),
				vrstaVpisa :  $("#vrstaVpisa option:selected").val(),
				smerModul :  $("#smerModul option:selected").val()
		}, function(responseData) {
			$("#vsebina").html(responseData);	
			$("#divTisk").show();
		});
		
	}
</script>

<h1>Seznam vpisanih</h1>

<%  String[] letniki=(String[])request.getAttribute("letniki");
	String[] studijskaLeta=(String[])request.getAttribute("studijskaLeta");
	String[] programiInSmeri=(String[])request.getAttribute("programiInSmeri");
	String[] nacinStudija=(String[])request.getAttribute("nacinStudija");
	String[] vrstaVpisa=(String[])request.getAttribute("vrstaVpisa");
	String[] smerModul=(String[])request.getAttribute("smerModul");
	
%>
<div class="forme">
	<table>
		<tr>
			<td>Letnik </td>
			<td>
				<select id="letniki" class="izbira">
					<% for(String letnik: letniki){
						int i=0;%>
						<option value="<%= letnik%>">
							<%= letnik %>
						</option>
					<% } %>
				</select>
			</td>
		</tr>
		
		<tr>
			<td>Študijsko leto</td>
			<td>
				<select id="studijskaLeta" class="izbira" >
					<% for(String studijskoLeto: studijskaLeta){
						int i=0;%>
						<option value="<%= studijskoLeto%>">
							<%= studijskoLeto %>
						</option>
					<% } %>
				</select>
			</td>
		</tr>
		
		<tr>
			<td>Študijski program </td>
			<td>
				<select id="programiInSmeri" class="izbira">
					<% for(String programInSmer: programiInSmeri){
						int i=0;%>
						<option value="<%= programInSmer%>">
							<%= programInSmer %>
						</option>
					<% } %>
				</select>
			</td>
		</tr>
		
		<tr>
			<td>Način študija </td>
			<td>
				<select id="nacinStudija" class="izbira">
					<% for(String nacinStudij: nacinStudija){
						int i=0;%>
						<option value="<%= nacinStudij%>">
							<%= nacinStudij %>
						</option>
					<% } %>
				</select>
			</td>
		</tr>
		
		<tr>
			<td>Vrsta vpisa </td>
			<td>
				<select id="vrstaVpisa" class="izbira">
					<% for(String vrstaVpis: vrstaVpisa){
						int i=0;%>
						<option value="<%= vrstaVpis%>">
							<%= vrstaVpis %>
						</option>
					<% } %>
				</select>
			</td>
		</tr>
		<tr>
			<td>Modul oz smer </td>
			<td>
				<select id="smerModul" class="izbira">
					<% for(String smerModul1: smerModul){
						int i=0;%>
						<option value="<%= smerModul1%>" <%if(smerModul1.startsWith("---")){ out.write("disabled='disabled'");} %>>
							<%= smerModul1 %>
						</option>
					<% } %>
				</select>
			</td>
		</tr>
		<tr>
			<td></td>
			<td class="gumbi"><button id="poizvedi" >Poizvedi</button>			
		</tr>	
	</table>
</div>
<br />
<div id="vsebina" style="min-height:150px" >

</div>
<br />
<div id="divTisk"><a href="SeznamVpisanihServlet">Natisni</a></div>