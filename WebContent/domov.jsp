<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="si.fri.estudent.jpa.*" %>
<!-- Za vsak primer če bo treba še kej dodatno narest -->
<h1>Dobrodošli na strani FRI e-Študent</h1>

<div id="domovDiv">
	Primer Ajax:
	<br />
	<button id="gumb">Test Ajax  z JSON na Servlet</button>
	<div id="odg"></div>
	
	<button id="gumb2">Test ajax z load na JSP</button>
	<br />
	<select id="ddl" style="width: 200px"></select>
	
	<br />
	<br />
	Primer Autocomplete (delujejo tudi ČĆŽŠĐ =)))))):
	<br />
	<!-- Kodo za avtocomplete pišite v JavaScript/autocomplete.js -->
	Vnesi ime države: <input id="drzave" type="text" /> <!-- Polje, v katerega vnašamo dinamično iskanje in dobivamo rezultate, ki se ujemajo z iskanim stringom -->
	<input type="hidden" id="idDrzava" value="" /> <!-- Sem shranimo dobljeni id države. Type je hidden zato, ker uporabnika zanima samo vrednost oz rezultat, ne id. -->
	<input type="button" onclick="alert($('#idDrzava').val());" value="Prikazi ključ države" /> <!-- Gumb, kateri alerta id izbrane države. Samo za test, to ni obvezno =) -->
	<script type="text/javascript">
		$(document).ready(function() {
			$("#drzave").autocomplete({ // $('nek selektor, ki unikatno določa polje, v katerega vnašamo dinamično iskanje').autocomplete({
		        source: function(request, response) { // Doličimo, iz kje polje črpa. Funkcija mora imeti obvezno 2 argumenta, samo z response ne deluje
		            $.post("TestAutocompleteServlet", {query: $('#drzave').val()}, function(json) { // argumenti: servlet, {podatki v JSON (samo query)}, funkcija(objekt odgovor) {
		                    response(json); // Kaj se izvede, ko dobimo rezultat. Tukaj bi lahko še kucali kodo. Kaj točno naredi ta vrsica še meni ni jasno, tko da sm prepište :D
		            }, "json"); // in še zadnji argument, tip prenosa
		        },
		        select: function(request, response) { // Kaj se zgodi, ko izberemo neko izbiro (zopet obvezno 2 argumenta)
	    			$('#idDrzava').val(response.item.id); // V input hidden shranimo id oz ključ
				}
			});
		});
	</script>
	
	<br />
	<br />
	Drag Drop
	<form action='VpisniListServlet' method='post' class="forme" style="width: 750px; margin-left: 170px;">
		<table>
			<tr>
				<th>Vpisna številka</th>
				<td>
					<input type="text" name="vpisnaStevilka" maxlength="8" value="Vpisna nima veze, samo da vidim, kako pride v formi =)"/>
				</td>
				<td class="error"></td>
			</tr>
			<tr>
				<th colspan="2">Predmetnik</th>
				<td class="error"></td>
			</tr>
			<tr>
				<td colspan="2" class="dragDrop"> <!-- Class je obvezen, drugače je stil v 3 krasnih. Pazite na colspane, glede na to, kok polj maste v vrstici tabele. -->
					<div id="drag"> <!-- Polje, iz katerega dragamo -->
			            <div id="1">Postavka št. 1</div> <!-- id nadomeščajo value, kakor jih imamo v input poljih -->
			            <div id="2">Postavka št. 2</div>
			            <div id="3">Postavka št. 3</div>
			            <div id="4">Postavka št. 4 je fuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuul dolga</div>
			            <div id="5">Postavka št. 5</div>
			            <div id="6">Postavka št. 6</div>
			            <div id="7">Postavka št. 7</div>
			            <div id="8">Postavka št. 8</div>
			            <div id="9">Postavka št. 9</div>
			            <div id="10">Postavka št. 10</div>
			        </div>
			        <div id="drop"></div> <!-- Polje, kamor dropamo -->
			        <input type="hidden" name="izbire" /> <!-- Sem shranjujemo id izbire. Izbire so ločene z ',' -->
				</td>
				<td class="error"></td>
			</tr>
			<tr>
				<td colspan="2" class="gumbi"><div><input type='button' onclick="alert($('input[name=izbire]').val());" value='PRIKAŽI IZBIRE' /></div></td> <!-- samo za test -->
				<td></td>
			</tr>
		</table>
	</form>
</div>
<script type="text/javascript">
	$('.forme th').css('width', '130px');
	$('.error').css('width', '200px');
	$('#domovDiv').hide();
</script>