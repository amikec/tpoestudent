<%@page import="si.fri.estudent.utilities.Constants"%>
<%@page import="si.fri.estudent.jpa.Vloga_oseba"%>
<%@page import="si.fri.estudent.jpa.Oseba"%>
<%@page import="si.fri.estudent.jpa.Vrsta_studija"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
boolean izpis = false;
boolean izpisOcen = false;

if (request.getParameter("izpis") != null)
	   izpis = true;

if (request.getParameter("izpisOcena") != null && request.getParameter("izpisOcena").toString().length() > 0)
	   izpisOcen = true;

Oseba profesor = null;
Oseba os = (Oseba) session.getAttribute("uporabnik");
Vloga_oseba vloga = (Vloga_oseba) session.getAttribute("vloga");
		
if(vloga.getIdVlogaOseba() == Constants.VLOGA_UCITELJ){
	profesor = os;
}else if(vloga.getIdVlogaOseba() != Constants.VLOGA_REFERENTKA){
	response.sendRedirect("index.jsp/stran=prijava");
}

List<Vrsta_studija> vrsteStudija = null;
if (request.getAttribute("vrsteStudija") != null) {
	vrsteStudija = (List<Vrsta_studija>)request.getAttribute("vrsteStudija");
}

if(izpisOcen)
	out.print("<h1>Izpis rezultatov pisnega dela izpita</h1>");
else if(izpis)
	out.print("<h1>Seznam prijavljenih kandidatov</h1>");
else
	out.print("<h1>Vnos rezultatov pisnega dela izpita</h1>");
%>


<form action="PregledPrijavljenihKandidatovAjaxServlet" method="post" class="forme" style="width: 690px; margin-left: 180px;">
	<input type="hidden" id="izpis" name="izpis" value="<%= izpis %>" />
	<input type="hidden" id="izpisOcena" name="izpisOcena" value="<%= izpisOcen %>" />
	<table>
		<% if (request.getAttribute("error") != null) { %>
			<tr>
				<td colspan="2" class="success"><%= (String)request.getAttribute("error") %></td>
				<td></td>
			</tr>
		<% } %>
		<tr>
			<th>Profesor</th>
			<td>
				<input type="text" name="profesor" 
					<% if (profesor != null) {
						out.print("readonly='readonly'");
						
						String imeProfesor = null;
						if (profesor.getPriimekDekliski() == null || profesor.getPriimekDekliski().equals("")) {
							imeProfesor = profesor.getIme() + " " + profesor.getPriimek();
						} else {
							imeProfesor = profesor.getIme() + " " + profesor.getPriimekDekliski() + " " + profesor.getPriimek();
						}
						out.print("value='" + imeProfesor + "'");
					} %>
				/>
				<input type="hidden" name="idProfesor"
					<% if (profesor != null) {
						out.print("value='" + profesor.getIdOseba() + "'");
					} %>
				/>
			</td>
			<td class="error"></td>
		</tr>
		<tr>
			<th>Vrsta študija</th>
			<td>
				<select name="vrstaStudijaKand">
					<%
					if (vrsteStudija.size() > 0) { %>
						<option value='0'>-- Izberi vrsto študija --</option>
						<%
						
						for (Vrsta_studija vs : vrsteStudija) { %>
							<option value='<%= vs.getIdVrstaStudija() %>'><%= vs.getKraticaVrstaStudija() + " - " + vs.getNazivVrstaStudija() %></option>
						<% }
					} %>
				</select>
			</td>
			<td></td>
		</tr>
		<tr>
			<th>Študijski program</th>
			<td>
				<select name="studijskiProgramKand">
				</select>
			</td>
			<td></td>
		</tr>
		<tr>
			<th>Studijska smer</th>
			<td>
				<select name="studijskaSmerKand">
				</select>
			</td>
			<td></td>
		</tr>
		<tr>
			<th>Študijsko leto</th>
			<td>
				<select name="studijskoLetoKand" disabled="disabled">
					<option value="0">-- Izberite študijsko leto --</option>
					<option value="2011/2012">2011/2012</option>
					<option value="2012/2013">2012/2013</option>
					<option value="2013/2014">2013/2014</option>
				</select>
			</td>
			<td></td>
		</tr>
		<tr>
			<th>Letnik</th>
			<td>
				<select name="letnikKand">
				</select>
			</td>
			<td></td>
		</tr>
		<tr>
			<th>Predmet</th>
			<td>
				<select name="predmetKand">
				</select>
			</td>
			<td></td>
		</tr>
		<tr>
			<th>Izpitni rok</th>
			<td>
				<select name="izpitniRokKand">
				</select>
			</td>
			<td></td>
		</tr>
	</table>
</form>
<div id="seznamPrijavljenih">

</div>
<div class="gumbi" style="width: 865px; text-align: right; padding: 0px; margin: 0px">
	
	<% if(!izpis && !izpisOcen){ %>
		<a href="#" id="posodobiOcene">POSODOBI OCENE</a>
	<%}%>
		<a id="tiskanje">IZPIS ZA TISKANJE</a>
	
</div>
<script type="text/javascript">
	$('.forme th').css('width', '120px');
	$('.error').css('width', '140px');
	$('.gumbi').hide();
</script>