INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63001,'﻿Osnove programiranja','6',1);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63002,'Osnove matematične analize','6',1);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63003,'Diskretne strukture','6',1);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63004,'Osnove digitalnih vezij','6',1);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63005,'Fizika','6',1);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63006,'Programiranje in algoritmi','6',1);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63007,'Linearna algebra','6',1);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63008,'Osnove podatkovnih baz','6',1);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63009,'Računalniške komunikacije','6',1);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63010,'Komunikacija človek računalnik','6',1);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63011,'Algoritmi in podatkovne strukture','6',2);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63012,'Arhitektura računalniških sistemov','6',2);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63013,'Verjetnost in statistika','6',2);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63014,'Osnove umetne inteligence','6',2);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63015,'Osnove informacijskih sistemov','6',2);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63016,'Teorija informacij in sistemov','6',2);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63017,'Operacijski sistemi','6',2);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63018,'Organizacija računalniških sistemov','6',2);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63019,'Strokovni izbirni predmet','6',2);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63020,'Prosto izbirni predmet','6',2);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63021,'Matematično modeliranje','6',2);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63022,'Principi programskih jezikov','6',2);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63023,'Računalniške tehnologije','6',2);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63024,'Komuniciranje in vodenje projektov','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63025,'Ekonomika in podjetništvo','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63026,'Modul I','18',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63027,'ModuI II','18',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63028,'Prosto izbirni predmet','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63029,'Diplomska naloga','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63030,'Elektronsko poslovanje','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63031,'Poslovna inteligenca','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63032,'Organizacija in management','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63033,'Razvoj informacijskih sistemov','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63034,'Tehnologija upravljanja podatkov','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63035,'Planiranje in upravljanje informatike','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63036,'Postopki razvoja programske opreme','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63037,'Spletno programiranje','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63038,'Tehnologija programske opreme','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63039,'Modeliranje računalniških omrežij','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63040,'Komunikacijski protokoli','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63041,'Brezžična in mobilna omrežja','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63042,'Digitalno načrtovanje','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63043,'Porazdeljeni sistemi','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63044,'Zanesljivost in zmogljivost računalniških sistemov','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63045,'Računska zahtevnost in hevristično programiranje','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63046,'Sistemska programska oprema','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63047,'Prevajalniki','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63048,'Inteligentni sistemi','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63049,'Umetno zaznavanje','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63050,'Razvoj inteligentnih sistemov','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63051,'Računalniška grafika in tehnologija iger (B-UNI)','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63052,'Multimedijski sistemi','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63053,'Osnove oblikovanja','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63054,'Angleški jezik nivo A','3',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63055,'Angleški jezik nivo B','3',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63056,'Angleški jezik nivo C','3',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63057,'Izbrana poglavja iz računalništva in informatike','6',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63058,'Računalništvo v praksi I','3',3);
INSERT INTO Predmet(sifra_predmeta, naziv_predmet, kreditne_tocke,fk_letnik) VALUES(63059,'Računalništvo v praksi II','3',3);

INSERT into Modul (naziv_modul) values ('Informacijski sistemi');
INSERT into Modul (naziv_modul) values ('Obvladovanje informatike');
INSERT into Modul (naziv_modul) values ('Razvoj programske opreme');
INSERT into Modul (naziv_modul) values ('Računalniška omrežja');
INSERT into Modul (naziv_modul) values ('Računalniški sistemi');
INSERT into Modul (naziv_modul) values ('Algoritmi in sistemski programi');
INSERT into Modul (naziv_modul) values ('Umetna inteligenca');
INSERT into Modul (naziv_modul) values ('Medijske tehnologije');

INSERT INTO  `skupinaTPO10`.`Ocena` (`fk_predmetnik_letnika` ,`fk_izpitni_rok` ,`ocena` ,`id_ocena`)VALUES ('2',  '2',  '9',  '2');