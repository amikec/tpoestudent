package si.fri.estudent.servlets;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import si.fri.estudent.jpa.Oseba;
import si.fri.estudent.jpa.Vloga_oseba;
import si.fri.estudent.jpa.Vrsta_studija;
import si.fri.estudent.utilities.Constants;

/**
 * Servlet implementation class VnosKoncnihOcenServlet
 */
@WebServlet("/VnosKoncnihOcenServlet")
public class VnosKoncnihOcenServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Oseba profesor = null;
    private boolean izpis = false;
    private EntityManager em = null;
    
    private EntityManager getEntityManager(){
    	if(em == null || em.isOpen() == false)
    		em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
    	
    	return em;
    }
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VnosKoncnihOcenServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		HttpSession session = request.getSession();
		Oseba os = (Oseba) session.getAttribute("uporabnik");
		Vloga_oseba vloga = (Vloga_oseba) session.getAttribute("vloga");
		
		if(vloga != null && vloga.getIdVlogaOseba() == Constants.VLOGA_UCITELJ){
			profesor = os;
		} else if(vloga != null && vloga.getIdVlogaOseba() != Constants.VLOGA_REFERENTKA) {
			response.sendRedirect("index.jsp?stran=login");
			return;
		}
		
		List<Vrsta_studija> vrstaStudija = vrniVrsteStudija();
		if(vrstaStudija != null) {
			request.setAttribute("vrsteStudija", vrstaStudija);
		}
		
		RequestDispatcher fw = request.getRequestDispatcher("index.jsp?stran=vnosKoncnihOcen");
		fw.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	private List<Vrsta_studija> vrniVrsteStudija() {
		if(profesor == null) {
			return new LinkedList<Vrsta_studija>();
		}
			
		try{
			String sql = "SELECT " +
								"distinct vrSt " +
						  "FROM " +
						  		"Vrsta_studija vrSt, Studij st, Letnik l, Predmetnik_letnika predL, " +
						  		"Predmet pr, Predmet_izvajalec prIz, Kombinacije_izvajalcev kombIz, " +
						  		"Osebje os " +
					  	  "WHERE " +
					  	  		"vrSt.idVrstaStudija = predL.letnik.studij.vrstaStudija.idVrstaStudija AND " +
					  	  		"predl.predmet.idPredmeta = prIz.predmet.idPredmeta AND " +
					  	  		"prIz.kombinacijeIzvajalcev = kombIz AND " +
					  	  		"( " +
					  	  			" kombIZ.osebje1 = os OR kombIz.osebje2 = os OR kombIz.osebje3 = os " +
					  	  		") " +
					  	  		" AND os.oseba = :profesor ";
					  	  		
			Query query = this.getEntityManager().createQuery(sql);
			query.setParameter("profesor", profesor);
			
			List<Vrsta_studija> vrsteStudija = query.getResultList();
			
			return vrsteStudija;

		}catch(Exception e){
			e.printStackTrace();
			return null;
		}	
	}	
}
