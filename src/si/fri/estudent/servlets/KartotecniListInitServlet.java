package si.fri.estudent.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import si.fri.estudent.jpa.Oseba;
import si.fri.estudent.jpa.Student;
import si.fri.estudent.jpa.Studijski_program;
import si.fri.estudent.jpa.Vpisni_list;
import si.fri.estudent.jpa.Vrsta_studija;
import si.fri.estudent.utilities.SkupenProgram;

/**
 * Servlet implementation class KartotecniListInitServlet
 */
@WebServlet("/KartotecniListInitServlet")
public class KartotecniListInitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public KartotecniListInitServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				
		Oseba o=(Oseba)request.getSession().getAttribute("uporabnik");		
		if(o==null){
			RequestDispatcher nov=request.getRequestDispatcher("index.jsp");
			nov.forward(request, response);
			return;
		}
		Student s=null;
		
		System.out.println("oseba"+o.getIme());
		try{
			if(request.getSession().getAttribute("studentIzIskanja")!=null){
	            s=(Student)request.getSession().getAttribute("studentIzIskanja");
	        }
			
			
			
				if(s!=null){
					//referentka
				}else{	
					List<Student> studenti=o.getStudents();
					if(studenti.size()==0){
						throw new  Exception();
					}
					s=o.getStudents().get(0);
				}
				List<Vpisni_list> vpisniListi=s.getVpisniLists();
				
				if(vpisniListi.size()==0){
					throw new Exception();
				}
				Set programi=new HashSet();
				List<SkupenProgram> skupenProgram=new ArrayList<SkupenProgram>();
				
				for(Vpisni_list vl:vpisniListi){
					Vrsta_studija vs=vl.getLetnik().getStudij().getVrstaStudija();
					Studijski_program sp=vl.getLetnik().getStudij().getStudijskiProgram();				
					
					boolean vstavljen=programi.add(vs.getKraticaVrstaStudija()+sp.getKraticaStudijskiProgram());
					if(vstavljen){
						skupenProgram.add(new SkupenProgram(sp, vs));
					}
					
				}
				request.setAttribute("skupenProgram", skupenProgram);
				request.getSession().setAttribute("skupenProgram", skupenProgram);
				System.out.println("skupen program je shranjen v sejo velikost: "+skupenProgram.size());
				RequestDispatcher nov=request.getRequestDispatcher("index.jsp?stran=kartotecniList");
				nov.forward(request, response);
				return;
			
			
		}catch (Exception e) {
			System.out.println("napaka "+e);
			RequestDispatcher nov=request.getRequestDispatcher("index.jsp");
			nov.forward(request, response);
			return;
			
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}


