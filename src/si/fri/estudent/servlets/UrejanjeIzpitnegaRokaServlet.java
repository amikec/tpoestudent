package si.fri.estudent.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import si.fri.estudent.jpa.Izpitni_rok;
import si.fri.estudent.utilities.DataValidator;
import si.fri.estudent.utilities.Utilities;

/**
 * Servlet implementation class UrejanjeIzpitnegaRokaServlet
 */
@WebServlet("/UrejanjeIzpitnegaRokaServlet")
public class UrejanjeIzpitnegaRokaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    EntityManager em;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UrejanjeIzpitnegaRokaServlet() {
        super();
        em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String posodobi = request.getParameter("posodabljanje");
		
		// če gre za polnenje podatkov
		if(posodobi.compareTo("yes") != 0) {
			
			JSONObject json = new JSONObject();
			
			int idIzpitnegaRoka = Integer.parseInt(request.getParameter("izpitniRok"));
			
			try{
				
				em.getTransaction().begin();
				
				Izpitni_rok ir = em.find(Izpitni_rok.class, idIzpitnegaRoka);
						
				String[] datumCas = Utilities.getDatumIzpita(ir.getDatumIzpit()).split(" ");
			
				json.put("datum", datumCas[0]);
				String cas = datumCas[1];
				if(datumCas[1].length() < 5) {
					 cas = "0" + datumCas[1];
				}
				json.put("cas", cas);
				json.put("predavalnica", ir.getPredavalnica());
				json.put("maksStPrijav", ir.getMaxStPrijav());
				json.put("mozneTocke", ir.getMozneTocke());
				json.put("mejaZaPozitivno", ir.getMejaZaPozitivno());
			
				em.getTransaction().commit();
				
			}catch (Exception e) {
				System.out.println(e.getMessage());
				em.getTransaction().rollback();
			}
			
			response.setContentType("text/html; charset=utf-8");   
			PrintWriter out = response.getWriter();
			out.print(json);
			
		// če gre za posodabljnaje izpitnega roka	
		} else {
			
			int idIzpitnegaRoka = Integer.parseInt(request.getParameter("idIzpitnegaRoka"));
			
			// branje podatkov iz forme
			String datum  = request.getParameter("datumIz");
			String cas = request.getParameter("cas");
			String predavalnica = request.getParameter("predavalnica");
			String mozneTocke = request.getParameter("mozneTocke");
			String maksStPrijav  = request.getParameter("maksStPrijav");
			String mejaZaPozitivno = request.getParameter("mejaZaPozitivno");
			
			
			DataValidator validator = new DataValidator();

			// Preverjanje vnosnih polj
			
			if (!validator.validateInputText(datum, 10, false, "^(0[1-9]|[12][0-9]|3[01]).(0[1-9]|1[012]).(19|20)\\d\\d$", "Prosim izberite datum", "Neveljanven datum", "Neveljaven datum")) {
				request.setAttribute("errorDatum", validator.getError());
			}
			if (!validator.validateInputText(cas, 5, false, "^([01][0-9]|2[0-3]):([0-5][0-9])$", "Prosim izberite čas", "Neveljaven čas", "Neveljaven čas")) {
				request.setAttribute("errorCas", validator.getError());
			}
			if (!validator.validateInputText(predavalnica, 10, false, "^[0-9A-ZČĆŽŠĐ\\-]{1,10}$", "Prosimo vnesite predavalnico", "Nevaljavo ime predavalnice", "Neveljavno ime predavalnica")) {
				request.setAttribute("errorPredavalnica", validator.getError());
			}
			if (!validator.validateInputText(maksStPrijav, 4, false, "^[0-9]{1,4}$", "Prosimo vnesite maksimalno število prijav", "Nevaljavo število", "Neveljavno število")) {
				request.setAttribute("errorMaksStPrijav", validator.getError());
			}
			if (!validator.validateInputText(mozneTocke, 4, false, "^[0-9]{1,4}$", "Prosimo vnesite število možnih točk", "Nevaljavo število možnih točk", "Neveljavno število možnih točk")) {
				request.setAttribute("errorMozneTocke", validator.getError());
			}
			if (!validator.validateInputText(mejaZaPozitivno, 4, false, "^[0-9]{1,4}$", "Prosimo vnesite mejo za pozitivno", "Nevaljava meja za pozitivno", "Neveljavna meja za pozitvino")) {
				request.setAttribute("errorMejaZaPozitivno", validator.getError());
			}
			
			
			// Generiranje timestampa z prebranim datumom in časom
			DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
			Date date = null;
			Date danRoka = new Date();
			long time = 0;
			try {
				date = dateFormat.parse(datum);
				danRoka.setTime(date.getTime());
				String[] razbitCas = cas.split(":"); 
				date.setHours(Integer.parseInt(razbitCas[0]));
				date.setMinutes(Integer.parseInt(razbitCas[1]));
				time = date.getTime();
				
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Timestamp timestamp = new Timestamp(time);
			
			try {
				
				em.getTransaction().begin();
				
				Izpitni_rok ir = em.find(Izpitni_rok.class, idIzpitnegaRoka);
				
				ir.setDatumIzpit(timestamp);
				ir.setMaxStPrijav(Integer.parseInt(maksStPrijav));
				ir.setMejaZaPozitivno(Integer.parseInt(mejaZaPozitivno));
				ir.setMozneTocke(Integer.parseInt(mozneTocke));
				ir.setPredavalnica(predavalnica);
				
				em.merge(ir);
				em.getTransaction().commit();
				
				request.setAttribute("uspeh", "Posodobitev uspešna");
			}catch (Exception e) {
				em.getTransaction().rollback();
				System.out.println(e.getLocalizedMessage());
				request.setAttribute("uspeh", "<span class='error'>Napaka pri vnosu</span>");
			}
			
			RequestDispatcher fw = request.getRequestDispatcher("index.jsp?stran=spremembaIzpitnegaRoka");
			fw.forward(request, response);
			
		}
	}

}
