package si.fri.estudent.servlets;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import si.fri.estudent.jpa.Izpitni_rok;
import si.fri.estudent.jpa.Ocena;
import si.fri.estudent.jpa.Oseba;
import si.fri.estudent.jpa.Student;

/**
 * Servlet implementation class OdjavaIzpitServlet
 */
@WebServlet("/OdjavaIzpitServlet")
public class OdjavaIzpitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private Oseba oseba;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OdjavaIzpitServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    private EntityManager em;
    private EntityManager getEntityManager(){
    	if(em == null || !em.isOpen())
    		em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
    	
    	return em;
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		
		if(session.getAttribute("studentIzIskanja") != null){
			Student student = (Student) session.getAttribute("studentIzIskanja");
			oseba = student.getOseba();
			request.setAttribute("referentka", "true");
		}else{
			oseba = (Oseba) session.getAttribute("uporabnik");
		}
		
		if(request.getParameter("mem") != null){
			hendlajOdjavo(request.getParameter("mem"));
		}
			
		request.setAttribute("roki", vrniAktivneIzpitneRoke());
		RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp?stran=odjavaIzpitStudent");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}
	
	private List<Izpitni_rok> vrniAktivneIzpitneRoke(){
		
		List<Izpitni_rok> roki = null;
		
		try{
			String sql = "SELECT "+
					"distinct ir " +
				"FROM "+
					"Oseba o, Student s, Vpisni_list vl, Predmetnik pr, Predmetnik_letnika prl, Ocena oc, Predmet p, Izpitni_rok ir "+
				"WHERE " +
					"s.oseba.idOseba = ?1 AND " +
					"s.idStudent = vl.student.idStudent AND " +
					"vl.idVpisni = pr.fkVpisniList AND " +
					"pr.idPredmetnik = oc.fkPredmetnik AND " +
					"oc.fkIzpitniRok = ir.idRok AND " +
					"pr.fkPredmetnikLetnika = prl.idPredmetnikLetnika AND " +
					"prl.predmet.idPredmeta = p.idPredmeta AND " +
					"oc.aktivna = 1 AND " +
					"oc.ocena = 0 " +
				"ORDER BY " +
					"ir.datumIzpit";
			
			Query query = this.getEntityManager().createQuery(sql);
			query.setParameter(1, oseba.getIdOseba());
			
			roki = query.getResultList();
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return roki;
	}
	
	private void hendlajOdjavo(String rokId){
		
		int rok = Integer.parseInt(rokId);
		
		try{
			
			em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
			em.getTransaction().begin();
			
			String sql = "SELECT "+
					"distinct oc " +
				"FROM "+
					"Oseba o, Student s, Vpisni_list vl, Predmetnik pr, Izpitni_rok ir, Ocena oc "+
				"WHERE " +
					"s.oseba.idOseba = ?2 AND " +
					"s.idStudent = vl.student.idStudent AND " +
					"vl.idVpisni = pr.fkVpisniList AND " +
					"pr.idPredmetnik = oc.fkPredmetnik AND " +
					"oc.fkIzpitniRok = ir.idRok AND " +
					"ir.idRok = ?1 AND " +
					"ir.status = 1 AND oc.aktivna = 1 AND oc.ocena = 0";
			
			Query query = this.getEntityManager().createQuery(sql);
			query.setParameter(1, rok);
			query.setParameter(2, oseba.getIdOseba());
			Ocena prijava = (Ocena) query.getSingleResult();
			
			prijava.setAktivna(false);
			
			this.getEntityManager().merge(prijava);
			this.getEntityManager().getTransaction().commit();
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
