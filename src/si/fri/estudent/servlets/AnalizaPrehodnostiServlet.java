package si.fri.estudent.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import si.fri.estudent.utilities.Tiskanje;

/**
 * Servlet implementation class AnalizaPrehodnosti
 */
@WebServlet("/AnalizaPrehodnostiServlet")
public class AnalizaPrehodnostiServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AnalizaPrehodnostiServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession seja=request.getSession(); // seja za shrant za tiskanje
		String studijskoLeto=(String)seja.getAttribute("leto1");
		String naslednjeLeto=(String)seja.getAttribute("leto2");
		String[][] tabela=(String[][])seja.getAttribute("tabela");
		String letnik=(String)seja.getAttribute("letnik");
		String program=(String)seja.getAttribute("program");
		String nacin=(String)seja.getAttribute("nacin");
		String vrsta=(String)seja.getAttribute("vrsta");		
		
		if(studijskoLeto!=null && naslednjeLeto!= null && tabela!=null){
		
			Tiskanje tiskanje=new Tiskanje(request, response, "Prehodnost iz "+letnik+".letnika v naslednji letnik v štud. letu "+studijskoLeto);
			tiskanje.dodajBesediloBrezProsteVrstice("Program: "+program);
			tiskanje.dodajBesediloBrezProsteVrstice("Način: "+nacin);
			tiskanje.dodajBesediloBrezProsteVrstice("Vrsta: "+vrsta);
			
			tiskanje.dodajPraznoVrstico();
			
			tiskanje.dodajTabeloAnalizaPrehodnost(tabela,studijskoLeto,naslednjeLeto);
			
			tiskanje.dobiDocument();
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		EntityManager em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		System.out.println("post");
		String letnik=request.getParameter("letnik");
		String studijskoLeto=request.getParameter("studijskoLeto");
		String programiInSmeri=request.getParameter("studijskiProgram");
		String nacinStudija=request.getParameter("nacinStudija");
		String vrstaVpisa=request.getParameter("vrstaVpisa");
		
		int studijskoLetoInt=Integer.parseInt(studijskoLeto.substring(0,4));
		
		System.out.println("let "+letnik +" "+studijskoLeto +" " +programiInSmeri+" "+nacinStudija+" "+vrstaVpisa);
		
		if(letnik==null || studijskoLeto == null || programiInSmeri == null || nacinStudija == null || vrstaVpisa == null){
			System.out.println("napaka");
			return;			
		}
		
		/////////////////////////////
		//gradnja sql stringa
		
		String sql="SELECT COUNT(DISTINCT st) " +
				" FROM Student st, Vpisni_list vl, Vpisni_list vl2, Letnik le, Studij studij, " +
				" Studijska_smer ss, Vrsta_studija vs, Studijski_program sp, Vrsta_vpisa vv " +
				" WHERE vl.student = st AND ";		
		
		sql+="le = vl.letnik AND ";
		sql+="le.nazivLetnik= :letnik AND "; //pogoj za letnik 
		sql+="vl.studijskoLeto = :studijskoLeto "; // pogoj za studijsko leto
		
		if(!programiInSmeri.equals("Ne glede")){ // ce je ne glede potem ne dodamo pogaja v sql			
			sql+=" AND studij = le.studij ";
			sql+=" AND sp= studij.studijskiProgram ";
			//sql+=" AND ss= studij.studijskaSmer ";	
			sql+=" AND vs= studij.vrstaStudija ";
			
			sql+=" AND vs.nazivVrstaStudija = :vrstaStudija ";
			sql+=" AND sp.nazivStudijskiProgram = :studijskiProgram ";
		}
		if(!nacinStudija.equals("Ne glede")){
			sql+=" AND vl.nacinStudija = :nacinStudija ";
		}
		if(!vrstaVpisa.equals("Ne glede")){
			sql+=" AND vv = vl.vrstaVpisa ";
			sql+=" AND vv.kraticaVrstaVpisa = :vrstaVpisa ";
		}
		
		
		
		////dodatne zahteve za vpisane tiste ki so napredovali
		String naslednjeLeto=""+(studijskoLetoInt+1)+"/"+(studijskoLetoInt+2);
		String naslednjiLetnik=""+(Integer.parseInt(letnik.substring(0, 1))+1);
		String sqlNapredovali=sql+ " AND vl2.studijskoLeto = :naslednjeStudijskoLeto " +
				" AND vl2.student = st " +
				" AND vl2.letnik.nazivLetnik = :naslednjiLetnik ";
		
	    ////dodatne zahteve za tiste ki so ponavljali		
		String sqlPonavljali=sql+ " AND vl2.studijskoLeto = :naslednjeStudijskoLeto " +
				" AND vl2.student = st " +
				" AND vl2.letnik.nazivLetnik = :istiLetnik ";
		
		////vsi vpisani (vse ki so nekam vpisani na tem faks in so bli prejsnje leto v zahtevanem programu)
		String sqlVsi=sql +" AND vl2.student = st " +				
				" AND vl2.studijskoLeto = :naslednjeStudijskoLeto ";
		
		
		////------------------------		
		//// gradnja queryja ( dodajanje parametrov v query)		
		
		Query queryVpisani=em.createQuery(sql);
		Query queryNapredovali=em.createQuery(sqlNapredovali);
		Query queryPonavljali=em.createQuery(sqlPonavljali);
		Query queryVsi=em.createQuery(sqlVsi);
		
		queryVpisani.setParameter("letnik", letnik);
		queryVpisani.setParameter("studijskoLeto", studijskoLeto);
		queryNapredovali.setParameter("letnik", letnik);
		queryNapredovali.setParameter("studijskoLeto", studijskoLeto);
		queryPonavljali.setParameter("letnik", letnik);
		queryPonavljali.setParameter("studijskoLeto", studijskoLeto);
		queryVsi.setParameter("letnik", letnik);
		queryVsi.setParameter("studijskoLeto", studijskoLeto);
		
		
		if(!programiInSmeri.equals("Ne glede")){
			String vrstaStudija="";
			String studijskiProgram="";
			
			//prevod na zdajsnji sistem baze
			if(programiInSmeri.equals("RAČUNALNIŠTVO IN INFORMATIKA - BUN")){
				vrstaStudija="univerzitetni";
				studijskiProgram="Računalništvo in informatika";
			}else if(programiInSmeri.equals("RAČUNALNIŠTVO IN INFORMATIKA - BVS")){
				vrstaStudija="visokošolski strokovni";
				studijskiProgram="Računalništvo in informatika";
			}else if(programiInSmeri.equals("RAČUNALNIŠTVO IN MATEMATIKA - BUN")){
				vrstaStudija="univerzitetni";
				studijskiProgram="Računalništvo in matematika";
			}
			queryVpisani.setParameter("vrstaStudija", vrstaStudija);
			queryVpisani.setParameter("studijskiProgram", studijskiProgram);
			queryNapredovali.setParameter("vrstaStudija", vrstaStudija);
			queryNapredovali.setParameter("studijskiProgram", studijskiProgram);
			queryPonavljali.setParameter("vrstaStudija", vrstaStudija);
			queryPonavljali.setParameter("studijskiProgram", studijskiProgram);
			queryVsi.setParameter("vrstaStudija", vrstaStudija);
			queryVsi.setParameter("studijskiProgram", studijskiProgram);
			
		}
		String nacinStudijaOriginal=nacinStudija;
		if(!nacinStudija.equals("Ne glede")){
			if(nacinStudija.startsWith("R")){
				nacinStudija="R";
			}else if(nacinStudija.startsWith("I")){
				nacinStudija="I";
			} 
			queryVpisani.setParameter("nacinStudija", nacinStudija);
			queryNapredovali.setParameter("nacinStudija", nacinStudija);
			queryPonavljali.setParameter("nacinStudija", nacinStudija);
			queryVsi.setParameter("nacinStudija", nacinStudija);
		}
		if(!vrstaVpisa.equals("Ne glede")){
			//treba kratico to je prvi dve crki
			queryVpisani.setParameter("vrstaVpisa", vrstaVpisa.substring(0, 2));
			queryNapredovali.setParameter("vrstaVpisa", vrstaVpisa.substring(0, 2));
			queryPonavljali.setParameter("vrstaVpisa", vrstaVpisa.substring(0, 2));
			queryVsi.setParameter("vrstaVpisa", vrstaVpisa.substring(0, 2));
		}		
		
		queryNapredovali.setParameter("naslednjeStudijskoLeto",naslednjeLeto );
		queryPonavljali.setParameter("naslednjeStudijskoLeto",naslednjeLeto );
		queryVsi.setParameter("naslednjeStudijskoLeto",naslednjeLeto );
		
		queryNapredovali.setParameter("naslednjiLetnik",naslednjiLetnik );
		queryPonavljali.setParameter("istiLetnik",letnik );		
		
		
		//rezultati
		long steviloZacetnih=(Long)queryVpisani.getSingleResult();
		long steviloNapredovali=(Long)queryNapredovali.getSingleResult();
		long steviloPonavljali=(Long)queryPonavljali.getSingleResult();
		long steviloVsi=(Long)queryVsi.getSingleResult();
		long steviloNevpisani=steviloZacetnih-steviloVsi;
		
		long delitelj=steviloZacetnih;
		if(delitelj==0){//da ne delimo z nic
			delitelj=1;
		}
		
		String procentiZacetnih=new DecimalFormat("0.00").format(100.00f);
		String procentiNapredovali=new DecimalFormat("0.00").format(((float)(steviloNapredovali*100))/delitelj);
		String procentiPonavljali=new DecimalFormat("0.00").format(((float)(steviloPonavljali*100))/delitelj);
		String procentiNevpisani=new DecimalFormat("0.00").format(((float)(steviloNevpisani*100))/delitelj);
		System.out.println(procentiNapredovali+" "+procentiPonavljali + steviloZacetnih);
		
		String[] prvaVrstica={"Vpisani v "+letnik.substring(0, 1)+". letnik",""+steviloZacetnih,""+procentiZacetnih+" %"};
		String[] drugaVrstica={"Ponovni vpis v "+letnik.substring(0, 1)+". letnik",""+steviloPonavljali,""+procentiPonavljali+" %"};
		String[] tretjaVrstica={"Nevpisani ",""+steviloNevpisani,""+procentiNevpisani+" %"};
		String[] cetrtaVrstica=new String[3];
		
		if(steviloNapredovali==0 && steviloZacetnih!=0){
			long steviloDrugam=steviloVsi - steviloPonavljali;
			String procentiDrugam=new DecimalFormat("0.00").format(((float)(steviloDrugam*100))/delitelj);
			
			cetrtaVrstica[0]="Vpisani na drug program ";
			cetrtaVrstica[1]=""+steviloDrugam;
			cetrtaVrstica[2]=""+procentiDrugam +" %";
		}else{
			cetrtaVrstica[0]="Napredovali v "+naslednjiLetnik+". letnik";
			cetrtaVrstica[1]=""+steviloNapredovali;
			cetrtaVrstica[2]=""+procentiNapredovali +" %";
		}
		
		String[][] tab={prvaVrstica,drugaVrstica,tretjaVrstica,cetrtaVrstica};
		
		request.setAttribute("leto1",studijskoLeto);
		request.setAttribute("leto2",naslednjeLeto);
		request.setAttribute("tabela", tab);
		
		HttpSession seja=request.getSession(); // seja za shrant za tiskanje
		seja.setAttribute("leto1",studijskoLeto);
		seja.setAttribute("leto2",naslednjeLeto);
		seja.setAttribute("tabela", tab);
		seja.setAttribute("letnik", letnik);
		seja.setAttribute("program", programiInSmeri);
		seja.setAttribute("nacin", nacinStudijaOriginal);
		seja.setAttribute("vrsta", vrstaVpisa);
		
		RequestDispatcher nov=request.getRequestDispatcher("Ajax/analizaPrehodnostiAjax.jsp");
		nov.forward(request, response);
		System.out.println("poslano");
		return;
		
		
		/*response.setContentType("text/html; charset=utf-8");  
		PrintWriter out = response.getWriter();
		out.print("heheheee " +steviloZacetnih +" "+steviloNapredovali +" "+steviloPonavljali+" "+steviloVsi);*/
	}


}
