package si.fri.estudent.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Servlet implementation class TiskanjeTest
 */
@WebServlet("/odjavaServlet")
public class odjavaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public odjavaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		HttpSession mySession = request.getSession(false);
        if (mySession != null)
        {
        			//mySession.removeAttribute("username");
        			//mySession.removeAttribute("password");
                    // Invalidate the Session Here !!!!!
                    mySession.invalidate();
                    Cookie[] myCookies = request.getCookies();
                    for(int i=0; i < myCookies.length; i++){
                    	myCookies[i].setMaxAge(0);
                    	
                    }
                    // Invalidate the Session Here !!!!!
        }
        response.sendRedirect("index.jsp");	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
