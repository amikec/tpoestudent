package si.fri.estudent.servlets;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SeznamVpisanihInitServlet
 */
@WebServlet("/SeznamVpisanihInitServlet")
public class SeznamVpisanihInitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SeznamVpisanihInitServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EntityManager em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		
		//letnik
		String[] letniki=new String[6];
		for(int i=0;i<5;i++){
			letniki[i]=""+(i+1);
		}
		letniki[5]="Ne glede";
		
		//studijsko leto
		String sql="SELECT DISTINCT le.studijskoLeto FROM Letnik AS le ORDER BY le.studijskoLeto";
		Query q=em.createQuery(sql);
		List<String> leta=(List<String>)q.getResultList();
		String[] studijskaLeta=new String[leta.size()+1];
		int i=0;
		for (String leto : leta) {
			studijskaLeta[i]=leto;
			i++;
			//System.out.println("leto "+leto);
		}
		studijskaLeta[leta.size()]="Ne glede";
		
		String[] programiInSmeri={"RAČUNALNIŠTVO IN INFORMATIKA - BUN","RAČUNALNIŠTVO IN INFORMATIKA - BVS",
				"RAČUNALNIŠTVO IN MATEMATIKA - BUN","Ne glede"};
		
		String[] nacinStudija={"Redni študij","Izredni študij","Ne glede"};
		String[] vrstaVpisa={"V1 - prvi vpis v letnik","V2 - ponavljanje letnika","V3 - nadaljevanje letnika","AB - absolvent","Ne glede"};
		
		//smer-modul		
		String sqlmodul="SELECT mo.nazivModul FROM Modul mo";
		q=em.createQuery(sqlmodul);
		List<String> moduli=q.getResultList();
		
		String sqlsmer="SELECT sm.nazivStudijskaSmer FROM Studijska_smer sm";
		q=em.createQuery(sqlsmer);
		List<String> smeri=q.getResultList();
		
		String[] smer_modul=new String[moduli.size()+smeri.size()+2];
		i=0;
		for(String modul:moduli){
			smer_modul[i]=modul;
			i++;
		}
		smer_modul[i]="----------";
		i++;
		for(String smer:smeri){
			smer_modul[i]=smer;
			i++;
		}
		smer_modul[i]="Ne glede";
		
		request.setAttribute("letniki", letniki);
		request.setAttribute("studijskaLeta", studijskaLeta);
		request.setAttribute("programiInSmeri", programiInSmeri);
		request.setAttribute("nacinStudija", nacinStudija);
		request.setAttribute("vrstaVpisa", vrstaVpisa);
		request.setAttribute("smerModul", smer_modul);
		
		RequestDispatcher nov=request.getRequestDispatcher("index.jsp?stran=seznamVpisanih");
		nov.forward(request, response);
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
