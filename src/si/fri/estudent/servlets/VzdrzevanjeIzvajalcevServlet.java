package si.fri.estudent.servlets;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import si.fri.estudent.jpa.Predmet_izvajalec;

/**
 * Servlet implementation class VzdrzevanjeIzvajalcevServlet
 */
@WebServlet("/VzdrzevanjeIzvajalcevServlet")
public class VzdrzevanjeIzvajalcevServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	EntityManager em;
    public VzdrzevanjeIzvajalcevServlet() {
        super();
        em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		if(request.getParameter("id") != null) {
			try {
				em.getTransaction().begin();
				Predmet_izvajalec pi = em.find(Predmet_izvajalec.class, Integer.parseInt(request.getParameter("id")));
				pi.setStatus(false);
				em.persist(pi);
				em.getTransaction().commit();
			} catch (Exception e) {
				em.getTransaction().rollback();
				System.out.println(e.getLocalizedMessage());
			}
		}
		Query q = em.createQuery("SELECT Pi FROM Predmet_izvajalec Pi WHERE Pi.status = true ORDER BY Pi.predmet.nazivPredmet ASC");
	    List<Predmet_izvajalec> predmetIzvajalec = (List<Predmet_izvajalec>)q.getResultList();
	    
	    request.setAttribute("predmetIzvajalec", predmetIzvajalec);
	    
		RequestDispatcher rd=request.getRequestDispatcher("index.jsp?stran=vzdrzevanjeIzvajalcev");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
