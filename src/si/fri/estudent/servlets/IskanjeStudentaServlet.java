package si.fri.estudent.servlets;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import si.fri.estudent.jpa.Student;
import si.fri.estudent.utilities.DataValidator;

/**
 * Servlet implementation class IskanjeStudentaServlet
 */
@WebServlet("/IskanjeStudentaServlet")
public class IskanjeStudentaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IskanjeStudentaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	EntityManager em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
    	String vpisna=request.getParameter("student");
    	String napaka="";
    	String posredujNa=(String)request.getSession().getAttribute("posredujNa");
    	if(posredujNa==null){
    		response.sendRedirect("index.jsp");
    	}
    	
    	DataValidator dv=new DataValidator();
    	boolean kulVpisna=dv.validateInputText(vpisna, 30, false,"^[0-9]{8}$", "Vpisna Številka ni vnesena.", "Vpisna številka je predolga.", "Vpisna številka ni veljavne oblike.");
		if(!kulVpisna){
			napaka+=dv.getError();
			request.setAttribute("napaka", napaka);
			request.setAttribute("nacin", "vpisna");
			RequestDispatcher nov=request.getRequestDispatcher("index.jsp?stran=iskanjeStudenta");
			nov.forward(request, response);
			return;
		}else{
			Student student=null;
			Query q;
			q=em.createQuery("SELECT st FROM Student st WHERE st.vpisnaSt=:vpisna");
			q.setParameter("vpisna", vpisna);
			try{
				student=(Student)q.getSingleResult();		
				
				
			}catch(NoResultException e){
				napaka+="Ni študenta s to vpisno številko.";
			}
			if(student!=null){			
				request.getSession().setAttribute("studentIzIskanja", student);
				
				if(posredujNa!=null){
					
					response.sendRedirect(posredujNa);
				}
			}
		}
		if(napaka.length()>0){
			request.setAttribute("napaka", napaka);
			String url="";
			if(posredujNa!=null){
				 url = "index.jsp?stran=iskanjeStudenta&posredujNa="+posredujNa;
			}else{
				url = "index.jsp";
			}
			RequestDispatcher nov=request.getRequestDispatcher(url);
			nov.forward(request, response);
		}
		
    }
    
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
