package si.fri.estudent.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import si.fri.estudent.jpa.Drzava;
import si.fri.estudent.jpa.Kombinacije_izvajalcev;
import si.fri.estudent.jpa.Kraj;
import si.fri.estudent.jpa.Naslov;
import si.fri.estudent.jpa.Oseba;
import si.fri.estudent.jpa.Predmet;
import si.fri.estudent.jpa.Predmet_izvajalec;
import si.fri.estudent.jpa.Vpisni_list;

/**
 * Servlet implementation class TestirajJpa
 */
@WebServlet("/TestirajJpa")
public class TestirajJpa extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestirajJpa() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("zacetek");
		
		EntityManager em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		Vpisni_list vl=em.find(Vpisni_list.class,1);
		System.out.println("aaa"+vl.getStudent().getVpisnaSt());
		
//		Drzava drzava=em.find(Drzava.class, 1);
//		System.out.println("Drzava: "+drzava.getNazivDrzava());
	
//		Kraj kraj=new Kraj();			
//		kraj.setPostnaSt("1202");
//		kraj.setPosta("mariborčšž");
//		
//		em.getTransaction().begin();
//		em.persist(kraj);
//		em.getTransaction().commit();
//		System.out.println("vstavljen kraj ");
		
//		Query q = em.createQuery("SELECT o FROM Oseba o, Osebje os,	Kombinacije_izvajalcev ki, Predmet_izvajalec pi, Predmet p " +
//	    "WHERE p.sifraPredmeta=:sifra AND p.idPredmeta=pi.predmet " +
//	    "AND pi.kombinacijeIzvajalcev=ki.idKombinacijeIzvajalcev " +
//	    "AND ( ki.osebje1=os.idOsebje  OR ki.osebje2=os.idOsebje OR ki.osebje3=os.idOsebje ) " +
//	    "AND os.oseba=o.idOseba");
//	    q.setParameter("sifra", "63001");
//	    q.getResultList();
		
//	    Query q = em.createQuery("SELECT pr FROM Predmet pr WHERE pr.sifraPredmeta=:sifra");
//	    q.setParameter("sifra", "63003");
//	    Predmet p=(Predmet)q.getSingleResult();
//	    List<Predmet_izvajalec> pi=p.getPredmetIzvajalecs();
//	    Iterator<Predmet_izvajalec> it=pi.iterator();
//	    ArrayList<Oseba> osebe=new ArrayList<Oseba>();
//	    int i=0;
//	    while(it.hasNext()){
//	    	Predmet_izvajalec prei=(Predmet_izvajalec)it.next();
//	    	Kombinacije_izvajalcev ki=prei.getKombinacijeIzvajalcev();
//	    	Oseba os;
//	    	try{
//		    	os=ki.getOsebje1().getOseba();
//		    	if(os!=null){osebe.add(os);i++;}
//	    	}catch (Exception e) {}
//	    	try{
//		    	os=ki.getOsebje2().getOseba();
//		    	if(os!=null){osebe.add(os);i++;}
//	    	}catch (Exception e) {}
//	    	try{
//		    	os=ki.getOsebje3().getOseba();
//		    	if(os!=null){osebe.add(os);i++;}	 
//			}catch (Exception e) {}
//	    }
//	    Iterator<Oseba> iterator=osebe.iterator();
//	    while(iterator.hasNext()){
//	    	Oseba oseba=iterator.next();
//	    	System.out.println("predavatelj"+oseba.getIme()+" "+oseba.getPriimek());
//	    }
//	    System.out.println("naziv"+p.getNazivPredmet()+" st: "+i);
	    
//		
//		Query q=em.createQuery("SELECT kr FROM Kraj kr WHERE kr.nazivKraj='maribor'");
//		List<Kraj> kraji=(List<Kraj>)(q.getResultList());
//		Kraj kr=kraji.get(kraji.size()-1);
		//System.out.println("zadnji kraj "+ kr.getIdKraj()+" "+kr.getPostnaSt()+" "+kr.getNazivKraj());
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
