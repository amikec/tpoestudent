package si.fri.estudent.servlets;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import si.fri.estudent.jpa.Letnik;
import si.fri.estudent.jpa.Modul;
import si.fri.estudent.jpa.Predmetnik;
import si.fri.estudent.jpa.Predmetnik_letnika;
import si.fri.estudent.jpa.Student;
import si.fri.estudent.jpa.Studij;
import si.fri.estudent.jpa.Studijska_smer;
import si.fri.estudent.jpa.Studijski_program;
import si.fri.estudent.jpa.Vpisni_list;
import si.fri.estudent.jpa.Vrsta_studija;
import si.fri.estudent.jpa.Vrsta_vpisa;
import si.fri.estudent.utilities.Tiskanje;

/**
 * Servlet implementation class SeznamVpisanihServlet
 */
@WebServlet("/SeznamVpisanihServlet")
public class SeznamVpisanihServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SeznamVpisanihServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession seja=request.getSession(); // seja za shrant za tiskanje
		String studijskoLeto=(String)seja.getAttribute("leto1");		
		String[][] tabela=(String[][])seja.getAttribute("tabela");
		String letnik=(String)seja.getAttribute("letnik");
		String program=(String)seja.getAttribute("program");
		String nacin=(String)seja.getAttribute("nacin");
		String vrsta=(String)seja.getAttribute("vrsta");
		String smerModul=(String)seja.getAttribute("smerModul");
		
		if(tabela!=null){
		
			Tiskanje tiskanje=new Tiskanje(request, response, "Seznam vpisanih študentov");
			tiskanje.dodajBesediloBrezProsteVrstice("Študijsko leto: "+studijskoLeto);
			tiskanje.dodajBesediloBrezProsteVrstice("Letnik: "+letnik);
			tiskanje.dodajBesediloBrezProsteVrstice("Program: "+program);
			tiskanje.dodajBesediloBrezProsteVrstice("Način: "+nacin);
			tiskanje.dodajBesediloBrezProsteVrstice("Vrsta: "+vrsta);
			tiskanje.dodajBesediloBrezProsteVrstice("Modul oz smer: "+smerModul);
			
			tiskanje.dodajPraznoVrstico();
			
			tiskanje.dodajTabelo(tabela, false, true);
			
			tiskanje.dobiDocument();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		EntityManager em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		//System.out.println("post");
		String letnik=request.getParameter("letnik");
		String studijskoLeto=request.getParameter("studijskoLeto");
		String programiInSmeri=request.getParameter("studijskiProgram");
		String nacinStudija=request.getParameter("nacinStudija");
		String vrstaVpisa=request.getParameter("vrstaVpisa");
		String smerModul=request.getParameter("smerModul");		
		
		int stNeGlede=0;
		
		//System.out.println("let "+letnik +" "+studijskoLeto +" " +programiInSmeri+" "+nacinStudija+" "+vrstaVpisa);
		
		if(letnik==null || studijskoLeto == null || programiInSmeri == null || nacinStudija == null || vrstaVpisa == null){
			//System.out.println("napaka");
			return;			
		}
		
		/////////////////////////////
		//gradnja sql stringa
		
		String sql="SELECT DISTINCT st, vl " +
				" FROM Student st, Vpisni_list vl, Letnik le, Studij studij, Predmetnik predmetnik, " +
				" Studijska_smer ss, Vrsta_studija vs, Studijski_program sp, Vrsta_vpisa vv , Predmetnik_letnika pl, Modul mo " +
				" WHERE vl.student = st AND ";		
		
		sql+="le = vl.letnik ";
		sql+=" AND studij = le.studij "; //prej je blo to v tretjem if-u
		
		if(!letnik.equals("Ne glede")){
			sql+=" AND le.nazivLetnik= '"+letnik+"' "; //pogoj za letnik 
		}else{stNeGlede++;}
		
		if(!studijskoLeto.equals("Ne glede")){
			sql+=" AND vl.studijskoLeto = '"+ studijskoLeto+"' "; // pogoj za studijsko leto
		}else{stNeGlede++;}
			
		if(!programiInSmeri.equals("Ne glede")){ // ce je ne glede potem ne dodamo pogaja v sql			
			//sql+=" AND studij = le.studij ";
			sql+=" AND sp= studij.studijskiProgram ";				
			sql+=" AND vs= studij.vrstaStudija ";
			
			String vrstaStudija="";
			String studijskiProgram="";
			
			//prevod na zdajsnji sistem baze
			if(programiInSmeri.equals("RAČUNALNIŠTVO IN INFORMATIKA - BUN")){
				vrstaStudija="univerzitetni";
				studijskiProgram="Računalništvo in informatika";
			}else if(programiInSmeri.equals("RAČUNALNIŠTVO IN INFORMATIKA - BVS")){
				vrstaStudija="visokošolski strokovni";
				studijskiProgram="Računalništvo in informatika";
			}else if(programiInSmeri.equals("RAČUNALNIŠTVO IN MATEMATIKA - BUN")){
				vrstaStudija="univerzitetni";
				studijskiProgram="Računalništvo in matematika";
			}
			
			sql+=" AND vs.nazivVrstaStudija = '"+vrstaStudija +"' ";
			sql+=" AND sp.nazivStudijskiProgram = '"+ studijskiProgram+"' ";
		}else{stNeGlede++;}
		
		if(!nacinStudija.equals("Ne glede")){
			String nacinStudija2="";
			if(nacinStudija.startsWith("R")){
				nacinStudija2="R";
			}else if(nacinStudija.startsWith("I")){
				nacinStudija2="I";
			} 
			
			sql+=" AND vl.nacinStudija = '"+nacinStudija2+"' ";
		}else{stNeGlede++;}
		
		if(!vrstaVpisa.equals("Ne glede")){
			String vrstaVpisa2=vrstaVpisa.substring(0, 2);
			
			
			sql+=" AND vv = vl.vrstaVpisa ";
			sql+=" AND vv.kraticaVrstaVpisa = '"+vrstaVpisa2+"' ";
		}else{stNeGlede++;}
		
		if(!smerModul.equals("Ne glede")){
			//ali je modul
			String sql3=" SELECT COUNT(mo) "+
			           		" FROM Modul mo "+
			           		" WHERE mo.nazivModul = '"+ smerModul+"' ";			           
			Query q3=em.createQuery(sql3);
			long aliJe=(Long)q3.getSingleResult();
						
			if(aliJe>0){
				//moduli
				sql+=" AND mo.nazivModul = '"+smerModul+"' ";
				sql+=" AND pl.modul = mo ";
				///sql+=" AND le = pl.letnik ";	
				sql+=" AND predmetnik.fkVpisniList = vl.idVpisni ";
				sql+=" AND pl.idPredmetnikLetnika = predmetnik.fkPredmetnikLetnika ";	
				
			}else{
				//smeri
				sql+=" AND ss.nazivStudijskaSmer = '"+ smerModul+"' ";
				sql+=" AND studij.studijskaSmer = ss ";
				
			}
		}else{stNeGlede++;}
		
		sql+=" ORDER BY st.oseba.priimek ";
		
		Query q=em.createQuery(sql);
		List<Object[]> studenti= q.getResultList();		
				
		//String[][] tab=new String[studenti.size()][4+stNeGlede];
		
		
		List<String[]> vrstice=new ArrayList<String[]>();		
		
		String[] naslovnaVrstica=new String[4+stNeGlede];
		naslovnaVrstica[0]="Št. ";
		naslovnaVrstica[1]="Vpisna št. ";
		naslovnaVrstica[2]="Priimek ";
		naslovnaVrstica[3]="Ime ";
		
		int i=1;
		for(Object[] stinvl :studenti){
			Student student=(Student)stinvl[0];
			Vpisni_list vpisniList=(Vpisni_list)stinvl[1];
			//System.out.println("student "+ student.getOseba().getIme() +" "+student.getOseba().getPriimek());
			List<Vpisni_list> vpisniListi=student.getVpisniLists();	
					
			
			
			
				String[] vrstica=new String[4+stNeGlede];
				vrstica[0]=" "+i;
				vrstica[1]=student.getVpisnaSt();
				vrstica[2]=student.getOseba().getPriimek();
				vrstica[3]=student.getOseba().getIme();
				
				
				Letnik le=vpisniList.getLetnik();
				Studij studij=le.getStudij();			
				Studijska_smer ss=studij.getStudijskaSmer();
				Vrsta_studija vs=studij.getVrstaStudija() ;
				Studijski_program sp=studij.getStudijskiProgram();
				Vrsta_vpisa vv =vpisniList.getVrstaVpisa();
				String sqlX="SELECT pl " +
						" FROM Vpisni_list vl, Predmetnik pre, Predmetnik_letnika pl " +
						" WHERE" +
						"	vl= :vpisni " +
						" AND pre.fkVpisniList = vl.idVpisni" +
						" AND pl.idPredmetnikLetnika = pre.fkPredmetnikLetnika " +
					"ORDER BY pl.predmet.nazivPredmet";
				Query qX=em.createQuery(sqlX);
				qX.setParameter("vpisni", vpisniList);
				List<Predmetnik_letnika> pls=qX.getResultList();
				//System.out.println("student "+vpisniList.getStudent().getVpisnaSt()+" stModulov "+pls.size());
				//Modul modul=pls.get(0).getModul();				
			
				
				//System.out.println("leto aaa " +vpisniList.getStudijskoLeto()+" "+le.getNazivLetnik() +" "+le.getStudijskoLeto());
			
				int stevecStolpec=0; //za sledenje v kater stolpec spada
				if(letnik.equals("Ne glede")){	
					vrstica[4+stevecStolpec]=le.getNazivLetnik();
					if(i==1){//samo ob prvem obhodu napolni naslovno vrstico
						naslovnaVrstica[4+stevecStolpec]="Letnik";
					}
					stevecStolpec++;					
				}
				
				if(studijskoLeto.equals("Ne glede")){	
					vrstica[4+stevecStolpec]=vpisniList.getStudijskoLeto();
					if(i==1){
						naslovnaVrstica[4+stevecStolpec]="Leto";
					}
					stevecStolpec++;					
				}
					
				if(programiInSmeri.equals("Ne glede")){ 	
					vrstica[4+stevecStolpec]=sp.getNazivStudijskiProgram() +" "+vs.getKraticaVrstaStudija();	
					if(i==1){
						naslovnaVrstica[4+stevecStolpec]="Studijski program";
					}
					stevecStolpec++;
				}
				
				if(nacinStudija.equals("Ne glede")){	
					if(vpisniList.getNacinStudija().equals("R")){
						vrstica[4+stevecStolpec]="";
					}else{
						vrstica[4+stevecStolpec]="    IZŠ";
					}
					if(i==1){
						naslovnaVrstica[4+stevecStolpec]="Način študija";
					}
					stevecStolpec++;
				}
				
				if(vrstaVpisa.equals("Ne glede")){	
					if(vpisniList.getVrstaVpisa().getKraticaVrstaVpisa().equals("V1")){
						vrstica[4+stevecStolpec]="";
					}else{
						vrstica[4+stevecStolpec]="    "+vpisniList.getVrstaVpisa().getKraticaVrstaVpisa();
						//System.out.println("a"+vpisniList.getVrstaVpisa().getKraticaVrstaVpisa()+"a");
					}
					if(i==1){
						naslovnaVrstica[4+stevecStolpec]="Vrsta vpisa";
					}
					stevecStolpec++;
				}
				
				if(smerModul.equals("Ne glede")){	
					String moduli="";
					List<String> moduliDoSedaj=new ArrayList<String>();
					
					for(Predmetnik_letnika pl:pls){
						if(pl.getModul()!=null){
							String m=pl.getModul().getNazivModul();
							boolean seNi=true;
							for(String m2:moduliDoSedaj){
								if(m2.equals(m)){
									seNi=false;
								}
							}
							if(seNi){
								if(moduli.length()>0){
									moduli+=", ";
								}
								moduli+=m;
								moduliDoSedaj.add(m);
							}
						}
					}
					
					if(pls.size()>0 && moduli.length()>1){
						vrstica[4+stevecStolpec]=moduli;//pls.get(0).getModul().getNazivModul();
						if(i==1){
							naslovnaVrstica[4+stevecStolpec]="Modul";
						}
					}else{
						vrstica[4+stevecStolpec]=ss.getNazivStudijskaSmer();
						if(i==1){
							naslovnaVrstica[4+stevecStolpec]="Smer";
						}
					}
					
					stevecStolpec++;
				}
				
				vrstice.add(vrstica);
				i++;
				
			
			//System.out.println("-----------------");
			
			
		}
		
		String[][] tab=new String[vrstice.size()+1][4+stNeGlede];
		tab[0]=naslovnaVrstica;
		i=1;		
		for(String[] vrsta:vrstice){
			tab[i]=vrsta;
			i++;
		}
				
		request.setAttribute("tabela", tab);
		
		HttpSession seja=request.getSession(); // seja za shrant za tiskanje
		seja.setAttribute("leto1",studijskoLeto);		
		seja.setAttribute("tabela", tab);
		seja.setAttribute("letnik", letnik);
		seja.setAttribute("program", programiInSmeri);
		seja.setAttribute("nacin", nacinStudija);
		seja.setAttribute("vrsta", vrstaVpisa);
		seja.setAttribute("smerModul", smerModul);
		
		RequestDispatcher nov=request.getRequestDispatcher("Ajax/seznamVpisanihAjax.jsp");
		nov.forward(request, response);
		//System.out.println("poslano");
		return;
	}

}
