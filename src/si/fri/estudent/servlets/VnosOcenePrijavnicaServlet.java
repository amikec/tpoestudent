package si.fri.estudent.servlets;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import si.fri.estudent.jpa.Izpitni_rok;
import si.fri.estudent.jpa.Ocena;
import si.fri.estudent.jpa.Oseba;
import si.fri.estudent.jpa.Predmet;
import si.fri.estudent.jpa.Predmet_izvajalec;
import si.fri.estudent.jpa.Predmetnik;
import si.fri.estudent.jpa.Student;
import si.fri.estudent.jpa.Vloga_oseba;
import si.fri.estudent.jpa.Vrsta_studija;
import si.fri.estudent.utilities.Constants;

/**
 * Servlet implementation class VnosOcenePrijavnicaServlet
 */
@WebServlet("/VnosOcenePrijavnicaServlet")
public class VnosOcenePrijavnicaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VnosOcenePrijavnicaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    private int profesor = -1;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		Student student = null;
		if(session.getAttribute("studentIzIskanja") != null){
            student = (Student)session.getAttribute("studentIzIskanja");
        } else {
        	response.sendRedirect("index.jsp?stran=domov");
        	return;
        }
		
		List<Vrsta_studija> vrsteStudija = vrniVrsteStudija(student.getIdStudent());
		
		request.setAttribute("student", student);
		request.setAttribute("oseba", student.getOseba());
		request.setAttribute("vrsteStudija", vrsteStudija);
		RequestDispatcher fw = request.getRequestDispatcher("index.jsp?stran=vnosOceneIzpitaSPrijavnice");
		fw.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String datum = null;
		String cas = "12:00:00";
		String tockeIzpita = "0";
		String dodatneTocke = "0";
		String ocenaIzpit = null;
		String ocenaVaje = null;
		String koncnaOcena = null;
		String steviloPolaganja = null;
		int predmetIzvajalec = -1;
		int idOdprtePrijave = -1;
		int letnik = -1;
		int idStudent = -1;
		
		if (request.getParameter("letnikVnosPrijavnica") != null) {
			letnik = Integer.parseInt(request.getParameter("letnikVnosPrijavnica"));
		}
		if (request.getParameter("idOdprtePrijave") != null) {
			idOdprtePrijave = Integer.parseInt(request.getParameter("idOdprtePrijave"));
		}
		if (request.getParameter("predmetVnosPrijavnica") != null) {
			predmetIzvajalec = Integer.parseInt(request.getParameter("predmetVnosPrijavnica"));
		}
		if (request.getParameter("datumVnosPrijavnica") != null) {
			datum = request.getParameter("datumVnosPrijavnica");
		}
		if (request.getParameter("idStudent") != null) {
			idStudent = Integer.parseInt(request.getParameter("idStudent"));
		}
		if (request.getParameter("letnikVnosPrijavnica") != null) {
			letnik = Integer.parseInt(request.getParameter("letnikVnosPrijavnica"));
		}
		if (request.getParameter("koncnaOcenaIzpitPrijavnica") != null) {
			ocenaIzpit = request.getParameter("koncnaOcenaIzpitPrijavnica");
		}
		if (request.getParameter("koncnaOcenaVajePrijavnica") != null) {
			ocenaVaje = request.getParameter("koncnaOcenaVajePrijavnica");
		}
		if (request.getParameter("koncnaOcenaPrijavnica") != null) {
			koncnaOcena = request.getParameter("koncnaOcenaPrijavnica");
		}
		if (request.getParameter("stPolaganjaPrijavnica") != null) {
			steviloPolaganja = request.getParameter("stPolaganjaPrijavnica");
		}
		
		HttpSession session = request.getSession();
		Oseba uporabnik = (Oseba) session.getAttribute("uporabnik");
		Vloga_oseba vloga = (Vloga_oseba) session.getAttribute("vloga");
		
		if(vloga.getIdVlogaOseba() == Constants.VLOGA_UCITELJ)
				profesor = uporabnik.getIdOseba();
		
		EntityManager em = this.getEntityManager();

		String uspesnost = "";
		
		try{
			em.getTransaction().begin();
			
			Ocena ocenaPrijava = null;
			
			if(idOdprtePrijave < 0)
				ocenaPrijava = new Ocena();
			else
				ocenaPrijava = em.find(Ocena.class, idOdprtePrijave);
			
			Oseba oseba = null;
			Izpitni_rok ir = new Izpitni_rok();
			Predmet_izvajalec pi = em.find(Predmet_izvajalec.class, predmetIzvajalec);
			Student student = em.find(Student.class, idStudent);
			oseba = student.getOseba();
						
			Date date = getDate(datum, cas);
			
			ir.setPredmetIzvajalec(pi);
			ir.setDatumIzpit(date);
			ir.setPredmet(pi.getPredmet());
			ir.setStatus(true);
			
			Predmet predmet = pi.getPredmet();
			
			int stPolaganja = Integer.parseInt(steviloPolaganja);
				
			if(veljavnoStPolaganja(predmetIzvajalec, idStudent, stPolaganja)){
				
				Predmetnik predmetnik = getPredmetnik(predmetIzvajalec, idStudent, letnik);
				
				int stPolaganjaLetos = opravljanjaLetos(predmetIzvajalec, letnik, oseba);
				
				if(idOdprtePrijave < 0 || ocenaPrijava.getStPolaganja() == 0){
					ocenaPrijava.setFkPredmetnik(predmetnik.getIdPredmetnik());
					ocenaPrijava.setStPolaganja(stPolaganja);
					ocenaPrijava.setStPolaganjaLetos(stPolaganjaLetos + 1);
				}
				
				if (tockeIzpita != null) {
					ocenaPrijava.setOcena(Double.parseDouble(tockeIzpita));
				}
				ocenaPrijava.setOcenaBonus(Double.parseDouble(dodatneTocke));
				
				String nacinOcen = predmet.getNacinOcenjevanja();
				int ocena = 0;
				int ocena2 = 0;
				
				if(nacinOcen.equals(Constants.OCENA_IZPIT)){
					ocena = Integer.parseInt(ocenaIzpit);
					
					if(ocena > 10 || ocena < 1)
						uspesnost = "<span class='error'>Nepravilna ocena v vrstici</span>";
						
					ocenaPrijava.setOcenaIzpit(ocena);
				}
				
				else if(nacinOcen.equals(Constants.OCENA_VAJE)){
					ocena = Integer.parseInt(ocenaVaje);
					
					if(ocena > 10 || ocena < 1)
						uspesnost = "<span class='error'>Nepravilna ocena v vrstici</span>";
					
					ocenaPrijava.setOcenaVaje(ocena);
				}
				
				else if(nacinOcen.equals(Constants.OCENA_KONCNA)){
					ocena = Integer.parseInt(koncnaOcena);
					
					if(ocena > 10 || ocena < 1)
						uspesnost = "<span class='error'>Nepravilna ocena v vrstici</span>";
	
					ocenaPrijava.setOcenaKoncna(ocena);
				}
				
				else if(nacinOcen.equals(Constants.OCENA_IZPIT_VAJE)){
					
					ocena = Integer.parseInt(ocenaIzpit);
					ocena2 = Integer.parseInt(ocenaVaje);
					
					if(ocena >= 6 && ocena2 >= 6 && ocena <= 10 && ocena2 <= 10){
					
					}else if(ocena <= 5 && ocena2 == 0 && ocena > 0){
						
					}else{
						uspesnost = "<span class='error'>Nepravilna ocena</span>";
					}
					
					ocenaPrijava.setOcenaIzpit(ocena);
					ocenaPrijava.setOcenaVaje(ocena2);
				}
				
				if(idOdprtePrijave < 0){
					em.persist(ir);
					em.getTransaction().commit();
					
					em.getTransaction().begin();
					ocenaPrijava.setFkIzpitniRok(ir.getIdRok());
					ocenaPrijava.setAktivna(true);
					em.persist(ocenaPrijava);
					em.getTransaction().commit();
				}else{
					em.merge(ocenaPrijava);
					em.getTransaction().commit();
				}
				
				Student s = em.find(Student.class, idStudent);
		   
				request.setAttribute("student", s);
				request.setAttribute("oseba", student.getOseba());
				request.setAttribute("vrsteStudija", vrniVrsteStudija(student.getIdStudent()));
				
				int stLetos = opravljanjaLetos(predmetIzvajalec, letnik, oseba);
				int stSkupno = opravljanjaLetos(predmetIzvajalec, -1, oseba);
				int stPonav = getStPolagPonavljanj(predmet.getIdPredmeta(), student.getIdStudent());
				
				String temp = "";
				if(stPonav > 0)
					temp = "- " + stPonav;
				
				uspesnost = String.format("Vnos uspel\n" +
										  "      Skupaj polaganj: %s %s \n" +
										  "      Letos polaganj:  %s", stSkupno, temp, stLetos);
			}else{
				uspesnost = "<span class='error'>Številka polaganja ni veljavna, poskusi drugo</span>";
			}
			
			request.setAttribute("student", student);
			request.setAttribute("oseba", student.getOseba());
			request.setAttribute("vrsteStudija", vrniVrsteStudija(student.getIdStudent()));
			
		}catch(Exception e){
			e.printStackTrace();
			em.getTransaction().rollback();
			uspesnost = "<span class='error'>Vnos ni uspel</span>";
		}
		
		request.setAttribute("errorText", uspesnost);
		RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp?stran=vnosOceneIzpitaSPrijavnice");
		dispatcher.forward(request, response);
	}
	
	/////////////////////////////////////////// METODE ///////////////////////////////////////////
	
	private EntityManager getEntityManager(){
		return Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
	}
	
	private Date getDate(String datum, String cas){
		Calendar cal = Calendar.getInstance();
	     
		String[] d = datum.split("\\.");
		
		int day = Integer.parseInt(d[0]);
		int month = Integer.parseInt(d[1]);
		int year = Integer.parseInt(d[2]);
		
		String[] c = cas.split(":");
		int hour = Integer.parseInt(c[0]);
		int min = Integer.parseInt(c[1]);
				
	    cal.set( cal.YEAR, year );
	    cal.set( cal.MONTH, month - 1);
	    cal.set( cal.DATE, day );
	     
	    cal.set( cal.HOUR_OF_DAY, hour );
	    cal.set( cal.MINUTE, min );
	    cal.set( cal.SECOND, 0 );
	    
	    return cal.getTime();
	}
	
	private int getStPolagPonavljanj(int predmet, int student){
		try {
			String sql = "SELECT " +
					"DISTINCT oc " +
			  "FROM " +
			  		"Ocena oc, Predmetnik pred, Vpisni_list vl, Vrsta_vpisa vp, Predmetnik_letnika predL, Predmet p " +
		  	  "WHERE " +
		  	  		"vl.student.idStudent = :student AND " +
		  	  		"vl.vrstaVpisa.idVrstaVpisa = :vrstaVpisa AND " +
		  	  		"vl.idVpisni = pred.fkVpisniList AND " +
		  	  		"pred.idPredmetnik = oc.fkPredmetnik AND " +
		  	  		"pred.fkPredmetnikLetnika = predL.idPredmetnikLetnika AND " +
		  	  		"predL.predmet.idPredmeta = :predmet AND " +
		  	  		"oc.aktivna = 1 AND " +
		  	  		"( " +
		  	  			" oc.ocenaKoncna > 0 OR " +
		  	  			" oc.ocenaIzpit > 0 OR " +
		  	  			" oc.ocenaVaje > 0 OR " +
		  	  			" oc.ocena > 0 " +
		  	  		")";
					  	  		
			Query query = this.getEntityManager().createQuery(sql);
			query.setParameter("predmet", predmet);
			query.setParameter("student", student);
			query.setParameter("vrstaVpisa", Constants.VPIS_PONAVLJANJE);
			
			List<Ocena> ocene = query.getResultList();
			
			int st = 0;
			
			for(Ocena oc : ocene)
				st += 1;
					
			return st;
			
		} catch(Exception e){
			e.printStackTrace();
			return 0;
		}	
	}
	
	private List<Vrsta_studija> vrniVrsteStudija(int idStudent) {
		try {
			String sql = "SELECT " +
						 	  "DISTINCT vs " +
						 "FROM " +
							 "Student student, Vpisni_list vl, Letnik l, Studij studij," +
							 "Vrsta_studija vs " +
						 "WHERE " +
							 "student.idStudent = :student AND " +
							 "vl.student = student AND " +
							 "vl.letnik = l AND " +
							 "l.studij = studij AND " +
							 "studij.vrstaStudija = vs " +
						 "ORDER BY " +
						 	"vs.nazivVrstaStudija";
	
		Query query = this.getEntityManager().createQuery(sql);
		query.setParameter("student", idStudent);
		
		List<Vrsta_studija> vrsteStudija = query.getResultList();
		return vrsteStudija;
		
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	private Predmetnik getPredmetnik(int idPredmetIzvajalca, int idStudent, int idLetnik){
		EntityManager em = this.getEntityManager();
		
		try{
			String sql = "SELECT "+
					"DISTINCT pr " +
				"FROM "+
					"Oseba o, Student s, Vpisni_list vl, Predmetnik pr, Predmet p, Predmetnik_letnika prl, Ocena oc, " +
					"Predmet_izvajalec pi, Izpitni_rok ir  "+
				"WHERE " +
					"s.idStudent = :idStudent AND " +
					"s = vl.student AND " +
					"vl.letnik.idLetnik = :letnik AND " +
					"vl.idVpisni = pr.fkVpisniList AND " +
					"pr.fkPredmetnikLetnika = prl.idPredmetnikLetnika AND " +
					"p = prl.predmet AND " +
					"p = pi.predmet AND " +
					"pi.idPredmetIzvajalca = :idPredmetIzvajalca";
			
			Query query = em.createQuery(sql);
			query.setParameter("idStudent", idStudent);
			query.setParameter("idPredmetIzvajalca", idPredmetIzvajalca);
			query.setParameter("letnik", idLetnik);
			Predmetnik predmetnik = (Predmetnik) query.getSingleResult();
			
			return predmetnik;
			
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	private boolean veljavnoStPolaganja(int idPredmetIzvajalca, int idStudent, int stPolaganja){
		EntityManager em = this.getEntityManager();
		
		if(stPolaganja < 1)
			return false;
		
		try{
			String sql = "SELECT "+
					"DISTINCT oc " +
				"FROM "+
					"Oseba o, Student s, Vpisni_list vl, Predmetnik pr, Predmet p, Predmetnik_letnika prl, Ocena oc, " +
					"Predmet_izvajalec pi, Izpitni_rok ir  "+
				"WHERE " +
					"s.idStudent = :idStudent AND " +
					"s = vl.student AND " +
					"vl.idVpisni = pr.fkVpisniList AND " +
					"pr.fkPredmetnikLetnika = prl.idPredmetnikLetnika AND " +
					"p = prl.predmet AND " +
					"p = pi.predmet AND " +
					"pi.idPredmetIzvajalca = :idPredmetIzvajalca AND " +
					"pr.idPredmetnik = oc.fkPredmetnik AND " +
					"oc.ocenaKoncna > 0 AND " +
					"oc.ocenaIzpit > 0 AND " +
					"oc.ocenaVaje > 0 AND " +
					"oc.aktivna = 1";
			
			Query query = em.createQuery(sql);
			query.setParameter("idStudent", idStudent);
			query.setParameter("idPredmetIzvajalca", idPredmetIzvajalca);
			List<Ocena> ocene = query.getResultList();
			
			
			for(Ocena ocena : ocene)
				if(ocena.getStPolaganja() == stPolaganja)
					return false;
			
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	private int opravljanjaLetos(int predmetIzvajalec, int idLetnika, Oseba oseba){
		
		EntityManager em = this.getEntityManager();
		
		String sql = "SELECT "+
				"DISTINCT oc " +
			"FROM "+
				"Oseba o, Student s, Vpisni_list vl, Predmetnik pr, Predmet p, Predmetnik_letnika prl, Ocena oc," +
				"Predmet_izvajalec pi "+
			"WHERE " +
				"s.oseba.idOseba = ?1 AND " +
				"s.idStudent = vl.student.idStudent AND " +
				"vl.idVpisni = pr.fkVpisniList AND " +
				"pr.fkPredmetnikLetnika = prl.idPredmetnikLetnika AND " +
				"p = prl.predmet AND " +
				"p = pi.predmet AND " +
				"pr.idPredmetnik = oc.fkPredmetnik AND " +
				"pi.idPredmetIzvajalca = :predmetIzvajalec AND " +
				"oc.aktivna = 1";
		
		if(idLetnika > 0)
				sql += " AND vl.letnik.idLetnik = ?3 ";
		
		Query query = em.createQuery(sql); 
		
		query.setParameter(1, oseba.getIdOseba());
		query.setParameter("predmetIzvajalec", predmetIzvajalec);
		
		if(idLetnika > 0)
			query.setParameter(3, idLetnika);
		
		List<Ocena>ocene = (List<Ocena>) query.getResultList();
		
		Predmetnik predmetnik = null;
		int max = 0;
		
		for(Ocena o : ocene){
			if(o.getStPolaganja() > max && idLetnika < 0)
					max = o.getStPolaganja();
			
			if(o.getStPolaganjaLetos() > max && idLetnika > 0)
					max = o.getStPolaganjaLetos();
		}
		
		return max;
	}
}
