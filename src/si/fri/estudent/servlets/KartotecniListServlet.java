package si.fri.estudent.servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;

import si.fri.estudent.jpa.*;
import si.fri.estudent.utilities.SkupenProgram;
import si.fri.estudent.utilities.Tiskanje;

/**
 * Servlet implementation class KartotecniListServlet
 */
@WebServlet("/KartotecniListServlet")
public class KartotecniListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static SimpleDateFormat format=new SimpleDateFormat("dd.MM.yyyy");
	List<String[][]> naslovneTabele;
	List<String[][]> vsebinskeTabele;
	List<String[][]> zakljucneTabele;
	List<String> imenaProgramov;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public KartotecniListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		
		imenaProgramov=(List<String>)session.getAttribute("imenaProgramov");
		naslovneTabele=(List<String[][]>)session.getAttribute("naslovneTabele");
		vsebinskeTabele=(List<String[][]>)session.getAttribute("vsebinskeTabele");
		zakljucneTabele=(List<String[][]>)session.getAttribute("zakljucneTabele");
		
		if(naslovneTabele==null && vsebinskeTabele==null && zakljucneTabele==null){
			response.sendRedirect("KartotecniListInitServlet");
			return;
		}
		
		Tiskanje tiskanje=new Tiskanje( request,response,"Kartotečni list");	
		int st=naslovneTabele.size();
		for(int i=0;i<st;i++){
			tiskanje.dodajBesedilo(imenaProgramov.get(i));
			tiskanje.dodajTabelo(naslovneTabele.get(i), false, false);
			if(vsebinskeTabele.get(i).length>0){
				//System.out.println("vsebinske size "+vsebinskeTabele.size() +" i: "+i+" dolzina "+vsebinskeTabele.get(i).length);
				tiskanje.dodajTabelo(vsebinskeTabele.get(i), false, false);
			}
			tiskanje.dodajTabelo(zakljucneTabele.get(i), false, false);
			tiskanje.dodajPraznoVrstico();
			tiskanje.dodajPraznoVrstico();
		}
//		Iterator<String[][]> iter=programiTab.iterator();
//		while(iter.hasNext()){		
//			tiskanje.dodajTabelo(iter.next(), false, true);
//		}
		tiskanje.dobiDocument();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		EntityManager em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		
		
		
		List<SkupenProgram> skupenProgram=(List<SkupenProgram>)request.getSession().getAttribute("skupenProgram");
		System.out.println("skupenprogram velikost: "+skupenProgram.size());
		List<SkupenProgram> koncniSkupenProgram=new ArrayList<SkupenProgram>();
		
		String varianta=request.getParameter("varianta");
		if(skupenProgram.size()>1){
			String program=request.getParameter("program");
			int izbira=Integer.parseInt(program);
			if(izbira==0){
				for(SkupenProgram sp:skupenProgram){
					koncniSkupenProgram.add(sp);
				}
				
			}else{
				SkupenProgram izbranProgram=skupenProgram.get(izbira-1);
				koncniSkupenProgram.add(izbranProgram);
			}
		}else{
			koncniSkupenProgram.add(skupenProgram.get(0));
		}
		
		naslovneTabele=new ArrayList<String[][]>();
		vsebinskeTabele=new ArrayList<String[][]>();
		zakljucneTabele=new ArrayList<String[][]>();
		imenaProgramov=new ArrayList<String>();
	
		Oseba oseba=(Oseba)request.getSession().getAttribute("uporabnik");		
		
		Student s=null;
		if(request.getSession().getAttribute("studentIzIskanja")!=null){
            s=(Student)request.getSession().getAttribute("studentIzIskanja");
        }
		
		System.out.println("oseba"+oseba.getIme());
		//try{
		if(s==null){
			List<Student> studenti=oseba.getStudents();
			if(studenti.size()==0){
					//throw new  Exception();
			}
			s=oseba.getStudents().get(0);	
		}
		List<Vpisni_list> vpisniListi=s.getVpisniLists();

		Collections.sort(vpisniListi, new VpisniComparator());
		
		for(Vpisni_list vl:vpisniListi){
			boolean izbran=false;
			
			Vrsta_studija vs=vl.getLetnik().getStudij().getVrstaStudija();
			Studijski_program sp=vl.getLetnik().getStudij().getStudijskiProgram();
			for(SkupenProgram skupen:koncniSkupenProgram){
				if(skupen.getVrstaStudija().getIdVrstaStudija()==vs.getIdVrstaStudija() && skupen.getStudijskiProgram().getIdStudijskiProgram()==sp.getIdStudijskiProgram()){
					izbran=true;
				}
			}
			if(izbran){
				imenaProgramov.add(sp.getNazivStudijskiProgram()+" "+vs.getNazivVrstaStudija());
				String[][] naslovna={{"Študijsko leto:",vl.getStudijskoLeto()},
						{"Smer:",vl.getLetnik().getStudij().getStudijskaSmer().getNazivStudijskaSmer()},
						{"Letnik:",vl.getLetnik().getNazivLetnik()},
						{"Vrsta vpisa:",vl.getVrstaVpisa().getNazivVrstaVpisa()},
						{"Način:",vl.getNacinStudija()}
						};
				naslovneTabele.add(naslovna);
				
				String sqlZOcenami = "SELECT " + 
							"pred, oc,pl " +
							"FROM " +
							"Predmetnik pred, Ocena oc, Predmetnik_letnika pl "+
							"WHERE " +						
							"pred.fkVpisniList = :vpisniId AND "+
							"oc.fkPredmetnik = pred.idPredmetnik AND "+
							//"ir.idRok = oc.fkIzpitniRok AND " +
							//"oc.ocenaKoncna != 0 AND "+
							"pl.idPredmetnikLetnika = pred.fkPredmetnikLetnika ";
				
				String sqlBrezOcen = "SELECT " + 
						"pred, pl "+
						"FROM "+
						"Predmetnik pred, Predmetnik_letnika pl "+
						"WHERE " +
						"pred.fkVpisniList = :vpisniId AND "+			
						"NOT EXISTS " +
						"	( " +
						"	SELECT oc " +
						"	FROM Ocena oc " +
						"	WHERE oc.fkPredmetnik = pred.idPredmetnik " +
						//"	AND oc.ocenaKoncna != 0 " +
						"	) AND "+
						" pl.idPredmetnikLetnika = pred.fkPredmetnikLetnika " ;
				
				
				
				Query query=em.createQuery(sqlZOcenami);
				query.setParameter("vpisniId", vl.getIdVpisni());	
				List<Object[]> razpisaniPredmeti = query.getResultList();
				
				List<Predmetnik> predmetniki = new ArrayList<Predmetnik>();
				List<Ocena> ocene = new ArrayList<Ocena>();
				
				ArrayList<String[]> vrsticeVsebine=new ArrayList<String[]>();
				
				double vsotaIzpitov=0;
				double vsotaVaj=0;
				double vsotaOcen=0;
				int stOpravljenihIzpitov=0;
				int stOpravljenihVaj=0;
				int stOpravljenihKoncnih=0;
				double stOpravljenihTock=0;
				double stVsehTock=0;
				
				//predmeti ki ze imajo oceno (tabelo ocena)
				for(Object[] o : razpisaniPredmeti){
					
					String[] vrstica={"","","","","","","",""};
					Predmetnik predmetnik = (Predmetnik) o[0];
					Ocena ocena=(Ocena)o[1];	
					//Izpitni_rok ir=(Izpitni_rok)o[2];
					Predmetnik_letnika pl=(Predmetnik_letnika)o[2];//3
					Predmet predmet=pl.getPredmet();
					
					
					//System.out.println("predmetnik "+predmetnik.getIdPredmetnik()+ " ocena "+ocena.getIdOcena() +
					//		" " + ocena.getOcenaKoncna() +" id izpitni rok "+ir.getIdRok()+" predmet izvajalec "+ir.getPredmetIzvajalec().getIdPredmetIzvajalca()+" "+" pl "+pl.getIdPredmetnikLetnika());
					vrstica[0]=predmet.getSifraPredmeta();
					vrstica[1]=predmet.getNazivPredmet();
					vrstica[2]=""+predmet.getKreditneTocke();
					
					String tipOcene=predmet.getNacinOcenjevanja();
					
					if(tipOcene.equals("I")){
						if(ocena.getOcenaIzpit()>0){
							String sqlIR="SELECT ir " +
									" FROM " +
										" Izpitni_rok ir, Ocena oc " +
									" WHERE " +
										" oc=:ocena AND " +
										" ir.idRok =oc.fkIzpitniRok ";
							Query q=em.createQuery(sqlIR);
							q.setParameter("ocena", ocena);							
							Izpitni_rok ir=(Izpitni_rok)q.getResultList().get(0);
							
							vrstica[3]=getPredavateljiIzpitnegaRoka(ir);
							StringBuilder datum=new StringBuilder(format.format(ir.getDatumIzpit()));
							vrstica[4]=datum.toString();
							vrstica[5]=""+ocena.getOcenaIzpit();
							vrstica[6]=""+ocena.getStPolaganja()+" , "+ocena.getStPolaganjaLetos();
							vrstica[7]="I";
						}else{
							
						}
						
					}else if(tipOcene.equals("V")){
						if(ocena.getOcenaVaje()>0){
							vrstica[3]=getPredavateljiPredmeta(predmet);
							vrstica[5]=""+ocena.getOcenaVaje();
							vrstica[6]=""+ocena.getStPolaganja()+" , "+ocena.getStPolaganjaLetos();
							vrstica[7]="V";
						}else{
							
						}
					}else if(tipOcene.equals("IV")){
						if(ocena.getOcenaIzpit()>0 && ocena.getOcenaVaje()>0){
							String sqlIR="SELECT ir " +
									" FROM " +
										" Izpitni_rok ir, Ocena oc " +
									" WHERE " +
										" oc=:ocena AND " +
										" ir.idRok =oc.fkIzpitniRok ";
							Query q=em.createQuery(sqlIR);
							q.setParameter("ocena", ocena);							
							Izpitni_rok ir=(Izpitni_rok)q.getResultList().get(0);
							
							vrstica[3]=getPredavateljiIzpitnegaRoka(ir);
							StringBuilder datum=new StringBuilder(format.format(ir.getDatumIzpit()));
							vrstica[4]=datum.toString();
							vrstica[5]=""+ocena.getOcenaVaje()+"/"+ocena.getOcenaIzpit();
							vrstica[6]=""+ocena.getStPolaganja()+" , "+ocena.getStPolaganjaLetos();
							vrstica[7]="IV";
						}else{
							
						}
					}else if(tipOcene.equals("K")){
						if(ocena.getOcenaKoncna()>0){
							String sqlIR="SELECT ir " +
									" FROM " +
										" Izpitni_rok ir, Ocena oc " +
									" WHERE " +
										" oc=:ocena AND " +
										" ir.idRok =oc.fkIzpitniRok ";
							Query q=em.createQuery(sqlIR);
							q.setParameter("ocena", ocena);							
							Izpitni_rok ir=(Izpitni_rok)q.getResultList().get(0);
							
							vrstica[3]=getPredavateljiIzpitnegaRoka(ir);
							StringBuilder datum=new StringBuilder(format.format(ir.getDatumIzpit()));
							vrstica[4]=datum.toString();
							vrstica[5]=""+ocena.getOcenaKoncna();
							vrstica[6]=""+ocena.getStPolaganja()+" , "+ocena.getStPolaganjaLetos();
							vrstica[7]="K";
						}else{
							
						}
					}
					
					
					vrsticeVsebine.add(vrstica);
				
					
					
					
				}	
				
				//predmeti, ki se nimajo ocene
				Query query2=em.createQuery(sqlBrezOcen);
				query2.setParameter("vpisniId", vl.getIdVpisni());
				List<Object[]> razpisaniPredmeti2 = query2.getResultList();
				for(Object[] o : razpisaniPredmeti2){
					String[] vrstica={"","","","","","","",""};
					Predmetnik predmetnik = (Predmetnik) o[0];
					Predmetnik_letnika pl=(Predmetnik_letnika)o[1];
					Predmet predmet=pl.getPredmet();
					System.out.println("pred brez ocene "+predmetnik.getIdPredmetnik()+" pl "+pl.getIdPredmetnikLetnika());
					
					vrstica[0]=predmet.getSifraPredmeta();
					vrstica[1]=predmet.getNazivPredmet();
					vrstica[2]=""+predmet.getKreditneTocke();
					
					vrsticeVsebine.add(vrstica);
				}
				Collections.sort(vrsticeVsebine, new Comparator(){
					 
		            public int compare(Object o1, Object o2) {
		                String[] p1 = (String[]) o1;
		                String[] p2 = (String[]) o2;
		                int razlikaSifra=p1[0].compareToIgnoreCase(p2[0]);
		                if(razlikaSifra==0){
		                	//ce ista sifra izpita primerjaj datume
		                	try {
		                		Date datum1=format.parse(p1[4]);
		                		Date datum2=format.parse(p2[4]);
		                		int razlikaDatum=datum1.compareTo(datum2);		                		
		                		return razlikaDatum;
							} catch (Exception e) {
								System.out.println("napaka datum");
							}
		                	
		                	System.out.println("datum "+p1[4]);
		                }
		                System.out.println("razlika sifra "+razlikaSifra);
		               return razlikaSifra;
		            }
		 
		        });
				
				int dolzina=vrsticeVsebine.size();
				
				
				//preveri ce sta dva polaganja od istega predmeta
				
				String prejsnjaSifra2="";
				for(int j=0;j<dolzina;j++){
					String[] vrstica=vrsticeVsebine.get(j);
					String sifra=vrstica[0];
					if(prejsnjaSifra2.equals(sifra)){						
							
						
						if(!varianta.equals("zadnja")){
							//vrstica[0]="";
							vrstica[1]="";	
							vrstica[2]="s"+vrstica[2];
						}
					}
					
					prejsnjaSifra2=sifra;
				}			
				
				
				String prejsnjaSifra="xxxxx";
				
				
				for(int j=dolzina-1;j>=0;j--){				
					String[] vrstica=vrsticeVsebine.get(j);
					String sifra=vrstica[0];
					
					
					if(prejsnjaSifra.equals(sifra)){
						if(varianta.equals("zadnja")){
							vrsticeVsebine.remove(j);
							continue;
						}
						if(!vrstica[2].startsWith("s")){
							stVsehTock+=Double.parseDouble(vrstica[2]);
						}
						
						System.out.println("ista sifra a"+sifra+ " a "+prejsnjaSifra+"a");
					}else{
						System.out.println("razlicna sifra a"+sifra+ " a "+prejsnjaSifra+"a");
						//dodaj oceno ker je koncna
						if(!vrstica[5].equals("")){	
							Integer oce=null;							
							boolean narejen=false;
							
							if(vrstica[7].equals("I")){
								oce=Integer.parseInt(vrstica[5]);
								if(oce>5){
									narejen=true;
									
										vsotaIzpitov+=oce;
										vsotaOcen+=oce;
										stOpravljenihIzpitov++;
										stOpravljenihKoncnih++;
									
								}
							}else if(vrstica[7].equals("V")){
								oce=Integer.parseInt(vrstica[5]);
								if(oce>5){
									narejen=true;
									
										vsotaVaj+=oce;
										vsotaOcen+=oce;
										stOpravljenihVaj++;
										stOpravljenihKoncnih++;
									
								}
							}else if(vrstica[7].equals("IV")){
								//oce=Integer.parseInt(vrstica[5]);
								String oceniSkupaj=vrstica[5];
								int posevnica=oceniSkupaj.indexOf("/");
								Integer oce1=Integer.parseInt(oceniSkupaj.substring(0,posevnica));								
								Integer oce2=Integer.parseInt(oceniSkupaj.substring(posevnica+1));
								
								if(oce1>5 && oce2>5){
									narejen=true;
									
										vsotaVaj+=oce1;
										vsotaIzpitov+=oce2;
										vsotaOcen+=(oce1+oce2)/2;
										stOpravljenihIzpitov++;
										stOpravljenihVaj++;
										stOpravljenihKoncnih++;
									
								}
							}else if(vrstica[7].equals("K")){
								oce=Integer.parseInt(vrstica[5]);
								if(oce>5){
									narejen=true;
									
										vsotaIzpitov+=oce;
										vsotaOcen+=oce;
										stOpravljenihIzpitov++;
										stOpravljenihKoncnih++;
									
								}
							}
							
							//Integer oce=Integer.parseInt(vrstica[5]);
							
							if(narejen){
								
								//stOpravljenihIzpitov++;
								//vsotaIzpitov+=oce;
								String tocke=vrstica[2];
								if(vrstica[2].startsWith("s")){
									tocke=vrstica[2].substring(1);
									
								}
								//if(!vrstica[2].startsWith("s")){
									stOpravljenihTock+=Double.parseDouble(tocke);
								//}
								
								
								
							}
							
							//stVsehTock+=Double.parseDouble(tocke);
							
						}else{
							
							//stVsehTock+=Double.parseDouble(vrstica[2]);
							
						}
						if(!vrstica[2].startsWith("s")){
							stVsehTock+=Double.parseDouble(vrstica[2]);
						}
						
						prejsnjaSifra=sifra;
					}	
					if(vrstica[2].startsWith("s")){
						vrstica[2]="";
					}
					
					System.out.println();
					for(int i=0;i<7;i++){					
						
						//System.out.print(vrstica[i]+" ");
					}
					
					
				}
				///pobrise sifre tm k se podvajajo
				prejsnjaSifra2="";
				dolzina=vrsticeVsebine.size();
				for(int j=0;j<dolzina;j++){
					String[] vrstica=vrsticeVsebine.get(j);
					String sifra=vrstica[0];
					if(prejsnjaSifra2.equals(sifra)){						
							
						
						if(!varianta.equals("zadnja")){
							vrstica[0]="";
							
						}
					}
					
					prejsnjaSifra2=sifra;
				}	
				
				Double povprecnaOcena=0d;
				Double povprecnaIzpiti=0d;
				Double povprecnaVaje=0d;
				if(stOpravljenihIzpitov>0){
					povprecnaIzpiti=((Double)vsotaIzpitov)/stOpravljenihIzpitov;
				}
				if(stOpravljenihVaj>0){
					povprecnaVaje=((Double)vsotaVaj)/stOpravljenihVaj;
				}
				if(stOpravljenihKoncnih>0){
					povprecnaOcena=((Double)vsotaOcen)/stOpravljenihKoncnih;
				}
				System.out.println("povprecna "+povprecnaOcena +" st ocen "+stOpravljenihKoncnih);
				System.out.println("povprecnaVaje "+povprecnaVaje +" st ocen "+stOpravljenihVaj);
				System.out.println("povprecnaIzpit "+povprecnaIzpiti +" st ocen "+stOpravljenihIzpitov);
				
				String[][] zakljucna={
						{"Povprečje izpitov:", skrajsaj(povprecnaIzpiti)},
						{"Povprečje vaj:", skrajsaj(povprecnaVaje)},
						{"Povprečna ocena:", skrajsaj(povprecnaOcena)},
						{"Skupno število kreditnih točk:",stOpravljenihTock+" od "+stVsehTock}						
				};
				zakljucneTabele.add(zakljucna);
				
				int stVrstic=vrsticeVsebine.size();
				String[][] vsebina=new String[stVrstic][8];
				int stevec=0;
				for(String[] vrst:vrsticeVsebine){
					vsebina[stevec]=vrst;
					stevec++;
				}
				vsebinskeTabele.add(vsebina);				
			}
		}	
		
		
		HttpSession session=request.getSession();
		
		session.setAttribute("imenaProgramov", imenaProgramov);
		session.setAttribute("naslovneTabele", naslovneTabele);
		session.setAttribute("vsebinskeTabele", vsebinskeTabele);
		session.setAttribute("zakljucneTabele", zakljucneTabele);
		
		request.setAttribute("imenaProgramov", imenaProgramov);
		request.setAttribute("naslovneTabele", naslovneTabele);
		request.setAttribute("vsebinskeTabele", vsebinskeTabele);
		request.setAttribute("zakljucneTabele", zakljucneTabele);
		RequestDispatcher nov=request.getRequestDispatcher("index.jsp?stran=kartotecniListIzpis");
		nov.forward(request, response);
		
	}
	private String skrajsaj(double d){
		String x=""+d;
		if(x.length()>4){
			x=x.substring(0, 4);
		}
		return x;
	}
	
	private String getPredavateljiIzpitnegaRoka(Izpitni_rok ir) {
		String predavatelji="";
		Kombinacije_izvajalcev ki= ir.getPredmetIzvajalec().getKombinacijeIzvajalcev();
		
		Oseba os;
    	Osebje osebje;
    	
    	osebje=ki.getOsebje1();
    	if(osebje!=null){
    		os=osebje.getOseba();
    		if(os!=null){
    			if(!predavatelji.equals("")){predavatelji+=", ";}
    			predavatelji+=os.getPriimek()+" "+os.getIme()+" ";}
    	}
    	osebje=ki.getOsebje2();
    	if(osebje!=null){
    		os=osebje.getOseba();
    		if(os!=null){
    			if(!predavatelji.equals("")){predavatelji+=", ";}
    			predavatelji+=os.getPriimek()+" "+os.getIme()+" ";}
    	}
    	osebje=ki.getOsebje3();
    	if(osebje!=null){
    		os=osebje.getOseba();
    		if(os!=null){
    			if(!predavatelji.equals("")){predavatelji+=", ";}
    			predavatelji+=os.getPriimek()+" "+os.getIme()+" ";}
    	}
		return predavatelji;
	}

	public String getPredavateljiPredmeta(Predmet predmet){
		String predavatelji="";
		for(Predmet_izvajalec prei:predmet.getPredmetIzvajalecs()){
			Kombinacije_izvajalcev ki=prei.getKombinacijeIzvajalcev();
	    	Oseba os;
	    	Osebje osebje;
	    	
	    	osebje=ki.getOsebje1();
	    	if(osebje!=null){
	    		os=osebje.getOseba();
	    		if(os!=null){
	    			if(!predavatelji.equals("")){predavatelji+=", ";}
	    			predavatelji+=os.getPriimek()+" "+os.getIme()+" ";}
	    	}
	    	osebje=ki.getOsebje2();
	    	if(osebje!=null){
	    		os=osebje.getOseba();
	    		if(os!=null){
	    			if(!predavatelji.equals("")){predavatelji+=", ";}
	    			predavatelji+=os.getPriimek()+" "+os.getIme()+" ";}
	    	}
	    	osebje=ki.getOsebje3();
	    	if(osebje!=null){
	    		os=osebje.getOseba();
	    		if(os!=null){
	    			if(!predavatelji.equals("")){predavatelji+=", ";}
	    			predavatelji+=os.getPriimek()+" "+os.getIme()+" ";}
	    	}
	    	
			
		}
		return predavatelji;
	}

}
class VpisniComparator implements Comparator{
	 
    public int compare(Object emp1, Object emp2){    
 
        //parameter are of type Object, so we have to downcast it to Employee objects
        Vpisni_list vl1=((Vpisni_list)emp1);   
    	Vpisni_list vl2=((Vpisni_list)emp2);
        
    	String leto1=vl1.getStudijskoLeto();
    	String leto2=vl2.getStudijskoLeto();
    	
        //uses compareTo method of String class to compare names of the employee
        return leto1.compareTo(leto2);
   
    }
 
}
