package si.fri.estudent.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import si.fri.estudent.jpa.Predmetnik_letnika;

/**
 * Servlet implementation class SeznamZaIzbirniInit
 */
@WebServlet("/SeznamZaIzbirniInit")
public class SeznamZaIzbirniInit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SeznamZaIzbirniInit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EntityManager em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		
		//poiscimo izbirne predmet
		String sql="SELECT DISTINCT pl FROM Predmetnik_letnika pl WHERE pl.obvezniPredmet=false " +
				" AND pl.status = 1 " +
				"ORDER BY pl.predmet.nazivPredmet";
		Query q=em.createQuery(sql);
		List<Predmetnik_letnika> pls=q.getResultList();
		//String[] predmeti=new String[pls.size()];
		List<String> predmetiList=new ArrayList<String>();
		int i=0;
		List<Integer> indeksi=new ArrayList<Integer>();
		for(Predmetnik_letnika pl:pls){
			
			String naziv=pl.getPredmet().getNazivPredmet();
			if(!indeksi.contains(pl.getPredmet().getIdPredmeta())){
				indeksi.add(pl.getPredmet().getIdPredmeta());
				//predmeti[i]=naziv;
				predmetiList.add(naziv);
				i++;
			}
		}
		String[] predmeti=new String[predmetiList.size()];
		i=0;
		for(String predmet:predmetiList){
			predmeti[i]=predmet;
			i++;
		}
		
		//poiscimo leta
		sql="SELECT DISTINCT le.studijskoLeto FROM Letnik AS le ORDER BY le.studijskoLeto";
		q=em.createQuery(sql);
		List<String> leta=(List<String>)q.getResultList();
		String[] studijskaLeta=new String[leta.size()];		
		i=0;
		for (String leto : leta) {
			studijskaLeta[i]=leto;
			i++;
			System.out.println("leto "+leto);
		}
		
		String[] studijskaLeta2=new String[leta.size()-1];
		for(int j=0;j<leta.size()-1;j++){
			studijskaLeta2[j]=studijskaLeta[j]+" in "+studijskaLeta[j+1];
		}
		
		
		//posreduj
		request.setAttribute("predmeti", predmeti);
		request.setAttribute("leta", studijskaLeta);
		request.setAttribute("leta2", studijskaLeta2);
		
		RequestDispatcher nov=request.getRequestDispatcher("index.jsp?stran=seznamZaIzbirni");
		nov.forward(request, response);
		return;
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
