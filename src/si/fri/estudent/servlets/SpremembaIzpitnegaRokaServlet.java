package si.fri.estudent.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import si.fri.estudent.jpa.Izpitni_rok;
import si.fri.estudent.jpa.Kombinacije_izvajalcev;
import si.fri.estudent.jpa.Ocena;
import si.fri.estudent.jpa.Oseba;
import si.fri.estudent.jpa.Osebje;
import si.fri.estudent.jpa.Predmet;
import si.fri.estudent.jpa.Predmet_izvajalec;
import si.fri.estudent.jpa.Vloga_oseba;

import com.sun.xml.internal.stream.Entity;

/**
 * Servlet implementation class SpremembaIzpitnegaRokaServlet
 */
@WebServlet("/SpremembaIzpitnegaRokaServlet")
public class SpremembaIzpitnegaRokaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	EntityManager em;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SpremembaIzpitnegaRokaServlet() {
        super();
        em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		request = pripravaPodatkovZaFormo(request);
		RequestDispatcher rd=request.getRequestDispatcher("index.jsp?stran=spremembaIzpitnegaRoka");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		JSONObject json = new JSONObject();
		
		if(request.getParameter("submitValue") != null) {
	
			// poberemo podatke iz forme
			int idRoka = Integer.parseInt(request.getParameter("izpitniRok"));
			
			// če je uporabnik pritisnil gumb izbriši
			if(Integer.parseInt(request.getParameter("submitValue")) != 0) {
				
				// preverjanje ali je možen izbris izpitnega roka
				if(preveriAliJeMozenIzbris(idRoka)) {
					
					// posodabljanje statusov v podatkovni bazi
					try {
						
						em.getTransaction().begin();
						
						// "brisanje" vseh prijav za izbran izpitni rok
						Query q = em.createQuery(	"SELECT oc " +
													"FROM Ocena oc " +
													"WHERE oc.fkIzpitniRok = :idIzpitnegaRoka " +
													"AND oc.aktivna = 'true'");
						
						q.setParameter("idIzpitnegaRoka", idRoka);
						List<Ocena> ocene = (List<Ocena>) q.getResultList();
						
						for(Ocena o : ocene) {
							o.setAktivna(false);
							em.merge(o);
						}
						
						// "brisanje" izpitnega roka
						Izpitni_rok ir = em.find(Izpitni_rok.class, idRoka);
						ir.setStatus(false);
						em.merge(ir);
						
						em.getTransaction().commit();
						
						//request.setAttribute("uspeh", "Uspešno brisanje izpitnega roka");
						json.put("uspeh", "Uspešno brisanje izpitnega roka"); 
						
					} catch (Exception e) {
						System.out.println(e.getMessage());
						em.getTransaction().rollback();
						request.setAttribute("uspeh", "<span class='error'>Napaka pri brisanju</span>");
					}
				
				} else {
					request.setAttribute("uspeh", "<span class='error'>Za izrani rok že obstajajo ocene</span>");
				}
				
				//RequestDispatcher rd=request.getRequestDispatcher("index.jsp?stran=spremembaIzpitnegaRoka");
				//rd.forward(request, response);
				response.setContentType("text/html; charset=utf-8");   
				PrintWriter out = response.getWriter();
				out.print(json);
			
			}
		}
	}
	
	// preverjanje ali je možna prijava na izpit
	private boolean preveriAliJeMozenIzbris(int idIzpitnegaRoka) {
		boolean status = true;
		
		// preverimo če ze obstaja ocena za ta izpitni rok
		try {
			
			Query q = em.createQuery(	"SELECT COUNT(oc) " +
										"FROM Ocena oc " +
										"WHERE oc.fkIzpitniRok = :idIzpitnegaRoka " +
										"AND oc.aktivna = 'true' " +
										"AND oc.ocena != 0");
			
			q.setParameter("idIzpitnegaRoka", idIzpitnegaRoka);
			int steviloPrijavZOceno = (Integer) q.getSingleResult();
			
			if(steviloPrijavZOceno > 0) {
				status = false;
			}
		
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return status;
	}

	// preberemo predmete
	private HttpServletRequest pripravaPodatkovZaFormo(HttpServletRequest request) {
		
		HttpSession session = request.getSession();
		
		Vloga_oseba vloga = (Vloga_oseba)session.getAttribute("vloga");
		
		
		if(vloga.getNazivVlogaOseba().compareTo("referentka") == 0) {
			// če je uprobnik vpisan kot referentka mu prikažemo vse predmete
			Query q = em.createQuery("SELECT DISTINCT(p) FROM Predmet p, Predmet_izvajalec pi WHERE pi.predmet.idPredmeta = p.idPredmeta AND pi.status = true ORDER BY p.nazivPredmet ASC");
			List<Predmet> predmeti = (List<Predmet>) q.getResultList();						
			request.setAttribute("predmeti", predmeti);	
			
		} else {
			// če je uporabnik učitelj mu prikažemo samo predmete katere poučuje
			ArrayList<Predmet> predmeti = new ArrayList<Predmet>();
			Oseba uporabnik = (Oseba)session.getAttribute("uporabnik");
			try {
				Query q = em.createQuery("SELECT os FROM Osebje os WHERE os.oseba.idOseba = :idUporabnika");
				q.setParameter("idUporabnika", uporabnik.getIdOseba());
				Osebje os = (Osebje)q.getSingleResult();
				
				q = em.createQuery("SELECT ki FROM Kombinacije_izvajalcev ki WHERE ki.osebje1.idOsebje = :idOsebe OR ki.osebje2.idOsebje = :idOsebe OR ki.osebje3.idOsebje = :idOsebe");
				q.setParameter("idOsebe", os.getIdOsebje());
				List<Kombinacije_izvajalcev> ki = (List<Kombinacije_izvajalcev>)q.getResultList();
				for(Kombinacije_izvajalcev komb: ki) {
					
					List<Predmet_izvajalec> pi =  komb.getPredmetIzvajalecs();
					
					for(Predmet_izvajalec p : pi) {
						if(p.isStatus() == true) {
							predmeti.add(p.getPredmet());
						}
					}	
				}
			}catch (Exception e) {
				System.out.println(e.getMessage());
			}
			request.setAttribute("predmeti", predmeti);	
		}
		
		return request;
	}
}
