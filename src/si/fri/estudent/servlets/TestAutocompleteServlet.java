package si.fri.estudent.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Servlet implementation class TestAutocompleteServlet
 */
@WebServlet("/TestAutocompleteServlet")
public class TestAutocompleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestAutocompleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8"); // Brez tega zopet ne gre =) ne delujejo ČĆŽŠĐ
		String query = request.getParameter("query").toLowerCase(); // Dobimo ven znake, po katerih iščemo. Obvezna pretvorba v LowerCase
		
		List<String> drzave = new LinkedList<String>(); // To bi sicer selectali iz baze
		drzave.add("Avstrija");
		drzave.add("Brazilija");
		drzave.add("Češka");
		drzave.add("Francija");
		drzave.add("Malta");
		drzave.add("Nigerija");
		drzave.add("Nizozemska");
		drzave.add("Slovenija");
		drzave.add("Slovaška");
		drzave.add("Srbija");
		drzave.add("Somalija");
		drzave.add("Švica");
		drzave.add("Švedska");
		drzave.add("Žiljadnija");
		drzave.add("Điljadnija");
		drzave.add("Ćiljadnija");
		
		JSONArray jsonArray = new JSONArray(); // JSONArray, ki predtavlja polje vseh podatkov, ki jih bomo vrnili
		JSONObject jsonRecord; // JSONObject, ki predstavlja posamezen record
		
		int kljuc = 0; // Tudi ključe bi seveda selectali iz baze, tukaj sem dal samo en int, da simuliram različne ključe
		for(String drzava : drzave) {
			if(drzava.toLowerCase().startsWith(query)) { // Primerjamo, če se trenutna država začne na enake znake kot string query (oboje v malih znakih!!!)
				jsonRecord = new JSONObject();
				jsonRecord.put("id", kljuc); // Ko ustvarjamo nov JSONObject, moramo nujno vstaviti ključ kot atribut "id"...
				jsonRecord.put("value", drzava); // in vrednost kod atribut "value". Če ne se ne sparsa pravilno.
				
				jsonArray.add(jsonRecord); // Potem ta JSONObject dodamo v JSONArray. To je nujno, ker jQuery hoče dobit specifično tako obliko.
			}
			kljuc++; // Simuliranje kluča
		}
		
		response.setContentType("text/html; charset=utf-8"); // copy paste =))
		PrintWriter out = response.getWriter();
		out.print(jsonArray);
	}
}
