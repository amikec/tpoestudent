package si.fri.estudent.servlets;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import si.fri.estudent.jpa.Kombinacije_izvajalcev;
import si.fri.estudent.jpa.Osebje;
import si.fri.estudent.jpa.Predmet;
import si.fri.estudent.jpa.Predmet_izvajalec;

/**
 * Servlet implementation class VzdrzevanjeIzvajalcevUrejanjeServlet
 */
@WebServlet("/VzdrzevanjeIzvajalcevUrejanjeServlet")
public class VzdrzevanjeIzvajalcevUrejanjeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	EntityManager em;
    public VzdrzevanjeIzvajalcevUrejanjeServlet() {
        super();
        em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		if(request.getParameter("id") != null) {
			int id = Integer.parseInt(request.getParameter("id"));
			try {
				em.getTransaction().begin();				
				Predmet_izvajalec pi = em.find(Predmet_izvajalec.class, id);
				request.setAttribute("predmetIzvajalec", pi);
				em.getTransaction().commit();
			} catch (Exception e) {
				em.getTransaction().rollback();
				System.out.println(e.getLocalizedMessage());
			}
		}
		pripravaPodatkovZaFormo(request);
		RequestDispatcher rd=request.getRequestDispatcher("index.jsp?stran=vzdrzevanjeIzvajalcevUrejanje");
		rd.forward(request, response);
	}
	private void pripravaPodatkovZaFormo(HttpServletRequest request) {
		
		Query q = em.createQuery("SELECT p FROM Predmet p WHERE p.status = true");
	    List<Predmet> predmeti = (List<Predmet>)q.getResultList();
	    
	    request.setAttribute("predmeti", predmeti);
	    
	    q = em.createQuery("SELECT o FROM Osebje o ORDER BY o.oseba.priimek ASC");
	    List<Osebje> osebje = (List<Osebje>)q.getResultList();
	    
	    request.setAttribute("osebje", osebje);
		
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		int id = 0;
		if(request.getParameter("id") != null) {
			id = Integer.parseInt(request.getParameter("id"));
		}
		int idPredmeta = Integer.parseInt(request.getParameter("predmet"));
		String izbireString = request.getParameter("izbire");
		String stareIzbire = request.getParameter("stareIzbire");
		if(izbireString.equals("")){
			request.setAttribute("uspeh", "<span class='error'>Niste izbrali predavatelja</span>");
		} else {
			int [] izbire = stringToIntTable(izbireString);
		
			if(idPredmeta != -1) {
				if(izbire.length <= 3) {
					try{
						//pobrisi staro kombinacijo
						Kombinacije_izvajalcev ki = new Kombinacije_izvajalcev();
						Query q = null;
						int [] stare = stringToIntTable(stareIzbire);
						em.getTransaction().begin();
						if(stare.length == 1) {
							q = em.createQuery("SELECT ki FROM Kombinacije_izvajalcev ki WHERE ki.osebje1 = :o1");
							q.setParameter("o1", em.find(Osebje.class, stare[0]));
							ki = (Kombinacije_izvajalcev)q.getResultList().get(0);
							ki.setStatus(false);
							em.persist(ki);
						} else if(stare.length == 2) {
							q = em.createQuery("SELECT ki FROM Kombinacije_izvajalcev ki WHERE ki.osebje1 = :o1 AND ki.osebje2 = :o2");
							q.setParameter("o1", em.find(Osebje.class, stare[0]));
							q.setParameter("o2", em.find(Osebje.class, stare[1]));
							ki = (Kombinacije_izvajalcev)q.getResultList().get(0);
							ki.setStatus(false);
							em.persist(ki);
						} else if(stare.length == 3) {
							q = em.createQuery("SELECT ki FROM Kombinacije_izvajalcev ki WHERE ki.osebje1 = :o1 AND ki.osebje2 = :o2 AND ki.osebje1 = :o3");
							q.setParameter("o1", em.find(Osebje.class, stare[0]));
							q.setParameter("o2", em.find(Osebje.class, stare[1]));
							q.setParameter("o3", em.find(Osebje.class, stare[2]));
							ki = (Kombinacije_izvajalcev)q.getResultList().get(0);
							ki.setStatus(false);
							em.persist(ki);
						}
						em.getTransaction().commit();
					} catch (Exception e) {
						em.getTransaction().rollback();
						System.out.println(e.getLocalizedMessage());
					}
					try {
						em.getTransaction().begin();
						Kombinacije_izvajalcev ki = new Kombinacije_izvajalcev();
						
						int dolzina = izbire.length;
						Query q;
						if(dolzina == 1) {
							ki.setOsebje1(em.find(Osebje.class, izbire[0]));
							ki.setStatus(true);
							em.persist(ki);
							q = em.createQuery("SELECT ki FROM Kombinacije_izvajalcev ki WHERE ki.osebje1 = :o1");
							q.setParameter("o1", em.find(Osebje.class, izbire[0]));
							ki = (Kombinacije_izvajalcev)q.getResultList().get(0);
						} else if(dolzina == 2) {
							ki.setOsebje1(em.find(Osebje.class, izbire[0]));
							ki.setOsebje2(em.find(Osebje.class, izbire[1]));
							ki.setStatus(true);
							em.persist(ki);
							q = em.createQuery("SELECT ki FROM Kombinacije_izvajalcev ki WHERE ki.osebje1 = :o1 AND ki.osebje2 = :o2");
							q.setParameter("o1", em.find(Osebje.class, izbire[0]));
							q.setParameter("o2", em.find(Osebje.class, izbire[1]));
							ki = (Kombinacije_izvajalcev)q.getResultList().get(0);
						} else if(dolzina == 3) {
							ki.setOsebje1(em.find(Osebje.class, izbire[0]));
							ki.setOsebje2(em.find(Osebje.class, izbire[1]));
							ki.setOsebje3(em.find(Osebje.class, izbire[2]));
							ki.setStatus(true);
							em.persist(ki);
							q = em.createQuery("SELECT ki FROM Kombinacije_izvajalcev ki WHERE ki.osebje1 = :o1 AND ki.osebje2 = :o2 AND ki.osebje3 = :o3");
							q.setParameter("o1", em.find(Osebje.class, izbire[0]));
							q.setParameter("o2", em.find(Osebje.class, izbire[1]));
							q.setParameter("o3", em.find(Osebje.class, izbire[2]));
							ki = (Kombinacije_izvajalcev)q.getResultList().get(0);
						}
						em.getTransaction().commit();
						
						em.getTransaction().begin();
						Predmet_izvajalec pi = em.find(Predmet_izvajalec.class, id);
						pi.setStatus(false);
						em.persist(pi);
						em.getTransaction().commit();
						em.getTransaction().begin();
						pi = new Predmet_izvajalec();
						pi.setKombinacijeIzvajalcev(ki);
						pi.setStatus(true);
						pi.setPredmet(em.find(Predmet.class, idPredmeta));				
						em.persist(pi);
						em.getTransaction().commit();
						
						request.setAttribute("uspeh", "Urejanje uspešno");
					} catch (Exception e) {
						System.out.println(e.getLocalizedMessage());
					}
				} else {
					request.setAttribute("uspeh", "<span class='error'>Maksimalno število predavateljev je 3</span>");
				}
			}
			else {
				request.setAttribute("uspeh", "<span class='error'>Napaka pri izbiri predmeta</span>");
			}
		}
		pripravaPodatkovZaFormo(request);
		vzdrzevanjeIzvajalcev(request);
		RequestDispatcher fw = request.getRequestDispatcher("index.jsp?stran=vzdrzevanjeIzvajalcev");
		fw.forward(request, response);
	}
	
	private void vzdrzevanjeIzvajalcev(HttpServletRequest request) {
		Query q = em.createQuery("SELECT Pi FROM Predmet_izvajalec Pi WHERE Pi.status = true ORDER BY Pi.predmet.nazivPredmet ASC");
	    List<Predmet_izvajalec> predmetIzvajalec = (List<Predmet_izvajalec>)q.getResultList();
	    
	    request.setAttribute("predmetIzvajalec", predmetIzvajalec);
		
	}

	private int[] stringToIntTable(String text) { // Metoda pretvori string števil ločenih z ',' v tabelo int
		  int[] tabelaInt = null;
		  
		  if (!text.equals("")) {
		   String[] izbire = text.split(",");
		   tabelaInt = new int[izbire.length];
		   int i = 0;;
		   
		   for (String izbira : izbire) {
		    tabelaInt[i++] = Integer.parseInt(izbira);
		   }
		   
		   return tabelaInt;
		  }
		  
		  return null;
	}
	

}
