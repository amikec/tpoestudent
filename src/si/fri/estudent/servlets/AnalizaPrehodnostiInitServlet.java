package si.fri.estudent.servlets;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class AnalizaPrehodnostiInitServlet
 */
@WebServlet("/AnalizaPrehodnostiInitServlet")
public class AnalizaPrehodnostiInitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AnalizaPrehodnostiInitServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EntityManager em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		
		//kateri letniki obstajajo
		/*
		String sqlLetniki="SELECT DISTINCT le.nazivLetnik FROM Letnik AS le ORDER BY le.nazivLetnik";		
		Query q=em.createQuery(sqlLetniki);
		List<String> letnikiList=(List<String>)q.getResultList();		
		String[] letniki=new String[letnikiList.size()];
		int i=0;
		for (String letnik : letnikiList) {
			letniki[i]=letnik;
			i++;
			System.out.println("letnik  "+letnik);
		}*/
		
		String[] letniki=new String[5];
		for(int i=0;i<5;i++){
			letniki[i]=""+(i+1);
		}
		
		//piscimo studijska leta
		String sql="SELECT DISTINCT le.studijskoLeto FROM Letnik AS le ORDER BY le.studijskoLeto";
		Query q=em.createQuery(sql);
		List<String> leta=(List<String>)q.getResultList();
		String[] studijskaLeta=new String[leta.size()];
		int i=0;
		for (String leto : leta) {
			studijskaLeta[i]=leto;
			i++;
			System.out.println("leto "+leto);
		}
		//programi
		
		//ta nacin bi blo dobr sam je jeba
		/*String sql2="SELECT pis.naziv FROM ProgramiInSmeri AS pis";
		q=em.createQuery(sql2);
		List<String> pis=(List<String>)q.getResultList();
		String[] programiInSmeri=new String[pis.size()+1];
		i=0;
		for (String prinsm : pis) {
			programiInSmeri[i]=prinsm;
			i++;
			System.out.println("program in smer "+prinsm);
		}
		programiInSmeri[pis.size()]="Ne glede";*/
		
		String[] programiInSmeri={"RAČUNALNIŠTVO IN INFORMATIKA - BUN","RAČUNALNIŠTVO IN INFORMATIKA - BVS",
				"RAČUNALNIŠTVO IN MATEMATIKA - BUN","Ne glede"};
		
		String[] nacinStudija={"Redni študij","Izredni študij","Ne glede"};
		String[] vrstaVpisa={"V1 - prvi vpis v letnik","V2 - ponavljanje letnika","V3 - nadaljevanje letnika","AB - absolvent","Ne glede"};
		
		
		request.setAttribute("letniki", letniki);
		request.setAttribute("studijskaLeta", studijskaLeta);
		request.setAttribute("programiInSmeri", programiInSmeri);
		request.setAttribute("nacinStudija", nacinStudija);
		request.setAttribute("vrstaVpisa", vrstaVpisa);
		
		/*
		HttpSession seja=request.getSession();
		seja.setAttribute("letniki", letniki);
		seja.setAttribute("studijskaLeta", studijskaLeta);
		seja.setAttribute("programiInSmeri", programiInSmeri);
		seja.setAttribute("nacinStudija", nacinStudija);
		seja.setAttribute("vrstaVpisa", vrstaVpisa);
		*/
		
		RequestDispatcher nov=request.getRequestDispatcher("index.jsp?stran=analizaPrehodnosti");
		nov.forward(request, response);
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
