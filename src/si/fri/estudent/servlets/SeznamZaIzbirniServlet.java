package si.fri.estudent.servlets;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import si.fri.estudent.jpa.Letnik;
import si.fri.estudent.jpa.Oseba;
import si.fri.estudent.jpa.Student;
import si.fri.estudent.utilities.Tiskanje;

/**
 * Servlet implementation class SeznamZaIzbirniServlet
 */
@WebServlet("/SeznamZaIzbirniServlet")
public class SeznamZaIzbirniServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SeznamZaIzbirniServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession seja=request.getSession(); // seja za shrant za tiskanje
		String[][] podatki = (String[][])seja.getAttribute("podatki");
		String leto = (String)seja.getAttribute("leto");
		String predmet = (String)seja.getAttribute("predmet");
		
		//dodat headerje
		String[] head={"Ime","Priimek","Vpisna številka"};
		String[][] podatki2=new String[podatki.length+1][3];
		podatki2[0]=head;
		for(int i=0;i<podatki.length;i++){
			podatki2[i+1]=podatki[i];
		}
		
		Tiskanje tiskanje=new Tiskanje(request, response, "Seznam za izbirni predmet");
		tiskanje.dodajBesediloBrezProsteVrstice("Predmet: "+predmet);
		tiskanje.dodajBesediloBrezProsteVrstice("Študijsko leto: "+ leto);
		tiskanje.dodajPraznoVrstico();
		tiskanje.dodajTabelo(podatki2, false, true);
		tiskanje.dobiDocument();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		EntityManager em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		
		String predmet=request.getParameter("predmet");
		String studijskoLeto=request.getParameter("studijskoLeto");
		System.out.println("predmet "+ predmet +" studijskoLeto "+studijskoLeto);
		String sql="";
		Query q;
		if(studijskoLeto.length()>15){//dve studijski leti
			int tole=studijskoLeto.indexOf(" in ");
			String stLeto1=studijskoLeto.substring(0,tole);
			String stLeto2=studijskoLeto.substring(tole+4);
			sql="SELECT st " +
					"FROM Student st, Predmet predmet, Predmetnik_letnika pl, Predmetnik predmetnik, Vpisni_list vl " +
					" WHERE " +
					" predmet.nazivPredmet = :nazivPredmet AND " +
					" pl.predmet = predmet AND " +
					" predmetnik.fkPredmetnikLetnika = pl.idPredmetnikLetnika AND " +
					" vl.idVpisni = predmetnik.fkVpisniList AND " +
					" ( vl.studijskoLeto = :leto1 OR vl.studijskoLeto = :leto2 ) AND " +
					" st = vl.student " +
				"ORDER BY " +
					"st.oseba.priimek ";
			q=em.createQuery(sql);
			q.setParameter("nazivPredmet", predmet);
			q.setParameter("leto1", stLeto1);
			q.setParameter("leto2", stLeto2);
		}else{
			sql="SELECT st " +
					"FROM Student st, Predmet predmet, Predmetnik_letnika pl, Predmetnik predmetnik, Vpisni_list vl " +
					" WHERE " +
					" predmet.nazivPredmet = :nazivPredmet AND " +
					" pl.predmet = predmet AND " +
					" predmetnik.fkPredmetnikLetnika = pl.idPredmetnikLetnika AND " +
					" vl.idVpisni = predmetnik.fkVpisniList AND " +
					" vl.studijskoLeto = :leto AND" +
					" st = vl.student " +
				"ORDER BY " +
					"st.oseba.priimek";
			q=em.createQuery(sql);
			q.setParameter("nazivPredmet", predmet);
			q.setParameter("leto", studijskoLeto);
		}
	
		List<Student> studenti=q.getResultList();
		String[][] podatki=new String[studenti.size()][3];
		int i = 0;
		for(Student st:studenti){
			Oseba os=st.getOseba();
			System.out.println("studnet "+os.getIme()+" "+os.getPriimek());
			podatki[i][0]=os.getIme();
			podatki[i][1]=os.getPriimek();
			podatki[i][2]=st.getVpisnaSt();
			i++;
		}
		
		request.setAttribute("podatki",podatki);
		HttpSession seja=request.getSession(); // seja za shrant za tiskanje
		seja.setAttribute("podatki",podatki);
		seja.setAttribute("leto",studijskoLeto);
		seja.setAttribute("predmet",predmet);
		
		RequestDispatcher nov=request.getRequestDispatcher("Ajax/seznamZaIzbirniAjax.jsp");
		nov.forward(request, response);
		System.out.println("poslano");
		return;
	}

}
