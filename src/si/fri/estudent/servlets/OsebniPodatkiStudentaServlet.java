package si.fri.estudent.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sun.org.apache.xerces.internal.impl.dv.DatatypeValidator;



import si.fri.estudent.jpa.Naslov;
import si.fri.estudent.jpa.Oseba;
import si.fri.estudent.jpa.Student;
import si.fri.estudent.jpa.Vpisni_list;
import si.fri.estudent.utilities.DataValidator;
import si.fri.estudent.utilities.Tiskanje;

/**
 * Servlet implementation class OsebniPodatkiStudentaServlet
 */
@WebServlet("/OsebniPodatkiStudentaServlet")
public class OsebniPodatkiStudentaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private String[][] tabela;
    private String vpisna;
    private String[][][] vpisi;
    private Oseba oseba;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OsebniPodatkiStudentaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	request.setCharacterEncoding("UTF-8");
		EntityManager em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		
		
		
		Student student=null;
		oseba=null;
		
		//dobi studenta iz seje
		if(request.getSession().getAttribute("studentIzIskanja")!=null){
			student=(Student)request.getSession().getAttribute("studentIzIskanja");
		}else{
			
		}
			
		oseba=student.getOseba();
		
		List<Vpisni_list> vpisniListi=student.getVpisniLists();  /// ta vrze error takoj po vpisu studenta
		
		///* TA KODA DELA ZA ISKANJE PO VPISNI
//		q = em.createQuery("SELECT v from Vpisni_list v, Student s WHERE v.student.idStudent=s.idStudent AND s.vpisnaSt=:vpisna");
//		q.setParameter("vpisna", student.getVpisnaSt());
//		List<Vpisni_list> vpisniListi= (List<Vpisni_list>)q.getResultList();
		//*/
		
		//List<Vpisni_list> vpisniListi=new ArrayList<Vpisni_list>();
		
		Naslov naslov1=oseba.getNaslov1();
		Naslov naslov2=oseba.getNaslov2();			
		
		ArrayList<String[]> table=new ArrayList<String[]>();
		//table.add(new String[]{"Vpisna številka",""+student.getVpisnaSt()});
		table.add(new String[]{"Ime:",oseba.getIme()});
		table.add(new String[]{"Priimek:",oseba.getPriimek()});
		table.add(new String[]{"Spol:",oseba.getSpol()});		
		if(oseba.getPriimekDekliski()!=null){
			table.add(new String[]{"Dekliški priiimek:",oseba.getPriimekDekliski()});
		}
		
		table.add(new String[]{"Ulica:",naslov1.getUlica()});
		//table.add(new String[]{"Kraj",naslov1.getKraj().getPostnaSt()+" " +naslov1.getKraj().getNazivKraj()});
		table.add(new String[]{"Država:",naslov1.getDrzava().getNazivDrzava()});
		if(naslov2!=null){			
			table.add(new String[]{"Ulica (Začasni naslov):",naslov2.getUlica()});
			//table.add(new String[]{"Kraj (Začasni naslov)",naslov2.getKraj().getPostnaSt()+" " +naslov1.getKraj().getNazivKraj()});
			
			if(naslov2.getDrzava()!=null){
				table.add(new String[]{"Država (Začasni naslov):",naslov2.getDrzava().getNazivDrzava()});
			}
		}
		table.add(new String[]{"Davčna številka:",oseba.getDavcnaSt()});
		table.add(new String[]{"Telefon:",oseba.getTelefon()});
		table.add(new String[]{"Mobitel:",oseba.getMobitel()});
		table.add(new String[]{"Email:",oseba.getEmail()});
		table.add(new String[]{"EMSO:",oseba.getEmso()});
		
		
		vpisi=new String[vpisniListi.size()][7][2]; 
		int i=0;
		for(Vpisni_list vl:vpisniListi){
			vpisi[i]=new String[][]{
					{"Študijsko leto:",""+vl.getStudijskoLeto()},
					{"Letnik:",""+vl.getLetnik().getNazivLetnik()},
					{"Način študija:",""+vl.getNacinStudija()},
					{"Vrsta študija:",""+vl.getLetnik().getStudij().getVrstaStudija().getNazivVrstaStudija()},
					{"Študijski program:",""+vl.getLetnik().getStudij().getStudijskiProgram().getNazivStudijskiProgram()},										
					{"Vrsta vpisa:",vl.getVrstaVpisa().getNazivVrstaVpisa()}};	
			i++;
		}		
		tabela=new String[table.size()][2];
		table.toArray(tabela);	
		vpisna=""+student.getVpisnaSt();
		
    	super.service(request, response);
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		String nacinIzpisa=request.getParameter("nacinIzpisa");
		if(nacinIzpisa!=null && nacinIzpisa.equals("tiskanje")){
			//tiskanje			
			Tiskanje tiskanje=new Tiskanje( request,response,"Osebni podatki študenta");			
			tiskanje.dodajTabelo(tabela,true,false);
			
			if(vpisi.length>0){
				tiskanje.dodajBesedilo("                                                                                                         Vpisi: ");
			}
			for(String[][] tab: vpisi){
				tiskanje.dodajTabelo(tab, true, false);
			}
			tiskanje.dobiDocument();
			
		}else{		
			//izpis na strani
			String url = "";
			request.setAttribute("tabela", tabela);
			request.setAttribute("vpisna", ""+vpisna);
			request.setAttribute("vpisi", vpisi);
			url = "index.jsp?stran=osebniPodatkiStudentaIzpis";
			RequestDispatcher nov=request.getRequestDispatcher(url);
			nov.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
//		String url = "";
//		HttpSession session = request.getSession();
//		
//		if(session.getAttribute("prijavaNaIzpit") != null){
//			session.removeAttribute("prijavaNaIzpit");
//			session.setAttribute("prijaviStudenta", oseba);
//			url = "PrijavaNaIzpitAjaxServlet";
//		}else{
//			request.setAttribute("tabela", tabela);
//			request.setAttribute("vpisna", ""+vpisna);
//			request.setAttribute("vpisi", vpisi);
//			url = "index.jsp?stran=osebniPodatkiStudentaIzpis";
//		}
//		
//		RequestDispatcher nov=request.getRequestDispatcher(url);
//		nov.forward(request, response);
	}
}
