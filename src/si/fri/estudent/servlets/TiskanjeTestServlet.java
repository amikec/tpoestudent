package si.fri.estudent.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import si.fri.estudent.utilities.Tiskanje;

/**
 * Servlet implementation class TiskanjeTest
 */
@WebServlet("/TiskanjeTest")

public class TiskanjeTestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TiskanjeTestServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("zagon get Tiskanje test");
		
		//ustvari objekt, podat je treba poleg request in response se glavni naslov
		Tiskanje tiskanje=new Tiskanje( request,response,"Naslov test");
		
		//dodaj besedilo
		String besedilo="Testni izpis normalnega besedila. Da pa preverimo delovanje, sem dodal še nekaj odvečnih besed, da bi mogoče vse skupaj prišlo vsaj nekaj vrstic.";
		String besedilo2="Testni izpis normalnega besedila.";
		tiskanje.dodajBesedilo(besedilo);
		
		//2d polje stringov, ki se izpise v tabelo
		String[][] tabela={{"assdsa&#272;;","bbbb ","vvcx"},{"ccc","cccc","dasdsa"},{"asdbb","sdgrt","hhhfg"}};
		tiskanje.dodajTabelo(tabela,true,true);
		
		//2d polje stringov, ki se izpise v tabelo, pri tej metodi je celotni prvi stolpec poudarjen tako kot prva vrstica		
		tiskanje.dodajTabelo(tabela,true,false);
		tiskanje.dodajTabelo(tabela,false,true);
		tiskanje.dodajTabelo(tabela,false,false);
		
		
		//na koncu da zakljuci in izdela pdf
		tiskanje.zakljuci();

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
