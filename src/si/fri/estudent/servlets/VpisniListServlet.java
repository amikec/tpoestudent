package si.fri.estudent.servlets;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import si.fri.estudent.jpa.*;
import si.fri.estudent.utilities.DataValidator;

/**
 * Servlet implementation class VpisniListServlet
 */
@WebServlet("/VpisniListServlet")
public class VpisniListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VpisniListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		request = pripraviPodatkeZaVpisniList(request);
		
		RequestDispatcher fw = request.getRequestDispatcher("index.jsp?stran=vpisniList");
		fw.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		// Input text
		String vpisnaStevilka = request.getParameter("vpisnaStevilka");
		String priimek = request.getParameter("priimek");
		String dekliskiPriimek = request.getParameter("dekliskiPriimek");
		String ime = request.getParameter("ime");
		String emso = request.getParameter("emso");
		String davcnaStevilka = request.getParameter("davcnaStevilka");
		
		String ulicaS = request.getParameter("ulicaS");
		String postaS = request.getParameter("postaS");
		String postnaStevlikaS = request.getParameter("postnaStevilkaS");
		String obcinaS = request.getParameter("obcinaS");
		String drzava = request.getParameter("drzava");
		String telefon = request.getParameter("telefon");
		String prenosni = request.getParameter("prenosni");
		String email = request.getParameter("email");
		
		String ulicaZ = request.getParameter("ulicaZ");
		String postaZ = request.getParameter("postaZ");
		String postnaStevlikaZ = request.getParameter("postnaStevilkaZ");
		String obcinaZ = request.getParameter("obcinaZ");
		
		String skupina = request.getParameter("skupina");
		String spol = "M";
		
		// Checkboxi
		String[] posebnePotrebe = request.getParameterValues("posebnePotrebe");
		
		// RadioButtni
		int vrstaStudija = Integer.parseInt(request.getParameter("vrstaStudija"));
		String letnikStudija = request.getParameter("letnikStudija");
		String nacinStudija = request.getParameter("nacinStudija");
		String studijNaDaljavo = request.getParameter("studijNaDaljavo");
		int vrstaVpisa = Integer.parseInt(request.getParameter("vrstaVpisa"));
		
		// Selecti
		int studijskiProgram = Integer.parseInt(request.getParameter("studijskiProgramVpisniList"));
		int smerSkupina = Integer.parseInt(request.getParameter("smerSkupina"));
		String studijskoLeto = request.getParameter("studijskoLeto");
		
		// Input hidden (autocomplete)
		int idPostaS = -1;
		int idPostaZ = -1;
		int idDrzava = -1;
		int idObcinaS = -1;
		int idObcinaZ = -1;
		if (!request.getParameter("idPostaS").equals("")) {
			idPostaS = Integer.parseInt(request.getParameter("idPostaS"));
		}
		if (!request.getParameter("idPostaZ").equals("")) {
			idPostaZ = Integer.parseInt(request.getParameter("idPostaZ"));
		}
		if (!request.getParameter("idDrzava").equals("")) {
			idDrzava = Integer.parseInt(request.getParameter("idDrzava"));
		}
		if (!request.getParameter("idObcinaS").equals("")) {
			idObcinaS = Integer.parseInt(request.getParameter("idObcinaS"));
		}
		if (!request.getParameter("idObcinaZ").equals("")) {
			idObcinaZ = Integer.parseInt(request.getParameter("idObcinaZ"));
		}
		
		EntityManager em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		DataValidator validator = new DataValidator();
		boolean napaka = false;
		Query q;
		
		// -------------------------------------------- VLADIACIJE -------------------------------------------- //
		
		// Osebni podatki študenta
		if (!validator.validateInputText(vpisnaStevilka, 8, false, "^63\\d{6}$", "Prosim vnesite vpsino številko", "Predolga vpisna številka", "Napačen format vpisne številke")) {
			request.setAttribute("errorVpisnaStevilka", validator.getError());
		}
		if (!validator.validateInputText(priimek, 45, false, "^[A-ZŠĐČĆŽ]{1}[a-zšđčćž]{1,44}$", "Prosim vnesite priimek", "Predolg priimek", "Napačen format priimka")) {
			request.setAttribute("errorPriimek", validator.getError());
		}
		if (!validator.validateInputText(dekliskiPriimek, 45, true, "^[A-ZŠĐČĆŽ]{1}[a-zšđčćž]{1,44}$", "", "Predolg dekliški priimek", "Napačen format dekliškega priimka")) {
			request.setAttribute("errorDekliskiPriimek", validator.getError());
		}
		if (!validator.validateInputText(ime, 45, false, "^[A-ZŠĐČĆŽ]{1}[a-zšđčćž]{1,44}$", "Prosim vnesite ime", "Predolgo ime", "Napačen format imena")) {
			request.setAttribute("errorIme", validator.getError());
		}
		if (!validator.validateEmso(emso, drzava)) {
			request.setAttribute("errorEmso", validator.getError());
		} else {
			if (emso.charAt(9) == '0') {
				spol = "M";
			} else {
				spol = "Ž";
			}
		}
		if (!validator.validateInputText(davcnaStevilka, 8, false, "^[0-9]{8}$", "Prosim vnesite davčno številko", "Predolga davčna številka", "Napačen format davčne številke")) {
			request.setAttribute("errorDavcnaStevilka", validator.getError());
		}
		
		// Naslov stalnega bivališča
		if (!validator.validateInputText(ulicaS, 45, false, "^[A-ZČĆŽŠĐ][A-ZČĆŽŠĐa-z0-9čćžšđ .]{1,44}$", "Prosim vnesite stalno ulico", "Predolga stalna ulica", "Napačen format stalne ulice")) {
			request.setAttribute("errorUlicaS", validator.getError());
		}
		if (!validator.validateInputText(postaS, 45, false, "^[A-ZČĆŽŠĐ][A-ZČĆŽŠĐa-zčćžšđ.\\-, ]{1,44}$", "Prosim vnesite stalno pošto", "Predolga stalna pošta", "Napačen format stalne pošte")) {
			request.setAttribute("errorPostaS", validator.getError());
		} else {
			Kraj stalniKraj = em.find(Kraj.class, idPostaS);
			if (stalniKraj == null) {
				request.setAttribute("errorPostaS", "Neveljaven id stalne pošte");
				napaka = true;
			} else if (!(stalniKraj.getPosta().equals(postaS) && stalniKraj.getPostnaSt().equals(postnaStevlikaS))) {
				request.setAttribute("errorPostaS", "Neujemanje pošte in poštne številke");
				napaka = true;
			}
		}
		if (!validator.validateInputText(postnaStevlikaS, 10, false, "^[1-9][0-9]{3,9}$", "Prosim vnesite stalno poštno številko", "Predolga stalna poštna številka", "Napačen format stalne poštne številke")) {
			request.setAttribute("errorPostnaStevilkaS", validator.getError());
		}
		if (!validator.validateInputText(obcinaS, 45, false, "^[A-ZČĆŽŠĐ][A-ZČĆŽŠĐa-zčćžšđ.\\-, /]{1,44}$", "Prosim vnesite stalno občino", "Predolga stalna občina", "Napačen format stalne občine")) {
			request.setAttribute("errorObcinaS", validator.getError());
		} else {
			Obcina stalnaObcina = em.find(Obcina.class, idObcinaS);
			if (stalnaObcina == null) {
				request.setAttribute("errorObcinaS", "Neveljaven id stalne občine");
				napaka = true;
			} 
		}
		if (!validator.validateInputText(drzava, 45, false, "^[A-ZČĆŽŠĐ][A-ZČĆŽŠĐa-zčćžšđ ]{1,44}$", "Prosim vnesite državo", "Predolgo ime države", "Napačen format države")) {
			request.setAttribute("errorDrzava", validator.getError());
		}else {
			Drzava drzavaJPA = em.find(Drzava.class, idDrzava);
			if (drzavaJPA == null) {
				request.setAttribute("errorDrzava", "Neveljaven id države");
				napaka = true;
			} 
		}
		if (!validator.validateInputText(telefon, 45, false, "^(0[1-7])?[ /]?[0-9]{3}[\\- ]?[0-9]{2}[\\- ]?[0-9]{2}$", "Prosim vnesite telefonsko številko", "Predolga telefonska številka", "Napačen format telefonske številke")) {
			request.setAttribute("errorTelefon", validator.getError());
		}
		if (!validator.validateInputText(prenosni, 45, false, "^[0-9]{3}[ /-]?[0-9]{3}[- ]?[0-9]{3}[- ]?$", "Prosim vnestie prenosni telefon", "Predolg prenosni telefon", "Napačen format prenosnega telefona")) {
			request.setAttribute("errorPrenosni", validator.getError());
		}
		if (!validator.validateInputText(email, 45, false, "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", "Prosim vnesite elektornsko pošto", "Predolga elektronska pošta", "Napačen format elektronske pošte")) {
			request.setAttribute("errorEmail", validator.getError());
		}
		
		// Naslov začanega bivališča
		if (!validator.validateInputText(ulicaZ, 45, true, "^[A-ZČĆŽŠĐ][A-ZČĆŽŠĐa-z0-9čćžšđ .]{1,44}$", "", "Predolga začasna ulica", "Napačen format začasne ulice")) {
			request.setAttribute("errorUlicaZ", validator.getError());
		}
		if (!validator.validateInputText(postaZ, 45, true, "^[A-ZČĆŽŠĐ][A-ZČĆŽŠĐa-zčćžšđ.\\-, ]{1,44}$", "", "Predolga začasna pošta", "Napačen format začašne pošte")) {
			request.setAttribute("errorPostaZ", validator.getError());
		} else {
			if (!postaZ.equals("")) { // ni obvezna
				Kraj zacasniKraj = em.find(Kraj.class, idPostaZ);
				if (zacasniKraj == null) {
					request.setAttribute("errorPostaZ", "Neveljaven id začase pošte");
					napaka = true;
				} else if (!(zacasniKraj.getPosta().equals(postaZ) && zacasniKraj.getPostnaSt().equals(postnaStevlikaZ))) {
					request.setAttribute("errorPostaZ", "Neujemanje pošte in poštne številke");
					napaka = true;
				}
			}
		}
		if (!validator.validateInputText(postnaStevlikaZ, 10, true, "^[1-9][0-9]{3,9}$", "", "Predolga začasna poštna številka", "Napačen format začasne poštne številke")) {
			request.setAttribute("errorPostnaStevilkaZ", validator.getError());
		}
		if (!validator.validateInputText(obcinaZ, 45, true, "^[A-ZČĆŽŠĐ][A-ZČĆŽŠĐa-zčćžšđ.\\-, /]{1,44}$", "", "Predolga začasna občina", "Napačen format začasne občine")) {
			request.setAttribute("errorObcinaZ", validator.getError());
		} else {
			if (!obcinaZ.equals("")) { // ni obvezna
				Obcina zacasnaObcina = em.find(Obcina.class, idObcinaZ);
				if (zacasnaObcina == null) {
					request.setAttribute("errorObcinaZ", "Neveljaven id začasne občine");
					napaka = true;
				}
			}
		}
		
		if (validator.getError() != null || napaka) {
			request = pripraviPodatkeZaVpisniList(request);
			request.setAttribute("error", "error");
			RequestDispatcher fw = request.getRequestDispatcher("index.jsp?stran=vpisniList");
			fw.forward(request, response);
		} else {
			// -------------------------------------------- VNOS V BAZO -------------------------------------------- //
			
			try {
				em.getTransaction().begin();
				// Država, Kraj, Obcina, Naslov
				Drzava drzavaJPA = em.find(Drzava.class, idDrzava);
				
				Kraj krajStalni = em.find(Kraj.class, idPostaS);
				Kraj krajZacasni = null;
				if (idPostaZ != -1) {
					krajZacasni = em.find(Kraj.class, idPostaZ);
				}
				
				Obcina obcinaStalna = em.find(Obcina.class, idObcinaS);
				Obcina obcinaZacasna = null;
				if (idObcinaZ != -1) {
					obcinaZacasna = em.find(Obcina.class, idObcinaZ);
				}
				
				Oseba osebaNova = new Oseba();
				Student student = new Student();
				Prijava prijava = new Prijava();
				Naslov naslovZacasni = null;
				Naslov naslovStalni = new Naslov();
				
				boolean studentObstaja = false;
				
				Student st = preveriObstojStudenta(emso, vpisnaStevilka);
				Oseba osebaObstojeca = null;
				
				if(st != null) {
					osebaNova = st.getOseba();
					studentObstaja = true;
					student = st;
					prijava = osebaNova.getPrijavas().get(0);
					naslovStalni = osebaNova.getNaslov1();
					naslovZacasni = osebaNova.getNaslov2();
				}
				
				naslovStalni.setUlica(ulicaS);
				naslovStalni.setKraj(krajStalni);
				naslovStalni.setDrzava(drzavaJPA);
				naslovStalni.setObcina(obcinaStalna);

				if (idPostaZ != -1 && idObcinaZ != -1) {
					naslovZacasni = new Naslov();
					naslovZacasni.setUlica(ulicaZ);
					naslovZacasni.setKraj(krajZacasni);
					naslovZacasni.setDrzava(null);
					naslovZacasni.setObcina(obcinaZacasna);
				}
				
				// Oseba
				
				osebaNova.setIme(ime);
				osebaNova.setPriimek(priimek);
				osebaNova.setPriimekDekliski(dekliskiPriimek);
				osebaNova.setEmso(emso);
				osebaNova.setUpIme(vpisnaStevilka);
				osebaNova.setGeslo(vpisnaStevilka);
				osebaNova.setDavcnaSt(davcnaStevilka);
				osebaNova.setNaslov1(naslovStalni);
				osebaNova.setNaslov2(naslovZacasni);
				osebaNova.setTelefon(telefon);
				osebaNova.setEmail(email);
				osebaNova.setMobitel(prenosni);
				osebaNova.setSpol(spol);
				
				// Prijava
				
				prijava.setOseba(osebaNova);
				prijava.setStPoizkusov(0);
				prijava.setZadnjiPoizkus(null);
				
				// Vloga
				Vloga_oseba vlogaOseba = em.find(Vloga_oseba.class, 1);
				Vloga_oseba_has_Oseba vlogaOsebaHasOseba = new Vloga_oseba_has_Oseba();
				vlogaOsebaHasOseba.setVlogaOseba(vlogaOseba);
				vlogaOsebaHasOseba.setOseba(osebaNova);
				
				// Student
				
				student.setVpisnaSt(vpisnaStevilka);
				student.setOseba(osebaNova);
				
				if(!studentObstaja) { // Oseba še ne obstaja
					if(naslovZacasni != null) {
						em.persist(naslovZacasni);
					}
					
					em.persist(naslovStalni);
					em.persist(osebaNova);
					em.persist(vlogaOsebaHasOseba);
					em.persist(prijava);
					em.persist(student);
					
				}else { // Oseba že obstaja
					
					//vlogaOsebaHasOseba = em.find(Vloga_oseba_has_Oseba.class, osebaNova.getIdOseba());
					
					if (osebaNova.getNaslov2() != null && idPostaZ != -1 && idObcinaZ != -1) {
						em.merge(naslovZacasni);
					}
					
					//osebaNova.getVlogaOsebaHasOsebas().add(vlogaOsebaHasOseba);
					
					//em.merge(vlogaOsebaHasOseba);
					em.merge(naslovStalni);
					em.merge(osebaNova);
					em.merge(student);
					em.merge(prijava);
					
					// Brisanje obstoječih posebnih potreb
					String sql = "SELECT distinct posp " +
							 "FROM Posebne_potrebe pos, Student st, Student_has_Posebne_potrebe posp " +
							 "WHERE " +
							 	" st.oseba.idOseba = ?1 AND " +
							 	" posp.student1.idStudent = st.idStudent";
				
					q = em.createQuery(sql);
					q.setParameter(1, osebaNova.getIdOseba());
					
					List<Student_has_Posebne_potrebe> pos = (List<Student_has_Posebne_potrebe>)q.getResultList();
					
					for(Student_has_Posebne_potrebe p : pos){
						em.remove(p);
					}
				}
				
				// Posebne potrebe
				if (posebnePotrebe != null) {
					Student_has_Posebne_potrebe student_has_Posebne_potrebe = null;
					for (String pp : posebnePotrebe) {
						student_has_Posebne_potrebe = new Student_has_Posebne_potrebe();
						Posebne_potrebe posebne_potrebe = em.find(Posebne_potrebe.class, Integer.parseInt(pp));
						student_has_Posebne_potrebe.setPosebnePotrebe1(posebne_potrebe);
						student_has_Posebne_potrebe.setStudent1(student);
						
						em.persist(student_has_Posebne_potrebe);
					}
				}
				
				// Študijska smer, Vrsta študija, Študijski program, Študij, Letnik
				
				q = em.createQuery("SELECT " +
										"l " +
						     	   "FROM " +
						     	       "Letnik l, Studij s, Studijska_smer sm, Vrsta_studija vs, Studijski_program sp " +
						     	   "WHERE " +
						     	   	   "sm.idStudijskaSmer = s.studijskaSmer.idStudijskaSmer AND " +
						     	   	   "vs.idVrstaStudija = s.vrstaStudija.idVrstaStudija AND " +
						     	   	   "sp.idStudijskiProgram = s.studijskiProgram.idStudijskiProgram AND " +
						     	   	   "s.idStudija = l.studij.idStudija AND " +
						     	   	   "sm.idStudijskaSmer = :studijskaSmer AND " +
						     	   	   "vs.idVrstaStudija = :vrstaStudija AND " +
						     	   	   "sp.idStudijskiProgram = :studijskiProgram AND " +
						     	   	   "l.nazivLetnik = :letnik AND " +
						     	   	   "l.studijskoLeto = :studijskoLeto");
				q.setParameter("studijskaSmer", smerSkupina);
				q.setParameter("vrstaStudija", vrstaStudija);
				q.setParameter("studijskiProgram", studijskiProgram);
				q.setParameter("letnik", letnikStudija);
				q.setParameter("studijskoLeto", studijskoLeto);
				
				List<Letnik> letniki = (List<Letnik>)q.getResultList();
				Letnik letnik = null;
				
				if (letniki.size() > 0) {
					letnik = letniki.get(0);
				} else {
					em.getTransaction().rollback();
					em.close();
					request.setAttribute("error", "error");
					request.setAttribute("uspesnostVnosa", "<span class='error'>Ta kombinacija študija ne obstaja</span>");
					request = pripraviPodatkeZaVpisniList(request);
					RequestDispatcher fw = request.getRequestDispatcher("index.jsp?stran=vpisniList");
					fw.forward(request, response);
				}
				
				// Vrsta vpisa
				Vrsta_vpisa vrstaVpisaJPA = em.find(Vrsta_vpisa.class, vrstaVpisa);
				
				// Vpisni list
				Vpisni_list vpisniList = new Vpisni_list();
				vpisniList.setStudijskoLeto(studijskoLeto);
				vpisniList.setLetnik(letnik);
				vpisniList.setVrstaVpisa(vrstaVpisaJPA);
				vpisniList.setStudijNaDaljavo(Integer.parseInt(studijNaDaljavo));
				vpisniList.setNacinStudija(nacinStudija);
				vpisniList.setStudent(student);
				em.persist(vpisniList);				
				em.getTransaction().commit();
				
				//trebal bi blo dodat se da napolne predmetnik
				List<Predmetnik_letnika> predmetniki_letnikov = vrniPredmeteBrezOcene(letnik, vpisniList);				

				for(Predmetnik_letnika pl : predmetniki_letnikov){
					
					if(!pl.getObvezniPredmet())
						continue;
					
					Predmetnik predmetnik = new Predmetnik();
					predmetnik.setFkPredmetnikLetnika(pl.getIdPredmetnikLetnika());
					predmetnik.setFkVpisniList(vpisniList.getIdVpisni());
					predmetnik.setStatus(true);
					predmetnik.setStOpravljanj(0);
					em.getTransaction().begin();
					em.persist(predmetnik);
					em.getTransaction().commit();
				}
				
				request.setAttribute("uspesnostVnosa", "Vnos uspešen");
			} catch (Exception e) {
				em.getTransaction().rollback();
				e.printStackTrace();
				request.setAttribute("error", "error");
				request.setAttribute("uspesnostVnosa", "<span class='error'>Napaka</span>");
			}
			
			if (em != null) {
				em.close();
			}
			
			request = pripraviPodatkeZaVpisniList(request);
			RequestDispatcher fw = request.getRequestDispatcher("index.jsp?stran=vpisniList");
			fw.forward(request, response);
		}
	}
	
	private List<Predmetnik_letnika> vrniPredmeteBrezOcene(Letnik letnik, Vpisni_list vl){
		try {
			EntityManager em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
			String sql = "SELECT " +
				 				"pl " +
						 "FROM " +
						 		"Letnik l, Predmetnik_letnika pl, Predmetnik p, Vpisni_list vl " +
				 		 "WHERE " +
				 		 		"vl = :vpisni AND " +
				 		 		"vl.letnik = :letnik AND " +
				 		 		"p.fkVpisniList = vl.idVpisni AND " +
				 		 		"p.fkPredmetnikLetnika = pl.idPredmetnikLetnika AND " +
				 		 		"(p.koncnaOcena > 5 OR p.ocenaVaje > 5 OR p.ocenaIzpit > 5)";
			
			Query q = em.createQuery(sql);
			q.setParameter("letnik", letnik);
			q.setParameter("vpisni", vl);
			
			List<Predmetnik_letnika> predmetnikiLetnikaZOceno = q.getResultList();
			
			List<Predmetnik_letnika> predmetnikiLetnika = letnik.getPredmetnikLetnikas();
			
			for(Predmetnik_letnika pl : predmetnikiLetnika){
				for(Predmetnik_letnika pl2 : predmetnikiLetnikaZOceno){
					if(pl.getIdPredmetnikLetnika() == pl2.getIdPredmetnikLetnika())
						predmetnikiLetnika.remove(pl);
				}
			}
			
			return predmetnikiLetnika;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return new LinkedList<Predmetnik_letnika> ();
	}
	
	private HttpServletRequest pripraviPodatkeZaVpisniList(HttpServletRequest request) {
		Query q;
		
		try {
			EntityManager em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
			
			q = em.createQuery("SELECT s FROM Studijski_program s");
			List<Studijski_program> studijskiProgrami = (List<Studijski_program>)(q.getResultList());
			request.setAttribute("studijskiProgrami", studijskiProgrami);
			
			q = em.createQuery("SELECT s FROM Studijska_smer s");
			List<Studijska_smer> studijskeSmeri = (List<Studijska_smer>)(q.getResultList());
			request.setAttribute("studijskeSmeri", studijskeSmeri);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return request;
	}
	
	private Student preveriObstojStudenta(String emso, String ime){
		Student student = null;
		
		try {
			EntityManager em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
			
			String sql = "SELECT st " +
						 "FROM Oseba o, Student st " +
						 "WHERE o.upIme = ?1 AND o.emso = ?2 AND o.idOseba = st.oseba.idOseba";
			
			Query query = em.createQuery(sql);
			query.setParameter(1, ime);
			query.setParameter(2, emso);
			
			List<Student> studetnje = (List<Student>)query.getResultList();
			if (studetnje.size() == 1) {
				student = studetnje.get(0);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return student;
	}
}