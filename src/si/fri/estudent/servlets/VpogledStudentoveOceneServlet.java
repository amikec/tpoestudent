package si.fri.estudent.servlets;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import si.fri.estudent.jpa.Oseba;
import si.fri.estudent.jpa.Student;
import si.fri.estudent.jpa.Vrsta_studija;

/**
 * Servlet implementation class VpogledStudentoveOceneServlet
 */
@WebServlet("/VpogledStudentoveOceneServlet")
public class VpogledStudentoveOceneServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Oseba profesor;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VpogledStudentoveOceneServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		profesor = (Oseba) session.getAttribute("uporabnik");
		
		Student student = null;
		if(session.getAttribute("studentIzIskanja") != null){
            student = (Student)session.getAttribute("studentIzIskanja");
        } else {
        	response.sendRedirect("index.jsp?stran=domov");
        	return;
        }
		
		List<Vrsta_studija> vrsteStudija = vrniVrsteStudija(student.getIdStudent());
		
		request.setAttribute("student", student);
		request.setAttribute("oseba", student.getOseba());
		request.setAttribute("vrsteStudija", vrsteStudija);
		RequestDispatcher fw = request.getRequestDispatcher("index.jsp?stran=vpogledStudentoveOcene");
		fw.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		profesor = (Oseba) session.getAttribute("uporabnik");
	}
	
	/////////////////////////////////// METODE ///////////////////////////////////
	
	private EntityManager getEntityManager(){
		return Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
	}
	
	private List<Vrsta_studija> vrniVrsteStudija(int idStudent) {
		try {
			String sql = "SELECT " +
								"DISTINCT vs " +
						  "FROM " +
						  		"Student student, Vpisni_list vl, Letnik l, Studij studij, " +
						  		"Predmetnik pred, Predmetnik_letnika predL, Kombinacije_izvajalcev ki, " +
						  		"Predmet_izvajalec pi, Osebje os, Oseba oseba, Predmet p, " +
						  		"Vrsta_studija vs " +
					  	  "WHERE " +
					  	  		"student.idStudent = :student AND " +
					  	  		"vl.student = student AND " +
					  	  		"vl.letnik = l AND " +
					  	  		"l.studij = studij AND " +
					  	  		"studij.vrstaStudija = vs AND " +
								"vl.idVpisni = pred.fkVpisniList AND " +
								"pred.fkPredmetnikLetnika = predL.idPredmetnikLetnika AND " +
								"predL.predmet = p AND " +
								"p = pi.predmet AND " +
								"pi.kombinacijeIzvajalcev = ki AND " +
								"(" +
									" ki.osebje1 = os OR " +
									" ki.osebje2 = os OR " +
									" ki.osebje3 = os " +
								") AND " +
								"os.oseba.idOseba = :profesor " +
							"ORDER BY " +
								"vs.nazivVrstaStudija";
					  	  		
			Query query = this.getEntityManager().createQuery(sql);
			query.setParameter("student", idStudent);
			query.setParameter("profesor", profesor.getIdOseba());
			
			List<Vrsta_studija> vrsteStudija = query.getResultList();
			return vrsteStudija;
			
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
}
