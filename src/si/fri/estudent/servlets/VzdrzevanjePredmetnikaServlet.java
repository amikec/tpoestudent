package si.fri.estudent.servlets;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import si.fri.estudent.jpa.Letnik;
import si.fri.estudent.jpa.Modul;
import si.fri.estudent.jpa.Predmet;
import si.fri.estudent.jpa.Predmetnik_letnika;
import si.fri.estudent.jpa.Vrsta_studija;

/**
 * Servlet implementation class VzdrzevanjePredmetnikaServlet
 */
@WebServlet("/VzdrzevanjePredmetnikaServlet")
public class VzdrzevanjePredmetnikaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VzdrzevanjePredmetnikaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		request = pripraviPodatke(request);
		RequestDispatcher fw = request.getRequestDispatcher("index.jsp?stran=vzdrzevanjePredmetnika");
		fw.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String stareIzbireString = request.getParameter("stareIzbire");
		String noveIzbireString = request.getParameter("izbire");
		int letnik = Integer.parseInt(request.getParameter("letnik"));
		String vrstaPredmetnika = null;
		int modul = -1;
		if (request.getParameter("vrstaPredmetnika") != null) {
			vrstaPredmetnika = request.getParameter("vrstaPredmetnika");
		}
		if (request.getParameter("modul") != null) {
			modul = Integer.parseInt(request.getParameter("modul"));
		}
		
		
		String [] izbire = null;
		int [] stareIzbire = null;
		int [] noveIzbire = null;
		EntityManager em = null;
		Predmetnik_letnika pl = null;
		
		// Parsanje starinh in novih izbir iz stringa v tabeli integerjev
		if (!stareIzbireString.equals("")) {
			izbire = stareIzbireString.split(",");
			stareIzbire = new int[izbire.length];
			int i = 0;;
			
			for (String izbira : izbire) {
				stareIzbire[i++] = Integer.parseInt(izbira);
			}
		}
		
		if (!noveIzbireString.equals("")) {
			izbire = noveIzbireString.split(",");
			noveIzbire = new int[izbire.length];
			int i = 0;
			
			for (String izbira : izbire) {
				noveIzbire[i++] =  Integer.parseInt(izbira);
			}
		}
		
		//////////////////////////// POSODABLJANJE BAZE ////////////////////////////
		try {
			em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
			em.getTransaction().begin();
			
			boolean obvezni = false;
			if (vrstaPredmetnika.equals("obvezni")) {
				obvezni = true;
			}
			
			// Stare izbire, ki jih ni med novimi izbirami, onemogočimo
			if (stareIzbire != null) {
				
				for (int izbira : stareIzbire) {
					if (!findElement(izbira, noveIzbire)) {
						pl = getPredmetnikLetnika(em, letnik, izbira);
						pl.setModul(null);
						if (modul == -1) {
							pl.setStatus(false);
						}
						em.merge(pl);
					}
				}
			}
			
			// Nove izbire, ki jih ni med starimi, posodobimo status na aktivno ali pa na novo dodamo
			if (noveIzbire != null) {
				
				for (int izbira : noveIzbire) {
					if (!findElement(izbira, stareIzbire)) {
						pl = getPredmetnikLetnika(em, letnik, izbira);
						Letnik l = em.find(Letnik.class, letnik);
						Predmet p = em.find(Predmet.class, izbira);
						Modul m  = em.find(Modul.class, modul);
						
						if (pl == null) {							
							pl = new Predmetnik_letnika();
							pl.setLetnik(l);
							pl.setPredmet(p);
							pl.setObvezniPredmet(obvezni);
							pl.setModul(m);
							pl.setStatus(true);
							
							em.persist(pl);							
						} else {
							pl.setObvezniPredmet(obvezni);
							pl.setModul(m);
							pl.setStatus(true);
							em.merge(pl);
						}
					}
				}
			}
			
			em.getTransaction().commit();
			request.setAttribute("error", "Vnos uspešen");
		} catch (Exception e) {
			em.getTransaction().rollback();
			request.setAttribute("error", "<span class='error'>Napaka</span>");
			e.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}
		}
		
		request = pripraviPodatke(request);
		RequestDispatcher fw = request.getRequestDispatcher("index.jsp?stran=vzdrzevanjePredmetnika");
		fw.forward(request, response);
	}
	
	//////////////////////////////// METODE //////////////////////////////////
	
	private static HttpServletRequest pripraviPodatke(HttpServletRequest request) { // Metoda pripravi podatke za <select> Vrsta študija
		EntityManager em = null;
		String query = null;
		Query q = null;
		List<Vrsta_studija> vrsteStudija = null;
		
		try {
			em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
			query = "SELECT vs FROM Vrsta_studija vs";
			q = em.createQuery(query);
			
			vrsteStudija = (List<Vrsta_studija>)q.getResultList();
			
			if (vrsteStudija != null) {
				request.setAttribute("vrsteStudija", vrsteStudija);
			} else {
				request.setAttribute("error", "<span class='error'>V bazi ni vrst studija</span>");
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
			request.setAttribute("error", "<span class='error'>Napaka pri branju vrst študija</span>");
			
		} finally {
			if (em != null) {
				em.close();
			}
		}
		
		return request;
	}
	
	private static boolean findElement(int element, int[] tab) { // Metoda vrne true, če se iskani int nahaja v tabeli
		if (tab != null) {
			for (int i : tab) {
				if (i == element) {
					return true;
				}
			}
		}
		return false;
	}
	
	private static Predmetnik_letnika getPredmetnikLetnika(EntityManager em, int letnik, int predmet) { // Metoda vrne Predmetnik_letnika
		String query = "SELECT " +
							"pl " +
					   "FROM " +
					   		"Predmetnik_letnika pl " +
					   "WHERE " +
					   		"pl.letnik.idLetnik = :letnik AND " +
					   		"pl.predmet.idPredmeta = :predmet";
		
		Query q = em.createQuery(query);
		q.setParameter("letnik", letnik);
		q.setParameter("predmet", predmet);
		
		List<Predmetnik_letnika> predmetnikiLetnika = (List<Predmetnik_letnika>)q.getResultList();
		
		if (predmetnikiLetnika.size() > 0) {
			return predmetnikiLetnika.get(0);
		} else {
			return null;
		}
	}
}
