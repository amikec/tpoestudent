package si.fri.estudent.servlets;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import si.fri.estudent.jpa.Izpitni_rok;
import si.fri.estudent.jpa.Kombinacije_izvajalcev;
import si.fri.estudent.jpa.Oseba;
import si.fri.estudent.jpa.Osebje;
import si.fri.estudent.jpa.Predmet;
import si.fri.estudent.jpa.Predmet_izvajalec;
import si.fri.estudent.jpa.Vloga_oseba;
import si.fri.estudent.utilities.Constants;
import si.fri.estudent.utilities.DataValidator;
import si.fri.estudent.utilities.IzpitValidator;

/**
 * Servlet implementation class VnosIzpitnegaRokaServlet
 */
@WebServlet("/VnosIzpitnegaRokaServlet")
public class VnosIzpitnegaRokaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	
	EntityManager em;
    public VnosIzpitnegaRokaServlet() {
        super();
        em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		request = pripravaPodatkovZaFormo(request);
		RequestDispatcher rd=request.getRequestDispatcher("index.jsp?stran=vnosIzpitnegaRoka");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("deprecation")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession();
		Vloga_oseba vloga = (Vloga_oseba)session.getAttribute("vloga");
		
		String predmet = request.getParameter("predmet");
		String datum = request.getParameter("datum");
		String predavatelj = request.getParameter("predavatelj");
		String cas = request.getParameter("cas");
		String predavalnica = request.getParameter("predavalnica");
		String maksStPrijav = request.getParameter("maksStPrijav");
		String mozneTocke = request.getParameter("mozneTocke");
		String mejaZaPozitivno = request.getParameter("mejaZaPozitivno");
		
		DataValidator validator = new DataValidator();

		// Preverjanje vnosnih polj
		
		if (!validator.validateInputText(datum, 10, false, "^(0[1-9]|[12][0-9]|3[01]).(0[1-9]|1[012]).(19|20)\\d\\d$", "Prosim izberite datum", "Neveljanven datum", "Neveljaven datum")) {
			request.setAttribute("errorDatum", validator.getError());
		}
		if (!validator.validateInputText(cas, 5, false, "^([01][0-9]|2[0-3]):([0-5][0-9])$", "Prosim izberite čas", "Neveljaven čas", "Neveljaven čas")) {
			request.setAttribute("errorCas", validator.getError());
		}
		if (!validator.validateInputText(predavalnica, 10, false, "^[0-9A-ZČĆŽŠĐ\\-]{1,10}$", "Prosimo vnesite predavalnico", "Nevaljavo ime predavalnice", "Neveljavno ime predavalnica")) {
			request.setAttribute("errorPredavalnica", validator.getError());
		}
		if (!validator.validateInputText(maksStPrijav, 4, false, "^[0-9]{1,4}$", "Prosimo vnesite maksimalno število prijav", "Nevaljavo število", "Neveljavno število")) {
			request.setAttribute("errorMaksStPrijav", validator.getError());
		}
		if (!validator.validateInputText(mozneTocke, 4, false, "^[0-9]{1,4}$", "Prosimo vnesite število možnih točk", "Nevaljavo število možnih točk", "Neveljavno število možnih točk")) {
			request.setAttribute("errorMozneTocke", validator.getError());
		}
		if (!validator.validateInputText(mejaZaPozitivno, 4, false, "^[0-9]{1,4}$", "Prosimo vnesite mejo za pozitivno", "Nevaljava meja za pozitivno", "Neveljavna meja za pozitvino")) {
			request.setAttribute("errorMejaZaPozitivno", validator.getError());
		}
		
		
		// Generiranje timestampa z prebranim datumom in časom
		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		Date date = null;
		Date danRoka = new Date();
		long time = 0;
		try {
			date = dateFormat.parse(datum);
			danRoka.setTime(date.getTime());
			String[] razbitCas = cas.split(":"); 
			date.setHours(Integer.parseInt(razbitCas[0]));
			date.setMinutes(Integer.parseInt(razbitCas[1]));
			time = date.getTime();
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Timestamp timestamp = new Timestamp(time);
		
		// preverimo ali možno razpistati rok
		Calendar datumCal = Calendar.getInstance();
		datumCal.setTimeInMillis(date.getTime());
		
		if(!IzpitValidator.mozenRazpisRoka(datumCal) || vloga.getIdVlogaOseba() == Constants.VLOGA_REFERENTKA) {
			
			if(!zeObstajaIzpitniRokNaIstiDan(predmet, danRoka, predavatelj) || vloga.getIdVlogaOseba() == Constants.VLOGA_REFERENTKA) {
		
				try {
					// Začetek transakcije
					em.getTransaction().begin();
					
					Query queryGetPredmet = null;
					Query queryGetPredmet_izvajalec = null;
					Predmet pr = null;
					Predmet_izvajalec predmetIzvajalec = null;
					
					// Izbira ustreznega predmeta glede na njegovo šifro
					queryGetPredmet = em.createQuery("SELECT p FROM Predmet p WHERE p.sifraPredmeta = :sifra");
					queryGetPredmet.setParameter("sifra", predmet);
			
					pr = (Predmet)queryGetPredmet.getSingleResult();
				
					queryGetPredmet_izvajalec = em.createQuery("SELECT pi FROM Predmet_izvajalec pi WHERE pi.kombinacijeIzvajalcev.idKombinacijeIzvajalcev = :id");
					queryGetPredmet_izvajalec.setParameter("id", Integer.parseInt(predavatelj));
					
					List<Predmet_izvajalec> pi = (List<Predmet_izvajalec>)queryGetPredmet_izvajalec.getResultList();
					
					for(Predmet_izvajalec p : pi) {
						if(p.getPredmet().getIdPredmeta() == pr.getIdPredmeta()) {
							predmetIzvajalec = p;
						}
					}
					
					// Generiranje izpitnega roka in vstavljanje v podatkovno bazo
					Izpitni_rok izpitniRok = new Izpitni_rok();
					izpitniRok.setDatumIzpit(timestamp);
					izpitniRok.setPredavalnica(predavalnica);
					izpitniRok.setPredmet(pr);
					izpitniRok.setPredmetIzvajalec(predmetIzvajalec);
					izpitniRok.setMaxStPrijav(Integer.parseInt(maksStPrijav));
					izpitniRok.setMejaZaPozitivno(Integer.parseInt(mejaZaPozitivno));
					izpitniRok.setMozneTocke(Integer.parseInt(mozneTocke));
					izpitniRok.setStatus(true);
					
					em.persist(izpitniRok);
					em.flush();
					em.getTransaction().commit(); 	// Konec transakcije
				
					request.setAttribute("uspeh", "Vnos uspešen");
					
				}catch (Exception e) {
					System.out.println(e.getMessage());
					request.setAttribute("uspeh", "<span class='error'>Napaka pri vnosu</span>");
				}
				
			} else {
				request.setAttribute("uspeh", "<span class='error'>Roka za izbran predmet na ta dan že obstaja</span>");
			}
			
		} else {
			request.setAttribute("uspeh", "<span class='error'>Datum mora biti večji od današnjega dne</span>");
		}
		// vrnemo podatke, da uporabnik vidi kakšen je bil vnos
		request.setAttribute("datum", datum);
		request.setAttribute("cas", cas);
		request.setAttribute("predavatelj", predavatelj);
		request.setAttribute("predavalnica", predavalnica);
		request.setAttribute("predmet", predmet);
		request.setAttribute("mejaZaPozitivno", mejaZaPozitivno);
		request.setAttribute("mozneTocke", mozneTocke);
		request.setAttribute("maksStPrijav", maksStPrijav);
		request = pripravaPodatkovZaFormo(request);
		RequestDispatcher fw = request.getRequestDispatcher("index.jsp?stran=vnosIzpitnegaRoka");
		fw.forward(request, response);
		
	}

	// preverimo če je izpitni rok že vnešen
	private boolean zeObstajaIzpitniRokNaIstiDan(String predmet, Date rok, String predavatelj) {
		
		boolean napaka = false;
		Query q = null;
		
		try {			
			q = em.createQuery("SELECT ir FROM Izpitni_rok ir " +
					"WHERE ir.predmet.sifraPredmeta = :sifraPredmeta");
			q.setParameter("sifraPredmeta", predmet);
			
			List<Izpitni_rok> ir = (List<Izpitni_rok>)q.getResultList();
			
			for(Izpitni_rok i : ir) {
				Date datumIzpita = i.getDatumIzpit();
				datumIzpita.setHours(0);
				datumIzpita.setMinutes(0);

				if(datumIzpita.compareTo(rok) == 0 && Integer.parseInt(predavatelj) == i.getPredmetIzvajalec().getKombinacijeIzvajalcev().getIdKombinacijeIzvajalcev()) {
					napaka = true;
				} 
			}

		}catch (Exception e) {
			System.out.println(e.getMessage());
			napaka = true;
		}
		
		return napaka;
	}

	private HttpServletRequest pripravaPodatkovZaFormo(HttpServletRequest request) {
		
		HttpSession session = request.getSession();
		
		Vloga_oseba vloga = (Vloga_oseba)session.getAttribute("vloga");
		
		
		if(vloga.getNazivVlogaOseba().compareTo("referentka") == 0) {
			// če je uprobnik vpisan kot referentka mu prikažemo vse predmete
			Query q = em.createQuery("SELECT DISTINCT(p) FROM Predmet p, Predmet_izvajalec pi WHERE pi.predmet.idPredmeta = p.idPredmeta AND pi.status = true ORDER BY p.nazivPredmet ASC");
			List<Predmet> predmeti = (List<Predmet>) q.getResultList();						
			request.setAttribute("predmeti", predmeti);	
			
		} else {
			// če je uporabnik učitelj mu prikažemo samo predmete katere poučuje
			ArrayList<Predmet> predmeti = new ArrayList<Predmet>();
			Oseba uporabnik = (Oseba)session.getAttribute("uporabnik");
			try {
				Query q = em.createQuery("SELECT os FROM Osebje os WHERE os.oseba.idOseba = :idUporabnika");
				q.setParameter("idUporabnika", uporabnik.getIdOseba());
				Osebje os = (Osebje)q.getSingleResult();
				
				q = em.createQuery("SELECT ki FROM Kombinacije_izvajalcev ki WHERE ki.osebje1.idOsebje = :idOsebe OR ki.osebje2.idOsebje = :idOsebe OR ki.osebje3.idOsebje = :idOsebe");
				q.setParameter("idOsebe", os.getIdOsebje());
				List<Kombinacije_izvajalcev> ki = (List<Kombinacije_izvajalcev>)q.getResultList();
			
				for(Kombinacije_izvajalcev komb: ki) {
					
					List<Predmet_izvajalec> pi =  komb.getPredmetIzvajalecs();
					
					for(Predmet_izvajalec p : pi) {
						if(p.isStatus() == true) {
							predmeti.add(p.getPredmet());
						}
						
					}	
				}
			}catch (Exception e) {
				// TODO: handle exception
			}
			request.setAttribute("predmeti", predmeti);	
		}
		
		return request;
	}
	
	

}
