package si.fri.estudent.servlets;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import si.fri.estudent.utilities.Constants;
import si.fri.estudent.utilities.DataValidator;
import si.fri.estudent.utilities.Utilities;
import si.fri.estudent.jpa.Oseba;
import si.fri.estudent.jpa.Prijava;
import si.fri.estudent.jpa.Vloga_oseba;

/**
 * Servlet implementation class LoginManagerServlet
 */
@WebServlet("/LoginManagerServlet")
public class LoginManagerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private EntityManager em;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginManagerServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		HttpSession session = request.getSession();
		
		String url = "index.jsp";
		String status = "";
		Oseba o = new Oseba();
		
		if(request.getParameter("mem") != null){
			
			int vlogaId = Integer.parseInt(request.getParameter("mem").toString());
			Vloga_oseba vloga = em.find(Vloga_oseba.class, vlogaId);
			session.setAttribute("vloga", vloga);
			
		}else{

			String username = request.getParameter("username");
			String password = request.getParameter("password");
			
			try{
				
				Query q = em.createQuery("SELECT o FROM Oseba o WHERE o.geslo = ?1 AND o.upIme = ?2");
				q.setParameter(1, password);
				q.setParameter(2, username);
			
				o = (Oseba) q.getSingleResult();
				
			}catch(Exception e){
				e.printStackTrace();
				o.setIdOseba(-1);
			}
			
			DataValidator validator = new DataValidator();
			
			if(!checkLoginCounter(request, response, username))
				status = Constants.STATUS_COUNTER_ERR;
			
			else if(o.getIdOseba() == -1 || validator.validateLoginData(username, password) == false){
				if(validator.getError() == null)
					status = Constants.STATUS_INVALID_DATA;
				else
					status = validator.getError();
				
			}else{	
				
				resetLoginCounter(request, response, o);
				
				status = Constants.STATUS_SUCCESS;
				session.setAttribute("uporabnik", o);
				List<Vloga_oseba> vloge = o.getVlogaOsebas();
				session.setAttribute("vloge", vloge);
				
				if(vloge.size() > 1)
					url += "?stran=izberi_vlogo";
				else if(vloge.size() == 1){
					Vloga_oseba vloga = vloge.get(0);
					session.setAttribute("vloga", vloga);
				}
					
			}
		}
		
		em.close();
		
		request.setAttribute("status", status);
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		
		dispatcher.forward(request, response);
	}
	
	private void resetLoginCounter(HttpServletRequest request, HttpServletResponse response, Oseba o){
		
		if(!em.getTransaction().isActive())
			em.getTransaction().begin();
		
		try{
			
			String sql = "SELECT p " +
						 "FROM Prijava p, Oseba o " +
						 "WHERE " +
						 	"o.idOseba = p.oseba.idOseba AND " +
						 	"o.idOseba = ?1 ";
			
			Query query = em.createQuery(sql);
			query.setParameter(1, o.getIdOseba());
			
			Prijava p = (Prijava) query.getSingleResult();
			
			long time = System.currentTimeMillis();
			java.sql.Timestamp timestamp = new java.sql.Timestamp(time);
			
			p.setStPoizkusov(0);
			p.setZadnjiPoizkus(timestamp);
			em.merge(p);
			em.getTransaction().commit();
		}catch(Exception e){
			em.getTransaction().rollback();
			e.printStackTrace();
		}
	}
	
	/*
	 * Metoda, ki preverja stevilo poskusov logiranja v nekem časovnem intervalu
	 */
	private boolean checkLoginCounter(HttpServletRequest request, HttpServletResponse response, String username){
		
		try{
			
			if(!em.getTransaction().isActive())
				em.getTransaction().begin();
			
			String sql = "SELECT p " +
						 "FROM Prijava p, Oseba o " +
						 "WHERE " +
						 	"o.idOseba = p.oseba.idOseba AND " +
						 	"o.upIme = ?1 ";
			
			Query query = em.createQuery(sql);
			query.setParameter(1, username);
			
			Prijava p = (Prijava) query.getSingleResult();

			long time = System.currentTimeMillis();
			java.sql.Timestamp timestamp = new java.sql.Timestamp(time);
			
			if(p.getZadnjiPoizkus() == null){
				p.setZadnjiPoizkus(timestamp);
				p.setStPoizkusov(p.getStPoizkusov() + 1);
				em.merge(p);
				em.getTransaction().commit();
				return true;
			}
			
			if(p.getStPoizkusov() > Constants.LOGIN_LIMIT){
				long d = (timestamp.getTime() - p.getZadnjiPoizkus().getTime());
				
				if(((timestamp.getTime() - p.getZadnjiPoizkus().getTime())) > Constants.LOGIN_DISABLE_TIME){
					p.setStPoizkusov(0);
					em.merge(p);
					em.getTransaction().commit();
					return true;
				}else{
					p.setZadnjiPoizkus(timestamp);
					p.setStPoizkusov(p.getStPoizkusov() + 1);
					em.merge(p);
					em.getTransaction().commit();
					return false;
				}
				
			}	
			
			p.setZadnjiPoizkus(timestamp);
			p.setStPoizkusov(p.getStPoizkusov() + 1);
			em.merge(p);
			em.getTransaction().commit();
			return true;
		
		}catch(Exception e){
			e.printStackTrace();
			em.getTransaction().rollback();
			
		}
		
		return true;
	}
}
