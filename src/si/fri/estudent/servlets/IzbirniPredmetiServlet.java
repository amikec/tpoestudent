package si.fri.estudent.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import si.fri.estudent.jpa.Ocena;
import si.fri.estudent.jpa.Predmetnik;
import si.fri.estudent.jpa.Predmetnik_letnika;
import si.fri.estudent.jpa.Student;
import si.fri.estudent.jpa.Vpisni_list;
import si.fri.estudent.jpa.Vrsta_studija;

/**
 * Servlet implementation class IzbirniPredmetiServlet
 */
@WebServlet("/IzbirniPredmetiServlet")
public class IzbirniPredmetiServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IzbirniPredmetiServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		Student student = null;
		if(session.getAttribute("studentIzIskanja") != null){
            student = (Student)session.getAttribute("studentIzIskanja");
        } else {
        	response.sendRedirect("index.jsp?stran=domov");
        	return;
        }
		
		List<Vrsta_studija> vrsteStudija = vrniVrsteStudija(student.getIdStudent());
		
		request.setAttribute("student", student);
		request.setAttribute("oseba", student.getOseba());
		request.setAttribute("vrsteStudija", vrsteStudija);
		RequestDispatcher fw = request.getRequestDispatcher("index.jsp?stran=izbirniPredmeti");
		fw.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  int idStudent = Integer.parseInt(request.getParameter("idStudent"));
		  int letnik = Integer.parseInt(request.getParameter("letnikIzbirniP"));
		  String stareIzbireString = request.getParameter("stareIzbire");
		  String noveIzbireString = request.getParameter("izbire");
		  String izbirniPredmetnikModul = request.getParameter("izbirniPredmetnikModulDragDrop");
		  
		  int [] stareIzbire = stringToIntTable(stareIzbireString);
		  int [] noveIzbire = stringToIntTable(noveIzbireString);
		  List<Integer> spremenjeni = new LinkedList<Integer> ();
		  
		  
		  // Pisanje v bazo
		  
		  String msg = "";
		  
		  List<Integer> skupni = new LinkedList<Integer>();
		  
		  if(stareIzbire != null)
			  for(int i = 0; i < stareIzbire.length; i++){
				  
				  boolean obstaja = false;
				  
				  if(noveIzbire != null)
					  for(int j = 0; j < noveIzbire.length; j++){
						  if(stareIzbire[i] == noveIzbire[j]){
							  obstaja = true;
						  }
					  }
				  
				  if(!obstaja)
					  skupni.add(stareIzbire[i]);
				  else{
					  for(int k = 0; k < noveIzbire.length; k++){
						  if(noveIzbire[k] == stareIzbire[i])
							  noveIzbire[k] = -1;
					  }
					  
					  stareIzbire[i] = -1;
				  }
			  }
		  
		  if (izbirniPredmetnikModul.equals("modul")) { // Urejanje modulov
			  msg = posodobiModule(idStudent, letnik, stareIzbire, noveIzbire, skupni);
		  } else { // Urejanje izbirnega predmetnika
			  msg = posodobiIzbirnePredmete(idStudent, letnik, stareIzbire, noveIzbire, skupni);
		  }
		  
		  Query q;
		  EntityManager em = null;
		  
		  try {
			  em = getEntityManager();
			  Student student = em.find(Student.class, idStudent);
		   
			   request.setAttribute("student", student);
			   request.setAttribute("oseba", student.getOseba());
			   request.setAttribute("vrsteStudija", vrniVrsteStudija(student.getIdStudent()));
			   
		  } catch (Exception e) {
			  e.printStackTrace();
		  }
		  
		  request.setAttribute("error", msg);
		  RequestDispatcher fw = request.getRequestDispatcher("index.jsp?stran=izbirniPredmeti");
		  fw.forward(request, response);
	 }
	/////////////////////////////////////////// METODE ///////////////////////////////////////////
	
	private EntityManager getEntityManager(){
		return Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
	}
	
	private int[] stringToIntTable(String text) { // Metoda pretvori string števil ločenih z ',' v tabelo int
		int[] tabelaInt = null;
		
		if (!text.equals("")) {
			String[] izbire = text.split(",");
			tabelaInt = new int[izbire.length];
			int i = 0;;
			
			for (String izbira : izbire) {
				tabelaInt[i++] = Integer.parseInt(izbira);
			}
			
			return tabelaInt;
		}
		
		return null;
	}
	
	private List<Vrsta_studija> vrniVrsteStudija(int idStudent) {
		try {
			String sql = "SELECT " +
								"DISTINCT vs " +
						  "FROM " +
						  		"Student student, Vpisni_list vl, Letnik l, Studij studij," +
						  		"Vrsta_studija vs " +
					  	  "WHERE " +
					  	  		"student.idStudent = :student AND " +
					  	  		"vl.student = student AND " +
					  	  		"vl.letnik = l AND " +
					  	  		"l.studij = studij AND " +
					  	  		"studij.vrstaStudija = vs " +
					  	  	"ORDER BY " +
					  	  		"vs.nazivVrstaStudija";
					  	  		
			Query query = this.getEntityManager().createQuery(sql);
			query.setParameter("student", idStudent);
			
			List<Vrsta_studija> vrsteStudija = query.getResultList();
			return vrsteStudija;
			
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	private List<Predmetnik_letnika> aliObstajaOcena(int idStudent, int letnik, List<Predmetnik> izbirniStari){
		
		if(izbirniStari.size() == 0)
			return null;
		
		EntityManager em = this.getEntityManager();
		try{
			
			String sql = "SELECT " +
					"DISTINCT predL " +
			  "FROM " +
			  		"Student student, Vpisni_list vl, Letnik l, Studij studij," +
			  		"Predmetnik pred, Ocena oc, Predmetnik_letnika predL " +
		  	  "WHERE " +
		  	  		"student.idStudent = :student AND " +
		  	  		"student = vl.student AND " +
		  	  		"vl.letnik.idLetnik = :letnik AND " +
		  	  		"vl.idVpisni = pred.fkVpisniList AND " +
		  	  		"pred.idPredmetnik = oc.fkPredmetnik AND " +
		  	  		"oc.aktivna = 1 AND " +
		  	  		"pred.fkPredmetnikLetnika = predL.idPredmetnikLetnika ";
			
			String dodatno = "";
			
			for(int i = 0; i < izbirniStari.size(); i++){
				Predmetnik pred = izbirniStari.get(i);
				
				if(dodatno.isEmpty())
					dodatno += " AND ( ";
								
				if(i == izbirniStari.size() - 1){
					dodatno += " pred.idPredmetnik = " + pred.getIdPredmetnik() + " )";
				}else{
					dodatno += " pred.idPredmetnik = " + pred.getIdPredmetnik() + " OR ";
				}
			}
			
			if(dodatno.isEmpty())
				dodatno = " AND 1 = -1 ";
			
			sql += dodatno;
			
			Query q = em.createQuery(sql);
			q.setParameter("student", idStudent);
			q.setParameter("letnik", letnik);
			
			List<Predmetnik_letnika> ocene = q.getResultList();
						
			return ocene;
			
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	private String posodobiIzbirnePredmete(int idStudent, int letnik, int[] izbirniStari, int[] izbirniNovi, List<Integer> skupni){

		EntityManager em = this.getEntityManager();
		try{
			em.getTransaction().begin();
			
			String sql = "SELECT " +
					"DISTINCT vl " +
			  "FROM " +
			  		"Student student, Vpisni_list vl, Letnik l, Studij studij," +
			  		"Vrsta_studija vs, Studijski_program sp, Studijska_smer ss, " +
			  		"Predmetnik_letnika predL, Predmetnik pred, Modul m " +
		  	  "WHERE " +
		  	  		"student.idStudent = :student AND " +
		  	  		"student = vl.student AND " +
		  	  		"vl.letnik.idLetnik = :letnik ";
			
			Query q = em.createQuery(sql);
			q.setParameter("student", idStudent);
			q.setParameter("letnik", letnik);
			
			Vpisni_list vl = (Vpisni_list) q.getSingleResult();
			
			int[] temp = new int[skupni.size()];
			for(int i = 0; i < skupni.size(); i++)
				temp[i] = skupni.get(i);
				
			List<Predmetnik> izbirniT = vrniIzbirniPredmetStudentaObstojec(em, idStudent, letnik, temp, vl);
			List<Predmetnik_letnika> ocene = aliObstajaOcena(idStudent, letnik, izbirniT);
			
			if(ocene != null && ocene.size() > 0){
				String error = "";
				for(Predmetnik_letnika o : ocene){
					if(!error.isEmpty())
						error += ", ";
					
					error += o.getPredmet().getNazivPredmet();
				}
				
				return "<span class='error'>" + error + " \n imajo aktivno prijavo </span>";
				
			}
				
			
			if(izbirniStari != null){
				List<Predmetnik> izbirni = vrniIzbirniPredmetStudentaObstojec(em, idStudent, letnik, izbirniStari, vl);
				
				for(Predmetnik p : izbirni){
					em.remove(p);
				}
			}
			
			if(izbirniNovi != null){
				for(int idIzbirni : izbirniNovi){
					
					Predmetnik p = new Predmetnik();
					p.setStatus(true);
					p.setStOpravljanj(0);
					p.setFkPredmetnikLetnika(idIzbirni);
					p.setFkVpisniList(vl.getIdVpisni());
					
					em.persist(p);
				}
			}
			
			em.getTransaction().commit();
			return "Uspešno";
			
		}catch(Exception e){
			e.printStackTrace();
			em.getTransaction().rollback();
			return "<span class='error'>Napaka</span>";
		}
	}
	
	private String posodobiModule(int idStudent, int letnik, int[] idModulaStari, int[] idModulaNovi, List<Integer> skupni){
		
		EntityManager em = this.getEntityManager();
		try{
			em.getTransaction().begin();
			
			String sql = "SELECT " +
					"DISTINCT vl " +
			  "FROM " +
			  		"Student student, Vpisni_list vl, Letnik l ," +
			  		"Predmetnik_letnika predL, Predmetnik pred, Modul m " +
		  	  "WHERE " +
		  	  		"student.idStudent = :student AND " +
		  	  		"student = vl.student AND " +
		  	  		"vl.letnik.idLetnik = :letnik ";
			
			Query q = em.createQuery(sql);
			q.setParameter("student", idStudent);
			q.setParameter("letnik", letnik);
			
			Vpisni_list vl = (Vpisni_list) q.getSingleResult();
			
			for(int idMod : skupni){
				List<Predmetnik> moduli = vrniPredmeteModulov(em, idStudent, letnik, idMod);
				
				List<Predmetnik_letnika> ocene = aliObstajaOcena(idStudent, letnik, moduli);
				
				if(ocene.size() > 0){
					String error = "";
					for(Predmetnik_letnika o : ocene){
						if(!error.isEmpty())
							error += ", ";
						
						error += o.getPredmet().getNazivPredmet();
					}
					
					return "<span class='error'>" + error + " \n imajo aktivno prijavo </span>";
					
				}
			}	
						
			if(idModulaStari != null){
				
				for(int mod : idModulaStari){
					List<Predmetnik> moduli = vrniPredmeteModulov(em, idStudent, letnik, mod);
					
					for(Predmetnik p : moduli){
						em.remove(p);
					}
				}
			}			
			
			if(idModulaNovi != null){
				for(int mod : idModulaNovi){
					List<Predmetnik> moduli = vrniPredmeteModulovZaVstavit(em, letnik, mod, vl);
					for(Predmetnik p : moduli){
						em.persist(p);
					}
				}
			}
			
			em.getTransaction().commit();
			
			return "Uspešno";
		}catch(Exception e){
			e.printStackTrace();
			em.getTransaction().rollback();
			 return "<span class='error'>Napaka</span>";
		}
	}
	
	private List<Predmetnik> vrniIzbirniPredmetStudentaObstojec(EntityManager em, int idStudent, int idLetnik, int[] idPredmet, Vpisni_list vl){
		
		String sql = 
	            "SELECT " +
	            	"distinct pred " +
	            "FROM " +
	            	"Predmetnik pred, Predmetnik_letnika predL, Vpisni_list vl " +
	            "WHERE " +
	            	"pred.fkVpisniList = :vpisni";
		
		String dodatni = "";
		
		for(int j = 0; j < idPredmet.length; j++){
			
			int i = idPredmet[j];
			
			if(dodatni.isEmpty())
				dodatni += " AND ( pred.fkPredmetnikLetnika = " + i;
			else
				dodatni += " OR pred.fkPredmetnikLetnika = " + i;
			
			if(j == idPredmet.length - 1)
				dodatni += " )";

		}
		
		if(dodatni.isEmpty())
			dodatni = " AND 1 = -1 ";
		
		sql += dodatni;
		
		Query query = em.createQuery(sql);
        query.setParameter("vpisni", vl.getIdVpisni());
        
        List<Predmetnik> predmetniki = query.getResultList();
        
		return predmetniki;
	}
	
	private List<Predmetnik> vrniPredmeteModulov(EntityManager em, int idStudent, int idLetnik, int idModula){
		String sql = "SELECT " +
				"DISTINCT pred " +
		  "FROM " +
		  		"Student student, Vpisni_list vl, Letnik l, Studij studij," +
		  		"Vrsta_studija vs, Studijski_program sp, Studijska_smer ss, " +
		  		"Predmetnik_letnika predL, Predmetnik pred, Modul m " +
	  	  "WHERE " +
	  	  		"student.idStudent = :student AND " +
	  	  		"student = vl.student AND " +
	  	  		"vl.letnik = l AND " +
	  	  		"vl.idVpisni = pred.fkVpisniList AND " +
	  	  		"pred.fkPredmetnikLetnika = predL.idPredmetnikLetnika AND " +
	  	  		"predL.modul.idModul = :modul AND " +
	  	  		"l.studij = studij AND " +
	  	  		"l.idLetnik = :letnik AND predL.status = 1 AND predL.modul.status = 1 ";
				  	  		
		Query q = em.createQuery(sql);
		q.setParameter("student", idStudent);
		q.setParameter("letnik", idLetnik);
		q.setParameter("modul", idModula);
		
		List<Predmetnik> predmeti = (List<Predmetnik>)q.getResultList();
		
		return predmeti;
	}
	
	private List<Predmetnik> vrniPredmeteModulovZaVstavit(EntityManager em, int idLetnik, int idModula, Vpisni_list vl){
		
		String sql = "SELECT " +
				"DISTINCT predL " +
		  "FROM " +
		  		"Vpisni_list vl, Letnik l," +
		  		"Predmetnik_letnika predL, Modul m " +
	  	  "WHERE " +
	  	  	"predL.modul.idModul = :modul AND " +
	  	  	"predL.letnik = l AND l.idLetnik = :letnik";
		
		Query q = em.createQuery(sql);
		q.setParameter("modul", idModula);
		q.setParameter("letnik", idLetnik);
		
		List<Predmetnik_letnika> pred = (List<Predmetnik_letnika>) q.getResultList();
		
		List<Predmetnik> predmetniki = new LinkedList<Predmetnik>();
		for(Predmetnik_letnika prL : pred){
			Predmetnik p = new Predmetnik();
			p.setFkVpisniList(vl.getIdVpisni());
			p.setKoncnaOcena(0);
			p.setStatus(true);
			p.setStOpravljanj(0);
			p.setFkPredmetnikLetnika(prL.getIdPredmetnikLetnika());
			predmetniki.add(p);
		}
		
		return predmetniki;
	}
}
