package si.fri.estudent.ajax;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import si.fri.estudent.jpa.Predmet;
import si.fri.estudent.jpa.Predmetnik_letnika;

import net.sf.json.JSONObject;

/**
 * Servlet implementation class VzdrzevanjeIzvajalcev
 */
@WebServlet("/VzdrzevanjeIzvajalcev")
public class VzdrzevanjeIzvajalcevAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VzdrzevanjeIzvajalcevAjaxServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		EntityManager em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();

		
		String studijskoLeto = request.getParameter("studijskoLeto");
		Query q = em.createQuery("SELECT pl FROM Predmetnik_letnika pl, Letnik l WHERE l.studijskoLeto = :studijskoLeto AND pl.status = true");
		q.setParameter("studijskoLeto", studijskoLeto);
	    List<Predmetnik_letnika> predmetnikLetnika = (List<Predmetnik_letnika>)q.getResultList();
		
	    List<String> predmeti = new ArrayList<String>();
	    for(Predmetnik_letnika pl : predmetnikLetnika) {
	    	predmeti.add(pl.getPredmet().getSifraPredmeta() + " " + pl.getPredmet().getNazivPredmet() + "<br />");
	    }
	    
	    
		JSONObject json = new JSONObject();
		json.accumulate("predmeti", predmeti);
		
		response.setContentType("text/html; charset=utf-8");   
		PrintWriter out = response.getWriter();
		out.print(json);
	}

}
