package si.fri.estudent.ajax;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import si.fri.estudent.jpa.Izpitni_rok;
import si.fri.estudent.jpa.Letnik;
import si.fri.estudent.jpa.Modul;
import si.fri.estudent.jpa.Ocena;
import si.fri.estudent.jpa.Oseba;
import si.fri.estudent.jpa.Predmet;
import si.fri.estudent.jpa.Predmet_izvajalec;
import si.fri.estudent.jpa.Predmetnik;
import si.fri.estudent.jpa.Predmetnik_letnika;
import si.fri.estudent.jpa.Student;
import si.fri.estudent.jpa.Studijska_smer;
import si.fri.estudent.jpa.Studijski_program;
import si.fri.estudent.jpa.Vloga_oseba;
import si.fri.estudent.utilities.Constants;

/**
 * Servlet implementation class VnosOcenePrijavnicaAjaxServlet
 */
@WebServlet("/VnosOcenePrijavnicaAjaxServlet")
public class VnosOcenePrijavnicaAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	int profesor = -1;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VnosOcenePrijavnicaAjaxServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	private EntityManager getEntityManager(){
		return Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		int idStudent = -1;
		int vrstaStudija = -1;
		int studijskiProgram = -1;
		int studijskaSmer = -1;
		int letnik = -1;
		int predmetIzvajalec = -1;
		
		HttpSession session = request.getSession();
		
		Oseba uporabnik = (Oseba) session.getAttribute("uporabnik");
		Vloga_oseba vloga = (Vloga_oseba) session.getAttribute("vloga");
		
		if(vloga.getIdVlogaOseba() == Constants.VLOGA_UCITELJ)
				profesor = uporabnik.getIdOseba();
				
		if (request.getParameter("idStudent") != null) {
			idStudent = Integer.parseInt(request.getParameter("idStudent"));
		}
		if (request.getParameter("vrstaStudija") != null) {
			vrstaStudija = Integer.parseInt(request.getParameter("vrstaStudija"));
		}
		if (request.getParameter("studijskiProgram") != null) {
			studijskiProgram = Integer.parseInt(request.getParameter("studijskiProgram"));
		}
		if (request.getParameter("studijskaSmer") != null) {
			studijskaSmer = Integer.parseInt(request.getParameter("studijskaSmer"));
		}
		if (request.getParameter("letnik") != null) {
			letnik = Integer.parseInt(request.getParameter("letnik"));
		}
		if (request.getParameter("predmet") != null) {
			predmetIzvajalec = Integer.parseInt(request.getParameter("predmet"));
		}
		
		EntityManager em = null;
		String query = null;
		Query q = null;
		String url = "";
				
		try {
			em = this.getEntityManager();

			/////////////// IZBRALI SMO PREDMET /////////////////
			if (predmetIzvajalec != -1) {
				Predmet p = vrniPredmet(predmetIzvajalec);
				List<Object[]> odprtePrijave = getOdprtePrijave(idStudent, predmetIzvajalec);
				
				int stPolaganjaLetos = opravljanjaLetos(predmetIzvajalec, letnik, idStudent);
				int stPolaganja = opravljanjaLetos(predmetIzvajalec, -1, idStudent);
				
				if(odprtePrijave != null && odprtePrijave.size() > 0){
					Object[] obj = odprtePrijave.get(0);
					Izpitni_rok ir = (Izpitni_rok) obj[1];
					Ocena oc = (Ocena) obj[0];
					
					if(oc.getStPolaganja() == 0) {
						request.setAttribute("predlaganStPolaganja", stPolaganja + 1);
					} else {
						request.setAttribute("predlaganStPolaganja", oc.getStPolaganja());
					}
					
					request.setAttribute("odprtaPrijava", oc);
					request.setAttribute("izpitniRokPrijave", ir);
				}else{
					request.setAttribute("predlaganStPolaganja", stPolaganja + 1);
				}
				
				request.setAttribute("predmet", p);
				url = "Ajax/podatkiIzpita.jsp";
			}
			
			/////////////// IZBRALI SMO ŠTUDIJSKO LETNIK /////////////////
			if (letnik != -1) {
				List<Predmet_izvajalec> predmetIzvajalci = vrniPredmete(letnik);
				
				request.setAttribute("predmetIzvajalci", predmetIzvajalci);
				url = "Ajax/predmetiVnosPrijavnica.jsp";
			}
			
			/////////////// IZBRALI SMO ŠTUDIJSKO SMER /////////////////
			if (studijskaSmer != -1) {
				List<Letnik> letniki = vrniLetnik(idStudent, vrstaStudija, studijskiProgram, studijskaSmer);
				
				request.setAttribute("letniki", letniki);
				url = "Ajax/letnikIzbirni.jsp";
			}
			
			///////////////////////// IZBRALI SMO ŠTUDIJSKI PROGRAM /////////////////////////
			else if (studijskiProgram != -1) {
				List<Studijska_smer> studijskeSmeri = vrniStudijskeSmeri(idStudent, vrstaStudija, studijskiProgram);
				
				request.setAttribute("studijskeSmeri", studijskeSmeri);
				url = "Ajax/studijskaSmer.jsp";
			}
			
			///////////////////////// IZBRALI SMO VRSTO ŠTUDIJA /////////////////////////
			else if (vrstaStudija != -1) {
				List<Studijski_program> studijskiProgrami = vrniStudijskePrograme(idStudent, vrstaStudija);
				
				request.setAttribute("studijskiProgrami", studijskiProgrami);
				url = "Ajax/studijskiProgram.jsp";
			}		
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}
		}
			
		RequestDispatcher fw = request.getRequestDispatcher(url);
		fw.forward(request, response);
	}
	
	private Predmet vrniPredmet(int idPredmetIzvajalec) {
		EntityManager em = this.getEntityManager();
		
		try {
			return em.find(Predmet_izvajalec.class, idPredmetIzvajalec).getPredmet();
		} catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}
	
	private List<Predmet_izvajalec> vrniPredmete(int idLetnika){
		
		EntityManager em = this.getEntityManager();

		try{
			
			String sql = "SELECT " +
								"DISTINCT pi " + 
						 "FROM " +
						 		"Predmet predmet, Predmetnik_letnika predL, Letnik letnik, " +
						 		"Predmet_izvajalec pi, Kombinacije_izvajalcev ki, Osebje os, Oseba o " +
						 "WHERE " +
						 		"letnik.idLetnik = :idLetnik AND " +
						 		"predL.letnik = letnik AND " +
						 		"predL.predmet = predmet AND " +
						 		"predmet = pi.predmet AND " +
						 		"pi.status = 1 ";
			
			if(profesor > 0)
				sql += " AND predmet = pi.predmet AND " +
						"pi.kombinacijeIzvajalcev = ki AND " +
						"(" +
							" ki.osebje1 = os OR " +
							" ki.osebje2 = os OR " +
							" ki.osebje3 = os " +
						") AND " +
						"os.oseba.idOseba = :profesor ";
			
			sql += " ORDER BY predmet.nazivPredmet";
					
			Query query = em.createQuery(sql);
			query.setParameter("idLetnik", idLetnika);
			
			if(profesor > 0)
				query.setParameter("profesor", profesor);
			
			List<Predmet_izvajalec> predmeti = query.getResultList();
			
			return predmeti;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}
	
	private List<Letnik> vrniLetnik(int idStudent, int vrstaStudija, int studijskiProgram, int studijskaSmer){
		try {
			String sql = "SELECT " +
					"DISTINCT l " +
			  "FROM " +
			  		"Student student, Vpisni_list vl, Letnik l, Studij studij," +
			  		"Vrsta_studija vs, Studijski_program sp, Studijska_smer ss, Predmetnik pred, " +
			  		"Predmetnik_letnika predL, Predmet p, Predmet_izvajalec pi, Kombinacije_izvajalcev ki," +
			  		"Osebje os, Oseba o " +
		  	  "WHERE " +
		  	  		"student.idStudent = :student AND " +
		  	  		"vl.student = student AND " +
		  	  		"vl.letnik = l AND " +
		  	  		"l.studij = studij AND " +
		  	  		"studij.vrstaStudija.idVrstaStudija = :vrstaStudija AND " +
		  	  		"studij.studijskiProgram.idStudijskiProgram = :studijskiProgram AND " +
		  	  		"studij.studijskaSmer.idStudijskaSmer =  :studijskaSmer";
			
			if(profesor > 0)
				sql += " AND vl.idVpisni = pred.fkVpisniList AND " +
						"pred.fkPredmetnikLetnika = predL.idPredmetnikLetnika AND " +
						"predL.predmet = p AND " +
						"p = pi.predmet AND " +
						"pi.kombinacijeIzvajalcev = ki AND " +
						"(" +
							" ki.osebje1 = os OR " +
							" ki.osebje2 = os OR " +
							" ki.osebje3 = os " +
						") AND " +
						"os.oseba.idOseba = :profesor ";
			
			sql += " ORDER BY l.nazivLetnik, l.idLetnik";
		
			Query q = this.getEntityManager().createQuery(sql);
			q.setParameter("student", idStudent);
			q.setParameter("vrstaStudija", vrstaStudija);
			q.setParameter("studijskiProgram", studijskiProgram);
			q.setParameter("studijskaSmer", studijskaSmer);
			
			if(profesor > 0)
				q.setParameter("profesor", profesor);
			
			List<Letnik> letniki = (List<Letnik>)q.getResultList();
			
			return letniki;
			
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	private List<Studijska_smer> vrniStudijskeSmeri(int idStudent, int vrstaStudija, int studijskiProgram) {
		try {
			String sql = "SELECT " +
					"DISTINCT ss " +
			  "FROM " +
			  		"Student student, Vpisni_list vl, Letnik l, Studij studij," +
			  		"Vrsta_studija vs, Studijski_program sp, Studijska_smer ss,  Predmetnik pred, " +
			  		"Predmetnik_letnika predL, Predmet p, Predmet_izvajalec pi, Kombinacije_izvajalcev ki," +
			  		"Osebje os, Oseba o " +
		  	  "WHERE " +
		  	  		"student.idStudent = :student AND " +
		  	  		"vl.student = student AND " +
		  	  		"vl.letnik = l AND " +
		  	  		"l.studij = studij AND " +
		  	  		"studij.vrstaStudija.idVrstaStudija = :vrstaStudija AND " +
		  	  		"studij.studijskiProgram.idStudijskiProgram = :studijskiProgram AND " +
		  	  		"studij.studijskaSmer =  ss";
			
			if(profesor > 0)
				sql += " AND vl.idVpisni = pred.fkVpisniList AND " +
						"pred.fkPredmetnikLetnika = predL.idPredmetnikLetnika AND " +
						"predL.predmet = p AND " +
						"p = pi.predmet AND " +
						"pi.kombinacijeIzvajalcev = ki AND " +
						"(" +
							" ki.osebje1 = os OR " +
							" ki.osebje2 = os OR " +
							" ki.osebje3 = os " +
						") AND " +
						"os.oseba.idOseba = :profesor ";
			
			sql += " ORDER BY ss.nazivStudijskaSmer";
		
			Query q = this.getEntityManager().createQuery(sql);
			q.setParameter("student", idStudent);
			q.setParameter("vrstaStudija", vrstaStudija);
			q.setParameter("studijskiProgram", studijskiProgram);
			
			if(profesor > 0)
				q.setParameter("profesor", profesor);
			
			List<Studijska_smer> studijskeSmeri = (List<Studijska_smer>)q.getResultList();
			return studijskeSmeri;
			
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	private List<Studijski_program> vrniStudijskePrograme(int idStudent, int vrstaStudija) {
		try {
			String sql = "SELECT " +
					"DISTINCT sp " +
			  "FROM " +
			  		"Student student, Vpisni_list vl, Letnik l, Studij studij," +
			  		"Vrsta_studija vs, Studijski_program sp, Predmetnik pred, " +
			  		"Predmetnik_letnika predL, Predmet p, Predmet_izvajalec pi, Kombinacije_izvajalcev ki," +
			  		"Osebje os, Oseba o " +
		  	  "WHERE " +
		  	  		"student.idStudent = :student AND " +
		  	  		"vl.student = student AND " +
		  	  		"vl.letnik = l AND " +
		  	  		"l.studij = studij AND " +
		  	  		"studij.vrstaStudija.idVrstaStudija = :vrstaStudija AND " +
		  	  		"studij.studijskiProgram = sp ";
			
			if(profesor > 0)
				sql += " AND vl.idVpisni = pred.fkVpisniList AND " +
						"pred.fkPredmetnikLetnika = predL.idPredmetnikLetnika AND " +
						"predL.predmet = p AND " +
						"p = pi.predmet AND " +
						"pi.kombinacijeIzvajalcev = ki AND " +
						"(" +
							" ki.osebje1 = os OR " +
							" ki.osebje2 = os OR " +
							" ki.osebje3 = os " +
						") AND " +
						"os.oseba.idOseba = :profesor ";
			
			sql += " ORDER BY  sp.nazivStudijskiProgram";
		
			Query q = this.getEntityManager().createQuery(sql);
			q.setParameter("student", idStudent);
			q.setParameter("vrstaStudija", vrstaStudija);
			
			if(profesor > 0)
				q.setParameter("profesor", profesor);
			
			List<Studijski_program> studijskiProgrami = (List<Studijski_program>)q.getResultList();
			return studijskiProgrami;
			
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}	
	
	private int opravljanjaLetos(int idPredmetIzvajalec, int idLetnika, int idStudent){
		
		EntityManager em = this.getEntityManager();
		
		String sql = "SELECT "+
				"DISTINCT oc " +
			"FROM "+
				"Oseba o, Student s, Vpisni_list vl, Predmetnik pr, Predmet p, Predmetnik_letnika prl, Ocena oc, Predmet_izvajalec pi "+
			"WHERE " +
				"s.idStudent = ?1 AND " +
				"s = vl.student AND " +
				"vl.idVpisni = pr.fkVpisniList AND " +
				"pr.fkPredmetnikLetnika = prl.idPredmetnikLetnika AND " +
				"p = prl.predmet AND " +
				"p = pi.predmet AND " +
				"pi.idPredmetIzvajalca = ?2 AND " +
				"pr.idPredmetnik = oc.fkPredmetnik AND " +
				"oc.aktivna = 1";
		
		if(idLetnika > 0)
				sql += " AND vl.letnik.idLetnik = ?3 AND " +
						"vl.vrstaVpisa.idVrstaVpisa = 2 ";
		
		Query query = em.createQuery(sql); 
		
		query.setParameter(1, idStudent);
		query.setParameter(2, idPredmetIzvajalec);
		
		if(idLetnika > 0)
			query.setParameter(3, idLetnika);
		
		List<Ocena>ocene = (List<Ocena>) query.getResultList();
		
		Predmetnik predmetnik = null;
		int max = 0;
		
		for(Ocena o : ocene){
			if(o.getStPolaganja() > max && idLetnika < 0)
					max = o.getStPolaganja();
			
			if(o.getStPolaganjaLetos() > max && idLetnika > 0)
					max = o.getStPolaganjaLetos();
		}
		
		return max;
	}
	
	private List<Object[]> getOdprtePrijave(int idStudent, int idPredmetIzvajalec) {
		List<Object[]> ocene_in_roki = null;

		String sql = "SELECT DISTINCT oc, ir " +
				 " FROM " +
				 	" Izpitni_rok ir, Student st, Predmet predmet, Ocena oc, Predmetnik pred, Vpisni_list vl, Predmet_izvajalec pi " +
				 " WHERE " +
				 	" vl.student.idStudent = :idStudent AND " +
				 	" pi.idPredmetIzvajalca = :idPredmetIzvajalec AND " +
				 	" predmet = pi.predmet AND " +
				 		
				 	" pred.fkVpisniList = vl.idVpisni AND " +
				 	" oc.fkPredmetnik = pred.idPredmetnik AND " +
				 	" (" +
				 		" oc.ocenaKoncna < 1 AND " +
				 		" oc.ocenaIzpit < 1 AND " +
				 		" oc.ocenaVaje < 1" +
				 	" ) AND " +
				 	" ir.idRok = oc.fkIzpitniRok AND " +
					" ir.predmet = predmet " +
		 			" AND oc.aktivna = 1 ";

		Query q = this.getEntityManager().createQuery(sql);
		q.setParameter("idStudent", idStudent);
		q.setParameter("idPredmetIzvajalec", idPredmetIzvajalec);
		ocene_in_roki = q.getResultList();
		
		// gre navzdol, da ko ga pobrise ne zjebe indeksov
		for (int i = ocene_in_roki.size() - 1; i >= 0; i--) {
			Object[] ocena_in_rok = ocene_in_roki.get(i);
			Izpitni_rok izpitniRok = (Izpitni_rok) ocena_in_rok[1];
			System.out.println("izpitni rok "+izpitniRok.getPredmet().getNazivPredmet() +" "+izpitniRok.getPredavalnica());
			Date datumIzpita = izpitniRok.getDatumIzpit();
			Date sedaj = new Date();
			if (datumIzpita.after(sedaj)) { // ce je datum izpita pred
														// sedanjostjo
		     	ocene_in_roki.remove(i);
				System.out.println("odstranjen " + i);
			}

		}

		return ocene_in_roki;
	}
}
