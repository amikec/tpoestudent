package si.fri.estudent.ajax;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import si.fri.estudent.jpa.Izpitni_rok;
import si.fri.estudent.jpa.Kombinacije_izvajalcev;
import si.fri.estudent.jpa.Letnik;
import si.fri.estudent.jpa.Ocena;
import si.fri.estudent.jpa.Oseba;
import si.fri.estudent.jpa.Osebje;
import si.fri.estudent.jpa.Predmet;
import si.fri.estudent.jpa.Predmetnik;
import si.fri.estudent.jpa.Predmetnik_letnika;
import si.fri.estudent.jpa.Student;
import si.fri.estudent.jpa.Vpisni_list;
import si.fri.estudent.utilities.Constants;
import si.fri.estudent.utilities.Tiskanje;
import si.fri.estudent.utilities.Utilities;

/**
 * Servlet implementation class VnosKoncnihOcenAjaxServlet
 */
@WebServlet("/VnosKoncnihOcenAjaxServlet")
public class VnosKoncnihOcenAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Oseba profesor = null;
    private EntityManager em = null;
    private boolean izpis = false;
    
    private EntityManager getEntityManager(){

    	return Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
    }
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VnosKoncnihOcenAjaxServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(request.getParameter("tiskanje") != null)
			tiskaj(request, response);
		else 
			return;
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		prikaz(request, response);
		
	}

	private void prikaz(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
				
		if(request.getParameterValues("ocene[]") != null){
			String uspesnost = handlePosodobitevOcen(request, response);
			request.setAttribute("uspesnostPosodabljanja", uspesnost);
		}
		
		izpis = Boolean.parseBoolean(request.getParameter("izpis"));
		int idPredmet = Integer.parseInt(request.getParameter("predmet"));
		int profesor = Integer.parseInt(request.getParameter("profesor"));
		int letnik = Integer.parseInt(request.getParameter("letnik"));
		int idIzpitniRok = Integer.parseInt(request.getParameter("izpitniRok"));
		
		String url = "";
		
		Predmet predmet = this.getEntityManager().find(Predmet.class, idPredmet);
		Object[] rezultat = vrniPrijavljeneStudente(idPredmet, letnik, profesor, idIzpitniRok);
		List<Student> studenti = (List<Student>) rezultat[0];
		List<Ocena> ocene = (List<Ocena>) rezultat[1];
		List<int[]> stOpravljanjStudenti = vrniStOpravljanjStudentov(studenti, idPredmet, letnik);
		Izpitni_rok izpitniRok = getIzpitniRok(idIzpitniRok);
				
		List<Integer> polaganjaPonavljanja = new LinkedList<Integer>();
		
		for(Student s : studenti){
			int temp = 0;
			temp = getStPolagPonavljanj(idPredmet, s.getIdStudent());
			polaganjaPonavljanja.add(temp);
		}
		
		request.setAttribute("polaganjaPonavljanja", polaganjaPonavljanja);
		
		request.setAttribute("predmet", predmet);
		request.setAttribute("stOpravljanjStudenti", stOpravljanjStudenti);
		request.setAttribute("studenti", studenti);
		request.setAttribute("ocene", ocene);
		request.setAttribute("izpitniRok", izpitniRok);
		url = "Ajax/koncneOcene.jsp";
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
	
	private Izpitni_rok getIzpitniRok(int idIzpitniRok) {
		EntityManager em = null;
		try {
			em = this.getEntityManager();
			return em.find(Izpitni_rok.class, idIzpitniRok);
		} catch (Exception e) {
			return null;
		}
	}
	
	private int getStPolagPonavljanj(int predmet, int student){
		try {
			String sql = "SELECT " +
					"DISTINCT oc " +
			  "FROM " +
			  		"Ocena oc, Predmetnik pred, Vpisni_list vl, Vrsta_vpisa vp, Predmetnik_letnika predL, Predmet p " +
		  	  "WHERE " +
		  	  		"vl.student.idStudent = :student AND " +
		  	  		"vl.vrstaVpisa.idVrstaVpisa = :vrstaVpisa AND " +
		  	  		"vl.idVpisni = pred.fkVpisniList AND " +
		  	  		"pred.idPredmetnik = oc.fkPredmetnik AND " +
		  	  		"pred.fkPredmetnikLetnika = predL.idPredmetnikLetnika AND " +
		  	  		"predL.predmet.idPredmeta = :predmet AND " +
		  	  		"oc.aktivna = 1 AND " +
		  	  		"( " +
		  	  			" oc.ocenaKoncna > 0 OR " +
		  	  			" oc.ocenaIzpit > 0 OR " +
		  	  			" oc.ocenaVaje > 0 OR " +
		  	  			" oc.ocena > 0 " +
		  	  		")";
					  	  		
			Query query = this.getEntityManager().createQuery(sql);
			query.setParameter("predmet", predmet);
			query.setParameter("student", student);
			query.setParameter("vrstaVpisa", Constants.VPIS_PONAVLJANJE);
			
			List<Ocena> ocene = query.getResultList();
			
			int st = 0;
			
			for(Ocena oc : ocene)
				st += 1;
					
			return st;
			
		} catch(Exception e){
			e.printStackTrace();
			return 0;
		}	
	}

	
	private String handlePosodobitevOcen(HttpServletRequest request, HttpServletResponse response){
		String nacinOcen = request.getParameter("nacinOcen");
		String[] oceneKoncne = request.getParameterValues("oceneKoncne[]");
		String[] oceneIzpit = request.getParameterValues("oceneIzpit[]");
		String[] oceneVaje = request.getParameterValues("oceneVaje[]"); 
		String[] ocene = request.getParameterValues("ocene[]");
		int izpitniRok = Integer.parseInt(request.getParameter("izpitniRok"));
		
		EntityManager em = this.getEntityManager();
		
		try{
			
			em.getTransaction().begin();
			
			int ocena = 0;
			int predmetnikId = 0;
			int ocena2 = 0;
			int ocenaId = 0;
			Predmetnik predmetnik;
			Izpitni_rok ir;
			Ocena ocenaPrijava;
			
			for(int i = 0; i < ocene.length; i++){
				
				String ocenaK = "";
				String ocenaI = "";
				String ocenaV = "";
				
				if(oceneKoncne != null)
					ocenaK = oceneKoncne[i];
				
				if(oceneIzpit != null)
					ocenaI = oceneIzpit[i];
				
				if(oceneVaje != null)
					ocenaV = oceneVaje[i];
				
				ocenaId = Integer.parseInt(ocene[i]);
				ocenaPrijava = em.find(Ocena.class, ocenaId);
				predmetnik = em.find(Predmetnik.class, ocenaPrijava.getFkPredmetnik());
				
				ir = em.find(Izpitni_rok.class, izpitniRok);
				
				Vpisni_list vl = em.find(Vpisni_list.class, predmetnik.getFkVpisniList());
				
				if(ocenaPrijava.getStPolaganja() == 0){
					int stLetos = opravljanjaLetos(ir.getPredmet(), vl.getStudijskoLeto(), vl.getStudent().getOseba());
					int st = opravljanjaLetos(ir.getPredmet(), "", vl.getStudent().getOseba());
					ocenaPrijava.setStPolaganja(st + 1);
					ocenaPrijava.setStPolaganjaLetos(stLetos + 1);
				}
				
				if(ocenaK.equals("VP") || ocenaI.equals("VP") || ocenaV.equals("VP")){
					ocenaPrijava.setAktivna(false);
					ocenaPrijava.setOcena(-1);
				}else{
				
					if(nacinOcen.equals(Constants.OCENA_IZPIT)){
						ocena = Integer.parseInt(ocenaI);
						if(ocena > 10 || ocena < 1){
							em.getTransaction().rollback();
							return "<span class='error'>Nepravilna ocena v vrstici " + (i+1) + "</span>";
						}
						ocenaPrijava.setOcenaIzpit(ocena);
					}
					
					else if(nacinOcen.equals(Constants.OCENA_VAJE)){
						ocena = Integer.parseInt(ocenaV);
						if(ocena > 10 || ocena < 1){
							em.getTransaction().rollback();
							return "<span class='error'>Nepravilna ocena v vrstici " + (i+1) + "</span>";
						}
						
						ocenaPrijava.setOcenaVaje(ocena);
					}
					
					else if(nacinOcen.equals(Constants.OCENA_KONCNA)){
						ocena = Integer.parseInt(ocenaK);
						if(ocena > 10 || ocena < 1){
							em.getTransaction().rollback();
							return "<span class='error'>Nepravilna ocena v vrstici " + (i+1) + "</span>";
						}
						
						ocenaPrijava.setOcenaKoncna(ocena);
					}
					
					else if(nacinOcen.equals(Constants.OCENA_IZPIT_VAJE)){
						
						ocena = Integer.parseInt(ocenaI);
						ocena2 = Integer.parseInt(ocenaV);
						
						if(ocena >= 6 && ocena2 >= 6 && ocena <= 10 && ocena2 <= 10){
						
						}else if(ocena <= 5 && ocena2 == 0 && ocena > 0){
							
						}else{
							em.getTransaction().rollback();
							return "<span class='error'>Nepravilna ocena v vrstici " + (i+1) + "</span>";
						}
						
						ocenaPrijava.setOcenaIzpit(ocena);
						ocenaPrijava.setOcenaVaje(ocena2);
					}
				}
				
				em.merge(ocenaPrijava);
			}
			
			em.getTransaction().commit();
			
			return "Vnos uspešen";
			
		}catch(Exception e){
			e.printStackTrace();
			em.getTransaction().rollback();
			
			return "<span class='error'>Vnos neuspešen</span>";
		}
	}

	private void tiskaj(HttpServletRequest request, HttpServletResponse response){
		em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		izpis = Boolean.parseBoolean(request.getParameter("izpis"));
		
		if(!izpis)
			return;
		
		int idPredmeta = Integer.parseInt(request.getParameter("predmet"));
		int idLetnika = Integer.parseInt(request.getParameter("letnik"));
		int profesor = Integer.parseInt(request.getParameter("profesor"));
		int izpitniRok = Integer.parseInt(request.getParameter("izpitniRok"));
		
		Izpitni_rok izRok = em.find(Izpitni_rok.class, izpitniRok);
		
		Object[] rezultat = vrniPrijavljeneStudente(idPredmeta, idLetnika, profesor, izpitniRok);
		
		List<Oseba> izvajalci = vrniIzvajalce(profesor, idPredmeta);
		String izvajalciString = "";
		for(Oseba o : izvajalci){
			if(!izvajalciString.isEmpty())
				izvajalciString += ", ";
			
			izvajalciString += o.getIme() + " " + o.getPriimek();
		}
			
		List<Student> studenti = (List<Student>) rezultat[0];
		List<Ocena> ocene = (List<Ocena>) rezultat[1];
		List<int[]> stOpravljanjStudenti = vrniStOpravljanjStudentov(studenti, idPredmeta, idLetnika);
		
		if(ocene.size() < 1)
			return;
		
		int idPredmetnik = ocene.get(0).getFkPredmetnik();
		Predmetnik predmetnik = em.find(Predmetnik.class, idPredmetnik);
		
		int idPredmetnikLetnika = predmetnik.getFkPredmetnikLetnika();
		Predmetnik_letnika predmetnikLetnika = em.find(Predmetnik_letnika.class, idPredmetnikLetnika);
		Predmet predmet = predmetnikLetnika.getPredmet();
		
		List<Integer> polaganjaPonavljanja = new LinkedList<Integer>();
		
		for(Student s : studenti){
			int temp = 0;
			temp = getStPolagPonavljanj(idPredmeta, s.getIdStudent());
			polaganjaPonavljanja.add(temp);
		}
		
		String header = String.format("Predmet: %s \n" +
				  "Izvajalci: %s \n" +
				  "Datum: %s", predmet.getSifraPredmeta() + " - " + predmet.getNazivPredmet(), izvajalciString, izRok.getDatumIzpit());
		
		Tiskanje tiskanje = new Tiskanje( request, response, "Seznam končnih ocen");
		
		tiskanje.dodajBesedilo(header);
		
		String nacinOcenjevanja = predmet.getNacinOcenjevanja();
		String[][] izpisZaTiskanje = null;
		
		if(nacinOcenjevanja.equals(Constants.OCENA_IZPIT)){
			izpisZaTiskanje = new String[studenti.size() + 1][7];
			izpisZaTiskanje[0][7] = "OCENA IZPITA";
		}else if(nacinOcenjevanja.equals(Constants.OCENA_IZPIT_VAJE)){
			izpisZaTiskanje = new String[studenti.size() + 1][8];
			izpisZaTiskanje[0][7] = "OCENA IZPITA";
			izpisZaTiskanje[0][8] = "OCENA VAJ";
		}else if(nacinOcenjevanja.equals(Constants.OCENA_VAJE)){
			izpisZaTiskanje = new String[studenti.size() + 1][7];
			izpisZaTiskanje[0][7] = "OCENA VAJ";
		}else if(nacinOcenjevanja.equals(Constants.OCENA_KONCNA)){
			izpisZaTiskanje = new String[studenti.size() + 1][7];
			izpisZaTiskanje[0][7] = "KONČNA OCENA";
		}
		
		izpisZaTiskanje[0][0] = "ZAPOREDNA ŠTEVILKA";
		izpisZaTiskanje[0][1] = "VPISNA ŠTEVILKA";
		izpisZaTiskanje[0][2] = "PRIIMEK IN IME";
		izpisZaTiskanje[0][3] = "ZAPOREDNA POLAGANJA";
		izpisZaTiskanje[0][4] = "ŠT. POLAGANJA";
		izpisZaTiskanje[0][5] = "TOČKE IZPITA";
		izpisZaTiskanje[0][6] = "BONUS TOČKE";
					
		String imeStudent = null;
		int [] stOpravljanj = null;
		Student student = null;
		Oseba oseba = null;
		Ocena oc = null;
		
		for (int i = 0; i < studenti.size(); i++) {
			student = studenti.get(i);
			oseba = student.getOseba();
			oc = ocene.get(i);
			
			int polagPonav = polaganjaPonavljanja.get(i);
			
			stOpravljanj = stOpravljanjStudenti.get(i);
			
			if (oseba.getPriimekDekliski() == null || oseba.getPriimekDekliski().equals("")) {
				imeStudent = oseba.getPriimek() + " " + oseba.getIme();
			} else {
				imeStudent = oseba.getPriimek() + " " + oseba.getPriimekDekliski() + " " + oseba.getIme();
			}
			
			String opravljanja = "";
			
			izpisZaTiskanje[i + 1][0] = Integer.toString(i + 1);
			izpisZaTiskanje[i + 1][1] = student.getVpisnaSt();
			izpisZaTiskanje[i + 1][2] = imeStudent;
			izpisZaTiskanje[i + 1][5] = Double.toString(oc.getOcena());
			izpisZaTiskanje[i + 1][6] = Double.toString(oc.getOcenaBonus());
			
			opravljanja = String.format("%d", stOpravljanj[0]);
			/*
			if(stOpravljanj[1] > 0)
				opravljanja = String.format("%d - %d", stOpravljanj[0], stOpravljanj[1]);
			else
				opravljanja = String.format("%d", stOpravljanj[0]);
			*/

			izpisZaTiskanje[i + 1][3] = opravljanja;
			izpisZaTiskanje[i + 1][4] = Integer.toString(polagPonav);
			
			if(nacinOcenjevanja.equals(Constants.OCENA_IZPIT)){
				izpisZaTiskanje[i + 1][7] = Integer.toString(oc.getOcenaIzpit());
			}else if(nacinOcenjevanja.equals(Constants.OCENA_IZPIT_VAJE)){
				izpisZaTiskanje[i + 1][7] = Integer.toString(oc.getOcenaIzpit());
				izpisZaTiskanje[i + 1][8] = Integer.toString(oc.getOcenaVaje());
			}else if(nacinOcenjevanja.equals(Constants.OCENA_VAJE)){
				izpisZaTiskanje[i + 1][7] = Integer.toString(oc.getOcenaVaje());
			}else if(nacinOcenjevanja.equals(Constants.OCENA_KONCNA)){
				izpisZaTiskanje[i + 1][7] = Integer.toString(oc.getOcenaKoncna());
			}
		}
		
		tiskanje.dodajTabelo(izpisZaTiskanje, false, true);		
		tiskanje.zakljuci();
	}
	
	private int opravljanjaLetos(Predmet p, String leto, Oseba oseba){
		
		EntityManager em = this.getEntityManager();
		
		String sql = "SELECT "+
				"DISTINCT oc " +
			"FROM "+
				"Oseba o, Student s, Vpisni_list vl, Predmetnik pr, Predmet p, Predmetnik_letnika prl, Ocena oc "+
			"WHERE " +
				"s.oseba.idOseba = ?1 AND " +
				"s.idStudent = vl.student.idStudent AND " +
				"vl.idVpisni = pr.fkVpisniList AND " +
				"pr.fkPredmetnikLetnika = prl.idPredmetnikLetnika AND " +
				"p.idPredmeta = prl.predmet.idPredmeta AND " +
				"p.idPredmeta = ?2 AND " +
				"pr.idPredmetnik = oc.fkPredmetnik AND " +
				"oc.aktivna = 1";
		
		if(!leto.isEmpty())
				sql += " AND vl.studijskoLeto = ?3 ";
		
		Query query = em.createQuery(sql); 
		
		query.setParameter(1, oseba.getIdOseba());
		query.setParameter(2, p.getIdPredmeta());
		
		if(!leto.isEmpty())
			query.setParameter(3, leto);
		
		List<Ocena>ocene = (List<Ocena>) query.getResultList();
		
		Predmetnik predmetnik = null;
		int max = 0;
		
		for(Ocena o : ocene){
			if(o.getStPolaganja() > max && leto.isEmpty())
					max = o.getStPolaganja();
			
			if(o.getStPolaganjaLetos() > max && !leto.isEmpty())
					max = o.getStPolaganjaLetos();
		}
		
		return max;
	}

	
	private List<Oseba> vrniIzvajalce(int idProfesor, int idPredmet){
		
		List<Oseba> izvajalci = new LinkedList<Oseba>();
		
		try{
			em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
			String sql = "SELECT " +
							"DISTINCT ki " +
						 "FROM " +
						 	"Oseba oseba, Osebje os, Predmet_izvajalec pi, Kombinacije_izvajalcev ki, " +
						 	"Predmet predmet " +
						 "WHERE " +
						 	"predmet.idPredmeta = :predmet AND " +
						 	"pi.predmet = predmet AND " +
						 	"pi.kombinacijeIzvajalcev = ki AND " +
						 	"pi.status = 1 AND " +
						 	"( " +
						 		"ki.osebje1 = os OR " +
						 		"ki.osebje2 = os OR " +
						 		"ki.osebje3 = os " +
						 	") AND " +
						 	"os.oseba = oseba AND " +
						 	"oseba.idOseba = :profesor " +
						 "ORDER BY " +
						 	"oseba.priimek";
			
			Query query = em.createQuery(sql);
			query.setParameter("predmet", idPredmet);
			query.setParameter("profesor", idProfesor);
			
			Kombinacije_izvajalcev ki = (Kombinacije_izvajalcev) query.getSingleResult();
			 
			izvajalci.add(ki.getOsebje1().getOseba());
			
			if(ki.getOsebje2() != null)
				izvajalci.add(ki.getOsebje2().getOseba());
			
			if(ki.getOsebje3() != null) 
				izvajalci.add(ki.getOsebje3().getOseba());
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return izvajalci;
	}
	
	private Object[] vrniPrijavljeneStudente(int predmet, int letnik, int profesor, int izpitniRok) {		
		try{
			String sql = "SELECT " +
							"DISTINCT st, ocena " +
						 "FROM " +
						 	"Izpitni_rok ir, Student st, Vpisni_list vpList, Predmetnik pred, Predmetnik_letnika predL, Ocena ocena, Predmet p, Letnik l, " +
						 	"Kombinacije_izvajalcev ki, Predmet_izvajalec pi, Osebje os, Oseba o " +
						 "WHERE " +
						 	"st = vpList.student AND " +
						 	"vpList.idVpisni = pred.fkVpisniList AND " +
						 	"vpList.letnik.idLetnik = :idLetnika AND " +
						 	"pred.fkPredmetnikLetnika = predL.idPredmetnikLetnika AND " +
						 	"predL.predmet = ir.predmet AND " +
						 	"predL.letnik.idLetnik = :idLetnika AND " +
						 	"predL.predmet = p AND " +
						 	"p.idPredmeta = :idPredmeta AND " +
						 	"ocena.fkPredmetnik = pred.idPredmetnik AND " +
						 	"ocena.aktivna = 1 AND " +
						 	"ocena.fkIzpitniRok = :izpitniRok AND " +
						 	"ir.idRok = ocena.fkIzpitniRok AND " +
						 	"ir.predmetIzvajalec = pi AND " +
						 	"pi.kombinacijeIzvajalcev = ki AND " +
						 	"(" +
						 		"ki.osebje1 = os OR " +
						 		"ki.osebje2 = os OR " +
						 		"ki.osebje3 = os " +
						 	") AND " +
						 	"os.oseba.idOseba = :profesor " +
						 "ORDER BY " +
						 	"st.oseba.priimek ";
			
			Query q = this.getEntityManager().createQuery(sql);
			q.setParameter("idLetnika", letnik);
			q.setParameter("idPredmeta", predmet);
			q.setParameter("izpitniRok", izpitniRok);
			q.setParameter("profesor", profesor);
			//q.setParameter("profesor", profesor);
			
			//ob[0] = student, ob[1] = ocena
			List<Object[]> rezultat = q.getResultList();
			List<Student> studenti = new LinkedList<Student>();
			List<Ocena> ocene = new LinkedList<Ocena>();
			
			for(Object[] ob : rezultat){
				studenti.add((Student) ob[0]);
				ocene.add((Ocena) ob[1]);
			}
			
			return new Object[] {studenti, ocene};
			
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	private List<int[]> vrniStOpravljanjStudentov(List<Student> studenti, int idPredmet, int letnik){
		List<int[]> stOpravljanj = new LinkedList<int[]>();
		
		Letnik l = this.getEntityManager().find(Letnik.class, letnik);
		Predmet predmet = this.getEntityManager().find(Predmet.class, idPredmet);

		for(Student student : studenti){
			int[] polaganja = new int[] {opravljanjaLetos(predmet, "", student.getOseba()), 
					opravljanjaLetos(predmet, l.getStudijskoLeto(), student.getOseba())};
			stOpravljanj.add(polaganja); 
		}
		
		return stOpravljanj;
	}
}
/*
SELECT 
	ocena, ki
FROM 
	Ocena ocena, Predmetnik pred, Predmetnik_letnika predL, Predmet p, Letnik l, 
	Izpitni_rok ir, Predmet_izvajalec pi, Kombinacije_izvajalcev ki
WHERE 
	letnik.studijskoLeto = :leto AND 
	letnik = predL.letnik AND 
	predL.idPredmetnikLetnika = pred.fkPredmetnikLetnika  AND 
	predL.predmet = p AND 
	p.idPredmeta = :predmet AND
	pred.idPredmetnik = ocena.fkPredmetnik AND 
	ocena.fkIzpitniRok = ir.idRok AND 
	ir.predmetIzvajalec = pi AND 
	pi.kombinacijeIzvajalcev = ki
	
.setParameter("leto", );
.setParameter("predmet", );

List<Object[]> rezultat = query.getResultList();

List<Object> ocene = (List<Ocena>) rezultat[0];
List<Kombinacije_izvajalcev> izvajalci = (List<Kombinacije_izvajalcev>) rezultat[1];

 * */
