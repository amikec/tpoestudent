package si.fri.estudent.ajax;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;

import si.fri.estudent.jpa.Drzava;
import si.fri.estudent.jpa.Kraj;
import si.fri.estudent.jpa.Obcina;
import si.fri.estudent.jpa.Oseba;
import si.fri.estudent.jpa.Osebje;
import si.fri.estudent.jpa.Predmet;
import si.fri.estudent.jpa.ProgramiInSmeri;
import si.fri.estudent.jpa.Studijska_smer;
import si.fri.estudent.jpa.Studijski_program;
import si.fri.estudent.utilities.DataValidator;

/**
 * Servlet implementation class SifrantiAjaxServlet
 */
@WebServlet("/SifrantiAjaxServlet")
public class SifrantiAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private EntityManager em;   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SifrantiAjaxServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		request.setCharacterEncoding("UTF-8");
		String sifrant=request.getParameter("sifrant");
		System.out.println("ajax");
		System.out.println("sifrant "+sifrant);
		
		if(sifrant.startsWith("shrani")){
			if(sifrant.equals("shraniobcina")){
				try{
					String id=request.getParameter("id");
					String text1=request.getParameter("text1");
					String text2=request.getParameter("text2");
					String status=request.getParameter("status");
					boolean bstat=status.equals("1");
					
					DataValidator dv=new DataValidator();
					boolean kulText1=dv.validateInputText(text1, 3, false, "^[0-9]{1,3}$", "Prazna šifra", "Predolga šifra", "Neveljavna šifra");
					
					if(kulText1 && text2.length()>0){
						Obcina obcina=em.find(Obcina.class, Integer.parseInt(id));
						System.out.println("id"+obcina.getIdObcina());
						obcina.setSifra(text1);
						obcina.setNazivObcina(text2);
						obcina.setStatus(bstat);
						em.getTransaction().begin();
						em.merge(obcina);							
						em.flush();
						em.getTransaction().commit();
						odgovor(response,"OK");
						
					}else{						
						odgovor(response,"Napaka. " +dv.getError());
					}
					
				}catch (Exception e) {
					odgovor(response,"Napaka");
					System.out.println(e.getLocalizedMessage());
				}
				
				
			}else if(sifrant.equals("shranidrzava")){
				try{
					String id=request.getParameter("id");
					String text1=request.getParameter("text1");
					String text2=request.getParameter("text2");
					String status=request.getParameter("status");
					boolean bstat=status.equals("1");
					
					DataValidator dv=new DataValidator();
					boolean kulText1=dv.validateInputText(text2, 3, false, "^[0-9]{1,3}$", "Prazna šifra", "Predolga šifra", "Neveljavna šifra");
					
					if(kulText1 && text2.length()>0 ){
						Drzava drzava=em.find(Drzava.class, Integer.parseInt(id));
						System.out.println("id"+drzava.getIdDrzava());
						drzava.setSifra(text1);
						drzava.setNazivDrzava(text2);
						drzava.setStatus(bstat);
						em.getTransaction().begin();
						em.merge(drzava);							
						em.flush();
						em.getTransaction().commit();
						odgovor(response,"OK");
						
					}else{						
						odgovor(response,"Napaka. " +dv.getError());
					}
					
				}catch (Exception e) {
					odgovor(response,"Napaka");
					System.out.println(e.getLocalizedMessage());
				}
			}else if(sifrant.equals("shraniposta")){
				try{
					String id=request.getParameter("id");
					String text1=request.getParameter("text1");
					String text2=request.getParameter("text2");
					String status=request.getParameter("status");
					boolean bstat=status.equals("1");
					
					DataValidator dv=new DataValidator();
					boolean kulText1=dv.validateInputText(text1, 50, false, "^[0-9]{4}$", "Prazna poštna številka", "Predolga številka", "Neveljavna številka");
					//boolean kulText2=dv.validateInputText(text2, 50, false, "^[a-zA-ZčćšđžČĆŠĐŽ]{1,50}$", "Prazno ime", "Predolgo ime", "Neveljavno ime");
					
					if(kulText1 && text2.length()>0){
						Kraj kraj=em.find(Kraj.class, Integer.parseInt(id));
						System.out.println("id"+kraj.getIdKraj());
						kraj.setPostnaSt(text1);
						kraj.setPosta(text2);
						kraj.setStatus(bstat);
						em.getTransaction().begin();
						em.merge(kraj);							
						em.flush();
						em.getTransaction().commit();
						odgovor(response,"OK");
						
					}else{						
						odgovor(response,"Napaka. " +dv.getError());
					}
					
				}catch (Exception e) {
					odgovor(response,"Napaka");
					System.out.println(e.getLocalizedMessage());
				}
			}else if(sifrant.equals("shraniprogramInSmer")){
				try{
					String id=request.getParameter("id");
					String text1=request.getParameter("text1");
					String text2=request.getParameter("text2");
					String status=request.getParameter("status");
					boolean bstat=status.equals("1");
					
					DataValidator dv=new DataValidator();
					boolean kulText1=dv.validateInputText(text1, 4, false, "^[a-zA-ZčćšđžČĆŠĐŽ0-9]{4}$", "Prazna kratica", "Predolga kratica", "Neveljavna kratica");
					//boolean kulText2=dv.validateInputText(text2, 50, false, "^[a-zA-ZčćšđžČĆŠĐŽ' ']{1,50}$", "Prazno ime", "Predolgo ime", "Neveljavno ime");
					
					if(kulText1 && text2.length()>0){
						System.out.println("1");
						ProgramiInSmeri program=em.find(ProgramiInSmeri.class, Integer.parseInt(id));
						System.out.println("2"); 
						System.out.println("id"+program.getIdProgramSmer());
						program.setSifra(text1);
						program.setNaziv(text2);
						program.setStatus(bstat);
						em.getTransaction().begin();
						em.merge(program);							
						em.flush();
						em.getTransaction().commit();
						odgovor(response,"OK");
						
					}else{						
						odgovor(response,"Napaka. " +dv.getError());
					}
					
				}catch (Exception e) {
					odgovor(response,"Napaka");
					System.out.println(e.getLocalizedMessage());
				}
			}else if(sifrant.equals("shranipredmet")){
				try{
					String id=request.getParameter("id");
					String text1=request.getParameter("text1");
					String text2=request.getParameter("text2");
					String text3=request.getParameter("text3");
					String text4=request.getParameter("text4");
					String text5=request.getParameter("text5");
					String status=request.getParameter("status");
					boolean bstat=status.equals("1");
					
					DataValidator dv=new DataValidator();
					boolean kulText1=dv.validateInputText(text1, 5, false, "^[0-9]{5}$", "Prazna šifra", "Predolga šifra", "Neveljavna šifra");
					boolean kulText2=dv.validateInputText(text2, 50, false, "^[a-zA-ZčćšđžČĆŠĐŽ]{1,50}$", "Prazno kratica", "Predolgo kratica", "Neveljavno kratica");
					boolean kulText3=dv.validateInputText(text3, 2, false, "^[0-9]{1,2}$", "Prazne točke", "Predolge točke", "Neveljavno točke");
					//boolean kulText4=dv.validateInputText(text4, 50, false, "^[a-zA-ZčćšđžČĆŠĐŽ \\s]{1,50}$", "Prazno ime", "Predolgo ime", "Neveljavno ime");
					//System.out.println(text4 +" "+kulText4);
					if(kulText1 && kulText2 && kulText3 && text4.length()>0 && text5.length()>0){
						Predmet predmet=em.find(Predmet.class, Integer.parseInt(id));
						System.out.println("id"+predmet.getIdPredmeta());
						predmet.setSifraPredmeta(text1);
						predmet.setKraticaPredmet(text2);
						predmet.setKreditneTocke(BigDecimal.valueOf(Double.parseDouble(text3)));
						predmet.setNazivPredmet(text4);
						predmet.setNacinOcenjevanja(text5);
						predmet.setStatus(bstat);
						em.getTransaction().begin();
						em.merge(predmet);							
						em.flush();
						em.getTransaction().commit();
						odgovor(response,"OK");
						
					}else{						
						odgovor(response,"Napaka. " +dv.getError());
					}
					
				}catch (Exception e) {
					odgovor(response,"Napaka");
					System.out.println(e.getLocalizedMessage());
				}
			}else if(sifrant.equals("shranipredavatelj")){
				try{
					String id=request.getParameter("id");
					String text1=request.getParameter("text1");
					String text2=request.getParameter("text2");
					String text3=request.getParameter("text3");
					String text4=request.getParameter("text4");
					String text5=request.getParameter("text5");
					String status=request.getParameter("status");
					boolean bstat=status.equals("1");
					System.out.println(text1+" "+text2+" "+text3+" "+text4+" "+text5);
					DataValidator dv=new DataValidator();
					boolean kulText1=dv.validateInputText(text1, 50, false, "^[0-9]{6,7}$", "Prazna šifra", "Predolga šifra", "Neveljavna šifra");
					boolean kulText2=dv.validateInputText(text2, 50, false, "^[a-zA-ZčćšđžČĆŠĐŽü]{1,50}$", "Prazno ime", "Predolgo ime", "Neveljavno ime");
					boolean kulText3=dv.validateInputText(text3, 50, false, "^[a-zA-ZčćšđžČĆŠĐŽü]{1,50}$", "Praznen priimek", "Predolg priimek", "Neveljaven priimek");
					boolean kulText4=dv.validateInputText(text4, 50, false, "^[a-zA-ZčćšđžČĆŠĐŽ]{1,50}$", "Prazno up ime", "Predolgo up ime", "Neveljavno up ime");
					boolean kulText5=dv.validateInputText(text5, 50, false, "^[a-zA-ZčćšđžČĆŠĐŽ]{1,50}$", "Prazno geslo", "Predolgo geslo", "Neveljavno geslo");
					
					if(kulText1 && kulText2 && kulText3 && kulText4 && kulText5){						
						Osebje osebje=em.find(Osebje.class, Integer.parseInt(id));						
						osebje.setSifraPredavatelja(text1);
						osebje.setStatus(bstat);
						Oseba oseba=osebje.getOseba();						
						oseba.setIme(text2);
						oseba.setPriimek(text3);
						oseba.setUpIme(text4);
						oseba.setGeslo(text5);
						oseba.setStatus(bstat);
						System.out.println("4");
						em.getTransaction().begin();
						em.merge(oseba);							
						em.flush();
						em.getTransaction().commit();
						odgovor(response,"OK");
						
					}else{						
						odgovor(response,"Napaka. " +dv.getError());
					}
					
				}catch (Exception e) {
					odgovor(response,"Napaka");
					System.out.println("napaka opis: "+e.getLocalizedMessage());
				}
			}
			
			
		}else if(sifrant.startsWith("izbrisi")){
			if(sifrant.equals("izbrisiobcina")){
				String id=request.getParameter("id");
				System.out.println("izbiris id "+id);
				try{		
					em.getTransaction().begin();
					Obcina obcina=em.find(Obcina.class, Integer.parseInt(id));		
					em.remove(obcina);
					em.getTransaction().commit();
					odgovor(response,"OK");
					return;
					
				}catch (Exception e) {
					odgovor(response,"Napaka");
					return;
				}
			}else if(sifrant.equals("izbrisidrzava")){
				String id=request.getParameter("id");
				System.out.println("izbiris id "+id);
				try{		
					em.getTransaction().begin();
					Drzava drzava=em.find(Drzava.class, Integer.parseInt(id));		
					em.remove(drzava);
					em.getTransaction().commit();
					odgovor(response,"OK");
					return;
					
				}catch (Exception e) {
					odgovor(response,"Napaka");
					return;
				}
			}else if(sifrant.equals("izbrisiposta")){
				String id=request.getParameter("id");
				System.out.println("izbiris id "+id);
				try{		
					em.getTransaction().begin();
					Kraj kraj=em.find(Kraj.class, Integer.parseInt(id));		
					em.remove(kraj);
					em.getTransaction().commit();
					odgovor(response,"OK");
					return;
					
				}catch (Exception e) {
					odgovor(response,"Napaka");
					return;
				}
			}else if(sifrant.equals("izbrisiprogramInSmer")){
				String id=request.getParameter("id");
				System.out.println("izbiris id "+id);
				try{		
					em.getTransaction().begin();
					ProgramiInSmeri sp=em.find(ProgramiInSmeri.class, Integer.parseInt(id));		
					em.remove(sp);
					em.getTransaction().commit();
					odgovor(response,"OK");
					return;
					
				}catch (Exception e) {
					odgovor(response,"Napaka");
					return;
				}
			}else if(sifrant.equals("izbrisipredmet")){
				String id=request.getParameter("id");
				System.out.println("izbiris id "+id);
				try{		
					em.getTransaction().begin();
					Predmet pr=em.find(Predmet.class, Integer.parseInt(id));		
					em.remove(pr);
					em.getTransaction().commit();
					odgovor(response,"OK");
					return;
					
				}catch (Exception e) {
					odgovor(response,"Napaka");
					return;
				}
			}else if(sifrant.equals("izbrisipredavatelj")){
				String id=request.getParameter("id");
				System.out.println("izbiris id "+id);
				try{		
					em.getTransaction().begin();
					Osebje osebje=em.find(Osebje.class, Integer.parseInt(id));	
					System.out.println("ma1");
					Oseba oseba=osebje.getOseba();
					System.out.println("ma2");
					em.remove(oseba);
					em.remove(osebje);					
					em.getTransaction().commit();
					odgovor(response,"OK");
					return;
					
				}catch (Exception e) {
					System.out.println("napoaka "+e.getLocalizedMessage());
					odgovor(response,"Napaka");
					return;
				}
			}
			
		}else if(sifrant.startsWith("dodaj")){
			if(sifrant.equals("dodajobcina")){
				try{					
					String text1=request.getParameter("text1");
					String text2=request.getParameter("text2");
					String status=request.getParameter("status");					
					boolean bstat=status.equals("1");		
					
					DataValidator dv=new DataValidator();
					boolean kulText1=dv.validateInputText(text1, 3, false, "^[0-9]{1,3}$", "Prazna sifra", "Predolga sifra", "Neveljavna sifra");
					
					if(kulText1 && text2.length()>0){
						System.out.println("dodajObcina "+text1+" "+status);
						
						Obcina obcina=new Obcina();
						
						obcina.setSifra(text1);
						obcina.setNazivObcina(text2);
						obcina.setStatus(bstat);
						
						em.getTransaction().begin();
						em.persist(obcina);							
						em.flush();
						em.getTransaction().commit();
						
						System.out.println("ok persisted "+obcina.getIdObcina()+obcina.getNazivObcina());
						
						request.setAttribute("vrstica", new String[]{""+obcina.getIdObcina(),text1,text2,status});
						RequestDispatcher nov=request.getRequestDispatcher("sifrantiAjaxDodaj.jsp");
						nov.forward(request, response);
						return;
						
						
					}else{						
						odgovor(response,"Napaka. " +dv.getError());
					}
					
				}catch (Exception e) {
					odgovor(response,"Napaka");
				}
			}else if(sifrant.equals("dodajdrzava")){
				try{					
					String text1=request.getParameter("text1");
					String text2=request.getParameter("text2");
					String status=request.getParameter("status");					
					boolean bstat=status.equals("1");		
					
					DataValidator dv=new DataValidator();
					boolean kulText1=dv.validateInputText(text1, 3, false, "^[0-9]{1,3}$", "Prazna šifra", "Predolga šifra", "Neveljavna šifra");
					
					if(kulText1 && text2.length()>0){						
						Drzava drzava=new Drzava();	
						drzava.setSifra(text1);
						drzava.setNazivDrzava(text2);
						drzava.setStatus(bstat);
						
						em.getTransaction().begin();
						em.persist(drzava);							
						em.flush();
						em.getTransaction().commit();					
						
						
						request.setAttribute("vrstica", new String[]{""+drzava.getIdDrzava(),text1,text2,status});
						RequestDispatcher nov=request.getRequestDispatcher("sifrantiAjaxDodaj.jsp");
						nov.forward(request, response);
						return;						
					}else{						
						odgovor(response,"Napaka. " +dv.getError());
					}
					
				}catch (Exception e) {
					odgovor(response,"Napaka");
				}
			}else if(sifrant.equals("dodajposta")){
				try{					
					String text1=request.getParameter("text1");
					String text2=request.getParameter("text2");
					String status=request.getParameter("status");					
					boolean bstat=status.equals("1");		
					
					DataValidator dv=new DataValidator();
					boolean kulText1=dv.validateInputText(text1, 4, false, "^[0-9]{4}$", "Prazna številka", "Predolga številka", "Neveljavna številka");
					//boolean kulText2=dv.validateInputText(text2, 50, false, "^[a-zA-ZčćšđžČĆŠĐŽ]{1,50}$", "Prazno ime", "Predolgo ime", "Neveljavno ime");
					System.out.println("text1"+text1+ " tex2 "+text2);
					
					if(kulText1 && text2.length()>0){						
						Kraj kraj=new Kraj();						
						kraj.setPostnaSt(text1);
						kraj.setPosta(text2);
						kraj.setStatus(bstat);
						
						em.getTransaction().begin();
						em.persist(kraj);							
						em.flush();
						em.getTransaction().commit();					
						
						request.setAttribute("vrstica", new String[]{""+kraj.getIdKraj(),text1,text2,status});
						RequestDispatcher nov=request.getRequestDispatcher("sifrantiAjaxDodaj.jsp");
						nov.forward(request, response);
						return;						
					}else{						
						odgovor(response,"Napaka. " +dv.getError());
					}
					
				}catch (Exception e) {
					odgovor(response,"Napaka");
				}
			}else if(sifrant.equals("dodajprogramInSmer")){
				try{					
					String text1=request.getParameter("text1");
					String text2=request.getParameter("text2");
					String status=request.getParameter("status");					
					boolean bstat=status.equals("1");		
					
					DataValidator dv=new DataValidator();
					boolean kulText1=dv.validateInputText(text1, 4, false, "^[a-zA-ZčćšđžČĆŠĐŽ0-9]{4}$", "Prazna kratica", "Predolga kratica", "Neveljavna kratica");
					//boolean kulText2=dv.validateInputText(text2, 50, false, "^[a-zA-ZčćšđžČĆŠĐŽ]{1,50}$", "Prazno ime", "Predolgo ime", "Neveljavno ime");
					System.out.println("text1"+text1+ " tex2 "+text2);
					
					if(kulText1 && text2.length()>0){						
						ProgramiInSmeri program=new ProgramiInSmeri();					
						program.setSifra(text1);
						program.setNaziv(text2);
						program.setStatus(bstat);
						
						em.getTransaction().begin();
						em.persist(program);							
						em.flush();
						em.getTransaction().commit();					
						
						request.setAttribute("vrstica", new String[]{""+program.getIdProgramSmer(),text1,text2,status});
						RequestDispatcher nov=request.getRequestDispatcher("sifrantiAjaxDodaj.jsp");
						nov.forward(request, response);
						return;						
					}else{						
						odgovor(response,"Napaka. " +dv.getError());
					}
					
				}catch (Exception e) {
					odgovor(response,"Napaka");
				}
			}else if(sifrant.equals("dodajpredmet")){
				try{					
					String text1=request.getParameter("text1");
					String text2=request.getParameter("text2");
					String text3=request.getParameter("text3");
					String text4=request.getParameter("text4");
					String text5=request.getParameter("text5");
					String status=request.getParameter("status");					
					boolean bstat=status.equals("1");		
					
					DataValidator dv=new DataValidator();
					boolean kulText1=dv.validateInputText(text1, 50, false, "^[0-9]{5}$", "Prazna šifra", "Predolga šifra", "Neveljavna šifra");
					boolean kulText2=dv.validateInputText(text2, 50, false, "^[a-zA-ZčćšđžČĆŠĐŽ]{1,50}$", "Prazno kratica", "Predolgo kratica", "Neveljavno kratica");
					boolean kulText3=dv.validateInputText(text3, 2, false, "^[0-9]{1,2}$", "Prazne točke", "Predolge točke", "Neveljavno točke");
					System.out.println("text1"+text1+ " tex2 "+text2);
					
					if(kulText1 && kulText2 && kulText3 && text4.length()>0 && text5.length()>0){						
						Predmet predmet=new Predmet();			
						predmet.setSifraPredmeta(text1);
						predmet.setKraticaPredmet(text2);
						predmet.setKreditneTocke(BigDecimal.valueOf(Double.parseDouble(text3)));
						predmet.setNazivPredmet(text4);
						predmet.setNacinOcenjevanja(text5);
						predmet.setStatus(bstat);
						
						em.getTransaction().begin();
						em.persist(predmet);							
						em.flush();
						em.getTransaction().commit();					
						
						request.setAttribute("vrstica", new String[]{""+predmet.getIdPredmeta(),text1,text2,text3,text4,text5,status});
						RequestDispatcher nov=request.getRequestDispatcher("sifrantiAjaxDodaj.jsp");
						nov.forward(request, response);
						return;						
					}else{						
						odgovor(response,"Napaka. " +dv.getError());
					}
					
				}catch (Exception e) {
					odgovor(response,"Napaka");
				}
			}else if(sifrant.equals("dodajpredavatelj")){
				try{					
					String text1=request.getParameter("text1");
					String text2=request.getParameter("text2");
					String text3=request.getParameter("text3");
					String text4=request.getParameter("text4");
					String text5=request.getParameter("text5");
					String status=request.getParameter("status");					
					boolean bstat=status.equals("1");		
					
					DataValidator dv=new DataValidator();
					boolean kulText1=dv.validateInputText(text1, 7, false, "^[0-9]{6,7}$", "Prazna šifra", "Predolga šifra", "Neveljavna šifra");
					boolean kulText2=dv.validateInputText(text2, 50, false, "^[a-zA-ZčćšđžČĆŠĐŽü]{1,50}$", "Prazno ime", "Predolgo ime", "Neveljavno ime");
					boolean kulText3=dv.validateInputText(text3, 50, false, "^[a-zA-ZčćšđžČĆŠĐŽü]{1,50}$", "Prazen priimek", "Predolg priimek", "Neveljaven priimek");
					boolean kulText4=dv.validateInputText(text4, 50, false, "^[a-zA-ZčćšđžČĆŠĐŽ]{1,50}$", "Prazno up ime", "Predolgo up ime", "Neveljavno up ime");
					boolean kulText5=dv.validateInputText(text5, 50, false, "^[a-zA-ZčćšđžČĆŠĐŽ]{1,50}$", "Prazno geslo", "Predolgo geslo", "Neveljavno geslo");
					
					System.out.println("text1"+text1+ " tex2 "+text2);
					
					if(kulText1 && kulText2 && kulText3 && kulText4 && kulText5){						
						Oseba oseba=new Oseba();						
						
						oseba.setIme(text2);
						oseba.setPriimek(text3);						
						oseba.setUpIme(text4);
						oseba.setGeslo(text5);						
						oseba.setStatus(bstat);
						//ostale na prazno
						oseba.setDavcnaSt("");
						oseba.setEmso("");						
						//---
						Osebje osebje=new Osebje();
						osebje.setOseba(oseba);
						osebje.setSifraPredavatelja(text1);
						
						em.getTransaction().begin();
						em.persist(oseba);	
						em.persist(osebje);	
						em.flush();
						em.getTransaction().commit();
						
							
						
						request.setAttribute("vrstica", new String[]{""+osebje.getIdOsebje(),text1,text2,text3,text4,text5,status});
						RequestDispatcher nov=request.getRequestDispatcher("sifrantiAjaxDodaj.jsp");
						nov.forward(request, response);
						return;						
					}else{						
						odgovor(response,"Napaka. " +dv.getError());
					}
					
				}catch (Exception e) {
					odgovor(response,"Napaka");
				}
			}
			
			
		}else{//ta del ureja zaceten izpis podatkov
			if(sifrant.equals("obcina")){
				String sql="SELECT x FROM Obcina x";
				Query q=em.createQuery(sql);
				List<Obcina> obcine=q.getResultList();
				
				String[] naslovna={"Šifra","Ime občine"};
				int st=obcine.size();
				String[][] tab=new String[st][4];
				for(int i=0;i<st;i++){
					Obcina obcina=obcine.get(i);
					tab[i][0]=""+obcina.getIdObcina();
					tab[i][1]=obcina.getSifra();
					tab[i][2]=obcina.getNazivObcina();
					int status=0;
					if(obcina.isStatus()){
						status=1;
					}
					tab[i][3]=""+status;
				}
				
				request.setAttribute("sifrant", "obcina");
				request.setAttribute("sifrantIme", "Šifranti občin");
				request.setAttribute("naslovna", naslovna);
				request.setAttribute("vsebina", tab);
				RequestDispatcher nov=request.getRequestDispatcher("sifrantiAjax.jsp");
				nov.forward(request, response);
				System.out.println("poslano");
				return;
				
			}else if(sifrant.equals("drzava")){
				String sql="SELECT x FROM Drzava x";
				Query q=em.createQuery(sql);
				List<Drzava> drzave=q.getResultList();
				
				String[] naslovna={"Šifra","Ime države"};
				int st=drzave.size();
				String[][] tab=new String[st][4];
				for(int i=0;i<st;i++){
					Drzava drzava=drzave.get(i);
					tab[i][0]=""+drzava.getIdDrzava();
					tab[i][1]=drzava.getSifra();
					tab[i][2]=drzava.getNazivDrzava();
					int status=0;
					if(drzava.isStatus()){
						status=1;
					}
					tab[i][3]=""+status;
				}
				
				request.setAttribute("sifrant", "drzava");
				request.setAttribute("sifrantIme", "Šifranti držav");
				request.setAttribute("naslovna", naslovna);
				request.setAttribute("vsebina", tab);
				RequestDispatcher nov=request.getRequestDispatcher("sifrantiAjax.jsp");
				nov.forward(request, response);
				System.out.println("poslano");
				return;
			}else if(sifrant.equals("posta")){
				String sql="SELECT x FROM Kraj x";
				Query q=em.createQuery(sql);
				List<Kraj> poste=q.getResultList();
				
				String[] naslovna={"Poštna številka","Kraj"};
				int st=poste.size();
				String[][] tab=new String[st][4];
				for(int i=0;i<st;i++){
					Kraj posta=poste.get(i);
					tab[i][0]=""+posta.getIdKraj();
					tab[i][1]=posta.getPostnaSt();
					tab[i][2]=posta.getPosta();
					int status=0;
					if(posta.isStatus()){
						status=1;
					}
					tab[i][3]=""+status;
				}
				
				request.setAttribute("sifrant", "posta");
				request.setAttribute("sifrantIme", "Šifranti pošt");
				request.setAttribute("naslovna", naslovna);
				request.setAttribute("vsebina", tab);
				RequestDispatcher nov=request.getRequestDispatcher("sifrantiAjax.jsp");
				nov.forward(request, response);
				System.out.println("poslano");
				return;
			}else if(sifrant.equals("programInSmer")){
				String sql="SELECT x FROM ProgramiInSmeri x";
				Query q=em.createQuery(sql);
				List<ProgramiInSmeri> programi=q.getResultList();
				
				String[] naslovna={"Kratica programa","Ime programa"};
				int st=programi.size();
				String[][] tab=new String[st][4];
				for(int i=0;i<st;i++){
					ProgramiInSmeri program=programi.get(i);
					tab[i][0]=""+program.getIdProgramSmer();
					tab[i][1]=program.getSifra();
					tab[i][2]=program.getNaziv();
					int status=0;
					if(program.isStatus()){
						status=1;
					}
					tab[i][3]=""+status;
				}
				
				request.setAttribute("sifrant", "programInSmer");
				request.setAttribute("sifrantIme", "Šifranti programov");
				request.setAttribute("naslovna", naslovna);
				request.setAttribute("vsebina", tab);
				RequestDispatcher nov=request.getRequestDispatcher("sifrantiAjax.jsp");
				nov.forward(request, response);
				System.out.println("poslano");
				return;
			}else if(sifrant.equals("smer")){
				String sql="SELECT x FROM Studijska_smer x";
				Query q=em.createQuery(sql);
				List<Studijska_smer> smeri=q.getResultList();
				
				String[] naslovna={"Kratica smeri","Ime smeri"};
				int st=smeri.size();
				String[][] tab=new String[st][4];
				for(int i=0;i<st;i++){
					Studijska_smer smer=smeri.get(i);
					tab[i][0]=""+smer.getIdStudijskaSmer();
					tab[i][1]=smer.getKraticaStudijskaSmer();
					tab[i][2]=smer.getNazivStudijskaSmer();
					int status=0;
					if(smer.isStatus()){
						status=1;
					}
					tab[i][3]=""+status;
				}
				
				request.setAttribute("sifrant", "smer");
				request.setAttribute("sifrantIme", "Šifranti smeri");
				request.setAttribute("naslovna", naslovna);
				request.setAttribute("vsebina", tab);
				RequestDispatcher nov=request.getRequestDispatcher("sifrantiAjax.jsp");
				nov.forward(request, response);
				System.out.println("poslano");
				return;
			}else if(sifrant.equals("predmet")){
				String sql="SELECT x FROM Predmet x";
				Query q=em.createQuery(sql);
				List<Predmet> predmeti=q.getResultList();
				
				String[] naslovna={"Šifra predmeta","Kratica predmeta","Kreditne točke","Ime predmeta","Ocenjevanje"};
				int st=predmeti.size();
				String[][] tab=new String[st][7];
				for(int i=0;i<st;i++){
					Predmet predmet=predmeti.get(i);
					tab[i][0]=""+predmet.getIdPredmeta();
					tab[i][1]=predmet.getSifraPredmeta();
					tab[i][2]=predmet.getKraticaPredmet();
					tab[i][3]=""+predmet.getKreditneTocke();
					tab[i][4]=predmet.getNazivPredmet();
					tab[i][5]=predmet.getNacinOcenjevanja();
					
					int status=0;
					if(predmet.isStatus()){
						status=1;
					}
					tab[i][6]=""+status;
				}
				
				request.setAttribute("sifrant", "predmet");
				request.setAttribute("sifrantIme", "Šifranti predmetov");
				request.setAttribute("naslovna", naslovna);
				request.setAttribute("vsebina", tab);
				RequestDispatcher nov=request.getRequestDispatcher("sifrantiAjax.jsp");
				nov.forward(request, response);
				System.out.println("poslano");
				return;
			}else if(sifrant.equals("predavatelj")){
				String sql="SELECT osj FROM Osebje osj";		
				
				Query q=em.createQuery(sql);
				List<Osebje> predavatelji=q.getResultList();
				
//				String[] naslovna={"Šifra","Ime","Priimek","Emso"
//						,"Up ime","Davčna št","Telefon","Email","Mobitel","Spol"};
				String[] naslovna={"Šifra","Ime","Priimek"
						,"Uporabniško ime","Geslo"};
				int st=predavatelji.size();
				String[][] tab=new String[st][7];
				for(int i=0;i<st;i++){
					Osebje predavatelj=predavatelji.get(i);
					tab[i][0]=""+predavatelj.getIdOsebje();
					tab[i][1]=predavatelj.getSifraPredavatelja();
					Oseba os=predavatelj.getOseba();
					tab[i][2]=os.getIme();
					tab[i][3]=os.getPriimek();
					//tab[i][4]=os.getEmso();
					tab[i][4]=os.getUpIme();
					tab[i][5]=os.getGeslo();
					//tab[i][6]=os.getDavcnaSt();
					//tab[i][4]=os.getTelefon();
					//tab[i][5]=os.getEmail();
					//tab[i][9]=os.getMobitel();
					//tab[i][10]=os.getSpol();					
					
					int status=0;
					if(os.isStatus()){
						status=1;
					}
					tab[i][6]=""+status;
				}
				
				request.setAttribute("sifrant", "predavatelj");
				request.setAttribute("sifrantIme", "Šifranti predavateljev");
				request.setAttribute("naslovna", naslovna);
				request.setAttribute("vsebina", tab);
				RequestDispatcher nov=request.getRequestDispatcher("sifrantiAjax.jsp");
				nov.forward(request, response);
				System.out.println("poslano");
				return;
			}
		}
	}
	
	public void odgovor(HttpServletResponse response,String izpis) throws IOException{
		response.setContentType("text/html; charset=utf-8");  
		PrintWriter out = response.getWriter();
		out.print(izpis);
	}

}
