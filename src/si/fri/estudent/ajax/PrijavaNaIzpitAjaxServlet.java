package si.fri.estudent.ajax;

import java.io.IOException;
import si.fri.estudent.utilities.*;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.oxm.sequenced.Setting;

import net.sf.json.JSONObject;

import si.fri.estudent.jpa.Izpitni_rok;
import si.fri.estudent.jpa.Kombinacije_izvajalcev;
import si.fri.estudent.jpa.Letnik;
import si.fri.estudent.jpa.Ocena;
import si.fri.estudent.jpa.Oseba;
import si.fri.estudent.jpa.Predmet;
import si.fri.estudent.jpa.Predmet_izvajalec;
import si.fri.estudent.jpa.Predmetnik;
import si.fri.estudent.jpa.Student;
import si.fri.estudent.jpa.Studij;
import si.fri.estudent.jpa.Vloga_oseba;
import si.fri.estudent.jpa.Vpisni_list;
import si.fri.estudent.utilities.*;

/**
 * Servlet implementation class PrijavaNaIzpitAjax
 */
@WebServlet("/PrijavaNaIzpitAjaxServlet")
public class PrijavaNaIzpitAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private EntityManager em;
	private boolean prijavaStudent = true;
	private Oseba oseba;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PrijavaNaIzpitAjaxServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		HttpSession session = request.getSession();
		
		if(session.getAttribute("studentIzIskanja") != null){
			Student st = (Student) session.getAttribute("studentIzIskanja");
			oseba = st.getOseba();
		}else{
			oseba = (Oseba) session.getAttribute("uporabnik");
		}
		
		Vloga_oseba vloga = (Vloga_oseba)session.getAttribute("vloga");
		if(vloga.getIdVlogaOseba() == Constants.VLOGA_STUDENT)
			prijavaStudent = true;
		else 
			prijavaStudent = false;
		
		
		pokaziStudijskePrograme(request, response, session);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		
		if(session.getAttribute("studentIzIskanja") != null){			
			Student student=(Student) session.getAttribute("studentIzIskanja");
			oseba = student.getOseba();
			prijavaStudent = false;
			
		}else{
			oseba = (Oseba) session.getAttribute("uporabnik");
			prijavaStudent = true;
		}
		
		Vloga_oseba vloga = (Vloga_oseba) session.getAttribute("vloga");
		JSONObject json = new JSONObject();
		
		if(request.getParameter("prijava") != null)
			json = obdelajPrijavo(request, response);
		else if(request.getParameter("idVpisni") != null)
			json = returnPredmeti(request, response);
		else if(request.getParameter("pi") != null)
			json = poIzbiriPredmetaAjax(request,response);
		else if(request.getParameter("idRoka") != null)
			json = poIzbiriRokaAjax(request, response, session);
		else
			pokaziStudijskePrograme(request, response, session);
		
		response.setContentType("text/html; charset=utf-8");  
		PrintWriter out = response.getWriter();
		out.print(json);
	}
	
	private void pokaziStudijskePrograme(HttpServletRequest request,  HttpServletResponse response, HttpSession session) throws ServletException, IOException{
		request.setCharacterEncoding("UTF-8");
		
		String url = "index.jsp?stran=prijavaIzpitStudent";
		
		EntityManager em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		
		String dodatno = "";

		Vloga_oseba vloga = (Vloga_oseba) session.getAttribute("vloga");
		
		if(vloga.getIdVlogaOseba() == Constants.VLOGA_STUDENT)
			dodatno += " AND vl.studijskoLeto = '" + Utilities.getStudijskoLeto() + "' ";
		
		String sql = "SELECT "+
						"DISTINCT vl "+
					"FROM "+
						"Student s, Vpisni_list vl "+
					"WHERE "+
						"vl.student = s AND " +
						"s.oseba.idOseba = :oseba " + dodatno +
					"ORDER BY " +
						"vl.letnik.studijskoLeto, vl.letnik.idLetnik";
		
		Query query = em.createQuery(sql);
		query.setParameter("oseba", oseba.getIdOseba());
		
		List<Vpisni_list> leta = new LinkedList<Vpisni_list>();
		
		leta = query.getResultList();
		
		request.setAttribute("leta", leta);
		
		RequestDispatcher dis = request.getRequestDispatcher(url);
		dis.forward(request, response);
	}
	
	private JSONObject returnPredmeti(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		request.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession();
		
		String sifra = (String) request.getParameter("idVpisni");
		
		EntityManager em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		
		String dodatno = "";
		
		String sql = "SELECT "+
						"DISTINCT pi "+
					"FROM "+
						"Oseba o, Student s, Vpisni_list vl,  Predmetnik pr, Predmetnik_letnika prl, Predmet p, Letnik l, " +
						"Predmet_izvajalec pi, " +
						"Ocena oc  "+
					"WHERE "+
						"pr.fkVpisniList = vl.idVpisni AND " +
						"prl.idPredmetnikLetnika = pr.fkPredmetnikLetnika AND " +
						"p.idPredmeta = prl.predmet.idPredmeta AND " +
						"vl.letnik.idLetnik = l.idLetnik AND " +
						"p = pi.predmet AND " +
						"pi.status = 1 AND " +
						"pi.kombinacijeIzvajalcev.status = 1 AND " +
						"vl.idVpisni = :vpisni " + dodatno +
					"ORDER BY " +
						"p.nazivPredmet";

		Query query = em.createQuery(sql);
		query.setParameter("vpisni", Integer.parseInt(sifra));
		
		List<Predmet_izvajalec> predmeti = query.getResultList();
		predmeti = filterPredmeti(predmeti);
		
		JSONObject json = new JSONObject();
		
		List<String> imenaPredmetov = new ArrayList<String>();
		List<String> sifre = new ArrayList<String>();
		List<String> izvajalce = new ArrayList<String>();
		List<String> pi = new ArrayList<String>();
		
		for(Predmet_izvajalec p : predmeti){
			imenaPredmetov.add(p.getPredmet().getNazivPredmet());
			sifre.add(p.getPredmet().getSifraPredmeta());
			
			String izvajalec = "";
			Kombinacije_izvajalcev ki = p.getKombinacijeIzvajalcev();
			
			izvajalec += ki.getOsebje1().getOseba().getPriimek();
			
			if(ki.getOsebje2() != null)
				izvajalec += ", " + ki.getOsebje2().getOseba().getPriimek();
			
			if(ki.getOsebje3() != null)
				izvajalec += ", " + ki.getOsebje3().getOseba().getPriimek();
			
			izvajalce.add(izvajalec);
			pi.add(Integer.toString(p.getIdPredmetIzvajalca()));
		}
		
		json.accumulate("predmeti", imenaPredmetov);
		json.accumulate("sifre", sifre);
		json.put("idStudij", sifra);
		json.accumulate("izvajalci", izvajalce);
		json.accumulate("pi", pi);
		
		return json;
	}
	
	private List<Predmet_izvajalec> filterPredmeti(List<Predmet_izvajalec> predmeti){
		List<Predmet_izvajalec> predmet = new LinkedList<Predmet_izvajalec>();
		
		for(Predmet_izvajalec p : predmeti){
					
			try{
				
			
				String sql = "SELECT DISTINCT oc " +
						 	"FROM Student s, Vpisni_list vl, Predmetnik pr, Predmetnik_letnika prl, Ocena oc, Izpitni_rok ir, Predmet p " +
						 	"WHERE " +
						 		"s.oseba.idOseba = ?1 AND " +
						 		"vl.student.idStudent = s.idStudent AND " +
						 		"pr.fkVpisniList = vl.idVpisni AND " +
						 		"prl.idPredmetnikLetnika = pr.fkPredmetnikLetnika AND " +
						 		"prl.predmet.idPredmeta = p.idPredmeta AND " +
						 		"p.idPredmeta = ir.predmet.idPredmeta AND " +
						 		"oc.fkIzpitniRok = ir.idRok AND " +
						 		"p.idPredmeta = ?2 " +
						 		" AND oc.aktivna = 1 AND " +
						 		"( oc.ocenaKoncna > 5 OR " +
						 		"oc.ocenaVaje > 5 OR " +
						 		"oc.ocenaIzpit > 5 )";
			  
			
				Query q = em.createQuery(sql);
				
				q.setParameter(1, oseba.getIdOseba());
				q.setParameter(2, p.getPredmet().getIdPredmeta());
				
				List<Ocena> ocene = (List<Ocena>) q.getResultList();
				
				if(ocene.size() == 0 || !prijavaStudent)
					predmet.add(p);
			
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		return predmet;
	}
	
	private JSONObject poIzbiriPredmetaAjax(HttpServletRequest request, HttpServletResponse response){

		String sifra = request.getParameter("pi");
		String idVpisni = request.getParameter("idVpisni2");
		
		Vpisni_list vl = this.em.find(Vpisni_list.class, Integer.parseInt(idVpisni));
		int idStudij = vl.getLetnik().getStudij().getIdStudija();
		
		String sql = "SELECT "+
						"DISTINCT ir " +						
					"FROM "+
						"Predmet p, Oseba o, Predmet_izvajalec pi, Osebje os, Kombinacije_izvajalcev ki, Izpitni_rok ir, Ocena oc," +
						"Letnik l, Predmetnik_letnika prl "+
					"WHERE " +
						"ir.predmetIzvajalec.idPredmetIzvajalca = :pi " +
					"ORDER BY " +
						"ir.datumIzpit";
		
		Query query = em.createQuery(sql);
		query.setParameter("pi", Integer.parseInt(sifra));

		List<Izpitni_rok> roki = query.getResultList();
		
		List<String> datumi = new ArrayList<String>();
		List<String> izvajalci = new ArrayList<String>();
		List<String> idRoki = new ArrayList<String>();
		
		for(Izpitni_rok ir : roki){
						
			Predmet pr = ir.getPredmet();
			String d = Utilities.getDatumIzpita(ir.getDatumIzpit());
			
			Ocena ocena = obstajaPrijavaZaTaRok(pr, ir);
			
			int oc = -1;
			if(ocena != null)
				oc = ocena.getIdOcena();
			
			if(oc == 0)
				d += "  * že prijavljen";
			
			datumi.add(d);
			idRoki.add(Integer.toString(ir.getIdRok()));
		}
		
		JSONObject json = new JSONObject();

		json.accumulate("datumi", datumi);
		json.accumulate("idRoki", idRoki);
		
		return json;
	}
	
	private JSONObject poIzbiriRokaAjax(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		
 		String sifra = request.getParameter("idRoka");
 		int idVpisni = Integer.parseInt(request.getParameter("idVpisni2"));
 		
 		Vpisni_list vpisniList = em.find(Vpisni_list.class, idVpisni);
 		
		String leto = vpisniList.getStudijskoLeto();
		
		int idRoka = 0;
		if(sifra != null)
			idRoka = Integer.parseInt(sifra);
		
		String sql = "SELECT "+
						"DISTINCT ir," +
						"p " +
					"FROM "+
						"Predmet p, Izpitni_rok ir " +
					"WHERE " +
						"ir.predmet.idPredmeta = p.idPredmeta AND " +
						"ir.idRok = ?1 AND ir.status = 1 AND p.status = 1 " +
					"ORDER BY " +
						" ir.datumIzpit ";
		
		Query query = em.createQuery(sql);
		query.setParameter(1, idRoka);
		
		Object[] roki = (Object[]) query.getSingleResult();
		
		String datum = "";
		String izvajalecId = "";
		String idRok = "";
		String predavalnica = "";
		String predmet = "";
		String napaka = "";
		String napaka2 = "";
		String prijava = "";
		String placljivo = "";
		int polaganjaPonavljanje = 0;
		
		Izpitni_rok ir = null;
		Predmet pr = null;
		
		List<String> izvajalci = new ArrayList<String>();
		
		ir = (Izpitni_rok) roki[0];
		pr = (Predmet) roki[1];
		
		Kombinacije_izvajalcev ki = ir.getPredmetIzvajalec().getKombinacijeIzvajalcev();
		
		Oseba o = ki.getOsebje1().getOseba();
		
		izvajalci.add(o.getIme() + " " + o.getPriimek());
		
		if(ki.getOsebje2() != null){
			o = ki.getOsebje2().getOseba();
			izvajalci.add(o.getIme() + " " + o.getPriimek());
		}
		
		if(ki.getOsebje3() != null){
			o = ki.getOsebje3().getOseba();
			izvajalci.add(o.getIme() + " " + o.getPriimek());
		}
		
		/*
		 * PREVERJANJA
		 */
		
		List<Ocena> prijave = vrniPrijaveZaPredmet(pr);
		
		/*
		 * Preveri če že obstaja prijava za ta predmet
		 */
		boolean manjKotDvaTednaMedRokoma = false;
		boolean prijavljenNaPredmet = false;
		boolean prijavljenNaRok = false;
		boolean imaAktivnoPozOceno = false;
		boolean imaAktivnoNegOceno = false;
		boolean imaOcenoZaRok = false;
		Ocena zadnjePolaganje = null;
		Ocena aktivnaPrijavaRok = null;
		
		for(Ocena prij : prijave){
			
			Predmetnik pred = em.find(Predmetnik.class, prij.getFkPredmetnik());
			Vpisni_list v = em.find(Vpisni_list.class, pred.getFkVpisniList());
			
			if(v.getVrstaVpisa().getIdVrstaVpisa() == Constants.VPIS_PONAVLJANJE && prij.getStPolaganja() > 0){
				polaganjaPonavljanje += 1;
			}
			
			if(prij.getAktivna() == true && (prij.getOcenaIzpit() == 0 & prij.getOcenaKoncna() == 0 && prij.getOcenaVaje() == 0 && prij.getOcena() == 0))
				prijavljenNaPredmet = true;
			
			if(prij.getAktivna() == true && (prij.getOcenaIzpit() == 0 & prij.getOcenaKoncna() == 0 && prij.getOcenaVaje() == 0 && prij.getOcena() == 0) && prij.getFkIzpitniRok() == ir.getIdRok()){
				prijavljenNaRok = true;
				aktivnaPrijavaRok = prij;
			}
			
			if(prij.getOcenaIzpit() > 5 || prij.getOcenaKoncna() > 5 || prij.getOcenaVaje() > 5)
				imaAktivnoPozOceno = true;
			
			if(prij.getAktivna() == true && (prij.getOcenaIzpit() > 5 || prij.getOcenaKoncna() > 5 || prij.getOcenaVaje() > 5) && prij.getFkIzpitniRok() == ir.getIdRok())
				imaOcenoZaRok = true;
			
			if(prij.getAktivna() == false && prij.getOcena() == 0 || prij.getFkIzpitniRok() == ir.getIdRok())
				continue;
			
			Izpitni_rok irTemp = em.find(Izpitni_rok.class, prij.getFkIzpitniRok());
			long daysBetween = (ir.getDatumIzpit().getTime() - irTemp.getDatumIzpit().getTime())/(1000 * 60 * 60 * 24);
			
			if(daysBetween < 14)
				manjKotDvaTednaMedRokoma = true;
		}
		
		int prijavaRok = -1;
		
		if(aktivnaPrijavaRok != null)
			prijavaRok = aktivnaPrijavaRok.getIdOcena();
		
		/*
		 * Preveri števila opravljanj
		 */
		int maxOpravljanjeLetos = opravljanjaLetos(pr, leto);
		int maxOpravljanjSkupno = opravljanjaLetos(pr, "");
		boolean presegelSkupno = false;
		
		if(maxOpravljanjSkupno == Constants.MAX_ST_OPRAVLJANJ_SKUPNO){
			presegelSkupno = true;
			placljivo = "Presegli ste skupno število brezplačnih opravljanj.";
		}
		
		boolean presegelLetno = maxOpravljanjSkupno >= (Constants.MAX_ST_OPRAVLJANJ_LETO - 1) ? true : false;
		
		if(vpisniList.getNacinStudija().equals(Constants.NACIN_IZREDNO))
				placljivo = "Nimate statusa.";
		
						
		if(maxOpravljanjSkupno >= Constants.MAX_ST_OPRAVLJANJ_LETO){
				placljivo = "Presegli ste število brezplačnih opravljanj.";
		}
		
		/*
		 * Preveri ali je še dovolj časa za prijavo
		 */
		boolean e = IzpitValidator.moznaPrijavaNaIzpit(ir.getDatumIzpit());
		
		/*
		 * Pregledamo rezultate preverjanj
		 */
		if(presegelSkupno)
			napaka2 = "Presegli ste največje možno skupno število polaganj";
		else if(presegelLetno)
			napaka2 = "Presegli ste največje možno število polaganj za letošnje leto.";
		else if(imaAktivnoPozOceno)
			napaka2 = "Za ta predmet imate aktivno pozitivno oceno.";
		else if(imaOcenoZaRok)
			napaka2 = "Za ta rok imate oceno.";
		else if(prijavljenNaRok)
			napaka = "Za ta rok že obstaja prijava.";
		else if(prijavljenNaPredmet)
			napaka2 = "Za ta predmet že obstaja aktivna prijava.";
		else if(e)
			napaka2 = "Rok za prijavo potekel";
		else if(e && prijavljenNaRok)
			napaka2 = "Rok za odjavo potekel";
		else if(manjKotDvaTednaMedRokoma)
			napaka2 = "Izpitna roka morata biti vsaj 14 dni narazen.";
		
		if(!prijavljenNaRok && !imaOcenoZaRok) 
			prijava = "";
		else 
			prijava = "ze prijavljen";
		
		datum = Utilities.getDatumIzpita(ir.getDatumIzpit());
		
		predavalnica = ir.getPredavalnica();
		idRok = Integer.toString(ir.getIdRok());
		predmet = pr.getNazivPredmet();
			
		JSONObject json = new JSONObject();
		
		json.put("datum", datum);
		json.put("izvajalecId", izvajalecId);
		json.accumulate("izvajalci", izvajalci);
		json.put("predavalnica", predavalnica);
		json.put("idRok", idRok);
		json.put("predmet", predmet);
		json.put("predmetId", pr.getIdPredmeta());
		json.put("napaka", napaka);
		json.put("prijava", prijava);
		json.put("napaka2", napaka2);
		json.put("placljivo", placljivo);
		json.put("polaganjaLetos", maxOpravljanjeLetos);
		json.put("polaganjaSkupno", maxOpravljanjSkupno);
		json.put("polaganjaPonavljanje", polaganjaPonavljanje);
		
		Vloga_oseba vloga = (Vloga_oseba) session.getAttribute("vloga");
		
		if(vloga.getIdVlogaOseba() == Constants.VLOGA_STUDENT)
			json.put("prijavlja", "student");
		else
			json.put("prijavlja", "referentka");
		
		return json;
	}
	
	private List<Ocena> vrniPrijaveZaPredmet(Predmet pr){
		
		List<Ocena> ocene = new LinkedList<Ocena>();
		
		//pogledamo če obstajajo prijave za ta predmet, ki nimajo ocene
		
		try{
			
			String sql = "SELECT "+
					"DISTINCT oc " +
				"FROM "+
					"Oseba o, Student s, Vpisni_list vl, Predmetnik pr, Predmetnik_letnika prl, Ocena oc, Predmet p, Izpitni_rok ir "+
				"WHERE " +
					"s.oseba.idOseba = ?1 AND " +
					"s = vl.student AND " +
					"vl.idVpisni = pr.fkVpisniList AND " +
					"pr.idPredmetnik = oc.fkPredmetnik AND " +
					"oc.fkIzpitniRok = ir.idRok AND " +
					"ir.predmet = p AND " +
					"p.idPredmeta = ?2 AND oc.aktivna = 1 ";
					
			Query query = em.createQuery(sql); 

			query.setParameter(1, oseba.getIdOseba());
			query.setParameter(2, pr.getIdPredmeta());
			
			ocene = (List<Ocena>) query.getResultList();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return ocene;
	}
	
	private int opravljanjaLetos(Predmet p, String leto){
		
		String sql = "SELECT "+
				"DISTINCT oc " +
			"FROM "+
				"Oseba o, Student s, Vpisni_list vl, Predmetnik pr, Predmet p, Predmetnik_letnika prl, Ocena oc "+
			"WHERE " +
				"s.oseba.idOseba = ?1 AND " +
				"s.idStudent = vl.student.idStudent AND " +
				"vl.idVpisni = pr.fkVpisniList AND " +
				"pr.fkPredmetnikLetnika = prl.idPredmetnikLetnika AND " +
				"p.idPredmeta = prl.predmet.idPredmeta AND " +
				"p.idPredmeta = ?2 AND " +
				"pr.idPredmetnik = oc.fkPredmetnik AND " +
				"oc.aktivna = 1 ";
		
		if(!leto.isEmpty())
				sql += " AND vl.studijskoLeto = ?3";
		
		Query query = em.createQuery(sql); 
		
		query.setParameter(1, oseba.getIdOseba());
		query.setParameter(2, p.getIdPredmeta());
		
		if(!leto.isEmpty())
			query.setParameter(3, leto);
		
		List<Ocena>ocene = (List<Ocena>) query.getResultList();
		
		Predmetnik predmetnik = null;
		int max = 0;
		
		for(Ocena o : ocene){
			if(o.getStPolaganja() > max && leto.isEmpty())
					max = o.getStPolaganja();
			
			if(o.getStPolaganjaLetos() > max && !leto.isEmpty())
					max = o.getStPolaganjaLetos();
		}
		
		return max;
	}

	private Ocena obstajaPrijavaZaTaRok(Predmet pr, Izpitni_rok ir){
		
		String sql = "SELECT "+
				"DISTINCT oc " +
			"FROM "+
				"Oseba o, Student s, Vpisni_list vl, Predmetnik pr, Izpitni_rok ir, Ocena oc, Predmet p "+
			"WHERE " +
				"s.oseba.idOseba = ?1 AND " +
				"s = vl.student AND " +
				"vl.idVpisni = pr.fkVpisniList AND " +
				"pr.idPredmetnik = oc.fkPredmetnik AND " +
				"oc.fkIzpitniRok = ir.idRok AND " +
				"ir.predmet = p AND " +
				"p.idPredmeta = ?2 AND " +
				"ir.idRok = ?3 ";

		Query query = em.createQuery(sql); 

		query.setParameter(1, oseba.getIdOseba());
		query.setParameter(2, pr.getIdPredmeta());
		query.setParameter(3, ir.getIdRok());
		
		List<Ocena> ocena = (List<Ocena>) query.getResultList();
		
		if(ocena.size() < 1){
			return null;
		}
		
		return ocena.get(0);
	}
	
	private JSONObject obdelajPrijavo(HttpServletRequest request, HttpServletResponse response){
		
		JSONObject json = new JSONObject();
		
		String uspelo = "true";
		String akcija = "";
		
		String prijavi = request.getParameter("prijava");
		int idRok = Integer.parseInt(request.getParameter("idRoka"));
		int idVpisni = Integer.parseInt(request.getParameter("idVpisni3"));
		int idPredmeta = Integer.parseInt(request.getParameter("idPredmeta"));
		
		em.getTransaction().begin();
		
		Izpitni_rok ir = (Izpitni_rok) em.find(Izpitni_rok.class, idRok);
		
		int idOcena = -1;
		
		String sql = "SELECT distinct pr " +
						"FROM Predmetnik pr, Oseba o, Student s, Vpisni_list vl, Predmet p, Predmetnik_letnika prl " +
					 "WHERE " +
					 	"o.idOseba = s.oseba.idOseba AND " +
					 	"vl.student.idStudent = s.idStudent AND " +
					 	"vl.idVpisni = pr.fkVpisniList AND " +
					 	"prl.idPredmetnikLetnika = pr.fkPredmetnikLetnika AND " +
						"p.idPredmeta = prl.predmet.idPredmeta AND " +
					 	"p.idPredmeta = ?1 AND o.idOseba = ?2 AND " +
					 	"vl.idVpisni = ?3";
		
		Query query = em.createQuery(sql);
		query.setParameter(1, idPredmeta);
		query.setParameter(2, oseba.getIdOseba());
		query.setParameter(3, idVpisni);
		
		Predmetnik predmetnik = (Predmetnik) query.getSingleResult();
		Predmet predmet = em.find(Predmet.class, idPredmeta);

		Ocena ocena = this.obstajaPrijavaZaTaRok(predmet, ir);
		boolean exists = true;
		
		
		try{
			
			if(ocena == null){
				ocena = new Ocena();
				exists = false;
			}
			
			if(prijavi.equals("true")){
				ocena.setFkIzpitniRok(ir.getIdRok());
				ocena.setFkPredmetnikLetnika(predmetnik.getIdPredmetnik());
				ocena.setAktivna(true);
				
				if(exists)
					em.merge(ocena);
				else
					em.persist(ocena);
				
				akcija = "prijava";
			}else{
				ocena.setAktivna(false);
				em.merge(ocena);
				akcija = "odjava";
			}
			
			em.getTransaction().commit();
			
		}catch(Exception e){
			em.getTransaction().rollback();
			e.printStackTrace();
			uspelo = "false";
			
		}
		
		json.put("uspelo", uspelo);
		json.put("akcija", akcija);
		json.put("ocenaId", idOcena);
		
		return json;
	}
}