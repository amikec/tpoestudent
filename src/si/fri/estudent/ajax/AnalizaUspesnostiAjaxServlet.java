package si.fri.estudent.ajax;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.io.IOException;
import si.fri.estudent.utilities.*;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.oxm.sequenced.Setting;

import net.sf.json.JSONObject;

import si.fri.estudent.jpa.Izpitni_rok;
import si.fri.estudent.jpa.Kombinacije_izvajalcev;
import si.fri.estudent.jpa.Letnik;
import si.fri.estudent.jpa.Ocena;
import si.fri.estudent.jpa.Oseba;
import si.fri.estudent.jpa.Predmet;
import si.fri.estudent.jpa.Predmetnik;
import si.fri.estudent.jpa.Student;
import si.fri.estudent.jpa.Studij;
import si.fri.estudent.jpa.Vloga_oseba;
import si.fri.estudent.jpa.Vpisni_list;

/**
 * Servlet implementation class AnalizaUspesnostiAjaxServlet
 */
@WebServlet("/AnalizaUspesnostiAjaxServlet")
public class AnalizaUspesnostiAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private EntityManager em;
	private boolean prijavaStudent = true;
	private Oseba oseba;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AnalizaUspesnostiAjaxServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	private EntityManager getEntityManager(){
		return Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		HttpSession session = request.getSession();
		preusmeriNaServlet(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//String studijskoLeto = request.getParameter("studLeto");�		
		int predmet = -1;
		String studijskoLeto = null;
		String od = null;
		String dox = null;

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		request.setCharacterEncoding("UTF-8");

		em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		
		HttpSession session = request.getSession();
		oseba = (Oseba) session.getAttribute("uporabnik");
		
		if (request.getParameter("predmet") != null) {
			predmet = Integer.parseInt(request.getParameter("predmet"));
		}
		if (request.getParameter("studLeto") != null) {
			studijskoLeto = request.getParameter("studLeto");	
		}
		if (request.getParameter("od") != null) {
			od = request.getParameter("od");

			//System.out.println(od);
		}
		if (request.getParameter("dox") != null) {
			dox = request.getParameter("dox");	

		}
		
		EntityManager em = null;
		String url = "";
		List<double[]> podatki1 = new LinkedList<double[]>();
		List<double[]> podatki2 = new LinkedList<double[]>();
		List<Kombinacije_izvajalcev> kombinacije = new LinkedList<Kombinacije_izvajalcev>();
		
		try {
			em = this.getEntityManager();

			/////////////// IZBRALI SMO PREDMET /////////////////
			if (predmet != -1) {
				
				Predmet p = em.find(Predmet.class, predmet);
				
				Date dateOd = getDate(od, "00:00:00");
				Date dateDo = getDate(dox, "00:00:00");

				
				List<Ocena> ocene = vrniPodatke(studijskoLeto, predmet, dateOd, dateDo); // id ali ime predmeta?????

				List<Kombinacije_izvajalcev> ki = new LinkedList<Kombinacije_izvajalcev>();
				
				for(Ocena o : ocene) {
					   Izpitni_rok ir = em.find(Izpitni_rok.class, o.getFkIzpitniRok());
					   
					   Kombinacije_izvajalcev k1 = ir.getPredmetIzvajalec().getKombinacijeIzvajalcev();
					   ki.add(k1);
				}
				
				
				List<Ocena> oceneTemp = new LinkedList<Ocena>();
				int stevec = -1;
				int stevec2 = 1;
				Kombinacije_izvajalcev k;
				Kombinacije_izvajalcev k2;
				
				for(int i = 0; i < ki.size(); i++){
					k = ki.get(i);
					
					if(i != ki.size() - 1)
						k2 = ki.get(i + 1);
					else
						k2 = new Kombinacije_izvajalcev();
					
					oceneTemp.add(ocene.get(i));
					
					if(k.getIdKombinacijeIzvajalcev() != k2.getIdKombinacijeIzvajalcev()){
						
						if(p.getNacinOcenjevanja().equals(Constants.OCENA_IZPIT_VAJE)){
							podatki1.add(izracunajPodatke(oceneTemp, Constants.OCENA_IZPIT));
							podatki2.add(izracunajPodatke(oceneTemp, Constants.OCENA_VAJE));
						}else{
							podatki1.add(izracunajPodatke(oceneTemp, p.getNacinOcenjevanja()));
						}
							
						oceneTemp.clear();
						
						kombinacije.add(k);
					}
				}
				
				request.setAttribute("nacin", p.getNacinOcenjevanja());
				request.setAttribute("izvajalci", kombinacije);
				request.setAttribute("podatki1", podatki1);
				request.setAttribute("od", od);
				request.setAttribute("dox", dox);
				if(p.getNacinOcenjevanja().equals(Constants.OCENA_IZPIT_VAJE))
					request.setAttribute("podatki2", podatki2);
				
				url = "Ajax/analizaUspesnosti.jsp";
				
				RequestDispatcher dispatcher = request.getRequestDispatcher(url);
				dispatcher.forward(request, response);
				
				// ocena = polaganje
			}
			if (studijskoLeto != null) {
				JSONObject json = new JSONObject();
				json = returnPredmeti(request, response);
				response.setContentType("text/html; charset=utf-8");
				//preusmeriNaServlet(request, response);
				PrintWriter out = response.getWriter();
				out.print(json);
				System.out.println(json);
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}
	
	private Date getDate(String datum, String cas){
		Calendar cal = Calendar.getInstance();
	     
		String[] d = datum.split("\\.");
		
		int day = Integer.parseInt(d[0]);
		int month = Integer.parseInt(d[1]);
		int year = Integer.parseInt(d[2]);
				
		String[] c = cas.split(":");
		int hour = Integer.parseInt(c[0]);
		int min = Integer.parseInt(c[1]);
				
	    cal.set( cal.YEAR, year );
	    cal.set( cal.MONTH, month - 1);
	    cal.set( cal.DATE, day );
	     
	    cal.set( cal.HOUR_OF_DAY, hour );
	    cal.set( cal.MINUTE, min );
	    cal.set( cal.SECOND, 0 );
	    
	    return cal.getTime();
	}
	
	
	private double[] izracunajPodatke(List<Ocena> ocene, String nacinOcen){
		
		EntityManager em = this.getEntityManager();
		
		int stStudentov = 1;
		Predmetnik predmetnik = null;
		Vpisni_list vl = null;
		Student student = null;
		int tmp = 0;
		int stevecStudenta = -1;
		
		for(Ocena o : ocene){
			
			tmp = o.getFkPredmetnik();
			predmetnik = em.find(Predmetnik.class, tmp);
			vl = em.find(Vpisni_list.class, predmetnik.getFkVpisniList());
			student = vl.getStudent();
			
			if(stevecStudenta == -1)
				stevecStudenta = student.getIdStudent();
			
			if(stevecStudenta != student.getIdStudent()){
				stStudentov ++;
				stevecStudenta = student.getIdStudent();
			}
		}
		
		double[] rezultat = new double[5];
		
		int stOcen = 0;
		int stPozOcen = 1;
		int povpStPolaganj = 2;
		int povpOcena = 3;
		int povpPozOcena = 4;
		// izracun st polaaganj
		int stPolaganj = 0;
		int stPolaganjSum = 0;
		
		
		for(Ocena o : ocene){
			rezultat[stOcen]++;
			
			if(nacinOcen.equals(Constants.OCENA_IZPIT)){
				if(o.getOcenaIzpit() > 5){
					rezultat[povpPozOcena] += o.getOcenaIzpit();
					rezultat[stPozOcen]++;
				}
				
				rezultat[povpOcena] += o.getOcenaIzpit();
			
			}else if(nacinOcen.equals(Constants.OCENA_VAJE)){
				if(o.getOcenaVaje() > 5){
					rezultat[povpPozOcena] += o.getOcenaVaje();
					rezultat[stPozOcen]++;
				}
			
				rezultat[povpOcena] += o.getOcenaVaje();
				
			}else if(nacinOcen.equals(Constants.OCENA_KONCNA)){
				if(o.getOcenaKoncna() > 5){
					rezultat[povpPozOcena] += o.getOcenaKoncna();
					rezultat[stPozOcen]++;
				}
				
				rezultat[povpOcena] += o.getOcenaKoncna();
			}
			stPolaganj = o.getStPolaganja();
			for(int i = 1; i <= stPolaganj; i++){
				stPolaganjSum+=i;
			}
		}
		if(rezultat[stPozOcen] == 0)
			rezultat[povpPozOcena] = 0;
		else
			rezultat[povpPozOcena] = rezultat[povpPozOcena] / rezultat[stPozOcen];
		if(rezultat[stOcen] == 0)
			rezultat[povpOcena] = 0;
		else
			rezultat[povpOcena] = rezultat[povpOcena] / rezultat[stOcen];
		if(stStudentov == 0)
			rezultat[povpStPolaganj] = 0;
		else
			rezultat[povpStPolaganj] = stPolaganjSum / stStudentov;
		
		return rezultat;
	}
	
	private List<Ocena> vrniPodatke(String studLeto, int predmet, Date od, Date dox){ // tle ce po id-ju al imenu predmeta

		try{
		EntityManager em = this.getEntityManager();
				
		String sql = "SELECT "+
				   "DISTINCT ocena "+
				   "FROM "+
				      "Ocena ocena, Predmetnik pred, Predmetnik_letnika predL, Predmet p, Letnik l, Izpitni_rok ir, Predmet_izvajalec pi, Kombinacije_izvajalcev ki "+
				   "WHERE ir.datumIzpit BETWEEN ?1 AND ?2"+
				      " AND " +
				      "predL.idPredmetnikLetnika = pred.fkPredmetnikLetnika  AND "+
				      "ir.predmet.idPredmeta = :predmet AND "+
				      "pred.idPredmetnik = ocena.fkPredmetnik AND "+
				      "ocena.fkIzpitniRok = ir.idRok AND "+
				      "ir.predmetIzvajalec = pi AND "+
				      "pi.kombinacijeIzvajalcev = ki " +
				    "GROUP BY " +
				    	"ki ";
		//System.out.println(od);
		
		Query query = em.createQuery(sql);
		query.setParameter("predmet", predmet);
		query.setParameter(1, od);
		query.setParameter(2, dox);
		
		List<Ocena> rezultat = query.getResultList();
		return rezultat;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	private void preusmeriNaServlet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		request.setCharacterEncoding("UTF-8");
		//HttpSession session = request.getSession();
		String url = "index.jsp?stran=analizaUspesnosti";
		
		EntityManager em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		RequestDispatcher dis = request.getRequestDispatcher(url);
		dis.forward(request, response);
		
	}
	
	private JSONObject returnPredmeti(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		request.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession();

		
		EntityManager em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		
		String sql = "SELECT "+
						"distinct p "+
					"FROM "+
						"Predmet p, Predmetnik_letnika predL, Letnik l "+
					"WHERE "+
						"p = predL.predmet AND " +
						"predL.letnik = l AND " +
						"l.studijskoLeto = :studijskoLeto" + 
						" ORDER BY " +
						"p.nazivPredmet ";

		Query query = em.createQuery(sql);
		String studijskoLeto = request.getParameter("studLeto");
		
		query.setParameter("studijskoLeto", studijskoLeto);
		
		List<Predmet> predmeti = (List<Predmet>) query.getResultList();
		
		JSONObject json = new JSONObject();
		
		List<String> imenaPredmetov = new ArrayList<String>();
		List<Integer> idPredmeti = new ArrayList<Integer>();
		List<String> sifrePredmetov = new ArrayList<String>();
		
		for(Predmet p : predmeti){
			imenaPredmetov.add(p.getNazivPredmet());
			idPredmeti.add(p.getIdPredmeta());
			sifrePredmetov.add(p.getSifraPredmeta());
		}
		
		json.accumulate("predmeti", imenaPredmetov);
		json.accumulate("idPredmeti", idPredmeti);
		json.accumulate("sifrePredmetov", sifrePredmetov);
		
		return json;
	}
}