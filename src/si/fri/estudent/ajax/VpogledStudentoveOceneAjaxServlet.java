package si.fri.estudent.ajax;
import	si.fri.estudent.utilities.*;

import java.io.IOException;
import java.text.DateFormat;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import si.fri.estudent.jpa.Izpitni_rok;
import si.fri.estudent.jpa.Ocena;
import si.fri.estudent.jpa.Oseba;
import si.fri.estudent.jpa.Predmet_izvajalec;
import si.fri.estudent.jpa.Predmetnik;
import si.fri.estudent.jpa.Studijska_smer;
import si.fri.estudent.jpa.Studijski_program;

/**
 * Servlet implementation class VpogledStudentoveOceneAjaxServlet
 */
@WebServlet("/VpogledStudentoveOceneAjaxServlet")
public class VpogledStudentoveOceneAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private Oseba profesor;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VpogledStudentoveOceneAjaxServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	private EntityManager getEntityManager(){
		return Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		profesor = (Oseba) session.getAttribute("uporabnik");
		
		int idStudent = -1;
		int vrstaStudija = -1;
		int studijskiProgram = -1;
		int studijskaSmer = -1;
		int predmetIzvajalec = -1;
		String nacin = null;
		String[][] tabelca = null;

				
		if (request.getParameter("idStudent") != null) {
			idStudent = Integer.parseInt(request.getParameter("idStudent"));
		}
		if (request.getParameter("vrstaStudija") != null) {
			vrstaStudija = Integer.parseInt(request.getParameter("vrstaStudija"));
		}
		if (request.getParameter("studijskiProgram") != null) {
			studijskiProgram = Integer.parseInt(request.getParameter("studijskiProgram"));
		}
		if (request.getParameter("studijskaSmer") != null) {
			studijskaSmer = Integer.parseInt(request.getParameter("studijskaSmer"));
		}
		if (request.getParameter("predmet") != null) {
			predmetIzvajalec = Integer.parseInt(request.getParameter("predmet"));
		}
		
		EntityManager em = null;
		String query = null;
		Query q = null;
		String url = "";
			
		try {
				em = this.getEntityManager();

				List<Object[]> rezultati = vrniPodatke(idStudent, predmetIzvajalec, vrstaStudija, studijskiProgram, studijskaSmer);
				List<Ocena> ocene = new LinkedList<Ocena>();
				List<Izpitni_rok> izpitniRoki = new LinkedList<Izpitni_rok>(); 
				List<Predmetnik> predmetniki = new LinkedList<Predmetnik>();
				
				List<Integer> ponavljanjaPolaganja = new LinkedList<Integer>();				
				
				for(Izpitni_rok ir : izpitniRoki){
					int polaganjaPonavljanja = 0;
					//polaganjaPonavljanja = getStPolagPonavljanj(ir.getPredmet().getIdPredmeta());
					ponavljanjaPolaganja.add(polaganjaPonavljanja);
				}
				
				request.setAttribute("polaganjaPonavljanja", ponavljanjaPolaganja);
					
				for(Object[] obj : rezultati){
					ocene.add((Ocena) obj[0]);
					izpitniRoki.add((Izpitni_rok) obj[1]);
					predmetniki.add((Predmetnik) obj[2]);
					}
			System.out.println(ocene);
			System.out.println(izpitniRoki);
			System.out.println(predmetniki);
			// TODO izpis za tiskanje
			nacin = izpitniRoki.get(0).getPredmet().getNacinOcenjevanja();

			
			if(nacin.equals("I") || nacin.equals("K") || nacin.equals("V")){
				tabelca=new String[rezultati.size()+1][6];
				tabelca[0][0]="TOČKE IZPIT";
				tabelca[0][1]="TOČKE BONUS";
				tabelca[0][2]="DATUM IZPIT";
				tabelca[0][3]="ŠT. POLAGANJA";
				tabelca[0][4]="ŠT. POLAGANJA LETOS";
				if(nacin.equals("K"))
					tabelca[0][5]="KONČNA";
				else if(nacin.equals("V"))
					tabelca[0][5]="OCENA VAJE";
				else if(nacin.equals("I"))
					tabelca[0][5]="OCENA IZPIT";
			}else if(nacin.equals("IV")){
				tabelca=new String[rezultati.size()+1][7];
				tabelca[0][0]="TOČKE IZPIT";
				tabelca[0][1]="TOČKE BONUS";
				tabelca[0][2]="DATUM IZPIT";
				tabelca[0][3]="ŠT. POLAGANJA";
				tabelca[0][4]="ŠT. POLAGANJA LETOS";
				tabelca[0][5]="OCENA IZPIT";
				tabelca[0][6]="OCENA VAJE";
			}
		
			for(int i=0;i<rezultati.size();i++){
					tabelca[i+1][0]=Double.toString(ocene.get(i).getOcena());
					tabelca[i+1][1]=Double.toString(ocene.get(i).getOcenaBonus());
					tabelca[i+1][2]=DateFormat.getInstance().format(izpitniRoki.get(i).getDatumIzpit()).toString();
					tabelca[i+1][3]=Integer.toString(ocene.get(i).getStPolaganja());
					tabelca[i+1][4]=Integer.toString(ocene.get(i).getStPolaganjaLetos());
					if(nacin.equals("I")){
						tabelca[i+1][5]=Integer.toString(ocene.get(i).getOcenaIzpit());
					}else if(nacin.equals("K")){
						tabelca[i+1][5]=Integer.toString(ocene.get(i).getOcenaKoncna());
					}else if(nacin.equals("V")){
						tabelca[i+1][5]=Integer.toString(ocene.get(i).getOcenaVaje());
					}
			}
			for(int i=0;i<rezultati.size();i++){
				if(nacin.equals("IV")){
					tabelca[i+1][5]=Integer.toString(ocene.get(i).getOcenaIzpit());
					tabelca[i+1][6]=Integer.toString(ocene.get(i).getOcenaVaje());
				}
			}
			Tiskanje tiskanje=new Tiskanje(request,response,"Vpogled v študentove ocene");
			if(izpitniRoki.size()>0)
				tiskanje.dodajTabelo(tabelca, false, true);
			tiskanje.dobiDocument();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		profesor = (Oseba) session.getAttribute("uporabnik");
		
		int idStudent = -1;
		int vrstaStudija = -1;
		int studijskiProgram = -1;
		int studijskaSmer = -1;
		int predmetIzvajalec = -1;
		
				
		if (request.getParameter("idStudent") != null) {
			idStudent = Integer.parseInt(request.getParameter("idStudent"));
		}
		if (request.getParameter("vrstaStudija") != null) {
			vrstaStudija = Integer.parseInt(request.getParameter("vrstaStudija"));
		}
		if (request.getParameter("studijskiProgram") != null) {
			studijskiProgram = Integer.parseInt(request.getParameter("studijskiProgram"));
		}
		if (request.getParameter("studijskaSmer") != null) {
			studijskaSmer = Integer.parseInt(request.getParameter("studijskaSmer"));
		}
		if (request.getParameter("predmet") != null) {
			predmetIzvajalec = Integer.parseInt(request.getParameter("predmet"));
		}
		
		EntityManager em = null;
		String query = null;
		Query q = null;
		String url = "";
				
		try {
			em = this.getEntityManager();

			/////////////// IZBRALI SMO PREDMET /////////////////
			if (predmetIzvajalec != -1) {
				List<Object[]> rezultati = vrniPodatke(idStudent, predmetIzvajalec, vrstaStudija, studijskiProgram, studijskaSmer);
				List<Ocena> ocene = new LinkedList<Ocena>();
				List<Izpitni_rok> izpitniRoki = new LinkedList<Izpitni_rok>(); 
				List<Predmetnik> predmetniki = new LinkedList<Predmetnik>();
				
				List<Integer> ponavljanjaPolaganja = new LinkedList<Integer>();
				
				for(Object[] obj : rezultati){
					ocene.add((Ocena) obj[0]);
					izpitniRoki.add((Izpitni_rok) obj[1]);
					predmetniki.add((Predmetnik) obj[2]);
				}
				
				for(Izpitni_rok i : izpitniRoki){
					int polaganjaPonavljanja = 0;
					polaganjaPonavljanja = getStPolagPonavljanj(i.getPredmet().getIdPredmeta(), idStudent);
					ponavljanjaPolaganja.add(polaganjaPonavljanja);
				}
				
				request.setAttribute("polaganjaPonavljanja", ponavljanjaPolaganja);
				
				request.setAttribute("izpitniRoki", izpitniRoki);
				request.setAttribute("ocene", ocene);
				url = "Ajax/studentoveOcene.jsp";
			}
			
			/////////////// IZBRALI SMO ≈†TUDIJSKO SMER /////////////////
			else if (studijskaSmer != -1) {
				List<Predmet_izvajalec> predmetIzvajalci = vrniPredmete(idStudent, vrstaStudija, studijskiProgram, studijskaSmer);
				
				request.setAttribute("predmetIzvajalci", predmetIzvajalci);
				url = "Ajax/predmetiVnosPrijavnica.jsp";
			}
			
			///////////////////////// IZBRALI SMO ≈†TUDIJSKI PROGRAM /////////////////////////
			else if (studijskiProgram != -1) {
				List<Studijska_smer> studijskeSmeri = vrniStudijskeSmeri(idStudent, vrstaStudija, studijskiProgram);
				
				request.setAttribute("studijskeSmeri", studijskeSmeri);
				url = "Ajax/studijskaSmer.jsp";
			}
			
			///////////////////////// IZBRALI SMO VRSTO ≈†TUDIJA /////////////////////////
			else if (vrstaStudija != -1) {
				List<Studijski_program> studijskiProgrami = vrniStudijskePrograme(idStudent, vrstaStudija);
				
				request.setAttribute("studijskiProgrami", studijskiProgrami);
				url = "Ajax/studijskiProgram.jsp";
			}		
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}
		}
			
		RequestDispatcher fw = request.getRequestDispatcher(url);
		fw.forward(request, response);
	}
	
	/////////////////////////////////// METODE ///////////////////////////////////

	private List<Studijska_smer> vrniStudijskeSmeri(int idStudent, int vrstaStudija, int studijskiProgram) {
		try {
			String sql = "SELECT " +
					"DISTINCT ss " +
			  "FROM " +
			  		"Student student, Vpisni_list vl, Letnik l, Studij studij," +
			  		"Vrsta_studija vs, Studijski_program sp, Studijska_smer ss,  Predmetnik pred, " +
			  		"Predmetnik_letnika predL, Predmet p, Predmet_izvajalec pi, Kombinacije_izvajalcev ki," +
			  		"Osebje os, Oseba o " +
		  	  "WHERE " +
		  	  		"student.idStudent = :student AND " +
		  	  		"vl.student = student AND " +
		  	  		"vl.letnik = l AND " +
		  	  		"l.studij = studij AND " +
		  	  		"studij.vrstaStudija.idVrstaStudija = :vrstaStudija AND " +
		  	  		"studij.studijskiProgram.idStudijskiProgram = :studijskiProgram AND " +
		  	  		"studij.studijskaSmer =  ss AND " +
					"vl.idVpisni = pred.fkVpisniList AND " +
					"pred.fkPredmetnikLetnika = predL.idPredmetnikLetnika AND " +
					"predL.predmet = p AND " +
					"p = pi.predmet AND " +
					"pi.kombinacijeIzvajalcev = ki AND " +
					"(" +
						" ki.osebje1 = os OR " +
						" ki.osebje2 = os OR " +
						" ki.osebje3 = os " +
					") AND " +
					"os.oseba.idOseba = :profesor " +
				"ORDER BY " +
					"ss.nazivStudijskaSmer";
		
			Query q = this.getEntityManager().createQuery(sql);
			q.setParameter("student", idStudent);
			q.setParameter("vrstaStudija", vrstaStudija);
			q.setParameter("studijskiProgram", studijskiProgram);	
			q.setParameter("profesor", profesor.getIdOseba());
			
			List<Studijska_smer> studijskeSmeri = (List<Studijska_smer>)q.getResultList();
			return studijskeSmeri;
			
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	private List<Studijski_program> vrniStudijskePrograme(int idStudent, int vrstaStudija) {
		try {
			String sql = "SELECT " +
					"DISTINCT sp " +
			  "FROM " +
			  		"Student student, Vpisni_list vl, Letnik l, Studij studij," +
			  		"Vrsta_studija vs, Studijski_program sp, Predmetnik pred, " +
			  		"Predmetnik_letnika predL, Predmet p, Predmet_izvajalec pi, Kombinacije_izvajalcev ki," +
			  		"Osebje os, Oseba o " +
		  	  "WHERE " +
		  	  		"student.idStudent = :student AND " +
		  	  		"vl.student = student AND " +
		  	  		"vl.letnik = l AND " +
		  	  		"l.studij = studij AND " +
		  	  		"studij.vrstaStudija.idVrstaStudija = :vrstaStudija AND " +
		  	  		"studij.studijskiProgram = sp AND " +
		  	  		"vl.idVpisni = pred.fkVpisniList AND " +
					"pred.fkPredmetnikLetnika = predL.idPredmetnikLetnika AND " +
					"predL.predmet = p AND " +
					"p = pi.predmet AND " +
					"pi.kombinacijeIzvajalcev = ki AND " +
					"(" +
						" ki.osebje1 = os OR " +
						" ki.osebje2 = os OR " +
						" ki.osebje3 = os " +
					") AND " +
					"os.oseba.idOseba = :profesor " +
				"ORDER BY " +
					"sp.nazivStudijskiProgram";
		
			Query q = this.getEntityManager().createQuery(sql);
			q.setParameter("student", idStudent);
			q.setParameter("vrstaStudija", vrstaStudija);
			q.setParameter("profesor", profesor.getIdOseba());
			
			List<Studijski_program> studijskiProgrami = (List<Studijski_program>)q.getResultList();
			return studijskiProgrami;
			
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}	
	
	private List<Object[]> vrniPodatke(int idStudent, int idPredmetIzvajalec, int vrstaStudija, int studijskiProgram, int studijskaSmer){
		
		EntityManager em = this.getEntityManager();
			
		try{
			
			String sql = "SELECT " +
					"DISTINCT oc, ir, pred " +
			  "FROM " +
			  		"Student student, Vpisni_list vl, Letnik l, Studij studij," +
			  		"Vrsta_studija vs, Studijski_program sp, Predmetnik pred, " +
			  		"Predmetnik_letnika predL, Predmet p, Predmet_izvajalec pi, Kombinacije_izvajalcev ki," +
			  		"Osebje os, Oseba o, Ocena oc, Izpitni_rok ir " +
		  	  "WHERE " +
				  	"student.idStudent = :student AND " +
		  	  		"vl.student = student AND " +
			  	  	"vl.letnik = l AND " +
		  	  		"l.studij = studij AND " +
		  	  		"vl.idVpisni = pred.fkVpisniList AND " +
		  	  		"pred.idPredmetnik = oc.fkPredmetnik AND " +
		  	  		"oc.fkIzpitniRok = ir.idRok AND " +
		  	  		"oc.aktivna = 1 AND " +
		  	  		"ir.predmetIzvajalec = pi AND " +
		  	  		"pi.idPredmetIzvajalca = :predmetIzvajalec AND " +
					"studij.vrstaStudija.idVrstaStudija = :vrstaStudija AND " +
		  	  		"studij.studijskiProgram.idStudijskiProgram = :studijskiProgram AND " +
		  	  		"studij.studijskaSmer.idStudijskaSmer =  :studijskaSmer AND " +
		  	  		"pi.status = 1 " +
		  	  	"ORDER BY " +
		  	  		"student.oseba.priimek";
			
			Query query = em.createQuery(sql);
			query.setParameter("vrstaStudija", vrstaStudija);
			query.setParameter("studijskiProgram", studijskiProgram);
			query.setParameter("studijskaSmer", studijskaSmer);
			query.setParameter("student", idStudent);
			query.setParameter("predmetIzvajalec", idPredmetIzvajalec);
			
			List<Object[]> rezultat = query.getResultList();
			
			return rezultat;
			
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	private int opravljanjaLetos(int idPredmetIzvajalec, int idLetnika, int idStudent){
		
		EntityManager em = this.getEntityManager();
		
		String sql = "SELECT "+
				"DISTINCT oc " +
			"FROM "+
				"Oseba o, Student s, Vpisni_list vl, Predmetnik pr, Predmet p, Predmetnik_letnika prl, Ocena oc, Predmet_izvajalec pi "+
			"WHERE " +
				"s.idStudent = ?1 AND " +
				"s = vl.student AND " +
				"vl.idVpisni = pr.fkVpisniList AND " +
				"pr.fkPredmetnikLetnika = prl.idPredmetnikLetnika AND " +
				"p = prl.predmet AND " +
				"p = pi.predmet AND " +
				"pi.idPredmetIzvajalca = ?2 AND " +
				"pr.idPredmetnik = oc.fkPredmetnik AND " +
				"oc.aktivna = 1";
		
		if(idLetnika > 0)
				sql += " AND vl.letnik.idLetnik = ?3";
		
		Query query = em.createQuery(sql); 
		
		query.setParameter(1, idStudent);
		query.setParameter(2, idPredmetIzvajalec);
		
		if(idLetnika > 0)
			query.setParameter(3, idLetnika);
		
		List<Ocena>ocene = (List<Ocena>) query.getResultList();
		
		Predmetnik predmetnik = null;
		int max = 0;
		
		for(Ocena o : ocene){
			if(o.getStPolaganja() > max && idLetnika < 0)
					max = o.getStPolaganja();
			
			if(o.getStPolaganjaLetos() > max && idLetnika > 0)
					max = o.getStPolaganjaLetos();
		}
		
		return max;
	}
	
	
	private List<Predmet_izvajalec> vrniPredmete(int idStudenta, int vrstaStudija, int studijskiProgram, int studijskaSmer){
		
		EntityManager em = this.getEntityManager();

		try{
			
			String sql = "SELECT " +
								"DISTINCT pi " + 
						 "FROM " +
						 		"Student st, Vpisni_list vl, Predmetnik pred," +
						 		"Predmet predmet, Predmetnik_letnika predL, Letnik letnik, " +
						 		"Predmet_izvajalec pi, Kombinacije_izvajalcev ki, Osebje os, Oseba o, Studij studij" +
						 		" " +
						 "WHERE " +
						 		"st.idStudent = :student AND " +
						 		"vl.student = st AND " +
						 		"vl.idVpisni = pred.fkVpisniList AND " +
						 		"vl.letnik = letnik AND " +
					  	  		"letnik.studij = studij AND " +
					  	  		"studij.vrstaStudija.idVrstaStudija = :vrstaStudija AND " +
					  	  		"studij.studijskiProgram.idStudijskiProgram = :studijskiProgram AND " +
					  	  		"studij.studijskaSmer.idStudijskaSmer =  :studijskaSmer AND " + 
						 		"predL.idPredmetnikLetnika = pred.fkPredmetnikLetnika AND " +
						 		"predL.predmet = predmet AND " +
								"predmet = pi.predmet AND " +
								"pi.kombinacijeIzvajalcev = ki AND " +
								"(" +
									" ki.osebje1 = os OR " +
									" ki.osebje2 = os OR " +
									" ki.osebje3 = os " +
								") AND " +
								"os.oseba.idOseba = :profesor AND " +
								"pi.status = 1 " +
						"ORDER BY " +
							   "predmet.nazivPredmet";
					
			Query query = em.createQuery(sql);
			query.setParameter("student", idStudenta);
			query.setParameter("profesor", profesor.getIdOseba());
			query.setParameter("vrstaStudija", vrstaStudija);
			query.setParameter("studijskiProgram", studijskiProgram);
			query.setParameter("studijskaSmer", studijskaSmer);
			
			List<Predmet_izvajalec> predmeti = query.getResultList();
			
			return predmeti;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}
	
	private int getStPolagPonavljanj(int predmet, int student){
		try {
			String sql = "SELECT " +
					"DISTINCT oc " +
			  "FROM " +
			  		"Ocena oc, Predmetnik pred, Vpisni_list vl, Vrsta_vpisa vp, Predmetnik_letnika predL, Predmet p " +
		  	  "WHERE " +
		  	  		"vl.student.idStudent = :student AND " +
		  	  		"vl.vrstaVpisa.idVrstaVpisa = :vrstaVpisa AND " +
		  	  		"vl.idVpisni = pred.fkVpisniList AND " +
		  	  		"pred.idPredmetnik = oc.fkPredmetnik AND " +
		  	  		"pred.fkPredmetnikLetnika = predL.idPredmetnikLetnika AND " +
		  	  		"predL.predmet.idPredmeta = :predmet AND " +
		  	  		"oc.aktivna = 1 AND " +
		  	  		"( " +
		  	  			" oc.ocenaKoncna > 0 OR " +
		  	  			" oc.ocenaIzpit > 0 OR " +
		  	  			" oc.ocenaVaje > 0 OR " +
		  	  			" oc.ocena > 0 " +
		  	  		")";
					  	  		
			Query query = this.getEntityManager().createQuery(sql);
			query.setParameter("predmet", predmet);
			query.setParameter("student", student);
			query.setParameter("vrstaVpisa", Constants.VPIS_PONAVLJANJE);
			
			List<Ocena> ocene = query.getResultList();
			
			int st = 0;
			
			for(Ocena oc : ocene)
				st += 1;
					
			return st;
			
		} catch(Exception e){
			e.printStackTrace();
			return 0;
		}	
	}

	
}
