package si.fri.estudent.ajax;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import si.fri.estudent.jpa.*;
import si.fri.estudent.utilities.DataValidator;

import net.sf.json.JSONObject;


/**
 * Servlet implementation class VpisniListServlet
 */
@WebServlet(name = "VpisniListAjaxServlet", urlPatterns = { "/VpisniListAjaxServlet" })
public class VpisniListAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VpisniListAjaxServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*
		request.setCharacterEncoding("UTF-8"); // brez tega ne delajo ČĆŠĐŽ
		
		/////// PARSANJE JSON ZAHTEVE
		
		String ime = request.getParameter("ime"); // poberemo ven vrednosti normalnih atributov
		String priimek = request.getParameter("priimek");
		String[] predmeti = request.getParameterValues("predmeti[]"); // če iz requesta hočemo dobit atribut z več vrednostmi, mu "pametna" java
																      // doda []. Vsakič, ko boste pobirali ven vrednosti atributa z več vrednostmi,
																	  // dodate [] => imeAtributa[], don't ask me why, ker ne vem.
		
		////// IZDELAVA JSON ODGOVORA
		
		List<String> predmetiList = new ArrayList<String>(); // Da naredimo atribut z več vrednostmi, moramo najprej narediti list.
		for (String predmet : predmeti) {
	        predmetiList.add(predmet);
		}
		
		JSONObject json = new JSONObject();
		json.put("ime", ime); // v json objekt pripnemo navadne atribute in vrednosti
		json.put("priimek", priimek);
		json.accumulate("predmeti", predmetiList); // pripnemo tudi atribut z več vrednostmi kot list
		
		response.setContentType("text/html; charset=utf-8");   // brez tega bo response brez ČŽĆŽĐ  
		PrintWriter out = response.getWriter();
		out.print(json); // Pošlje se {"ime":"Janez","priimek":"Novak","predmeti":["Predmet1","Predmet2","Predmet3"]}, povsem isto, kot smo poslali z jQuery-ja na servlet */
		
		// MOJA KODA, KI SE TE TIČE ZGORNJEGA TEMPLATA
		
		request.setCharacterEncoding("UTF-8");

		String funkcija = request.getParameter("funkcija");
		EntityManager em;
		Query q;
		JSONObject json = new JSONObject();
		DataValidator validator = new DataValidator();
		
		if (funkcija.equals("nov")) { // Dobi novo vpisno številko
			
			try {
				em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
				q = em.createQuery("SELECT MAX(s.vpisnaSt) FROM Student s");
				List<String> vpisnaStevilka = (List<String>)(q.getResultList());
				json.put("vpisnaStevilka", Integer.toString((Integer.parseInt(vpisnaStevilka.get(0)) + 1)));
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}
		} else if (funkcija.equals("obstojeci")) { // Dobi podatke o obstoječem študentu
			String vpisnaStevilka = request.getParameter("vpisnaStevilka");
			
			if (!validator.validateInputText(vpisnaStevilka, 8, false, "^63\\d{6}$", "", "", "")) {
				return;
			}
			
			try {
				em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
				em.getTransaction().begin();
				
				q = em.createQuery("SELECT o FROM Student s, Oseba o WHERE s.oseba.idOseba=o.idOseba AND s.vpisnaSt LIKE :vpisnaStevilka");
				q.setParameter("vpisnaStevilka", vpisnaStevilka);
				
				// Preverim če obstaja Oseba s to vpisno številko
				List<Oseba> osebe = (List<Oseba>)(q.getResultList());
				if (osebe.size() == 0) {
					return;
				} 
				Oseba oseba = osebe.get(0);
				
				// Osebni podatki
				json.put("priimek", oseba.getPriimek());
				json.put("dekliskiPriimek", oseba.getPriimekDekliski());
				json.put("ime", oseba.getIme());
				json.put("emso", oseba.getEmso());
				json.put("davcnaStevilka", oseba.getDavcnaSt());
				
				// Posebne potrebe
				q = em.createQuery(
						"SELECT p " +
						"FROM Posebne_potrebe p, Student_has_Posebne_potrebe sp, Student s " +
						"WHERE s.idStudent=sp.student1.idStudent AND p.idPosebnePotrebe=sp.posebnePotrebe1.idPosebnePotrebe AND " +
						"s.vpisnaSt = :vpisna"
						);
				
				q.setParameter("vpisna", vpisnaStevilka); 
				
				List<Posebne_potrebe> posebnePotrebeJPA = (List<Posebne_potrebe>)q.getResultList();
				List<String> posebnePotrebe = new ArrayList<String>();
				
				for (Posebne_potrebe pp : posebnePotrebeJPA) {
					posebnePotrebe.add(Integer.toString(pp.getIdPosebnePotrebe()));
				}
				json.accumulate("posebnePotrebe", posebnePotrebe);
				
				// Stalni in začasni naslov
				q = em.createQuery("SELECT distinct o FROM Naslov n, Student s, Oseba o " +
						"WHERE " +
						"s.oseba.idOseba=o.idOseba AND s.vpisnaSt LIKE :vpisnaStevilka");
				
				q.setParameter("vpisnaStevilka", vpisnaStevilka);
				Oseba o = (Oseba) q.getSingleResult();
				
				Naslov naslovStalni = o.getNaslov1();
				Naslov naslovZacasni = o.getNaslov2();
				
				// Naslov stalnega bivališča
				json.put("ulicaS", naslovStalni.getUlica());
				json.put("postaS", naslovStalni.getKraj().getPosta());
				json.put("idPostaS", naslovStalni.getKraj().getIdKraj());
				json.put("postnaStevilkaS", naslovStalni.getKraj().getPostnaSt());
				json.put("obcinaS", naslovStalni.getObcina().getNazivObcina());
				json.put("idObcinaS", naslovStalni.getObcina().getIdObcina());
				json.put("drzava", naslovStalni.getDrzava().getNazivDrzava());
				json.put("idDrzava", naslovStalni.getDrzava().getIdDrzava());
				json.put("telefon", oseba.getTelefon());
				json.put("prenosni", oseba.getMobitel());
				json.put("email", oseba.getEmail());
				
				// Naslov začasnega bivališča
				if(naslovZacasni != null) {
					json.put("ulicaZ", naslovZacasni.getUlica());
					json.put("postaZ", naslovZacasni.getKraj().getPosta());
					json.put("idPostaZ", naslovZacasni.getKraj().getIdKraj());
					json.put("postnaStevilkaZ", naslovZacasni.getKraj().getPostnaSt());
					json.put("obcinaZ", naslovZacasni.getObcina().getNazivObcina());
					json.put("idObcinaZ", naslovZacasni.getObcina().getIdObcina());
					json.put("niZacasnegaNaslova", "false");
				}else{
					json.put("niZacasnegaNaslova", "true");
				}
				
				em.getTransaction().commit();
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}
		}
		
		response.setContentType("text/html; charset=utf-8");
		PrintWriter out = response.getWriter();
		out.print(json);
	}
}