package si.fri.estudent.ajax;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import si.fri.estudent.jpa.Oseba;
import si.fri.estudent.jpa.Student;
import si.fri.estudent.utilities.DataValidator;

/**
 * Servlet implementation class IskanjeStudentaAjaxServlet
 */
@WebServlet("/IskanjeStudentaAjaxServlet")
public class IskanjeStudentaAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private EntityManager em;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IskanjeStudentaAjaxServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		request.setCharacterEncoding("UTF-8");
		String ime=request.getParameter("ime");
		String priimek=request.getParameter("priimek");
		
		//----- validacija  
		String napaka="";
		DataValidator dv=new DataValidator();
		boolean kulIme=dv.validateInputText(ime, 30, false,"^[a-zA-ZčćšđžČĆŠĐŽ]{1,}$", "Ime ni vneseno.", "Predolgo ime.", "Ime ni veljavno.");
		if(ime.length()==0){kulIme=true;}
		
		boolean kulPriimek=dv.validateInputText(priimek, 30, false,"^[a-zA-ZčćšđžČĆŠĐŽ]{1,}$", "Priimek ni vnesen.", "Predolgo priimek.", "Priimek ni veljavno.");
		if(priimek.length()==0){kulPriimek=true;}
		if(!kulPriimek || !kulIme){
			response.setContentType("text/html; charset=utf-8");  
			PrintWriter out = response.getWriter();
			out.print("");
			return;
		}		
		//-------------------
		
		String imeServlet="IskanjeStudentaServlet";  //na kater servlet bo kazala povezava
		
		String sql="SELECT os, st " +
				"FROM Student st, Oseba os " +
				"WHERE os.ime LIKE :ime AND os.priimek LIKE :priimek AND st.oseba=os";
		Query q=em.createQuery(sql);
		q.setParameter("ime", ime+"%");
		q.setParameter("priimek", priimek+"%");
		
		String html="";
		List<Object[]> rezultati=q.getResultList();
		
		if(rezultati.size()>0){
			html+="<table>";
			for(Object[] ob:rezultati){
				Oseba os=(Oseba)ob[0];
				Student st=(Student)ob[1];
				
				
				html+="<tr><td><a href='"+imeServlet+
						"?student="+st.getVpisnaSt()+"'>"+
						os.getIme()+"  "+os.getPriimek()+"  "+os.getEmso()+
						"</a></td></tr>";
			}
			html+="</table>";
		}else{
			html+="Noben študent ne ustreza poizvedbi.";
		}
		
		response.setContentType("text/html; charset=utf-8");  
		PrintWriter out = response.getWriter();
		out.print(html);
	}

}
