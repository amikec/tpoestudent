package si.fri.estudent.ajax;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import si.fri.estudent.jpa.Letnik;
import si.fri.estudent.jpa.Modul;
import si.fri.estudent.jpa.Predmet;
import si.fri.estudent.jpa.Studijska_smer;
import si.fri.estudent.jpa.Studijski_program;
import si.fri.estudent.jpa.Vrsta_studija;

/**
 * Servlet implementation class VzdrzevanjePredmetnikaAjaxServlet
 */
@WebServlet("/VzdrzevanjePredmetnikaAjaxServlet")
public class VzdrzevanjePredmetnikaAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VzdrzevanjePredmetnikaAjaxServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int vrstaStudija = -1;
		int studijskiProgram = -1;
		int studijskaSmer = -1;
		String studijskoLeto = null;
		int letnik = -1;
		String predmetnikModul = null;
		String vrstaPredmetnika = null;
		int modul = -1;
		
		if (request.getParameter("vrstaStudija") != null) {
			vrstaStudija = Integer.parseInt(request.getParameter("vrstaStudija"));
		}
		if (request.getParameter("studijskiProgram") != null) {
			studijskiProgram = Integer.parseInt(request.getParameter("studijskiProgram"));
		}
		if (request.getParameter("studijskaSmer") != null) {
			studijskaSmer = Integer.parseInt(request.getParameter("studijskaSmer"));
		}
		if (request.getParameter("studijskoLeto") != null) {
			studijskoLeto = request.getParameter("studijskoLeto");
		}
		if (request.getParameter("letnik") != null) {
			letnik = Integer.parseInt(request.getParameter("letnik"));
		}
		if (request.getParameter("predmetnikModul") != null) {
			predmetnikModul = request.getParameter("predmetnikModul");
		}
		if (request.getParameter("vrstaPredmetnika") != null) {
			vrstaPredmetnika = request.getParameter("vrstaPredmetnika");
		}
		if (request.getParameter("modul") != null) {
			modul = Integer.parseInt(request.getParameter("modul"));
		}
		
		EntityManager em = null;
		String query = null;
		Query q = null;
		
		try {
			em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
			
			///////////////////////// IZBRALI SMO PREDMETNIK / MODUL /////////////////////////
			if (predmetnikModul != null) {
				//////////// MODULI //////////////
				if (predmetnikModul.equals("modul")) {
					if (modul == -1) { // Modula še nismo izbrali
						query = "SELECT " +
									"m " +
								"FROM " +
									"Modul m ";
						
						q = em.createQuery(query);
						List<Modul> moduli = (List<Modul>)q.getResultList();
						
						request.setAttribute("moduli", moduli);
						RequestDispatcher fw = request.getRequestDispatcher("Ajax/modul.jsp");
						fw.forward(request, response);
						
					} else {			// Modul smo izbrali
						query = "SELECT " + // Še ne izbrani predmeti (DRAG)
									"DISTINCT p " +
								"FROM " +
									"Letnik l, Predmetnik_letnika pl, Predmet p " +
								"WHERE " +
									"l.idLetnik = pl.letnik.idLetnik AND " +
									"p.idPredmeta = pl.predmet.idPredmeta AND " +
									"pl.status = TRUE AND " +
									"pl.obvezniPredmet = FALSE AND " +
									"pl.modul IS NULL AND " +
									"l.idLetnik = :letnik " +
								"ORDER BY " +
								 	"p.sifraPredmeta, p.nazivPredmet";
						
						q = em.createQuery(query);
						q.setParameter("letnik", letnik);
						
						List<Predmet> neIzbraniPredmeti = (List<Predmet>)q.getResultList();
						request.setAttribute("neIzbraniPredmeti", neIzbraniPredmeti);
						
						query = "SELECT " + // Že izbrani predmeti (DROP)
									"DISTINCT p " +
								"FROM " +
									"Letnik l, Predmetnik_letnika pl, Predmet p " +
								"WHERE " +
									"l.idLetnik = pl.letnik.idLetnik AND " +
									"p.idPredmeta = pl.predmet.idPredmeta AND " +
									"pl.status = TRUE AND " +
									"pl.obvezniPredmet = FALSE AND " +
									"pl.modul.idModul = :modul AND " +
									"l.idLetnik = :letnik " +
								"ORDER BY " +
								 	"p.sifraPredmeta, p.nazivPredmet";
						
						q = em.createQuery(query);
						q.setParameter("modul", modul);
						q.setParameter("letnik", letnik);
						
						List<Predmet> izbraniPredmeti = (List<Predmet>)q.getResultList();
						request.setAttribute("izbraniPredmeti", izbraniPredmeti);
						
						RequestDispatcher fw = request.getRequestDispatcher("Ajax/dragDropPredmeti.jsp");
						fw.forward(request, response);
					}
				
				//////////// PREDMETNIKI //////////////
				} else if (predmetnikModul.equals("predmetnik")) {
					query = "SELECT " + // Še ne izbrani predmeti (DRAG)
								"DISTINCT p " +
							"FROM " +
								"Predmet p " +
							"WHERE " +
								"p.idPredmeta NOT IN" +
								"(" +
									"SELECT " +
										"DISTINCT p1.idPredmeta " +
									"FROM " +
										"Letnik l, Predmetnik_letnika pl, Predmet p1 " +
									"WHERE " +
										"l.idLetnik = pl.letnik.idLetnik AND " +
										"p1.idPredmeta = pl.predmet.idPredmeta AND " +
										"pl.status = 1 AND " +
										"l.idLetnik = :letnik" +
								") " +
							 "ORDER BY " +
							 	"p.sifraPredmeta, p.nazivPredmet";
					
					q = em.createQuery(query);
					q.setParameter("letnik", letnik);
					
					List<Predmet> neIzbraniPredmeti = (List<Predmet>)q.getResultList();
					request.setAttribute("neIzbraniPredmeti", neIzbraniPredmeti);
					
					query = "SELECT " + // Že izbrani predmeti (DROP)
								"DISTINCT p " +
							"FROM " +
								"Letnik l, Predmetnik_letnika pl, Predmet p " +
							"WHERE " +
								"l.idLetnik = pl.letnik.idLetnik AND " +
								"p.idPredmeta = pl.predmet.idPredmeta AND " +
								"pl.status = 1 AND " +
								"l.idLetnik = :letnik AND " +
								"pl.obvezniPredmet = :obvezniPredmet " +
							"ORDER BY " +
							 	"p.sifraPredmeta, p.nazivPredmet";
					
					q = em.createQuery(query);
					q.setParameter("letnik", letnik);
					if (vrstaPredmetnika.equals("obvezni")) {
						q.setParameter("obvezniPredmet", true);
					} else {
						q.setParameter("obvezniPredmet", false);
					}
					
					List<Predmet> izbraniPredmeti = (List<Predmet>)q.getResultList();
					request.setAttribute("izbraniPredmeti", izbraniPredmeti);
					
					RequestDispatcher fw = request.getRequestDispatcher("Ajax/dragDropPredmeti.jsp");
					fw.forward(request, response);
				}
				
			/////////////// IZBRALI SMO ŠTUDIJSKO SMER IN ŠTUDIJSKO LETO /////////////////
			} else if (studijskoLeto != null) {
				query = "SELECT " +
							 "DISTINCT l " +
			     	    "FROM " +
			     	        "Letnik l, Studij s, Studijska_smer sm, Vrsta_studija vs, Studijski_program sp " +
			     	    "WHERE " +
			     	   	    "sm.idStudijskaSmer = s.studijskaSmer.idStudijskaSmer AND " +
			     	   	    "vs.idVrstaStudija = s.vrstaStudija.idVrstaStudija AND " +
			     	   	    "sp.idStudijskiProgram = s.studijskiProgram.idStudijskiProgram AND " +
			     	   	    "s.idStudija = l.studij.idStudija AND " +
			     	   	    "sm.idStudijskaSmer = :studijskaSmer AND " +
			     	   	    "vs.idVrstaStudija = :vrstaStudija AND " +
			     	   	    "sp.idStudijskiProgram = :studijskiProgram AND " +
			     	   	    "l.studijskoLeto = :studijskoLeto";
				
				q = em.createQuery(query);
				q.setParameter("studijskaSmer", studijskaSmer);
				q.setParameter("vrstaStudija", vrstaStudija);
				q.setParameter("studijskiProgram", studijskiProgram);
				q.setParameter("studijskoLeto", studijskoLeto);
			
				List<Letnik> letniki = (List<Letnik>)q.getResultList();
				
				request.setAttribute("letniki", letniki);
				RequestDispatcher fw = request.getRequestDispatcher("Ajax/letnik.jsp");
				fw.forward(request, response);
			}
			
			///////////////////////// IZBRALI SMO ŠTUDIJSKI PROGRAM /////////////////////////
			else if (studijskiProgram != -1) {
				query = "SELECT " +
							"DISTINCT ss " +
						"FROM " +
							"Vrsta_studija vs, Studijski_program sp, Studijska_smer ss, Studij s " +
						"WHERE " +
							"vs.idVrstaStudija = s.vrstaStudija.idVrstaStudija AND " +
							"sp.idStudijskiProgram = s.studijskiProgram.idStudijskiProgram AND " +
							"ss.idStudijskaSmer = s.studijskaSmer.idStudijskaSmer AND " +
							"vs.idVrstaStudija = :vrstaStudija AND " +
							"sp.idStudijskiProgram = :studijskiProgram";
				
				q = em.createQuery(query);
				q.setParameter("vrstaStudija", vrstaStudija);
				q.setParameter("studijskiProgram", studijskiProgram);
				
				List<Studijska_smer> studijskeSmeri = (List<Studijska_smer>)q.getResultList();
				
				request.setAttribute("studijskeSmeri", studijskeSmeri);
				RequestDispatcher fw = request.getRequestDispatcher("Ajax/studijskaSmer.jsp");
				fw.forward(request, response);
			
			}
			
			///////////////////////// IZBRALI SMO VRSTO ŠTUDIJA /////////////////////////
			else if (vrstaStudija != -1) {
				query = "SELECT " +
							"DISTINCT sp " +
						"FROM " +
							"Vrsta_studija vs, Studijski_program sp, Studij s " +
						"WHERE " +
							"vs.idVrstaStudija = s.vrstaStudija.idVrstaStudija AND " +
							"sp.idStudijskiProgram = s.studijskiProgram.idStudijskiProgram AND " +
							"vs.idVrstaStudija = :vrstaStudija ";
				
				q = em.createQuery(query);
				q.setParameter("vrstaStudija", vrstaStudija);
				
				List<Studijski_program> studijskiProgrami = (List<Studijski_program>)q.getResultList();
				
				request.setAttribute("studijskiProgrami", studijskiProgrami);
				RequestDispatcher fw = request.getRequestDispatcher("Ajax/studijskiProgram.jsp");
				fw.forward(request, response);
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}
}
