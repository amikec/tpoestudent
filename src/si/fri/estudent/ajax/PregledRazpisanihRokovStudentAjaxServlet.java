package si.fri.estudent.ajax;

import java.io.IOException;
import si.fri.estudent.utilities.*;

import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import si.fri.estudent.jpa.Izpitni_rok;
import si.fri.estudent.jpa.Kombinacije_izvajalcev;
import si.fri.estudent.jpa.Ocena;
import si.fri.estudent.jpa.Oseba;
import si.fri.estudent.jpa.Predmet;
import si.fri.estudent.jpa.Predmet_izvajalec;
import si.fri.estudent.jpa.Predmetnik;
import si.fri.estudent.jpa.Student;

/**
 * Servlet implementation class PrijavaNaIzpitAjax
 */
@WebServlet("/PregledRazpisanihRokovStudentAjaxServlet")
public class PregledRazpisanihRokovStudentAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	EntityManager em;
	
	Oseba oseba;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PregledRazpisanihRokovStudentAjaxServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ce je bla pritisnjena povezava tiskanje
		if(request.getParameter("tiskanje")!=null){
			if(request.getSession().getAttribute("predmeti")!=null & request.getSession().getAttribute("datumi")!=null&request.getSession().getAttribute("leto")!=null){
				List<String> datumi = (List<String>)request.getSession().getAttribute("datumi");
				List<String> predmeti = (List<String>)request.getSession().getAttribute("predmeti");
				List<String> predavalnice = (List<String>)request.getSession().getAttribute("predavalnice");
				List<String> ure = (List<String>)request.getSession().getAttribute("ure");
				List<String> kombinacijeIzvajalcev = (List<String>)request.getSession().getAttribute("kombinacijeIzvajalcev");
				List<Integer> stevci = (List<Integer>)request.getSession().getAttribute("stevci");
				String leto=(String)request.getSession().getAttribute("leto");
				
				String[][] tabelca=new String[datumi.size()+1][5];
				tabelca[0][0]="Predmet";
				tabelca[0][1]="Datum";
				tabelca[0][2]="Predavalnica";
				tabelca[0][3]="Ura";
				tabelca[0][4]="Izvajalec/i";
				//for(int i=0;i<datumi.size();i++){
				//	tabelca[i+1][0]=predmeti.get(i);
				//	tabelca[i+1][1]=datumi.get(i);
				//}
				int count = 0;
				for(int i=0;i<datumi.size();i++){
					for(int j = 0; j < stevci.get(i); j++) {
						tabelca[i+1][0]=predmeti.get(i);
						tabelca[i+1][1]=datumi.get(i);
						tabelca[i+1][2]=predavalnice.get(i);
						tabelca[i+1][3]=ure.get(i);
						tabelca[i+1][4]=kombinacijeIzvajalcev.get(i+count);
						count++;
					}
					count=0;
				}
					
				
					
				Tiskanje tiskanje=new Tiskanje(request,response,"Pregled razpisanih rokov");
				tiskanje.dodajBesedilo("                                                                   "+
				"                 Študijsko leto "+leto);
				if(datumi.size()>0){
					tiskanje.dodajTabelo(tabelca, false, true);
				}else{
					tiskanje.dodajBesedilo("                                                               "+
				"                   Ni nobenih razpisanih rokov.");
				}
				tiskanje.dobiDocument();
			}
		}
		
		preusmeriNaServlet(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		
		HttpSession session = request.getSession();
		System.out.println("lala");
		oseba = (Oseba) session.getAttribute("uporabnik");
		
		JSONObject json = new JSONObject();	
		
		json = izpisiPredmete(request, response);
		System.out.println(json);
		response.setContentType("text/html; charset=utf-8");
		//preusmeriNaServlet(request, response);
		PrintWriter out = response.getWriter();
		out.print(json);
	}
	
	private void preusmeriNaServlet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		request.setCharacterEncoding("UTF-8");
		//HttpSession session = request.getSession();
		String url = "index.jsp?stran=pregledRazpisanihRokovStudent";
		
		EntityManager em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		
		//Oseba o = (Oseba) session.getAttribute("uporabnik");	
		RequestDispatcher dis = request.getRequestDispatcher(url);
		dis.forward(request, response);
		
	}
	
	private JSONObject izpisiPredmete(HttpServletRequest request, HttpServletResponse response){
		HttpSession session = request.getSession();
		
		oseba = (Oseba) session.getAttribute("uporabnik");
		
		String sql = "SELECT "+
					"DISTINCT ir " +
					"FROM "+
					"Oseba o, Student s, Vpisni_list vl, Predmetnik pr, Predmetnik_letnika prl, Ocena oc, Predmet p, Izpitni_rok ir "+
					"WHERE " +
					"s.oseba.idOseba = ?1 AND " +
					"s.idStudent = vl.student.idStudent AND " +
					"vl.idVpisni = pr.fkVpisniList AND " +
					"pr.fkPredmetnikLetnika = prl.idPredmetnikLetnika AND " +
					"prl.predmet.idPredmeta = p.idPredmeta AND " +
				    "p.idPredmeta = ir.predmet.idPredmeta AND " +
				 	"vl.studijskoLeto = ?2" +
				 	"ORDER BY p.nazivPredmet";
		
		Query query = em.createQuery(sql);
		query.setParameter(1, oseba.getIdOseba());	
		query.setParameter(2, request.getParameter("studijskoLeto"));
		
		List<Izpitni_rok> razpisaniPredmeti = query.getResultList();
		
		List<String> datumi = new ArrayList<String>();
		List<String> predmeti = new ArrayList<String>();
		List<String> ure = new ArrayList<String>();
		List<String> kombinacijeIzvajalcev = new ArrayList<String>();
		List<String> predavalnice = new ArrayList<String>();
		List<Integer> stevci = new ArrayList<Integer>();
				
		for(Izpitni_rok ir: razpisaniPredmeti){
			Predmet p = ir.getPredmet();
			
			// predavalnica
			String predavalnica = ir.getPredavalnica();
			
			//datumcki
			java.util.Date date = ir.getDatumIzpit();
			// konvert datume
			SimpleDateFormat df = new SimpleDateFormat();
			df.applyPattern("dd.MM.yyyy");
			// konvert ura
			SimpleDateFormat df2 = new SimpleDateFormat();
			df2.applyPattern("HH:mm");			
					
			String imePredmeta = p.getNazivPredmet();			
			
			Kombinacije_izvajalcev kombinacije = ir.getPredmetIzvajalec().getKombinacijeIzvajalcev();
			Oseba os1 = kombinacije.getOsebje1().getOseba();
			int i = 1;
			Oseba os2 = null;
			Oseba os3 = null;
			if(kombinacije.getOsebje2() != null) {
				os2 = kombinacije.getOsebje2().getOseba();
				kombinacijeIzvajalcev.add(os2.getIme() + " " + os2.getPriimek());
				i++;
			}
			
			if(kombinacije.getOsebje3() != null) {
				os3 = kombinacije.getOsebje3().getOseba();
				kombinacijeIzvajalcev.add(os3.getIme() + " " + os3.getPriimek());
				i++;
			}
				
			datumi.add(df.format(date));
			ure.add(df2.format(date));
			predmeti.add(imePredmeta);	
			predavalnice.add(predavalnica);	
			kombinacijeIzvajalcev.add(os1.getIme() + " " + os1.getPriimek());
			stevci.add(i);
		}
		
		
		
		//---------- za to da pol tiskanje lahko dobi te podatke
		request.getSession().setAttribute("predmeti", predmeti);
		request.getSession().setAttribute("datumi", datumi);
		request.getSession().setAttribute("predavalnice", predavalnice);
		request.getSession().setAttribute("ure", ure);
		request.getSession().setAttribute("kombinacijeIzvajalcev", kombinacijeIzvajalcev);
		request.getSession().setAttribute("stevci", stevci);
		request.getSession().setAttribute("leto", request.getParameter("studijskoLeto"));
		//----------
		
		JSONObject json = new JSONObject();
		json.accumulate("datumi", datumi);
		json.accumulate("predmeti", predmeti);
		json.accumulate("predavalnice", predavalnice);
		json.accumulate("ure", ure);
		json.accumulate("kombinacijeIzvajalcev", kombinacijeIzvajalcev);
		json.accumulate("stevci", stevci);
	//	System.out.println(json);
		if(json != null){
			json.put("headerPredmet", "Predmet");
			json.put("headerDatum", "Datum");
			json.put("headerPredavalnica", "Predavalnica");
			json.put("headerUra", "Ura");
			json.put("headerIzvajalec", "Izvajalec/i");
		}
		return json;
	}
}


