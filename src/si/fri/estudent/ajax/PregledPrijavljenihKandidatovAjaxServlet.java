package si.fri.estudent.ajax;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import si.fri.estudent.jpa.*;
import si.fri.estudent.utilities.Constants;
import si.fri.estudent.utilities.DataValidator;
import si.fri.estudent.utilities.Tiskanje;
import si.fri.estudent.utilities.Utilities;

/**
 * Servlet implementation class PregledPrijavljenihKandidatovAjaxServlet
 */
@WebServlet("/PregledPrijavljenihKandidatovAjaxServlet")
public class PregledPrijavljenihKandidatovAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private Oseba profesor = null;
	boolean izpis = false;
	boolean izpisOcen = false;
	
    private EntityManager getEntityManager(){
    	return Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
    }
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PregledPrijavljenihKandidatovAjaxServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int izpitniRok = -1;
		
		if(request.getParameter("izpis") != null)
			izpis = Boolean.parseBoolean(request.getParameter("izpis").toString());
		
		if (request.getParameter("izpitniRok") != null) {
			izpitniRok = Integer.parseInt(request.getParameter("izpitniRok"));
		} else {
			return;
		}
		EntityManager em = this.getEntityManager();
		
		Izpitni_rok ir = em.find(Izpitni_rok.class, izpitniRok);
		
		int letnik = letnik = Integer.parseInt(request.getParameter("letnik"));
		Letnik l = em.find(Letnik.class, letnik);
		
		Object[] rezultat = vrniPrijavljeneStudente(izpitniRok, l.getIdLetnik());
		
		LinkedList<Student> studenti = (LinkedList<Student>) rezultat[0];
		LinkedList<Ocena> ocene = (LinkedList<Ocena>) rezultat[1];
		
		List<int[]> stOpravljanjStudenti = vrniStOpravljanjStudentov(studenti, izpitniRok, l.getStudijskoLeto());
		
		Tiskanje tiskanje = null;
		
		if(izpis)
			tiskanje = new Tiskanje( request, response, "Seznam prijavljenih kandidatov");
		else
			tiskanje = new Tiskanje( request, response, "Izpis ocen pisnega dela izpita");
		
		
		Kombinacije_izvajalcev ki = ir.getPredmetIzvajalec().getKombinacijeIzvajalcev();
		String nazivPredmeta = ir.getPredmet().getNazivPredmet();
		String predavatelj = ki.getOsebje1().getOseba().getIme() + " " + ki.getOsebje1().getOseba().getPriimek();
		
		if(ki.getOsebje2() != null)
			predavatelj += ", " + ki.getOsebje2().getOseba().getIme() + " " + ki.getOsebje2().getOseba().getPriimek();
		if(ki.getOsebje3() != null)
			predavatelj += ", " + ki.getOsebje3().getOseba().getIme() + " " + ki.getOsebje3().getOseba().getPriimek();
		
		String besedilo = String.format("Predmet:   %s \n" +
										"Izvajalec: %s \n" +
										"Datum:     %s", nazivPredmeta, predavatelj, ir.getDatumIzpit());
		
		tiskanje.dodajBesedilo(besedilo);
		
		List<Integer> ponavljanja = new LinkedList<Integer>();
		
		for(Student s : studenti){
			int temp = 0;
			temp = getStPolagPonavljanj(ir.getPredmet().getIdPredmeta(), s.getIdStudent());
			
			ponavljanja.add(temp);
		}
		
		int steviloPolj = 5;
		if(!izpis)
			steviloPolj = 7;
		
		String[][] izpisZaTiskanje = new String[studenti.size() + 1][steviloPolj];
		izpisZaTiskanje[0][0] = "ZAPOREDNA ŠTEVILKA";
		izpisZaTiskanje[0][1] = "VPISNA ŠTEVILKA";
		izpisZaTiskanje[0][2] = "PRIIMEK IN IME";
		izpisZaTiskanje[0][3] = "ZAPOREDNA POLAGANJA";
		izpisZaTiskanje[0][4] = "ŠT. POLAGANJA";	
		
		if(!izpis){
			izpisZaTiskanje[0][5] = "OCENA";
			izpisZaTiskanje[0][6] = "BONUS";
		}
		
		String imeStudent = null;
		int [] stOpravljanj = null;
		Student student = null;
		Oseba oseba = null;
		
		for (int i = 0; i < studenti.size(); i++) {
			student = studenti.get(i);
			oseba = student.getOseba();
		
			int ponav = ponavljanja.get(i);
			
			stOpravljanj = stOpravljanjStudenti.get(i);
			
			if (oseba.getPriimekDekliski() == null || oseba.getPriimekDekliski().equals("")) {
				imeStudent = oseba.getPriimek() + " " + oseba.getIme();
			} else {
				imeStudent = oseba.getPriimek() + " " + oseba.getPriimekDekliski() + " " + oseba.getIme();
			}
			
			String opravljanja = "";
			
			izpisZaTiskanje[i + 1][0] = Integer.toString(i + 1);
			izpisZaTiskanje[i + 1][1] = student.getVpisnaSt();
			izpisZaTiskanje[i + 1][2] = imeStudent;
			

			opravljanja = String.format("%d", stOpravljanj[0]);
			

			izpisZaTiskanje[i + 1][3] = opravljanja;
			izpisZaTiskanje[i + 1][4] = Integer.toString(ponav);
			
			if(!izpis){
				izpisZaTiskanje[i + 1][5] = Double.toString(ocene.get(i).getOcena());
				izpisZaTiskanje[i + 1][6] = Double.toString(ocene.get(i).getOcenaBonus());
			}
		}
		
		tiskanje.dodajTabelo(izpisZaTiskanje, false, true);		
		tiskanje.zakljuci();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Oseba os = (Oseba) session.getAttribute("uporabnik");
		Vloga_oseba vloga = (Vloga_oseba) session.getAttribute("vloga");
				
		if(vloga.getIdVlogaOseba() == Constants.VLOGA_UCITELJ){
			profesor = os;
		}else if(vloga.getIdVlogaOseba() != Constants.VLOGA_REFERENTKA){
			response.sendRedirect("index.jsp/stran=login");
		}
		
		int idProfesor = -1;
		int vrstaStudija = -1;
		int studijskiProgram = -1;
		int studijskaSmer = -1;
		String studijskoLeto = null;
		int letnik = -1;
		int predmet = -1;
		int izpitniRok = -1;
		String[] vpisneStevilke = null;
		String[] oceneStudenti = null;
		String[] bonusTocke = null;
		
		if (request.getParameter("idProfesor") != null) {
			idProfesor = Integer.parseInt(request.getParameter("idProfesor"));
		}
		if (request.getParameter("vrstaStudija") != null) {
			vrstaStudija = Integer.parseInt(request.getParameter("vrstaStudija"));
		}
		if (request.getParameter("studijskiProgram") != null) {
			studijskiProgram = Integer.parseInt(request.getParameter("studijskiProgram"));
		}
		if (request.getParameter("studijskaSmer") != null) {
			studijskaSmer = Integer.parseInt(request.getParameter("studijskaSmer"));
		}
		if (request.getParameter("studijskoLeto") != null) {
			studijskoLeto = request.getParameter("studijskoLeto");
		}
		if (request.getParameter("letnik") != null) {
			letnik = Integer.parseInt(request.getParameter("letnik"));
		}
		if (request.getParameter("predmet") != null) {
			predmet = Integer.parseInt(request.getParameter("predmet"));
		}
		if (request.getParameter("izpitniRok") != null) {
			izpitniRok = Integer.parseInt(request.getParameter("izpitniRok"));
		}
		if (request.getParameterValues("vpisneStevilke[]") != null) {
			vpisneStevilke = request.getParameterValues("vpisneStevilke[]");
		}
		if (request.getParameterValues("ocene[]") != null) {
			oceneStudenti = request.getParameterValues("ocene[]");
		}
		if(request.getParameterValues("bonusTocke[]") != null){
			bonusTocke = request.getParameterValues("bonusTocke[]");
		}
		
		EntityManager em = null;
		String query = null;
		Query q = null;
		String url = "";
		
		try{
			em = this.getEntityManager();
			
			/////////////// POSODABLJANJE OCEN /////////////////
			if(vpisneStevilke != null && oceneStudenti != null){
				String result = zapisiOcene(vpisneStevilke, oceneStudenti, bonusTocke, izpitniRok);
				
				request.setAttribute("uspesnostPosodabljanja", result);
			}
			
			/////////////// IZBRALI SMO IZPITNI ROK /////////////////
			if (izpitniRok != -1) {

				Object[] rezultat = vrniPrijavljeneStudente(izpitniRok, letnik);
				
				Izpitni_rok ir = em.find(Izpitni_rok.class, izpitniRok);
				Letnik l = em.find(Letnik.class, letnik);
				LinkedList<Student> studenti = (LinkedList<Student>) rezultat[0];
				LinkedList<Ocena> ocene = (LinkedList<Ocena>) rezultat[1];
				List<int[]> stOpravljanjStudenti = vrniStOpravljanjStudentov(studenti, izpitniRok, l.getStudijskoLeto());
				
				List<Integer> ponavljanjaPolaganja = new LinkedList<Integer>();				
				
				for(Student student : studenti){
					int polaganjaPonavljanja = 0;
					polaganjaPonavljanja = getStPolagPonavljanj(ir.getPredmet().getIdPredmeta(), student.getIdStudent());
					ponavljanjaPolaganja.add(polaganjaPonavljanja);
				}
				
				request.setAttribute("polaganjaPonavljanja", ponavljanjaPolaganja);
				request.setAttribute("ir", ir);
				request.setAttribute("studenti", studenti);
				
				request.setAttribute("ocene", ocene);
				request.setAttribute("stOpravljanjStudenti", stOpravljanjStudenti);
				url = "Ajax/seznamPrijavljenih.jsp";
			}
			
			/////////////// IZBRALI SMO PREDMET /////////////////
			else if (predmet != -1) {
				List<Izpitni_rok> izpitniRoki = vrniIzpitneRoke(idProfesor, predmet);
				
				request.setAttribute("izpitniRoki", izpitniRoki);
				url = "Ajax/izpitniRok.jsp";
			}
			
			/////////////// IZBRALI SMO LETNIK /////////////////
			else if (letnik != -1) {
				List<Predmet> predmeti = vrniPredmete(idProfesor, letnik);
				
				request.setAttribute("predmeti", predmeti);
				url = "Ajax/predmet.jsp";
			}
			
			/////////////// IZBRALI SMO ŠTUDIJSKO SMER IN ŠTUDIJSKO LETO /////////////////
			else if (studijskoLeto != null) {
				List<Letnik> letniki = vrniLetnike(idProfesor, vrstaStudija, studijskiProgram, studijskaSmer, studijskoLeto);
				
				request.setAttribute("letniki", letniki);
				url = "Ajax/letnik.jsp";
			}
			
			///////////////////////// IZBRALI SMO ŠTUDIJSKI PROGRAM /////////////////////////
			else if (studijskiProgram != -1) {
				List<Studijska_smer> studijskeSmeri = vrniStudijskeSmeri(idProfesor, vrstaStudija, studijskiProgram);
				
				request.setAttribute("studijskeSmeri", studijskeSmeri);
				url = "Ajax/studijskaSmer.jsp";
			}
			
			///////////////////////// IZBRALI SMO VRSTO ŠTUDIJA /////////////////////////
			else if (vrstaStudija != -1) {
				List<Studijski_program> studijskiProgrami = vrniStudijskePrograme(idProfesor, vrstaStudija);
				
				request.setAttribute("studijskiProgrami", studijskiProgrami);
				url = "Ajax/studijskiProgram.jsp";
			}
			
			///////////////////////// IZBRALI SMO PROFESORJA /////////////////////////
			else if (idProfesor != -1) {
				List<Vrsta_studija> vrsteStudija = vrniVrsteStudija(idProfesor);
				
				request.setAttribute("vrsteStudija", vrsteStudija);
				url = "Ajax/vrstaStudija.jsp";
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
		}
			
		RequestDispatcher fw = request.getRequestDispatcher(url);
		fw.forward(request, response);
	}
	
	/////////////////////////////////////////////////////////////////// METODE ///////////////////////////////////////////////////////////////////
	
	private Object[] vrniPrijavljeneStudente(int izpitniRok, int letnik) {		
		try{
			String sql = "SELECT " +
							"DISTINCT st, oc " +
						 "FROM " +
						 	"Izpitni_rok ir, Student st, Vpisni_list vpList, Predmetnik pred, Predmetnik_letnika predL, Ocena oc " +
						 "WHERE " +
						 	"st = vpList.student AND " +
						 	"vpList.idVpisni = pred.fkVpisniList AND " +
						 	"vpList.letnik.idLetnik = :letnik AND " +
						 	"pred.fkPredmetnikLetnika = predL.idPredmetnikLetnika AND " +
						 	"predL.predmet = ir.predmet AND " +
						 	"ir.idRok = :idIzpitniRok AND " +
						 	"pred.idPredmetnik = oc.fkPredmetnik AND " +
						 	"oc.fkIzpitniRok = ir.idRok AND " +
						 	"oc.aktivna = 1 " +
						 "ORDER BY st.oseba.priimek";
			
			Query q = this.getEntityManager().createQuery(sql);
			q.setParameter("idIzpitniRok", izpitniRok);
			q.setParameter("letnik", letnik);
			
			//ob[0] = student, ob[1] = ocena
			List<Object[]> rezultat = q.getResultList();
			List<Student> studenti = new LinkedList<Student>();
			List<Ocena> ocene = new LinkedList<Ocena>();
			
			for(Object[] ob : rezultat){
				studenti.add((Student) ob[0]);
				ocene.add((Ocena) ob[1]);
			}
			
			return new Object[] {studenti, ocene};
			
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	private List<int[]> vrniStOpravljanjStudentov(List<Student> studenti, int idIzpitniRok, String leto){
		List<int[]> stOpravljanj = new LinkedList<int[]>();
		
		Izpitni_rok ir = this.getEntityManager().find(Izpitni_rok.class, idIzpitniRok);
		Predmet predmet = ir.getPredmet();
		
		for(Student student : studenti){
			int[] opravljanja = new int[] {opravljanjaLetos(predmet, leto ,student.getOseba()), 
					opravljanjaLetos(predmet, "", student.getOseba())};
			stOpravljanj.add(opravljanja); 
		}
		
		return stOpravljanj;
	}
	
	private int opravljanjaLetos(Predmet p, String leto, Oseba oseba){
		
		EntityManager em = this.getEntityManager();
		
		String sql = "SELECT "+
				"DISTINCT oc " +
			"FROM "+
				"Oseba o, Student s, Vpisni_list vl, Predmetnik pr, Predmet p, Predmetnik_letnika prl, Ocena oc "+
			"WHERE " +
				"s.oseba.idOseba = ?1 AND " +
				"s.idStudent = vl.student.idStudent AND " +
				"vl.idVpisni = pr.fkVpisniList AND " +
				"pr.fkPredmetnikLetnika = prl.idPredmetnikLetnika AND " +
				"p.idPredmeta = prl.predmet.idPredmeta AND " +
				"p.idPredmeta = ?2 AND " +
				"pr.idPredmetnik = oc.fkPredmetnik AND " +
				"oc.aktivna = 1";
		
		if(!leto.isEmpty())
				sql += " AND vl.studijskoLeto = ?3";
		
		Query query = em.createQuery(sql); 
		
		query.setParameter(1, oseba.getIdOseba());
		query.setParameter(2, p.getIdPredmeta());
		
		if(!leto.isEmpty())
			query.setParameter(3, leto);
		
		List<Ocena>ocene = (List<Ocena>) query.getResultList();
		
		Predmetnik predmetnik = null;
		int max = 0;
		
		for(Ocena o : ocene){
			if(o.getStPolaganja() > max && leto.isEmpty())
					max = o.getStPolaganja();
			
			if(o.getStPolaganjaLetos() > max && !leto.isEmpty())
					max = o.getStPolaganjaLetos();
		}
		
		return max;
	}

	private List<Izpitni_rok> vrniIzpitneRoke(int idProfesor, int idPredmeta) {
		try{
			String sql = "SELECT " +
								"DISTINCT ir " +
						 "FROM " +
						 		"Predmet p, Predmet_izvajalec izPr, Izpitni_rok ir, " +
						 		"Kombinacije_izvajalcev kombIz, Osebje os " +
						 "WHERE " +
						 		"izPr = ir.predmetIzvajalec AND " +
						 		"izPr.kombinacijeIzvajalcev = kombIz AND " +
						 		" ( " +
						 			"kombIz.osebje1 = os OR " +
						 			"kombIz.osebje2 = os OR " +
						 			"kombIz.osebje3 = os " +
						 		" ) AND " +
						 		"os.oseba.idOseba = :oseba AND " +
						 		"izPr.predmet.idPredmeta = :idPredmeta	" +
						 "ORDER BY " +
						 		"ir.datumIzpit";
			
			Query q = this.getEntityManager().createQuery(sql);
			
			q.setParameter("oseba", idProfesor);
			q.setParameter("idPredmeta", idPredmeta);
			
			List<Izpitni_rok> izpitniRoki = q.getResultList();
			return izpitniRoki;
			
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	private List<Predmet> vrniPredmete(int idProfesor, int idLetnik) {
		try{
			String sql = "SELECT " +
							"DISTINCT pr " +
						 "FROM " +
						 	"Predmetnik_letnika predL, Predmet pr, Predmet_izvajalec prIz, Kombinacije_izvajalcev kombIz, " +
						 	"Osebje os " +
						 "WHERE " +
						 	"prIz.predmet = pr AND " +
						 	"predL.predmet = pr AND " +
						 	"prIz.status = 1 AND " +
						 	"prIz.kombinacijeIzvajalcev = kombIz AND " +
						 	"kombIz.status = 1 AND " +
						 	" ( " +
						 		"kombIz.osebje1 = os OR kombIz.osebje2 = os OR kombIz.osebje3 = os " +
						 	" ) AND " +
						 	"os.oseba.idOseba = :idProfesor AND " +
						 	"predL.letnik.idLetnik = :idLetnik " +
						 "ORDER BY pr.nazivPredmet";
			
			Query query = this.getEntityManager().createQuery(sql);
			
			query.setParameter("idProfesor", idProfesor);
			query.setParameter("idLetnik", idLetnik);
			
			List<Predmet> predmeti = (List<Predmet>)query.getResultList();
			return predmeti;
			
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	private List<Letnik> vrniLetnike(int idProfesor, int vrstaStudija, int studijskiProgram, int studijskaSmer, String studijskoLeto) {
		try {
			
			EntityManager em = this.getEntityManager();
			
			String sql = "SELECT " +
							"DISTINCT l " +
						 "FROM " +
							"Vrsta_studija vs, Studijski_program sp, Studijska_smer ss, Studij st, " +
							"Letnik l, Predmetnik_letnika predL, Predmet pr, " +
							"Predmet_izvajalec prIz, Kombinacije_izvajalcev kombIz, Osebje os " +
						 "WHERE " +
							"st.vrstaStudija.idVrstaStudija = :vrstaStudija AND " +
							"st.studijskiProgram.idStudijskiProgram = :studijskiProgram AND " +
							"st.studijskaSmer.idStudijskaSmer = :studijskaSmer AND " +
							"st.idStudija = l.studij.idStudija AND " +
							"predL.letnik = l AND " +
							"predL.predmet.idPredmeta = prIz.predmet.idPredmeta AND " +
				  	  		"prIz.kombinacijeIzvajalcev = kombIz AND " +
				  	  		"(" +
				  	  			"kombIZ.osebje1 = os OR kombIz.osebje2 = os OR kombIz.osebje3 = os" +
				  	  		") AND " +
				  	  		"os.oseba.idOseba = :profesor AND " +
			     	   	    "predL.letnik.studijskoLeto = :studijskoLeto " +
			     	   	  "ORDER BY l.nazivLetnik";
		
			Query q = em.createQuery(sql);
			q.setParameter("profesor", idProfesor);
			q.setParameter("vrstaStudija", vrstaStudija);
			q.setParameter("studijskiProgram", studijskiProgram);
			q.setParameter("studijskaSmer", studijskaSmer);
			q.setParameter("studijskoLeto", studijskoLeto);

			List<Letnik> letniki = (List<Letnik>)q.getResultList();
			return letniki;
			
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	private List<Studijska_smer> vrniStudijskeSmeri(int idProfesor, int vrstaStudija, int studijskiProgram) {
		try {
			
			EntityManager em = this.getEntityManager();
			
			String sql = "SELECT " +
							"DISTINCT ss " +
						 "FROM " +
							"Vrsta_studija vs, Studijski_program sp, Studijska_smer ss, Studij st, " +
							"Letnik l, Predmetnik_letnika predL, Predmet pr, " +
							"Predmet_izvajalec prIz, Kombinacije_izvajalcev kombIz, Osebje os " +
						 "WHERE " +
							"vs.idVrstaStudija = st.vrstaStudija.idVrstaStudija AND " +
							"sp.idStudijskiProgram = st.studijskiProgram.idStudijskiProgram AND " +
							"ss.idStudijskaSmer = st.studijskaSmer.idStudijskaSmer AND " +
							"st.idStudija = predL.letnik.studij.idStudija AND " +
							"predL.predmet.idPredmeta = prIz.predmet.idPredmeta AND " +
				  	  		"prIz.kombinacijeIzvajalcev = kombIz AND " +
				  	  		"(" +
				  	  			"kombIZ.osebje1 = os OR kombIz.osebje2 = os OR kombIz.osebje3 = os" +
				  	  		") AND " +
				  	  		"os.oseba.idOseba = :profesor AND " +
							"vs.idVrstaStudija = :vrstaStudija AND " +
							"sp.idStudijskiProgram = :studijskiProgram " +
						"ORDER BY ss.nazivStudijskaSmer";
		
			Query q = em.createQuery(sql);
			q.setParameter("profesor", idProfesor);
			q.setParameter("vrstaStudija", vrstaStudija);
			q.setParameter("studijskiProgram", studijskiProgram);
			
			List<Studijska_smer> studijskeSmeri = (List<Studijska_smer>)q.getResultList();
			return studijskeSmeri;
			
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	private List<Studijski_program> vrniStudijskePrograme(int idProfesor, int vrstaStudija) {
		try {
			
			EntityManager em = this.getEntityManager();
			
			String sql = "SELECT " +
						 	"DISTINCT sp " +
						 "FROM " +
							"Vrsta_studija vs, Studijski_program sp, Studij st, Letnik l, " +
							"Predmetnik_letnika predL, Predmet pr, Predmet_izvajalec prIz, " +
							"Kombinacije_izvajalcev kombIz, Osebje os " +
						 "WHERE " +
							"vs.idVrstaStudija = st.vrstaStudija.idVrstaStudija AND " +
							"sp.idStudijskiProgram = st.studijskiProgram.idStudijskiProgram AND " +
							"st.idStudija = predL.letnik.studij.idStudija AND " +
							"predL.predmet.idPredmeta = prIz.predmet.idPredmeta AND " +
				  	  		"prIz.kombinacijeIzvajalcev = kombIz AND " +
				  	  		"(" +
				  	  			"kombIZ.osebje1 = os OR kombIz.osebje2 = os OR kombIz.osebje3 = os" +
				  	  		") AND " +
				  	  		"os.oseba.idOseba = :profesor AND " +
							"vs.idVrstaStudija = :vrstaStudija " +
						"ORDER BY sp.nazivStudijskiProgram";
		
			Query q = em.createQuery(sql);
			q.setParameter("profesor", idProfesor);
			q.setParameter("vrstaStudija", vrstaStudija);
			
			List<Studijski_program> studijskiProgrami = (List<Studijski_program>)q.getResultList();
			return studijskiProgrami;
			
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	private List<Vrsta_studija> vrniVrsteStudija(int idProfesor) {
		try {
			String sql = "SELECT " +
								"DISTINCT vrSt " +
						  "FROM " +
						  		"Vrsta_studija vrSt, Studij st, Letnik l, Predmetnik_letnika predL, " +
						  		"Predmet pr, Predmet_izvajalec prIz, Kombinacije_izvajalcev kombIz, " +
						  		"Osebje os " +
					  	  "WHERE " +
					  	  		"vrSt.idVrstaStudija = predL.letnik.studij.vrstaStudija.idVrstaStudija AND " +
					  	  		"predl.predmet.idPredmeta = prIz.predmet.idPredmeta AND " +
					  	  		"prIz.kombinacijeIzvajalcev = kombIz AND " +
					  	  		"( " +
					  	  			" kombIZ.osebje1 = os OR kombIz.osebje2 = os OR kombIz.osebje3 = os " +
					  	  		") " +
					  	  		" AND os.oseba.idOseba = :profesor " +
					  	  	"ORDER BY " +
					  	  		"vrSt.nazivVrstaStudija ";
					  	  		
			Query query = this.getEntityManager().createQuery(sql);
			query.setParameter("profesor", idProfesor);
			
			List<Vrsta_studija> vrsteStudija = query.getResultList();
			return vrsteStudija;
			
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}	
	}		
	
	private String zapisiOcene(String[] vpisne, String[] ocene, String[] bonusTocke, int izpitniRokId){
		
		EntityManager em = this.getEntityManager();
		
		try{
			
			
			if(!em.getTransaction().isActive())
				em.getTransaction().begin();
			
			String sql = "";
			Query query;
			Ocena prijava;
			Double ocena = 0.0;
			
			for(int i = 0; i < vpisne.length; i ++){
								
				sql = "SELECT " +
							"DISTINCT oc " +
					  "FROM " +
					  		"Student st, Ocena oc, Predmetnik pr, Izpitni_rok ir, Vpisni_list vl " +
					  "WHERE " +
					  		"st.vpisnaSt = :vpisna AND " +
					  		"st = vl.student AND " +
					  		"vl.idVpisni = pr.fkVpisniList AND " +
					  		"pr.idPredmetnik = oc.fkPredmetnik AND " +
					  		"oc.fkIzpitniRok = ir.idRok AND " +
					  		"ir.idRok = :idRok";
				
				query = em.createQuery(sql);
				query.setParameter("vpisna", vpisne[i]);
				query.setParameter("idRok", izpitniRokId);
				prijava = (Ocena) query.getSingleResult();
				
				DataValidator validator = new DataValidator();
				Izpitni_rok izpitniRok = em.find(Izpitni_rok.class, izpitniRokId);
				
				Double test = -100.0;
				Double bonus = prijava.getOcenaBonus();
				
				try{
					
					bonus = Double.parseDouble(bonusTocke[i]);
					
					if(ocene[i].equals("VP") || ocene[i].equals("vp"))
						ocena = -1.0;
					else
						ocena = Double.parseDouble(ocene[i]);
					
				}catch(Exception e){
					e.printStackTrace();
					return "<span class='error'>Napačen format ocene v vrstici " + (i+1) + "</span>";
				}
				
				if(ocena > izpitniRok.getMozneTocke() || ocena < -1.0){
					return "<span class='error'>Število točk ni znotraj dovoljenih meja (vrstica " + (i+1) + " )</span>";
				}
				
				Predmetnik predmetnik = em.find(Predmetnik.class, prijava.getFkPredmetnikLetnika());
				Vpisni_list vl = em.find(Vpisni_list.class, predmetnik.getFkVpisniList());
				
				int maxLetos = opravljanjaLetos(izpitniRok.getPredmet(), vl.getStudijskoLeto(), vl.getStudent().getOseba());
				int maxSkupaj = opravljanjaLetos(izpitniRok.getPredmet(), "", vl.getStudent().getOseba());
				
				if((prijava.getOcena() == 0 && ocena > 0) || (ocena > 0 && prijava.getOcena() == -1)){
					prijava.setStPolaganja(maxSkupaj + 1);
					prijava.setStPolaganjaLetos(maxLetos +1);
				}else if(prijava.getOcena() > 0 && ocena == -1 && prijava.getAktivna() == true){
					prijava.setStPolaganja(0);
					prijava.setStPolaganjaLetos(0);
				}
				
				prijava.setOcenaBonus(bonus);
				
				if(ocena == -1){					
					prijava.setAktivna(false);
					prijava.setOcena(-1);
				}else{
					prijava.setOcena(ocena);
					prijava.setAktivna(true);
				}
				
				this.getEntityManager().merge(prijava);				
				this.getEntityManager().merge(predmetnik);
				
			}
			
			em.getTransaction().commit();
			
		}catch(Exception e){
			e.printStackTrace();
			em.getTransaction().rollback();
			
			return "<span class='error'>Operacija ni uspela.</span>";
		}
		
		return "Ocene posodobljene.";
	}
	
	private int getStPolagPonavljanj(int predmet, int student){
		try {
			String sql = "SELECT " +
					"DISTINCT oc " +
			  "FROM " +
			  		"Ocena oc, Predmetnik pred, Vpisni_list vl, Vrsta_vpisa vp, Predmetnik_letnika predL, Predmet p " +
		  	  "WHERE " +
		  	  		"vl.student.idStudent = :student AND " +
		  	  		"vl.vrstaVpisa.idVrstaVpisa = :vrstaVpisa AND " +
		  	  		"vl.idVpisni = pred.fkVpisniList AND " +
		  	  		"pred.idPredmetnik = oc.fkPredmetnik AND " +
		  	  		"pred.fkPredmetnikLetnika = predL.idPredmetnikLetnika AND " +
		  	  		"predL.predmet.idPredmeta = :predmet AND " +
		  	  		"oc.aktivna = 1 AND " +
		  	  		"( " +
		  	  			" oc.ocenaKoncna > 0 OR " +
		  	  			" oc.ocenaIzpit > 0 OR " +
		  	  			" oc.ocenaVaje > 0 OR " +
		  	  			" oc.ocena > 0 " +
		  	  		")";
					  	  		
			Query query = this.getEntityManager().createQuery(sql);
			query.setParameter("predmet", predmet);
			query.setParameter("student", student);
			query.setParameter("vrstaVpisa", Constants.VPIS_PONAVLJANJE);
			
			List<Ocena> ocene = query.getResultList();
			
			int st = 0;
			
			for(Ocena oc : ocene)
				st += 1;
					
			return st;
			
		} catch(Exception e){
			e.printStackTrace();
			return 0;
		}	
	}

	
}


