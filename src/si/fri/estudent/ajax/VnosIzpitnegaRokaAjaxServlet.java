package si.fri.estudent.ajax;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import si.fri.estudent.jpa.Kombinacije_izvajalcev;
import si.fri.estudent.jpa.Oseba;
import si.fri.estudent.jpa.Osebje;
import si.fri.estudent.jpa.Predmet;
import si.fri.estudent.jpa.Predmet_izvajalec;

import net.sf.json.JSONObject;

/**
 * Servlet implementation class VnosIzpitnegaRokaAjaxServlet
 */
@WebServlet("/VnosIzpitnegaRokaAjaxServlet")
public class VnosIzpitnegaRokaAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VnosIzpitnegaRokaAjaxServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		EntityManager em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		
		String sifra = request.getParameter("sifraPredmeta");	// preberemo šifro predmeta
		
		List<Integer> idKombinacije = new ArrayList<Integer>();		// list v katerem hranimo id-je kombinacije za izbran predmet
	    List<String> listPredavatelji = new ArrayList<String>();	// list v katerem hranimo nize vseh izvajalcev v kombinaciji za izbran predmet 
		
		if(Integer.parseInt(sifra) != -1) {
		
			// vrnemo vse osebe, katere izvaljalci predmeta z podano šifro predmeta
			Query q = em.createQuery("SELECT pr FROM Predmet pr WHERE pr.sifraPredmeta=:sifra");
		    q.setParameter("sifra", sifra);
		    Predmet p=(Predmet)q.getSingleResult();
		    List<Predmet_izvajalec> pi=p.getPredmetIzvajalecs();	    
		    ArrayList<Oseba> osebe=new ArrayList<Oseba>();
		    
		    for(Predmet_izvajalec prei : pi){	  
		    	if((prei.getKombinacijeIzvajalcev().isStatus() == true) && (prei.isStatus() == true)) {
			    	Kombinacije_izvajalcev ki  = prei.getKombinacijeIzvajalcev();
			    	idKombinacije.add(ki.getIdKombinacijeIzvajalcev());
			    	
			    	Oseba os;
			    	Osebje osebje;
			    	
			    	osebje=ki.getOsebje1();
			    	if(osebje!=null){
			    		
			    		os=osebje.getOseba();
			    		if(os!=null){
			    			osebe.add(os);
			    		}
			    	}
			    	
			    	osebje=ki.getOsebje2();
			    	if(osebje!=null){
			  
			    		os=osebje.getOseba();
			    		if(os!=null){
			    			osebe.add(os);
			    		}
			    	}
			    	
			    	osebje=ki.getOsebje3();
			    	if(osebje!=null){
			    	
			    		os=osebje.getOseba();
			    		if(os!=null){
			    			osebe.add(os);
			    		}
			    	}
			    	
			    	// sestavljanje izpisa profesorjev
					String predavatelji = "";
					for(Oseba oseba : osebe) {
						predavatelji += oseba.getIme() + " " + oseba.getPriimek() + ", ";
					}
					if(predavatelji.length() > 2) {
						listPredavatelji.add(predavatelji.substring(0, predavatelji.length() - 2));
					}
					
					osebe.clear();
		    	}
		    }
	    
		}
				
		JSONObject json = new JSONObject();
		json.accumulate("listPredavatelji", listPredavatelji); 
		json.accumulate("idKombinacije", idKombinacije);
		
		response.setContentType("text/html; charset=utf-8");   
		PrintWriter out = response.getWriter();
		out.print(json);
		
	}

}
