package si.fri.estudent.ajax;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import si.fri.estudent.jpa.Izpitni_rok;
import si.fri.estudent.jpa.Oseba;
import si.fri.estudent.jpa.Predmet;
import si.fri.estudent.jpa.Predmet_izvajalec;
import si.fri.estudent.utilities.Utilities;

/**
 * Servlet implementation class SpremembaIzpitnegaRokaAjaxServlet
 */
@WebServlet("/SpremembaIzpitnegaRokaAjaxServlet")
public class SpremembaIzpitnegaRokaAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SpremembaIzpitnegaRokaAjaxServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		EntityManager em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
		
		JSONObject json = new JSONObject();
		if(request.getParameter("dialog") != null) {

			
			int idIzpitnegaRoka = Integer.parseInt(request.getParameter("idIzpitnegaRoka"));
			String stevilo = "-1";
			
			try {
				
				Query q = em.createQuery(	"SELECT COUNT(oc) " +
											"FROM Ocena oc " +
											"WHERE oc.fkIzpitniRok = :id " +
											"AND oc.aktivna = true " +
											"AND oc.ocena > 0");
				q.setParameter("id", idIzpitnegaRoka);
				stevilo = q.getSingleResult().toString();
				
				if(Integer.parseInt(stevilo) > 0) {
					
					stevilo = "-2";
					
				} else {
					
					q = em.createQuery(	"SELECT COUNT(oc) " +
							"FROM Ocena oc " +
							"WHERE oc.fkIzpitniRok = :id " +
							"AND oc.aktivna = true " +
							"AND oc.ocena = 0");
					q.setParameter("id", idIzpitnegaRoka);
					stevilo = q.getSingleResult().toString();
				}
				
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		
			
			json.put("stPrijav", stevilo); 
		
		} else {
		
			String sifraPredmeta = request.getParameter("sifraPredmeta");	// preberemo šifro predmeta
			String sifraKombinacije = request.getParameter("sifaKombinacije");	// preberemo šifro predmeta
			
			List<Integer> idIzpitnegaRoka = new ArrayList<Integer>();		// list v katerem hranimo id-je izpitnih rokov
		    List<String> listIzpitiniRoki = new ArrayList<String>();	// list v katerem hranimo datume izpitinih rokov 
			
		    
		    if(Integer.parseInt(sifraPredmeta) != -1 && Integer.parseInt(sifraKombinacije) != -1) {
		    	
		    	// vrnemo vse izpitne roke, kateri so razpisani za izpran predmet in izbrano kombinacijo
				Query q = em.createQuery(	"SELECT ir " +
											"FROM Izpitni_rok ir " +
											"WHERE ir.predmet.sifraPredmeta = :sifraP " +
											"AND ir.predmetIzvajalec.kombinacijeIzvajalcev.idKombinacijeIzvajalcev = :sifraK " +
											"AND ir.status != 0");
			    q.setParameter("sifraP", sifraPredmeta);
			    q.setParameter("sifraK", Integer.parseInt(sifraKombinacije));
			    List<Izpitni_rok> ir = (List<Izpitni_rok>)q.getResultList();
			    
			    for(Izpitni_rok i : ir) {
			    	idIzpitnegaRoka.add(i.getIdRok());
			    	String datum = Utilities.getDatumIzpita(i.getDatumIzpit());
			    	listIzpitiniRoki.add(datum);
			    }
			    
		    }
		    
			json.accumulate("listIzpitniRoki", listIzpitiniRoki); 
			json.accumulate("idIzpitnegaRoka", idIzpitnegaRoka);
		}
		
		response.setContentType("text/html; charset=utf-8");   
		PrintWriter out = response.getWriter();
		out.print(json);
		
		
	}

}
