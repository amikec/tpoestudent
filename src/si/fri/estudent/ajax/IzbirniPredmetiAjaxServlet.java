package si.fri.estudent.ajax;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import si.fri.estudent.jpa.Izpitni_rok;
import si.fri.estudent.jpa.Letnik;
import si.fri.estudent.jpa.Modul;
import si.fri.estudent.jpa.Ocena;
import si.fri.estudent.jpa.Predmet;
import si.fri.estudent.jpa.Student;
import si.fri.estudent.jpa.Predmetnik;
import si.fri.estudent.jpa.Predmetnik_letnika;
import si.fri.estudent.jpa.Studijska_smer;
import si.fri.estudent.jpa.Studijski_program;
import si.fri.estudent.jpa.Vpisni_list;
import si.fri.estudent.jpa.Vrsta_studija;

/**
 * Servlet implementation class IzbirniPredmetiAjaxServlet
 */
@WebServlet("/IzbirniPredmetiAjaxServlet")
public class IzbirniPredmetiAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private EntityManager getEntityManager(){
		return Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
	}
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IzbirniPredmetiAjaxServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		int idStudent = -1;
		int vrstaStudija = -1;
		int studijskiProgram = -1;
		int studijskaSmer = -1;
		int letnik = -1;
		String izbirniPredmetnikModul = null;
		
		if (request.getParameter("idStudent") != null) {
			idStudent = Integer.parseInt(request.getParameter("idStudent"));
		}
		if (request.getParameter("vrstaStudija") != null) {
			vrstaStudija = Integer.parseInt(request.getParameter("vrstaStudija"));
		}
		if (request.getParameter("studijskiProgram") != null) {
			studijskiProgram = Integer.parseInt(request.getParameter("studijskiProgram"));
		}
		if (request.getParameter("studijskaSmer") != null) {
			studijskaSmer = Integer.parseInt(request.getParameter("studijskaSmer"));
		}
		if (request.getParameter("letnik") != null) {
			letnik = Integer.parseInt(request.getParameter("letnik"));
		}
		if (request.getParameter("izbirniPredmetnikModul") != null) {
			izbirniPredmetnikModul = request.getParameter("izbirniPredmetnikModul");
		}
		
		EntityManager em = null;
		String query = null;
		Query q = null;
		String url = "";
		
		try {
			em = this.getEntityManager();
			
			/*
			vrniVseModule(idLetnik)
			vrniStudentoveModule(idStudent, idLetnik)
			vrniPredmeteModulov(em, idStudent, idLetnik, idModula)
			neizbraniModuli(izbraniModuli, idLetnik)
			posodobiModule(idStudent, letnik, idModulaStari, idModulaNovi)
			
			vrniVseIzbirnePredmete(idLetnik)
			vrniStudentoveIzbirnePredmete(idStudent, idLetnik)
			neizbraniIzbirniPredmeti(izbraniIzbirni, idLetnik)
			posodobiIzbirnePredmete(idStudent, letnik, izbirniStari, izbirniNovi)
			*/
			
			/////////////// IZBRALI SMO IZBIRNI PREDMETNIK / MODUL /////////////////
			if (izbirniPredmetnikModul != null) {
				// UREJANJE IZBIRNEGA PREDMETNIKA
				if (izbirniPredmetnikModul.equals("izbirniPredmetnik")) {
					List<Predmetnik_letnika> izbraniIzbirni = vrniStudentoveIzbirnePredmete(idStudent, letnik);
					List<Predmetnik_letnika> neizbraniIzbirni = neizbraniIzbirniPredmeti(izbraniIzbirni, letnik);
					
					request.setAttribute("izbraniIzbirni", izbraniIzbirni);
					request.setAttribute("neizbraniIzbirni", neizbraniIzbirni);
					url = "Ajax/dragDropIzbirniPredmeti.jsp";
				// UREJANJE MODULOV
				} else if (izbirniPredmetnikModul.equals("modul")) {
					List<Modul> izbraniModuli = vrniStudentoveModule(idStudent, letnik);
					List<Modul> neIzbraniModuli = neizbraniModuli(izbraniModuli, letnik);
					
					request.setAttribute("neIzbraniModuli", neIzbraniModuli);
					request.setAttribute("izbraniModuli", izbraniModuli);
					url = "Ajax/dragDropModuli.jsp";
				}
			}
			
			/////////////// IZBRALI SMO ŠTUDIJSKO SMER /////////////////
			if (studijskaSmer != -1) {
				List<Letnik> letniki = vrniLetnik(idStudent, vrstaStudija, studijskiProgram, studijskaSmer);
				
				request.setAttribute("letniki", letniki);
				url = "Ajax/letnikIzbirni.jsp";
			}
			
			///////////////////////// IZBRALI SMO ŠTUDIJSKI PROGRAM /////////////////////////
			else if (studijskiProgram != -1) {
				List<Studijska_smer> studijskeSmeri = vrniStudijskeSmeri(idStudent, vrstaStudija, studijskiProgram);
				
				request.setAttribute("studijskeSmeri", studijskeSmeri);
				url = "Ajax/studijskaSmer.jsp";
			}
			
			///////////////////////// IZBRALI SMO VRSTO ŠTUDIJA /////////////////////////
			else if (vrstaStudija != -1) {
				List<Studijski_program> studijskiProgrami = vrniStudijskePrograme(idStudent, vrstaStudija);
				
				request.setAttribute("studijskiProgrami", studijskiProgrami);
				url = "Ajax/studijskiProgram.jsp";
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}
		}
			
		RequestDispatcher fw = request.getRequestDispatcher(url);
		fw.forward(request, response);
	}
	
	private List<Modul> neizbraniModuli(List<Modul> izbraniModuli, int idLetnik){
		
		List<Modul> vsiModuli = vrniVseModule(idLetnik);
		List<Modul> neizbraniModuli = new LinkedList();
		boolean izbran = false;
		
		for(Modul m : vsiModuli){
			
			izbran = false;
			
			for(Modul m2 : izbraniModuli){
				if(m.getIdModul() == m2.getIdModul())
					izbran = true;
			}
			
			if(izbran == false)
				neizbraniModuli.add(m);
		}
		
		return neizbraniModuli;
	}
	
	private List<Predmetnik_letnika> neizbraniIzbirniPredmeti(List<Predmetnik_letnika> izbraniIzbirni, int idLetnik){
		
		List<Predmetnik_letnika> vsiIzbirni = vrniVseIzbirnePredmete(idLetnik);
		List<Predmetnik_letnika> neizbraniIzbirni = new LinkedList();
		boolean izbran = false;
		
		for(Predmetnik_letnika p : vsiIzbirni){
			
			izbran = false;
			
			for(Predmetnik_letnika p2 : izbraniIzbirni){
				if(p.getIdPredmetnikLetnika() == p2.getIdPredmetnikLetnika())
					izbran = true;
			}
			
			if(izbran == false)
				neizbraniIzbirni.add(p);
		}
		
		return neizbraniIzbirni;
	}
	
	private List<Letnik> vrniLetnik(int idStudent, int vrstaStudija, int studijskiProgram, int studijskaSmer){
		try {
			String sql = "SELECT " +
					"DISTINCT l " +
			  "FROM " +
			  		"Student student, Vpisni_list vl, Letnik l, Studij studij," +
			  		"Vrsta_studija vs, Studijski_program sp, Studijska_smer ss " +
		  	  "WHERE " +
		  	  		"student.idStudent = :student AND " +
		  	  		"vl.student = student AND " +
		  	  		"vl.letnik = l AND " +
		  	  		"l.studij = studij AND " +
		  	  		"studij.vrstaStudija.idVrstaStudija = :vrstaStudija AND " +
		  	  		"studij.studijskiProgram.idStudijskiProgram = :studijskiProgram AND " +
		  	  		"studij.studijskaSmer.idStudijskaSmer =  :studijskaSmer";
		
			Query q = this.getEntityManager().createQuery(sql);
			q.setParameter("student", idStudent);
			q.setParameter("vrstaStudija", vrstaStudija);
			q.setParameter("studijskiProgram", studijskiProgram);
			q.setParameter("studijskaSmer", studijskaSmer);
			
			List<Letnik> letniki = (List<Letnik>)q.getResultList();
			
			return letniki;
			
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	private List<Studijska_smer> vrniStudijskeSmeri(int idStudent, int vrstaStudija, int studijskiProgram) {
		try {
			String sql = "SELECT " +
					"DISTINCT ss " +
			  "FROM " +
			  		"Student student, Vpisni_list vl, Letnik l, Studij studij," +
			  		"Vrsta_studija vs, Studijski_program sp, Studijska_smer ss " +
		  	  "WHERE " +
		  	  		"student.idStudent = :student AND " +
		  	  		"vl.student = student AND " +
		  	  		"vl.letnik = l AND " +
		  	  		"l.studij = studij AND " +
		  	  		"studij.vrstaStudija.idVrstaStudija = :vrstaStudija AND " +
		  	  		"studij.studijskiProgram.idStudijskiProgram = :studijskiProgram AND " +
		  	  		"studij.studijskaSmer =  ss";
		
			Query q = this.getEntityManager().createQuery(sql);
			q.setParameter("student", idStudent);
			q.setParameter("vrstaStudija", vrstaStudija);
			q.setParameter("studijskiProgram", studijskiProgram);
			
			List<Studijska_smer> studijskeSmeri = (List<Studijska_smer>)q.getResultList();
			return studijskeSmeri;
			
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	private List<Studijski_program> vrniStudijskePrograme(int idStudent, int vrstaStudija) {
		try {
			String sql = "SELECT " +
					"DISTINCT sp " +
			  "FROM " +
			  		"Student student, Vpisni_list vl, Letnik l, Studij studij," +
			  		"Vrsta_studija vs, Studijski_program sp " +
		  	  "WHERE " +
		  	  		"student.idStudent = :student AND " +
		  	  		"vl.student = student AND " +
		  	  		"vl.letnik = l AND " +
		  	  		"l.studij = studij AND " +
		  	  		"studij.vrstaStudija.idVrstaStudija = :vrstaStudija AND " +
		  	  		"studij.studijskiProgram = sp ";
		
			Query q = this.getEntityManager().createQuery(sql);
			q.setParameter("student", idStudent);
			q.setParameter("vrstaStudija", vrstaStudija);
			
			List<Studijski_program> studijskiProgrami = (List<Studijski_program>)q.getResultList();
			return studijskiProgrami;
			
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}	
	

	private List<Predmetnik_letnika> vrniVseIzbirnePredmete(int idLetnik){
		try {
			String sql = "SELECT " +
					"DISTINCT pred " +
			  "FROM " +
			  		"Student student, Vpisni_list vl, Letnik l, Studij studij," +
			  		"Vrsta_studija vs, Studijski_program sp, Studijska_smer ss, " +
			  		"Predmetnik_letnika pred " +
		  	  "WHERE " +
		  	  		"l.studij = studij AND " +
		  	  		"l = pred.letnik AND " +
		  	  		"pred.obvezniPredmet = 0 AND " +
		  	  		"pred.modul IS NULL AND " +
		  	  		"l.idLetnik = :letnik AND pred.status = 1 ";
					  	  		
			Query q = this.getEntityManager().createQuery(sql);
			q.setParameter("letnik", idLetnik);
			
			List<Predmetnik_letnika> predmeti = (List<Predmetnik_letnika>)q.getResultList();
			
			return predmeti;
			
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}	
	}
	
	private List<Predmetnik_letnika> vrniStudentoveIzbirnePredmete(int idStudent, int idLetnik){
		try {
			String sql = "SELECT " +
					"DISTINCT predL " +
			  "FROM " +
			  		"Student student, Vpisni_list vl, Letnik l, Studij studij," +
			  		"Vrsta_studija vs, Studijski_program sp, Studijska_smer ss, " +
			  		"Predmetnik_letnika predL, Predmetnik pred " +
		  	  "WHERE " +
		  	  		"student.idStudent = :student AND " +
		  	  		"student = vl.student AND " +
		  	  		"vl.idVpisni = pred.fkVpisniList AND " +
		  	  		"vl.letnik = l AND " +
		  	  		"pred.fkPredmetnikLetnika = predL.idPredmetnikLetnika AND " +
		  	  		"predL.modul IS NULL AND " +
		  	  		"predL.obvezniPredmet = 0 AND " +
		  	  		"l.idLetnik = :letnik AND predL.status = 1 ";
					  	  		
			Query q = this.getEntityManager().createQuery(sql);
			q.setParameter("student", idStudent);
			q.setParameter("letnik", idLetnik);
			
			List<Predmetnik_letnika> izbirniPredmeti = (List<Predmetnik_letnika>)q.getResultList();
			
			return izbirniPredmeti;
			
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}	
	}
	
	private List<Modul> vrniVseModule(int idLetnik){
		try {
			String sql = "SELECT " +
					"DISTINCT m " +
			  "FROM " +
			  		"Student student, Vpisni_list vl, Letnik l, Studij studij," +
			  		"Vrsta_studija vs, Studijski_program sp, Studijska_smer ss, " +
			  		"Predmetnik_letnika pred, Modul m " +
		  	  "WHERE " +
		  	  		"l.studij = studij AND " +
		  	  		"l = pred.letnik AND " +
		  	  		"pred.modul = m AND " +
		  	  		"l.idLetnik = :letnik AND " +
		  	  		"m.status = 1 AND pred.status = 1 ";
					  	  		
			Query q = this.getEntityManager().createQuery(sql);
			q.setParameter("letnik", idLetnik);
			
			List<Modul> moduli = (List<Modul>)q.getResultList();
			
			return moduli;
			
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}	
	}
	
	private List<Modul> vrniStudentoveModule(int idStudent, int idLetnik){
		try {
			String sql = "SELECT " +
					"DISTINCT m " +
			  "FROM " +
			  		"Student student, Vpisni_list vl, Letnik l, Studij studij," +
			  		"Vrsta_studija vs, Studijski_program sp, Studijska_smer ss, " +
			  		"Predmetnik_letnika predL, Predmetnik pred, Modul m " +
		  	  "WHERE " +
		  	  		"student.idStudent = :student AND " +
		  	  		"student = vl.student AND " +
		  	  		"vl.idVpisni = pred.fkVpisniList AND " +
		  	  		"vl.letnik = l AND " +
		  	  		"pred.fkPredmetnikLetnika = predL.idPredmetnikLetnika AND " +
		  	  		"predL.modul = m AND " +
		  	  		"l.idLetnik = :letnik and predL.status = 1 AND m.status = 1 ";
					  	  		
			Query q = this.getEntityManager().createQuery(sql);
			q.setParameter("student", idStudent);
			q.setParameter("letnik", idLetnik);
			
			List<Modul> moduli = (List<Modul>)q.getResultList();
			
			return moduli;
			
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}	
	}
}
