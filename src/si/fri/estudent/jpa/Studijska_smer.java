package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the Studijska_smer database table.
 * 
 */
@Entity
@Table(name="Studijska_smer")
public class Studijska_smer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_studijska_smer")
	private int idStudijskaSmer;

	@Column(name="kratica_studijska_smer")
	private String kraticaStudijskaSmer;

	@Column(name="naziv_studijska_smer")
	private String nazivStudijskaSmer;

	//bi-directional many-to-one association to Studij
	@OneToMany(mappedBy="studijskaSmer")
	private List<Studij> studijs;
	
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

    public Studijska_smer() {
    }

	public int getIdStudijskaSmer() {
		return this.idStudijskaSmer;
	}

	public void setIdStudijskaSmer(int idStudijskaSmer) {
		this.idStudijskaSmer = idStudijskaSmer;
	}

	public String getKraticaStudijskaSmer() {
		return this.kraticaStudijskaSmer;
	}

	public void setKraticaStudijskaSmer(String kraticaStudijskaSmer) {
		this.kraticaStudijskaSmer = kraticaStudijskaSmer;
	}

	public String getNazivStudijskaSmer() {
		return this.nazivStudijskaSmer;
	}

	public void setNazivStudijskaSmer(String nazivStudijskaSmer) {
		this.nazivStudijskaSmer = nazivStudijskaSmer;
	}

	public List<Studij> getStudijs() {
		return this.studijs;
	}

	public void setStudijs(List<Studij> studijs) {
		this.studijs = studijs;
	}
	
}