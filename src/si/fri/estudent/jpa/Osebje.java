package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the Osebje database table.
 * 
 */
@Entity
@Table(name="Osebje")
public class Osebje implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_osebje")
	private int idOsebje;

	@Column(name="sifra_predavatelja")
	private String sifraPredavatelja;

	//bi-directional many-to-one association to Kombinacije_izvajalcev
	@OneToMany(mappedBy="osebje1")
	private List<Kombinacije_izvajalcev> kombinacijeIzvajalcevs1;

	//bi-directional many-to-one association to Kombinacije_izvajalcev
	@OneToMany(mappedBy="osebje2")
	private List<Kombinacije_izvajalcev> kombinacijeIzvajalcevs2;

	//bi-directional many-to-one association to Kombinacije_izvajalcev
	@OneToMany(mappedBy="osebje3")
	private List<Kombinacije_izvajalcev> kombinacijeIzvajalcevs3;

	//bi-directional many-to-one association to Oseba
    @ManyToOne
	@JoinColumn(name="fk_oseba")
	private Oseba oseba;
    
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

    public Osebje() {
    }

	public int getIdOsebje() {
		return this.idOsebje;
	}

	public void setIdOsebje(int idOsebje) {
		this.idOsebje = idOsebje;
	}

	public String getSifraPredavatelja() {
		return this.sifraPredavatelja;
	}

	public void setSifraPredavatelja(String sifraPredavatelja) {
		this.sifraPredavatelja = sifraPredavatelja;
	}

	public List<Kombinacije_izvajalcev> getKombinacijeIzvajalcevs1() {
		return this.kombinacijeIzvajalcevs1;
	}

	public void setKombinacijeIzvajalcevs1(List<Kombinacije_izvajalcev> kombinacijeIzvajalcevs1) {
		this.kombinacijeIzvajalcevs1 = kombinacijeIzvajalcevs1;
	}
	
	public List<Kombinacije_izvajalcev> getKombinacijeIzvajalcevs2() {
		return this.kombinacijeIzvajalcevs2;
	}

	public void setKombinacijeIzvajalcevs2(List<Kombinacije_izvajalcev> kombinacijeIzvajalcevs2) {
		this.kombinacijeIzvajalcevs2 = kombinacijeIzvajalcevs2;
	}
	
	public List<Kombinacije_izvajalcev> getKombinacijeIzvajalcevs3() {
		return this.kombinacijeIzvajalcevs3;
	}

	public void setKombinacijeIzvajalcevs3(List<Kombinacije_izvajalcev> kombinacijeIzvajalcevs3) {
		this.kombinacijeIzvajalcevs3 = kombinacijeIzvajalcevs3;
	}
	
	public Oseba getOseba() {
		return this.oseba;
	}

	public void setOseba(Oseba oseba) {
		this.oseba = oseba;
	}
	
}