package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Predmetnik database table.
 * 
 */
@Entity
@Table(name="Predmetnik")
public class Predmetnik implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_predmetnik")
	private int idPredmetnik;

	@Column(name="fk_predmetnik_letnika")
	private int fkPredmetnikLetnika;

	@Column(name="fk_vpisni_list")
	private int fkVpisniList;

	@Column(name="st_opravljanj")
	private int stOpravljanj;
	
	@Column(name="koncna_ocena")
	private int koncnaOcena;
	
	@Column(name="status")
	private boolean status;
	
	@Column(name="ocena_vaje")
	private int ocenaVaje;
	
	@Column(name="ocena_izpit")
	private int ocenaIzpit;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

    public Predmetnik() {
    }

	public int getIdPredmetnik() {
		return this.idPredmetnik;
	}

	public void setIdPredmetnik(int idPredmetnik) {
		this.idPredmetnik = idPredmetnik;
	}

	public int getFkPredmetnikLetnika() {
		return this.fkPredmetnikLetnika;
	}

	public void setFkPredmetnikLetnika(int fkPredmetnikLetnika) {
		this.fkPredmetnikLetnika = fkPredmetnikLetnika;
	}

	public int getFkVpisniList() {
		return this.fkVpisniList;
	}

	public void setFkVpisniList(int fkVpisniList) {
		this.fkVpisniList = fkVpisniList;
	}

	public int getStOpravljanj() {
		return this.stOpravljanj;
	}

	public void setStOpravljanj(int stOpravljanj) {
		this.stOpravljanj = stOpravljanj;
	}

	public int getKoncnaOcena() {
		return koncnaOcena;
	}

	public void setKoncnaOcena(int koncnaOcena) {
		this.koncnaOcena = koncnaOcena;
	}

	public int getOcenaVaje() {
		return ocenaVaje;
	}

	public void setOcenaVaje(int ocenaVaje) {
		this.ocenaVaje = ocenaVaje;
	}

	public int getOcenaIzpit() {
		return ocenaIzpit;
	}

	public void setOcenaIzpit(int ocenaIzpit) {
		this.ocenaIzpit = ocenaIzpit;
	}

}