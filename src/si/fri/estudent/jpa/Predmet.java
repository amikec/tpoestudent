package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the Predmet database table.
 * 
 */
@Entity
@Table(name="Predmet")
public class Predmet implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_predmeta")
	private int idPredmeta;

	@Column(name="kratica_predmet")
	private String kraticaPredmet;

	@Column(name="kreditne_tocke")
	private BigDecimal kreditneTocke;

	@Column(name="naziv_predmet")
	private String nazivPredmet;
	
	@Column(name="nacin_ocenjevanja")
	private String nacinOcenjevanja;

	@Column(name="sifra_predmeta")
	private String sifraPredmeta;

	//bi-directional many-to-one association to Izpitni_rok
	@OneToMany(mappedBy="predmet")
	private List<Izpitni_rok> izpitniRoks;

	//bi-directional many-to-one association to Predmet_izvajalec
	@OneToMany(mappedBy="predmet")
	private List<Predmet_izvajalec> predmetIzvajalecs;

	//bi-directional many-to-one association to Predmetnik_letnika
	@OneToMany(mappedBy="predmet")
	private List<Predmetnik_letnika> predmetnikLetnikas;
	
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

    public Predmet() {
    }

	public int getIdPredmeta() {
		return this.idPredmeta;
	}

	public void setIdPredmeta(int idPredmeta) {
		this.idPredmeta = idPredmeta;
	}

	public String getKraticaPredmet() {
		return this.kraticaPredmet;
	}

	public void setKraticaPredmet(String kraticaPredmet) {
		this.kraticaPredmet = kraticaPredmet;
	}

	public BigDecimal getKreditneTocke() {
		return this.kreditneTocke;
	}

	public void setKreditneTocke(BigDecimal kreditneTocke) {
		this.kreditneTocke = kreditneTocke;
	}

	public String getNazivPredmet() {
		return this.nazivPredmet;
	}

	public void setNazivPredmet(String nazivPredmet) {
		this.nazivPredmet = nazivPredmet;
	}

	public String getSifraPredmeta() {
		return this.sifraPredmeta;
	}

	public void setSifraPredmeta(String sifraPredmeta) {
		this.sifraPredmeta = sifraPredmeta;
	}

	public List<Izpitni_rok> getIzpitniRoks() {
		return this.izpitniRoks;
	}

	public void setIzpitniRoks(List<Izpitni_rok> izpitniRoks) {
		this.izpitniRoks = izpitniRoks;
	}
	
	public List<Predmet_izvajalec> getPredmetIzvajalecs() {
		return this.predmetIzvajalecs;
	}

	public void setPredmetIzvajalecs(List<Predmet_izvajalec> predmetIzvajalecs) {
		this.predmetIzvajalecs = predmetIzvajalecs;
	}
	
	public List<Predmetnik_letnika> getPredmetnikLetnikas() {
		return this.predmetnikLetnikas;
	}

	public void setPredmetnikLetnikas(List<Predmetnik_letnika> predmetnikLetnikas) {
		this.predmetnikLetnikas = predmetnikLetnikas;
	}

	public String getNacinOcenjevanja() {
		return nacinOcenjevanja;
	}

	public void setNacinOcenjevanja(String nacinOcenjevanja) {
		this.nacinOcenjevanja = nacinOcenjevanja;
	}
	
}