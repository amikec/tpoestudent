package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Vloga_oseba_has_Oseba database table.
 * 
 */
@Entity
@Table(name="Vloga_oseba_has_Oseba")
public class Vloga_oseba_has_Oseba implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_vloga_has_oseba")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idVlogaHasOseba;

	//bi-directional many-to-one association to Oseba
    @ManyToOne
	@JoinColumn(name="fk_oseba")
	private Oseba oseba;

	//bi-directional many-to-one association to Vloga_oseba
    @ManyToOne
	@JoinColumn(name="fk_vloga_oseba")
	private Vloga_oseba vlogaOseba;

	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
    public Vloga_oseba_has_Oseba() {
    }

	public int getIdVlogaHasOseba() {
		return this.idVlogaHasOseba;
	}

	public void setIdVlogaHasOseba(int idVlogaHasOseba) {
		this.idVlogaHasOseba = idVlogaHasOseba;
	}

	public Oseba getOseba() {
		return this.oseba;
	}

	public void setOseba(Oseba oseba) {
		this.oseba = oseba;
	}
	
	public Vloga_oseba getVlogaOseba() {
		return this.vlogaOseba;
	}

	public void setVlogaOseba(Vloga_oseba vlogaOseba) {
		this.vlogaOseba = vlogaOseba;
	}
	
}