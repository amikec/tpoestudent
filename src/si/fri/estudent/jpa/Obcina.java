package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the Obcina database table.
 * 
 */
@Entity
@Table(name="Obcina")
public class Obcina implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_obcina")
	private int idObcina;

	@Column(name="naziv_obcina")
	private String nazivObcina;
	
	@Column(name="sifra")
	private String sifra;

	//bi-directional many-to-one association to Naslov
	@OneToMany(mappedBy="obcina")
	private List<Naslov> naslovs;
	
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

    public Obcina() {
    }

	public int getIdObcina() {
		return this.idObcina;
	}

	public void setIdObcina(int idObcina) {
		this.idObcina = idObcina;
	}

	public String getNazivObcina() {
		return this.nazivObcina;
	}

	public void setNazivObcina(String nazivObcina) {
		this.nazivObcina = nazivObcina;
	}

	public List<Naslov> getNaslovs() {
		return this.naslovs;
	}

	public void setNaslovs(List<Naslov> naslovs) {
		this.naslovs = naslovs;
	}

	public String getSifra() {
		return sifra;
	}

	public void setSifra(String sifra) {
		this.sifra = sifra;
	}
	
}