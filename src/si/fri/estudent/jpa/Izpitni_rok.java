package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the Izpitni_rok database table.
 * 
 */
@Entity
@Table(name="Izpitni_rok")
public class Izpitni_rok implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_rok")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idRok;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="datum_izpit")
	private Date datumIzpit;

	@Column(name="max_st_prijav")
	private int maxStPrijav;

	@Column(name="meja_za_pozitivno")
	private int mejaZaPozitivno;

	@Column(name="mozne_tocke")
	private int mozneTocke;

	private String predavalnica;

	//bi-directional many-to-one association to Predmet
    @ManyToOne
	@JoinColumn(name="fk_predmeta")
	private Predmet predmet;

	//bi-directional many-to-one association to Predmet_izvajalec
    @ManyToOne
	@JoinColumn(name="fk_predmet_izvajalca")
	private Predmet_izvajalec predmetIzvajalec;
    
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

    public Izpitni_rok() {
    }

	public int getIdRok() {
		return this.idRok;
	}

	public void setIdRok(int idRok) {
		this.idRok = idRok;
	}

	public Date getDatumIzpit() {
		return this.datumIzpit;
	}

	public void setDatumIzpit(Date datumIzpit) {
		this.datumIzpit = datumIzpit;
	}

	public int getMaxStPrijav() {
		return this.maxStPrijav;
	}

	public void setMaxStPrijav(int maxStPrijav) {
		this.maxStPrijav = maxStPrijav;
	}

	public int getMejaZaPozitivno() {
		return this.mejaZaPozitivno;
	}

	public void setMejaZaPozitivno(int mejaZaPozitivno) {
		this.mejaZaPozitivno = mejaZaPozitivno;
	}

	public int getMozneTocke() {
		return this.mozneTocke;
	}

	public void setMozneTocke(int mozneTocke) {
		this.mozneTocke = mozneTocke;
	}

	public String getPredavalnica() {
		return this.predavalnica;
	}

	public void setPredavalnica(String predavalnica) {
		this.predavalnica = predavalnica;
	}

	public Predmet getPredmet() {
		return this.predmet;
	}

	public void setPredmet(Predmet predmet) {
		this.predmet = predmet;
	}
	
	public Predmet_izvajalec getPredmetIzvajalec() {
		return this.predmetIzvajalec;
	}

	public void setPredmetIzvajalec(Predmet_izvajalec predmetIzvajalec) {
		this.predmetIzvajalec = predmetIzvajalec;
	}
	
}