package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the Kombinacije_izvajalcev database table.
 * 
 */
@Entity
@Table(name="Kombinacije_izvajalcev")
public class Kombinacije_izvajalcev implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_kombinacije_izvajalcev")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idKombinacijeIzvajalcev;

	//bi-directional many-to-one association to Osebje
    @ManyToOne
	@JoinColumn(name="izvajalec_1")
	private Osebje osebje1;

	//bi-directional many-to-one association to Osebje
    @ManyToOne
	@JoinColumn(name="izvajalec_2")
	private Osebje osebje2;

	//bi-directional many-to-one association to Osebje
    @ManyToOne
	@JoinColumn(name="izvajalec_3")
	private Osebje osebje3;

	//bi-directional many-to-one association to Predmet_izvajalec
	@OneToMany(mappedBy="kombinacijeIzvajalcev")
	private List<Predmet_izvajalec> predmetIzvajalecs;
	
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

    public Kombinacije_izvajalcev() {
    }

	public int getIdKombinacijeIzvajalcev() {
		return this.idKombinacijeIzvajalcev;
	}

	public void setIdKombinacijeIzvajalcev(int idKombinacijeIzvajalcev) {
		this.idKombinacijeIzvajalcev = idKombinacijeIzvajalcev;
	}

	public Osebje getOsebje1() {
		return this.osebje1;
	}

	public void setOsebje1(Osebje osebje1) {
		this.osebje1 = osebje1;
	}
	
	public Osebje getOsebje2() {
		return this.osebje2;
	}

	public void setOsebje2(Osebje osebje2) {
		this.osebje2 = osebje2;
	}
	
	public Osebje getOsebje3() {
		return this.osebje3;
	}

	public void setOsebje3(Osebje osebje3) {
		this.osebje3 = osebje3;
	}
	
	public List<Predmet_izvajalec> getPredmetIzvajalecs() {
		return this.predmetIzvajalecs;
	}

	public void setPredmetIzvajalecs(List<Predmet_izvajalec> predmetIzvajalecs) {
		this.predmetIzvajalecs = predmetIzvajalecs;
	}
	
}