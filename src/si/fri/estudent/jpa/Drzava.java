package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the Drzava database table.
 * 
 */
@Entity
@Table(name="Drzava")
public class Drzava implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_drzava")
	private int idDrzava;

	@Column(name="naziv_drzava")
	private String nazivDrzava;

	@Column(name="sifra")
	private String sifra;
	
	//bi-directional many-to-one association to Naslov
	@OneToMany(mappedBy="drzava")
	private List<Naslov> naslovs;
	
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Drzava() {
    }

	public int getIdDrzava() {
		return this.idDrzava;
	}

	public void setIdDrzava(int idDrzava) {
		this.idDrzava = idDrzava;
	}

	public String getNazivDrzava() {
		return this.nazivDrzava;
	}

	public void setNazivDrzava(String nazivDrzava) {
		this.nazivDrzava = nazivDrzava;
	}

	public List<Naslov> getNaslovs() {
		return this.naslovs;
	}

	public void setNaslovs(List<Naslov> naslovs) {
		this.naslovs = naslovs;
	}

	public String getSifra() {
		return sifra;
	}

	public void setSifra(String sifra) {
		this.sifra = sifra;
	}
	
}