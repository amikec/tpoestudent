package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Ocena database table.
 * 
 */
@Entity
@Table(name="Ocena")
public class Ocena implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_ocena")
	private int idOcena;

	@Column(name="st_polaganja")
	private int stPolaganja;
	
	@Column(name="st_polaganja_letos")
	private int stPolaganjaLetos;

	@Column(name="ocenaKoncna")
	private int ocenaKoncna;
	
	@Column(name="ocenaVaje")
	private int ocenaVaje;
	
	@Column(name="ocenaIzpit")
	private int ocenaIzpit;
	
	@Column(name="aktivna")
	private boolean aktivna;

	@Column(name="fk_izpitni_rok")
	private int fkIzpitniRok;

	@Column(name="fk_predmetnik_letnika")
	private int fkPredmetnik;

	@Column(name="ocena")
	private double ocena;
	
	@Column(name="ocena_bonus")
	private double ocenaBonus;

    public Ocena() {
    }

	public int getIdOcena() {
		return this.idOcena;
	}

	public void setIdOcena(int idOcena) {
		this.idOcena = idOcena;
	}

	public boolean getAktivna() {
		return this.aktivna;
	}

	public void setAktivna(boolean aktivna) {
		this.aktivna = aktivna;
	}

	public int getFkIzpitniRok() {
		return this.fkIzpitniRok;
	}

	public void setFkIzpitniRok(int fkIzpitniRok) {
		this.fkIzpitniRok = fkIzpitniRok;
	}

	public int getFkPredmetnikLetnika() {
		return this.fkPredmetnik;
	}

	public int getStPolaganjaLetos() {
		return stPolaganjaLetos;
	}

	public void setStPolaganjaLetos(int stPolaganjaLetos) {
		this.stPolaganjaLetos = stPolaganjaLetos;
	}
	
	public void setFkPredmetnikLetnika(int fkPredmetnikLetnika) {
		this.fkPredmetnik = fkPredmetnikLetnika;
	}

	public double getOcena() {
		return this.ocena;
	}

	public void setOcena(double ocena) {
		this.ocena = ocena;
	}
	
	public double getOcenaBonus() {
		return this.ocenaBonus;
	}

	public void setOcenaBonus(double ocena) {
		this.ocenaBonus = ocena;
	}

	public int getOcenaKoncna() {
		return ocenaKoncna;
	}

	public void setOcenaKoncna(int ocenaKoncna) {
		this.ocenaKoncna = ocenaKoncna;
	}

	public int getOcenaVaje() {
		return ocenaVaje;
	}

	public void setOcenaVaje(int ocenaVaje) {
		this.ocenaVaje = ocenaVaje;
	}

	public int getOcenaIzpit() {
		return ocenaIzpit;
	}

	public void setOcenaIzpit(int ocenaIzpit) {
		this.ocenaIzpit = ocenaIzpit;
	}

	public int getFkPredmetnik() {
		return fkPredmetnik;
	}

	public void setFkPredmetnik(int fkPredmetnik) {
		this.fkPredmetnik = fkPredmetnik;
	}

	public int getStPolaganja() {
		return stPolaganja;
	}

	public void setStPolaganja(int stPolaganja) {
		this.stPolaganja = stPolaganja;
	}

}