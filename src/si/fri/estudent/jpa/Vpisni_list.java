package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Vpisni_list database table.
 * 
 */
@Entity
@Table(name="Vpisni_list")
public class Vpisni_list implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_vpisni")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idVpisni;

	@Column(name="nacin_studija")
	private String nacinStudija;

	@Column(name="studij_na_daljavo")
	private int studijNaDaljavo;

	@Column(name="studijsko_leto")
	private String studijskoLeto;

	//bi-directional many-to-one association to Letnik
    @ManyToOne
	@JoinColumn(name="fk_letnik")
	private Letnik letnik;

	//bi-directional many-to-one association to Student
    @ManyToOne
	@JoinColumn(name="fk_student")
	private Student student;

	//bi-directional many-to-one association to Vrsta_vpisa
    @ManyToOne
	@JoinColumn(name="fk_vrsta_vpisa")
	private Vrsta_vpisa vrstaVpisa;
    
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

    public Vpisni_list() {
    }

	public int getIdVpisni() {
		return this.idVpisni;
	}

	public void setIdVpisni(int idVpisni) {
		this.idVpisni = idVpisni;
	}

	public String getNacinStudija() {
		return this.nacinStudija;
	}

	public void setNacinStudija(String nacinStudija) {
		this.nacinStudija = nacinStudija;
	}

	public int getStudijNaDaljavo() {
		return this.studijNaDaljavo;
	}

	public void setStudijNaDaljavo(int studijNaDaljavo) {
		this.studijNaDaljavo = studijNaDaljavo;
	}

	public String getStudijskoLeto() {
		return this.studijskoLeto;
	}

	public void setStudijskoLeto(String studijskoLeto) {
		this.studijskoLeto = studijskoLeto;
	}

	public Letnik getLetnik() {
		return this.letnik;
	}

	public void setLetnik(Letnik letnik) {
		this.letnik = letnik;
	}
	
	public Student getStudent() {
		return this.student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
	
	public Vrsta_vpisa getVrstaVpisa() {
		return this.vrstaVpisa;
	}

	public void setVrstaVpisa(Vrsta_vpisa vrstaVpisa) {
		this.vrstaVpisa = vrstaVpisa;
	}
	
}