package si.fri.estudent.jpa;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;


/**
 * The persistent class for the Prijava database table.
 * 
 */
@Entity
@Table(name="Prijava")
public class Prijava implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_prijava")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idPrijava;

	@Column(name="st_poizkusov")
	private int stPoizkusov;

	@Column(name="zadnji_poizkus")
	private Timestamp zadnjiPoizkus;

	//bi-directional many-to-one association to Oseba
    @ManyToOne
	@JoinColumn(name="fk_oseba")
	private Oseba oseba;

	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
    
    public Prijava() {
    }

	public int getIdPrijava() {
		return this.idPrijava;
	}

	public void setIdPrijava(int idPrijava) {
		this.idPrijava = idPrijava;
	}

	public int getStPoizkusov() {
		return this.stPoizkusov;
	}

	public void setStPoizkusov(int stPoizkusov) {
		this.stPoizkusov = stPoizkusov;
	}

	public Timestamp getZadnjiPoizkus() {
		return this.zadnjiPoizkus;
	}

	public void setZadnjiPoizkus(Timestamp zadnjiPoizkus) {
		this.zadnjiPoizkus = zadnjiPoizkus;
	}

	public Oseba getOseba() {
		return this.oseba;
	}

	public void setOseba(Oseba oseba) {
		this.oseba = oseba;
	}
	
}