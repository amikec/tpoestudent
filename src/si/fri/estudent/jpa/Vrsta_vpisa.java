package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the Vrsta_vpisa database table.
 * 
 */
@Entity
@Table(name="Vrsta_vpisa")
public class Vrsta_vpisa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_vrsta_vpisa")
	private int idVrstaVpisa;

	@Column(name="kratica_vrsta_vpisa")
	private String kraticaVrstaVpisa;

	@Column(name="naziv_vrsta_vpisa")
	private String nazivVrstaVpisa;

	//bi-directional many-to-one association to Vpisni_list
	@OneToMany(mappedBy="vrstaVpisa")
	private List<Vpisni_list> vpisniLists;
	
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

    public Vrsta_vpisa() {
    }

	public int getIdVrstaVpisa() {
		return this.idVrstaVpisa;
	}

	public void setIdVrstaVpisa(int idVrstaVpisa) {
		this.idVrstaVpisa = idVrstaVpisa;
	}

	public String getKraticaVrstaVpisa() {
		return this.kraticaVrstaVpisa;
	}

	public void setKraticaVrstaVpisa(String kraticaVrstaVpisa) {
		this.kraticaVrstaVpisa = kraticaVrstaVpisa;
	}

	public String getNazivVrstaVpisa() {
		return this.nazivVrstaVpisa;
	}

	public void setNazivVrstaVpisa(String nazivVrstaVpisa) {
		this.nazivVrstaVpisa = nazivVrstaVpisa;
	}

	public List<Vpisni_list> getVpisniLists() {
		return this.vpisniLists;
	}

	public void setVpisniLists(List<Vpisni_list> vpisniLists) {
		this.vpisniLists = vpisniLists;
	}
	
}