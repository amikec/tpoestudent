package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;


/**
 * The persistent class for the Splosna_pravila database table.
 * 
 */
@Entity
@Table(name="Splosna_pravila")
public class Splosna_pravila implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_pravila")
	private int idPravila;

	@Column(name="cena_brez_statusa")
	private BigDecimal cenaBrezStatusa;

	@Column(name="max_st_opravljanj")
	private int maxStOpravljanj;

	@Column(name="min_dni_pred_prijavo")
	private int minDniPredPrijavo;
	
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

    public Splosna_pravila() {
    }

	public int getIdPravila() {
		return this.idPravila;
	}

	public void setIdPravila(int idPravila) {
		this.idPravila = idPravila;
	}

	public BigDecimal getCenaBrezStatusa() {
		return this.cenaBrezStatusa;
	}

	public void setCenaBrezStatusa(BigDecimal cenaBrezStatusa) {
		this.cenaBrezStatusa = cenaBrezStatusa;
	}

	public int getMaxStOpravljanj() {
		return this.maxStOpravljanj;
	}

	public void setMaxStOpravljanj(int maxStOpravljanj) {
		this.maxStOpravljanj = maxStOpravljanj;
	}

	public int getMinDniPredPrijavo() {
		return this.minDniPredPrijavo;
	}

	public void setMinDniPredPrijavo(int minDniPredPrijavo) {
		this.minDniPredPrijavo = minDniPredPrijavo;
	}

}