package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the Drzava database table.
 * 
 */
@Entity
@Table(name="ProgramiInSmeri")
public class ProgramiInSmeri implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_programSmer")
	private int idProgramSmer;

	@Column(name="naziv")
	private String naziv;

	@Column(name="sifra")
	private String sifra;
	
	
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public ProgramiInSmeri() {
    }

	public String getSifra() {
		return sifra;
	}

	public void setSifra(String sifra) {
		this.sifra = sifra;
	}

	public int getIdProgramSmer() {
		return idProgramSmer;
	}

	public void setIdProgramSmer(int idProgramSmer) {
		this.idProgramSmer = idProgramSmer;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	
}