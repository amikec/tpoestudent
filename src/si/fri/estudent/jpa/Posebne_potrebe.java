package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the Posebne_potrebe database table.
 * 
 */
@Entity
@Table(name="Posebne_potrebe")
public class Posebne_potrebe implements 
Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_posebne_potrebe")
	private int idPosebnePotrebe;

	@Column(name="naziv_posebne_potrebe")
	private String nazivPosebnePotrebe;

	@Column(name="sifra_posebne_potrebe")
	private String sifraPosebnePotrebe;

	//bi-directional many-to-one association to Student_has_Posebne_potrebe
	@OneToMany(mappedBy="posebnePotrebe1")
	private List<Student_has_Posebne_potrebe> studentHasPosebnePotrebes1;
	
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

    public Posebne_potrebe() {
    }

	public int getIdPosebnePotrebe() {
		return this.idPosebnePotrebe;
	}

	public void setIdPosebnePotrebe(int idPosebnePotrebe) {
		this.idPosebnePotrebe = idPosebnePotrebe;
	}

	public String getNazivPosebnePotrebe() {
		return this.nazivPosebnePotrebe;
	}

	public void setNazivPosebnePotrebe(String nazivPosebnePotrebe) {
		this.nazivPosebnePotrebe = nazivPosebnePotrebe;
	}

	public String getSifraPosebnePotrebe() {
		return this.sifraPosebnePotrebe;
	}

	public void setSifraPosebnePotrebe(String sifraPosebnePotrebe) {
		this.sifraPosebnePotrebe = sifraPosebnePotrebe;
	}

	public List<Student_has_Posebne_potrebe> getStudentHasPosebnePotrebes1() {
		return this.studentHasPosebnePotrebes1;
	}

	public void setStudentHasPosebnePotrebes1(List<Student_has_Posebne_potrebe> studentHasPosebnePotrebes1) {
		this.studentHasPosebnePotrebes1 = studentHasPosebnePotrebes1;
	}
}