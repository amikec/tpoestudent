package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the Letnik database table.
 * 
 */
@Entity
@Table(name="Letnik")
public class Letnik implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_letnik")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idLetnik;

	@Column(name="naziv_letnik")
	private String nazivLetnik;

	@Column(name="studijsko_leto")
	private String studijskoLeto;

	//bi-directional many-to-one association to Studij
    @ManyToOne
	@JoinColumn(name="fk_studij")
	private Studij studij;

	//bi-directional many-to-one association to Predmetnik_letnika
	@OneToMany(mappedBy="letnik")
	private List<Predmetnik_letnika> predmetnikLetnikas;

	//bi-directional many-to-one association to Vpisni_list
	@OneToMany(mappedBy="letnik")
	private List<Vpisni_list> vpisniLists;
	
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

    public Letnik() {
    }

	public int getIdLetnik() {
		return this.idLetnik;
	}

	public void setIdLetnik(int idLetnik) {
		this.idLetnik = idLetnik;
	}

	public String getNazivLetnik() {
		return this.nazivLetnik;
	}

	public void setNazivLetnik(String nazivLetnik) {
		this.nazivLetnik = nazivLetnik;
	}

	public String getStudijskoLeto() {
		return this.studijskoLeto;
	}

	public void setStudijskoLeto(String studijskoLeto) {
		this.studijskoLeto = studijskoLeto;
	}

	public Studij getStudij() {
		return this.studij;
	}

	public void setStudij(Studij studij) {
		this.studij = studij;
	}
	
	public List<Predmetnik_letnika> getPredmetnikLetnikas() {
		return this.predmetnikLetnikas;
	}

	public void setPredmetnikLetnikas(List<Predmetnik_letnika> predmetnikLetnikas) {
		this.predmetnikLetnikas = predmetnikLetnikas;
	}
	
	public List<Vpisni_list> getVpisniLists() {
		return this.vpisniLists;
	}

	public void setVpisniLists(List<Vpisni_list> vpisniLists) {
		this.vpisniLists = vpisniLists;
	}
	
}