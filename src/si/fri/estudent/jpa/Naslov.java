package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the Naslov database table.
 * 
 */
@Entity
@Table(name="Naslov")
public class Naslov implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_naslov")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idNaslov;

	private String ulica;

	//bi-directional many-to-one association to Drzava
    @ManyToOne
	@JoinColumn(name="fk_drzava")
	private Drzava drzava;

	//bi-directional many-to-one association to Kraj
    @ManyToOne
	@JoinColumn(name="fk_kraj")
	private Kraj kraj;

	//bi-directional many-to-one association to Obcina
    @ManyToOne
	@JoinColumn(name="fk_obcina")
	private Obcina obcina;

	//bi-directional many-to-one association to Oseba
	@OneToMany(mappedBy="naslov1")
	private List<Oseba> osebas1;

	//bi-directional many-to-one association to Oseba
	@OneToMany(mappedBy="naslov2")
	private List<Oseba> osebas2;
	
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

    public Naslov() {
    }

	public int getIdNaslov() {
		return this.idNaslov;
	}

	public void setIdNaslov(int idNaslov) {
		this.idNaslov = idNaslov;
	}

	public String getUlica() {
		return this.ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public Drzava getDrzava() {
		return this.drzava;
	}

	public void setDrzava(Drzava drzava) {
		this.drzava = drzava;
	}
	
	public Kraj getKraj() {
		return this.kraj;
	}

	public void setKraj(Kraj kraj) {
		this.kraj = kraj;
	}
	
	public Obcina getObcina() {
		return this.obcina;
	}

	public void setObcina(Obcina obcina) {
		this.obcina = obcina;
	}
	
	public List<Oseba> getOsebas1() {
		return this.osebas1;
	}

	public void setOsebas1(List<Oseba> osebas1) {
		this.osebas1 = osebas1;
	}
	
	public List<Oseba> getOsebas2() {
		return this.osebas2;
	}

	public void setOsebas2(List<Oseba> osebas2) {
		this.osebas2 = osebas2;
	}
	
}