package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Student_has_Posebne_potrebe database table.
 * 
 */
@Entity
@Table(name="Student_has_Posebne_potrebe")
public class Student_has_Posebne_potrebe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_potrebe_student")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idPotrebeStudent;

	//bi-directional many-to-one association to Posebne_potrebe
    @ManyToOne
	@JoinColumn(name="fk_posebne_potrebe")
	private Posebne_potrebe posebnePotrebe1;

	//bi-directional many-to-one association to Student
    @ManyToOne
	@JoinColumn(name="fk_student")
	private Student student1;
    
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

    public Student_has_Posebne_potrebe() {
    }

	public int getIdPotrebeStudent() {
		return this.idPotrebeStudent;
	}

	public void setIdPotrebeStudent(int idPotrebeStudent) {
		this.idPotrebeStudent = idPotrebeStudent;
	}

	public Posebne_potrebe getPosebnePotrebe1() {
		return this.posebnePotrebe1;
	}

	public void setPosebnePotrebe1(Posebne_potrebe posebnePotrebe1) {
		this.posebnePotrebe1 = posebnePotrebe1;
	}
	
	public Student getStudent1() {
		return this.student1;
	}

	public void setStudent1(Student student1) {
		this.student1 = student1;
	}
}