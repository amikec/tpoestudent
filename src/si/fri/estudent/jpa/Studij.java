package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the Studij database table.
 * 
 */
@Entity
@Table(name="Studij")
public class Studij implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_studija")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idStudija;

	@Column(name="sifra_studija")
	private String sifraStudija;

	//bi-directional many-to-one association to Letnik
	@OneToMany(mappedBy="studij")
	private List<Letnik> letniks;

	//bi-directional many-to-one association to Studijska_smer
    @ManyToOne
	@JoinColumn(name="fk_studijska_smer")
	private Studijska_smer studijskaSmer;

	//bi-directional many-to-one association to Studijski_program
    @ManyToOne
	@JoinColumn(name="fk_studijski_program")
	private Studijski_program studijskiProgram;

	//bi-directional many-to-one association to Vrsta_studija
    @ManyToOne
	@JoinColumn(name="fk_studijska_vrsta")
	private Vrsta_studija vrstaStudija;
    
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

    public Studij() {
    }

	public int getIdStudija() {
		return this.idStudija;
	}

	public void setIdStudija(int idStudija) {
		this.idStudija = idStudija;
	}

	public String getSifraStudija() {
		return this.sifraStudija;
	}

	public void setSifraStudija(String sifraStudija) {
		this.sifraStudija = sifraStudija;
	}

	public List<Letnik> getLetniks() {
		return this.letniks;
	}

	public void setLetniks(List<Letnik> letniks) {
		this.letniks = letniks;
	}
	
	public Studijska_smer getStudijskaSmer() {
		return this.studijskaSmer;
	}

	public void setStudijskaSmer(Studijska_smer studijskaSmer) {
		this.studijskaSmer = studijskaSmer;
	}
	
	public Studijski_program getStudijskiProgram() {
		return this.studijskiProgram;
	}

	public void setStudijskiProgram(Studijski_program studijskiProgram) {
		this.studijskiProgram = studijskiProgram;
	}
	
	public Vrsta_studija getVrstaStudija() {
		return this.vrstaStudija;
	}

	public void setVrstaStudija(Vrsta_studija vrstaStudija) {
		this.vrstaStudija = vrstaStudija;
	}
	
}