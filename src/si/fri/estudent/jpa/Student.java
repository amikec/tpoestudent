package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the Student database table.
 * 
 */
@Entity
@Table(name="Student")
public class Student implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_student")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idStudent;

	@Column(name="vpisna_st")
	private String vpisnaSt;

	//bi-directional many-to-one association to Oseba
    @ManyToOne
	@JoinColumn(name="fk_oseba")
	private Oseba oseba;

	//bi-directional many-to-one association to Student_has_Posebne_potrebe
	@OneToMany(mappedBy="student1")
	private List<Student_has_Posebne_potrebe> studentHasPosebnePotrebes1;

	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	@OneToMany(mappedBy="student")
	private List<Vpisni_list> vpisniLists;

    public Student() {
    }

	public int getIdStudent() {
		return this.idStudent;
	}

	public void setIdStudent(int idStudent) {
		this.idStudent = idStudent;
	}

	public String getVpisnaSt() {
		return this.vpisnaSt;
	}

	public void setVpisnaSt(String vpisnaSt) {
		this.vpisnaSt = vpisnaSt;
	}

	public Oseba getOseba() {
		return this.oseba;
	}

	public void setOseba(Oseba oseba) {
		this.oseba = oseba;
	}
	
	public List<Student_has_Posebne_potrebe> getStudentHasPosebnePotrebes1() {
		return this.studentHasPosebnePotrebes1;
	}

	public void setStudentHasPosebnePotrebes1(List<Student_has_Posebne_potrebe> studentHasPosebnePotrebes1) {
		this.studentHasPosebnePotrebes1 = studentHasPosebnePotrebes1;
	}
	
	
	public List<Vpisni_list> getVpisniLists() {
		return this.vpisniLists;
	}

	public void setVpisniLists(List<Vpisni_list> vpisniLists) {
		this.vpisniLists = vpisniLists;
	}
	
}