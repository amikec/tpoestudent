package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the Vrsta_studija database table.
 * 
 */
@Entity
@Table(name="Vrsta_studija")
public class Vrsta_studija implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_vrsta_studija")
	private int idVrstaStudija;

	@Column(name="kratica_vrsta_studija")
	private String kraticaVrstaStudija;

	@Column(name="naziv_vrsta_studija")
	private String nazivVrstaStudija;

	@Column(name="sifra_vl")
	private String sifraVl;

	@Column(name="stopnja_studija")
	private String stopnjaStudija;

	//bi-directional many-to-one association to Studij
	@OneToMany(mappedBy="vrstaStudija")
	private List<Studij> studijs;
	
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

    public Vrsta_studija() {
    }

	public int getIdVrstaStudija() {
		return this.idVrstaStudija;
	}

	public void setIdVrstaStudija(int idVrstaStudija) {
		this.idVrstaStudija = idVrstaStudija;
	}

	public String getKraticaVrstaStudija() {
		return this.kraticaVrstaStudija;
	}

	public void setKraticaVrstaStudija(String kraticaVrstaStudija) {
		this.kraticaVrstaStudija = kraticaVrstaStudija;
	}

	public String getNazivVrstaStudija() {
		return this.nazivVrstaStudija;
	}

	public void setNazivVrstaStudija(String nazivVrstaStudija) {
		this.nazivVrstaStudija = nazivVrstaStudija;
	}

	public String getSifraVl() {
		return this.sifraVl;
	}

	public void setSifraVl(String sifraVl) {
		this.sifraVl = sifraVl;
	}

	public String getStopnjaStudija() {
		return this.stopnjaStudija;
	}

	public void setStopnjaStudija(String stopnjaStudija) {
		this.stopnjaStudija = stopnjaStudija;
	}

	public List<Studij> getStudijs() {
		return this.studijs;
	}

	public void setStudijs(List<Studij> studijs) {
		this.studijs = studijs;
	}
	
}