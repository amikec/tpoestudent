package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the Oseba database table.
 * 
 */
@Entity
@Table(name="Oseba")
public class Oseba implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_oseba")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idOseba;

	@Column(name="davcna_st")
	private String davcnaSt;

	private String email;

	private String emso;

	private String geslo;

	private String ime;

	private String mobitel;

	private String priimek;

	@Column(name="priimek_dekliski")
	private String priimekDekliski;

	private String spol;

	private String telefon;

	@Column(name="up_ime")
	private String upIme;

	//bi-directional many-to-one association to Naslov
    @ManyToOne
	@JoinColumn(name="fk_naslov_stalni")
	private Naslov naslov1;

	//bi-directional many-to-one association to Naslov
    @ManyToOne
	@JoinColumn(name="fk_naslov_zacasni")
	private Naslov naslov2;

	//bi-directional many-to-many association to Vloga_oseba
    @ManyToMany
	@JoinTable(
		name="Vloga_oseba_has_Oseba"
		, joinColumns={
			@JoinColumn(name="fk_oseba")
			}
		, inverseJoinColumns={
			@JoinColumn(name="fk_vloga_oseba")
			}
		)
	private List<Vloga_oseba> vlogaOsebas;

	//bi-directional many-to-one association to Osebje
	@OneToMany(mappedBy="oseba")
	private List<Osebje> osebjes;

	//bi-directional many-to-one association to Prijava
	@OneToMany(mappedBy="oseba")
	private List<Prijava> prijavas;

	//bi-directional many-to-one association to Student
	@OneToMany(mappedBy="oseba")
	private List<Student> students;

	//bi-directional many-to-one association to Vloga_oseba_has_Oseba
	@OneToMany(mappedBy="oseba")
	private List<Vloga_oseba_has_Oseba> vlogaOsebaHasOsebas;
	
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

    public Oseba() {
    }

	public int getIdOseba() {
		return this.idOseba;
	}

	public void setIdOseba(int idOseba) {
		this.idOseba = idOseba;
	}

	public String getDavcnaSt() {
		return this.davcnaSt;
	}

	public void setDavcnaSt(String davcnaSt) {
		this.davcnaSt = davcnaSt;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmso() {
		return this.emso;
	}

	public void setEmso(String emso) {
		this.emso = emso;
	}

	public String getGeslo() {
		return this.geslo;
	}

	public void setGeslo(String geslo) {
		this.geslo = geslo;
	}

	public String getIme() {
		return this.ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getMobitel() {
		return this.mobitel;
	}

	public void setMobitel(String mobitel) {
		this.mobitel = mobitel;
	}

	public String getPriimek() {
		return this.priimek;
	}

	public void setPriimek(String priimek) {
		this.priimek = priimek;
	}

	public String getPriimekDekliski() {
		return this.priimekDekliski;
	}

	public void setPriimekDekliski(String priimekDekliski) {
		this.priimekDekliski = priimekDekliski;
	}

	public String getSpol() {
		return this.spol;
	}

	public void setSpol(String spol) {
		this.spol = spol;
	}

	public String getTelefon() {
		return this.telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public String getUpIme() {
		return this.upIme;
	}

	public void setUpIme(String upIme) {
		this.upIme = upIme;
	}

	public Naslov getNaslov1() {
		return this.naslov1;
	}

	public void setNaslov1(Naslov naslov1) {
		this.naslov1 = naslov1;
	}
	
	public Naslov getNaslov2() {
		return this.naslov2;
	}

	public void setNaslov2(Naslov naslov2) {
		this.naslov2 = naslov2;
	}
	
	public List<Vloga_oseba> getVlogaOsebas() {
		return this.vlogaOsebas;
	}

	public void setVlogaOsebas(List<Vloga_oseba> vlogaOsebas) {
		this.vlogaOsebas = vlogaOsebas;
	}
	
	public List<Osebje> getOsebjes() {
		return this.osebjes;
	}

	public void setOsebjes(List<Osebje> osebjes) {
		this.osebjes = osebjes;
	}
	
	public List<Prijava> getPrijavas() {
		return this.prijavas;
	}

	public void setPrijavas(List<Prijava> prijavas) {
		this.prijavas = prijavas;
	}
	
	public List<Student> getStudents() {
		return this.students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}
	
	public List<Vloga_oseba_has_Oseba> getVlogaOsebaHasOsebas() {
		return this.vlogaOsebaHasOsebas;
	}

	public void setVlogaOsebaHasOsebas(List<Vloga_oseba_has_Oseba> vlogaOsebaHasOsebas) {
		this.vlogaOsebaHasOsebas = vlogaOsebaHasOsebas;
	}
	
}