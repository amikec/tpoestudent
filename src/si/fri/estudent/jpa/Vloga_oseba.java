package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the Vloga_oseba database table.
 * 
 */
@Entity
@Table(name="Vloga_oseba")
public class Vloga_oseba implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_vloga_oseba")
	private int idVlogaOseba;

	@Column(name="naziv_vloga_oseba")
	private String nazivVlogaOseba;

	//bi-directional many-to-many association to Oseba
	@ManyToMany(mappedBy="vlogaOsebas")
	private List<Oseba> osebas;

	//bi-directional many-to-one association to Vloga_oseba_has_Oseba
	@OneToMany(mappedBy="vlogaOseba")
	private List<Vloga_oseba_has_Oseba> vlogaOsebaHasOsebas;
	
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

    public Vloga_oseba() {
    }

	public int getIdVlogaOseba() {
		return this.idVlogaOseba;
	}

	public void setIdVlogaOseba(int idVlogaOseba) {
		this.idVlogaOseba = idVlogaOseba;
	}

	public String getNazivVlogaOseba() {
		return this.nazivVlogaOseba;
	}

	public void setNazivVlogaOseba(String nazivVlogaOseba) {
		this.nazivVlogaOseba = nazivVlogaOseba;
	}

	public List<Oseba> getOsebas() {
		return this.osebas;
	}

	public void setOsebas(List<Oseba> osebas) {
		this.osebas = osebas;
	}
	
	public List<Vloga_oseba_has_Oseba> getVlogaOsebaHasOsebas() {
		return this.vlogaOsebaHasOsebas;
	}

	public void setVlogaOsebaHasOsebas(List<Vloga_oseba_has_Oseba> vlogaOsebaHasOsebas) {
		this.vlogaOsebaHasOsebas = vlogaOsebaHasOsebas;
	}
	
}