package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Predmetnik_letnika database table.
 * 
 */
@Entity
@Table(name="Predmetnik_letnika")
public class Predmetnik_letnika implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_predmetnik_letnika")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idPredmetnikLetnika;

	@Column(name="obvezni_predmet")
	private boolean obvezniPredmet;

	//bi-directional many-to-one association to Letnik
    @ManyToOne
	@JoinColumn(name="fk_letnik")
	private Letnik letnik;

	//bi-directional many-to-one association to Predmet
    @ManyToOne
	@JoinColumn(name="fk_predmet")
	private Predmet predmet;

	//bi-directional many-to-one association to Modul
    @ManyToOne
	@JoinColumn(name="fk_modul")
	private Modul modul;
    
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

    public Predmetnik_letnika() {
    }

	public int getIdPredmetnikLetnika() {
		return this.idPredmetnikLetnika;
	}

	public void setIdPredmetnikLetnika(int idPredmetnikLetnika) {
		this.idPredmetnikLetnika = idPredmetnikLetnika;
	}

	public boolean getObvezniPredmet() {
		return this.obvezniPredmet;
	}

	public void setObvezniPredmet(boolean obvezniPredmet) {
		this.obvezniPredmet = obvezniPredmet;
	}

	public Letnik getLetnik() {
		return this.letnik;
	}

	public void setLetnik(Letnik letnik) {
		this.letnik = letnik;
	}
	
	public Predmet getPredmet() {
		return this.predmet;
	}

	public void setPredmet(Predmet predmet) {
		this.predmet = predmet;
	}
	
	public Modul getModul() {
		return this.modul;
	}

	public void setModul(Modul modul) {
		this.modul = modul;
	}
	
}