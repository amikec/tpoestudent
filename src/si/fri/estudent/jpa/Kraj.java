package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the Kraj database table.
 * 
 */
@Entity
@Table(name="Kraj")
public class Kraj implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_kraj")
	private int idKraj;

	private String posta;

	@Column(name="postna_st")
	private String postnaSt;

	//bi-directional many-to-one association to Naslov
	@OneToMany(mappedBy="kraj")
	private List<Naslov> naslovs;
	
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

    public Kraj() {
    }

	public int getIdKraj() {
		return this.idKraj;
	}

	public void setIdKraj(int idKraj) {
		this.idKraj = idKraj;
	}

	public String getPosta() {
		return this.posta;
	}

	public void setPosta(String posta) {
		this.posta = posta;
	}

	public String getPostnaSt() {
		return this.postnaSt;
	}

	public void setPostnaSt(String postnaSt) {
		this.postnaSt = postnaSt;
	}

	public List<Naslov> getNaslovs() {
		return this.naslovs;
	}

	public void setNaslovs(List<Naslov> naslovs) {
		this.naslovs = naslovs;
	}
	
}