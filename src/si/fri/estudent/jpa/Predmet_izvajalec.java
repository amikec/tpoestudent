package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the Predmet_izvajalec database table.
 * 
 */
@Entity
@Table(name="Predmet_izvajalec")
public class Predmet_izvajalec implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_predmet_izvajalca")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idPredmetIzvajalca;

	//bi-directional many-to-one association to Izpitni_rok
	@OneToMany(mappedBy="predmetIzvajalec")
	private List<Izpitni_rok> izpitniRoks;

	//bi-directional many-to-one association to Kombinacije_izvajalcev
    @ManyToOne
	@JoinColumn(name="fk_kombinacije_izvajalcev")
	private Kombinacije_izvajalcev kombinacijeIzvajalcev;

	//bi-directional many-to-one association to Predmet
    @ManyToOne
	@JoinColumn(name="fk_predmeta")
	private Predmet predmet;
    
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

    public Predmet_izvajalec() {
    }

	public int getIdPredmetIzvajalca() {
		return this.idPredmetIzvajalca;
	}

	public void setIdPredmetIzvajalca(int idPredmetIzvajalca) {
		this.idPredmetIzvajalca = idPredmetIzvajalca;
	}

	public List<Izpitni_rok> getIzpitniRoks() {
		return this.izpitniRoks;
	}

	public void setIzpitniRoks(List<Izpitni_rok> izpitniRoks) {
		this.izpitniRoks = izpitniRoks;
	}
	
	public Kombinacije_izvajalcev getKombinacijeIzvajalcev() {
		return this.kombinacijeIzvajalcev;
	}

	public void setKombinacijeIzvajalcev(Kombinacije_izvajalcev kombinacijeIzvajalcev) {
		this.kombinacijeIzvajalcev = kombinacijeIzvajalcev;
	}
	
	public Predmet getPredmet() {
		return this.predmet;
	}

	public void setPredmet(Predmet predmet) {
		this.predmet = predmet;
	}
	
}