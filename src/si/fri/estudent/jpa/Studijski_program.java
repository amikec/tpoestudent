package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the Studijski_program database table.
 * 
 */
@Entity
@Table(name="Studijski_program")
public class Studijski_program implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_studijski_program")
	private int idStudijskiProgram;

	@Column(name="kratica_studijski_program")
	private String kraticaStudijskiProgram;

	@Column(name="naziv_studijski_program")
	private String nazivStudijskiProgram;

	//bi-directional many-to-one association to Studij
	@OneToMany(mappedBy="studijskiProgram")
	private List<Studij> studijs;
	
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

    public Studijski_program() {
    }

	public int getIdStudijskiProgram() {
		return this.idStudijskiProgram;
	}

	public void setIdStudijskiProgram(int idStudijskiProgram) {
		this.idStudijskiProgram = idStudijskiProgram;
	}

	public String getKraticaStudijskiProgram() {
		return this.kraticaStudijskiProgram;
	}

	public void setKraticaStudijskiProgram(String kraticaStudijskiProgram) {
		this.kraticaStudijskiProgram = kraticaStudijskiProgram;
	}

	public String getNazivStudijskiProgram() {
		return this.nazivStudijskiProgram;
	}

	public void setNazivStudijskiProgram(String nazivStudijskiProgram) {
		this.nazivStudijskiProgram = nazivStudijskiProgram;
	}

	public List<Studij> getStudijs() {
		return this.studijs;
	}

	public void setStudijs(List<Studij> studijs) {
		this.studijs = studijs;
	}
	
}