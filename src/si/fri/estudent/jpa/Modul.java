package si.fri.estudent.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the Modul database table.
 * 
 */
@Entity
@Table(name="Modul")
public class Modul implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_modul")
	private int idModul;

	@Column(name="kratica_modul")
	private String kraticaModul;

	@Column(name="naziv_modul")
	private String nazivModul;

	//bi-directional many-to-one association to Predmetnik_letnika
	@OneToMany(mappedBy="modul")
	private List<Predmetnik_letnika> predmetnikLetnikas;
	
	@Column(name="status")
	private boolean status;

    public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

    public Modul() {
    }

	public int getIdModul() {
		return this.idModul;
	}

	public void setIdModul(int idModul) {
		this.idModul = idModul;
	}

	public String getKraticaModul() {
		return this.kraticaModul;
	}

	public void setKraticaModul(String kraticaModul) {
		this.kraticaModul = kraticaModul;
	}

	public String getNazivModul() {
		return this.nazivModul;
	}

	public void setNazivModul(String nazivModul) {
		this.nazivModul = nazivModul;
	}

	public List<Predmetnik_letnika> getPredmetnikLetnikas() {
		return this.predmetnikLetnikas;
	}

	public void setPredmetnikLetnikas(List<Predmetnik_letnika> predmetnikLetnikas) {
		this.predmetnikLetnikas = predmetnikLetnikas;
	}
	
}