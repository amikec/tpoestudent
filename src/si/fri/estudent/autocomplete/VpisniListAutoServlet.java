package si.fri.estudent.autocomplete;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import si.fri.estudent.jpa.Drzava;
import si.fri.estudent.jpa.Kraj;
import si.fri.estudent.jpa.Obcina;
import si.fri.estudent.utilities.DataValidator;

/**
 * Servlet implementation class VpisniListAutoServlet
 */
@WebServlet("/VpisniListAutoServlet")
public class VpisniListAutoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	EntityManager em;
    public VpisniListAutoServlet() {
        super();
        em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		String kriterij = request.getParameter("query").toLowerCase();
		String funkcija = request.getParameter("funkcija");
		
		Query q;
		JSONArray jsonArray = new JSONArray();
		DataValidator validator = new DataValidator();
		
		if (funkcija.equals("drzava")) { // Države
			if (!validator.validateInputText(kriterij, 45, false, "^[a-zčćžšđ ]{1,45}$", "", "", "")) {
				return;
			}
			
			q = em.createQuery("SELECT d FROM Drzava d WHERE LOWER(d.nazivDrzava) LIKE :kriterij");
			q.setParameter("kriterij", kriterij + "%");
			
			List<Drzava> drzave = (List<Drzava>)(q.getResultList());
			
			JSONObject jsonRecord;
			
			for(Drzava drzava : drzave) {
				jsonRecord = new JSONObject();
				jsonRecord.put("id", drzava.getIdDrzava());
				jsonRecord.put("value", drzava.getNazivDrzava());
				
				jsonArray.add(jsonRecord);
			}
		} else if (funkcija.equals("posta")) { // Pošte
			if (!validator.validateInputText(kriterij, 45, false, "^[a-zčćžšđ.\\-, ]{1,45}$", "", "", "")) {
				return;
			}
			
			q = em.createQuery("SELECT k FROM Kraj k WHERE LOWER(k.posta) LIKE :kriterij");
			q.setParameter("kriterij", kriterij + "%");
			
			List<Kraj> kraji = (List<Kraj>)(q.getResultList());
			
			JSONObject jsonRecord;
			
			for(Kraj kraj : kraji) {
				jsonRecord = new JSONObject();
				jsonRecord.put("value", kraj.getPosta());
				jsonRecord.put("id", kraj.getIdKraj());
				jsonRecord.put("postnaStevilka", kraj.getPostnaSt());
				
				jsonArray.add(jsonRecord);
			}
		} else if (funkcija.equals("obcine")) { // Obcina
			if (!validator.validateInputText(kriterij, 45, false, "^[a-zčćžšđ.\\-, ]{1,45}$", "", "", "")) {
				return;
			}
			
			q = em.createQuery("SELECT o FROM Obcina o WHERE LOWER(o.nazivObcina) LIKE :kriterij");
			q.setParameter("kriterij", kriterij + "%");
			
			List<Obcina> obcine = (List<Obcina>)(q.getResultList());
			
			JSONObject jsonRecord;
			
			for(Obcina obcina : obcine) {
				jsonRecord = new JSONObject();
				jsonRecord.put("value", obcina.getNazivObcina());
				jsonRecord.put("id", obcina.getIdObcina());
				
				jsonArray.add(jsonRecord);
			}
		}
		
		response.setContentType("text/html; charset=utf-8");
		PrintWriter out = response.getWriter();
		out.print(jsonArray);
	}
}