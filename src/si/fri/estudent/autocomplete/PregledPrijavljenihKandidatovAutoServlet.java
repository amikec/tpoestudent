package si.fri.estudent.autocomplete;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import si.fri.estudent.jpa.Drzava;
import si.fri.estudent.jpa.Oseba;
import si.fri.estudent.utilities.DataValidator;

/**
 * Servlet implementation class PregledPrijavljenihKandidatovAutoServlet
 */
@WebServlet("/PregledPrijavljenihKandidatovAutoServlet")
public class PregledPrijavljenihKandidatovAutoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PregledPrijavljenihKandidatovAutoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String kriterij = request.getParameter("query").toLowerCase();
		
		JSONArray jsonArray = new JSONArray();
		DataValidator validator = new DataValidator();
		EntityManager em;
		Query q = null;
		String query = null;
		
		if (!validator.validateInputText(kriterij, 135, false, "^[A-Za-zČĆŽŠĐčćžšđ ]{1,135}$", "", "", "")) {
			return;
		}
		
		try {
			em = Persistence.createEntityManagerFactory("TPOestudent").createEntityManager();
			
			query = "SELECT " +
						"o " +
					"FROM " +
						"Oseba o, Osebje os " +
					"WHERE " +
						"o.idOseba = os.oseba.idOseba AND " +
						"(" +
							"(o.priimekDekliski IS NOT NULL AND LOWER(CONCAT(o.ime, ' ', o.priimekDekliski, ' ', o.priimek)) LIKE :kriterij) " +
							"OR " +
							"(o.priimekDekliski IS NULL AND LOWER(CONCAT(o.ime, ' ', o.priimek)) LIKE :kriterij)" +
						")";
			
			q = em.createQuery(query);
			q.setParameter("kriterij", kriterij + "%");
			List<Oseba> profesorji = (List<Oseba>) q.getResultList();
			
			JSONObject jsonRecord = null;
			String imeProfesor = null;
			
			for(Oseba profesor : profesorji) {
				jsonRecord = new JSONObject();
				jsonRecord.put("id", profesor.getIdOseba());
				
				if (profesor.getPriimekDekliski() == null || profesor.getPriimekDekliski().equals("")) {
					imeProfesor = profesor.getIme() + " " + profesor.getPriimek();
				} else {
					imeProfesor = profesor.getIme() + " " + profesor.getPriimekDekliski() + " " + profesor.getPriimekDekliski();
				}
				
				jsonRecord.put("value", imeProfesor);
				jsonArray.add(jsonRecord);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		response.setContentType("text/html; charset=utf-8");
		PrintWriter out = response.getWriter();
		out.print(jsonArray);
	}

}
