package si.fri.estudent.utilities;

//import java.text.DateFormatSymbols;
import java.util.Calendar;



public class IzpitValidator {
	
	private static final int KOLIKO_DNI_PRED_IZPITOM_MORA_BITI_RAZPISAN_IZPITNI_ROK = 0;

	public static void main(String[] args) {
	      
		 Calendar cal = Calendar.getInstance();
		 cal.set(2012, 3, 16);
		 //System.out.println(moznaPrijavaNaIzpit(cal));
		 
		 //System.out.println(mozenRazpisRoka(cal));
	}
	 
	public static boolean mozenRazpisRoka(Calendar date) {
		 
		 boolean napaka = false;
		 
//		 String dayNames[] = new DateFormatSymbols().getWeekdays();
		 Calendar now = Calendar.getInstance();
		 now.add(Calendar.DAY_OF_WEEK, KOLIKO_DNI_PRED_IZPITOM_MORA_BITI_RAZPISAN_IZPITNI_ROK);
		 
		 if(!now.before(date)) {
			 napaka = true;
		 }
		 
		 return napaka;
	}
	 
	 // če se ne moremo prijavti na izpit funkcija vrača true
	 public static boolean moznaPrijavaNaIzpit(java.util.Date date){
		 boolean napaka = false;
		 
		 Calendar now = Calendar.getInstance();
		 Calendar rok = Calendar.getInstance();
		 rok.setTime(date);
		 
		 int day = rok.get(Calendar.DAY_OF_WEEK);
	 	 //System.out.println("Trenuten čas: " + now.getTime().toString());
	 	 
	 	rok.set(Calendar.HOUR_OF_DAY, 0);
 		rok.set(Calendar.MINUTE, 0);
 		rok.set(Calendar.SECOND, -1);
		 switch (day) {
		 	// če je izpit v ponedeljek je zadnji rok za prijavo v četertek do 24:00 
		 	case 2:	 		
		 		rok.add(Calendar.DATE, -3);
		 		//System.out.println("Se dovoljena prijava: " + rok.getTime().toString());
		 		if(now.after(rok))
		 			napaka = true;
		 		break;
		 	// če je izpit v torek je zadnji rok za prijavo v nedeljo do 24:00
		 	case 3:
		 		rok.add(Calendar.DATE, -1);
		 		//System.out.println("Se dovoljena prijava: " + rok.getTime().toString());
		 		if(now.after(rok))
		 			napaka = true;
		 		break;
		 	// če je izpit v sredo je zadnji rok za prijavo v nedeljo do 24:00
		 	case 4:
		 		rok.add(Calendar.DATE, -2);
		 		//System.out.println("Se dovoljena prijava: " + rok.getTime().toString());
		 		if(now.after(rok))
		 			napaka = true;
		 		break;
		 	// če je izpit v četrtek je zadnji rok za prijavo v ponedeljek od 24:00
		 	case 5:
		 		rok.add(Calendar.DATE, -2);
		 		//System.out.println("Se dovoljena prijava: " + rok.getTime().toString());
		 		if(now.after(rok))
		 			napaka = true;
		 		break;
		 	// če je izpit v petek je zadnji rok za prijavo v torek do 24:00
		 	case 6:
		 		rok.add(Calendar.DATE, -2);
		 		//System.out.println("Se dovoljena prijava: " + rok.getTime().toString());
		 		if(now.after(rok))
		 			napaka = true;
		 		break;
		 	case 1:
		 		if(now.after(rok))
		 			napaka = true;
		 		break;
		 	case 7:
		 		if(now.after(rok))
		 			napaka = true;
		 		break;
		 }
		 
		 return napaka;
	 }
}
