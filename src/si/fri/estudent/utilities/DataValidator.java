package si.fri.estudent.utilities;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

// Narejene so validacije za:
// * Prijavo

public class DataValidator {
	
	protected String error;
	
	public String getError(){
		return error;
	}
	
	protected void setError(String err){
		error = err;
	}
	
	public DataValidator(){
		error = null;
	}
	
	public boolean validateLoginData(String username, String password){
		return (this.validateUsername(username) && this.validatePassword(password));
	}
	
	public boolean validatePassword(String password){
		
		if(password == null || password.length() == 0){
			this.setError("Please input your password.");
			
			return false;
		}
		
		if(password.length() > 20){
			this.setError("Password is too long.");
			
			return false;
		}
		
		Pattern p = Pattern.compile("^[A-Za-z0-9]+$");
		
		Matcher matchPassword = p.matcher(password);
		
		if(!matchPassword.find()){
			this.setError("Password in a wrong format.");
			
			return false;
		}
		
		return true;
	}
	
	public boolean validatePassword(String password, String password2){
		
		if(!password.equals(password2)){
			this.setError("Passwords don't match.");
			
			return false;
		}
		
		if(password == null || password.length() == 0){
			this.setError("Please input your password.");
			
			return false;
		}
		
		if(password.length() > 20){
			this.setError("Password is too long.");
			
			return false;
		}
		
		Pattern p = Pattern.compile("^[A-Za-z0-9]+$");
		
		Matcher matchPassword = p.matcher(password);
		
		if(!matchPassword.find()){
			this.setError("Password in a wrong format.");
			
			return false;
		}
		
		return true;
	}
	
	public boolean validateUsername(String username){
		
		if(username == null || username.length() == 0){
			this.setError("Please input your username.");
			
			return false;
		}
		
		if(username.length() > 30){
			this.setError("Username is too long.");
			
			return false;
		}
		
		Pattern p = Pattern.compile("^[a-zA-Z0-9 _]+$");
		
		Matcher matchName = p.matcher(username);
		
		if(!matchName.find()){
			this.setError("Username in a wrong format.");
			
			return false;
		}
		return true;
	}
	
	// ------------------------------------------------ POD TEM GLEJTE, GOR NI QL :D
	
	// Univerzalna metoda za input text. validateInputText(string ki ga validiramo, njegova max dožina, ali je lahko prazen ali ne, regex, 3 string errorji glede na vrsto napake);
	// Demonstracija uporabe v VpisniListServlet
	public boolean validateInputText(String inputString, int maxLength, boolean empty, String regex, String prazenVnosError, String predolgVnosError, String napačenRegexError) {
		if(inputString == null || inputString.length() == 0) {
			if (empty) {
				return true;
			}
			
			this.setError(prazenVnosError);
			return false;
		}
		
		if(inputString.length() > maxLength) {
			this.setError(predolgVnosError);
			return false;
		}
		
		Pattern pattern = Pattern.compile(regex); // "^63\\d{6}$"
		Matcher matcher = pattern.matcher(inputString);
		
		if(!matcher.find()) {
			this.setError(napačenRegexError);
			return false;
		}
		
		return true;
	}
	
	public boolean validateEmso(String emso, String drzava) { // EMŠO je malo bolj extrem za poračunat
		if(emso == null || emso.length() == 0){
			this.setError("Prosim vnesite EMŠO");
			
			return false;
		}
		
		if(emso.length() > 13){
			this.setError("Predolg EMŠO");
			
			return false;
		}
		
		Pattern patternEmso = null;
		if (drzava.equals("Slovenija")) {
			patternEmso = Pattern.compile("^(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[012])(9[1-9][0-9]|00[1-9]|0[1-9][0-9])50[0-9]{4}$");
		} else {
			patternEmso = Pattern.compile("^(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[012])(19[1-9][0-9]|200[1-9]|20[1-9][0-9])00000$");
		}
		Matcher matcherEmso = patternEmso.matcher(emso);
		
		if(!matcherEmso.find()){
			this.setError("Napačen format EMŠO-a");
			
			return false;
		}
		
		if (drzava.equals("Slovenija")) {
			int sum = 0;
			for (int i = 0; i < 12; i++) {
				sum += (emso.charAt(i) - '0') * (7 - (i % 6));
			}
			
			
			int kontrolnaStevilka;
			int ostanek = sum % 11;
			
			if(ostanek == 0) {
				kontrolnaStevilka = 0;
			} else {
				kontrolnaStevilka = 11 - ostanek;
			}
			
			if (kontrolnaStevilka != (emso.charAt(12) - '0')) {
				this.setError("Neveljaven EMŠO.");
				
				return false;
			}
		}
		
		return true;
	}
}