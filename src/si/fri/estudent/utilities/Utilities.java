package si.fri.estudent.utilities;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.Cookie;

public class Utilities {
	public static Cookie getCookie(String name, Cookie[] cookies){
		for(Cookie c : cookies)
			if(c.getName().equals(name))
				return c;
				
		return null;
	}
	
	public static String getStudijskoLeto(){
		String result = "";
		
		Date date = new Date();
		
		SimpleDateFormat simpleDateformat = new SimpleDateFormat("yyyy");
		int year = Integer.parseInt(simpleDateformat.format(date));
		simpleDateformat = new SimpleDateFormat("MM");
		int month = Integer.parseInt(simpleDateformat.format(date));
		
		if(month > 8)
			result = year + "/" + (year+1);
		else
			result = (year-1) + "/" + year;
		
		return result;
	}
	
	public static String getDatumIzpita(Date date){
		
		return DateFormat.getInstance().format(date).toString();
	}
	
}
