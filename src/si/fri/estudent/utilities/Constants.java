package si.fri.estudent.utilities;

public class Constants {

	/*
	 * LOGIN SETTINGS
	 */
	public static final String LOGIN_COUNTER_COOKIE = "loginCounter";
	public static final int LOGIN_LIMIT = 2;
	public static final int LOGIN_DISABLE_TIME = 1 * 1000 * 60; 
	
	/*
	 * ERRORS
	 */
	public static final String STATUS_COUNTER_ERR = "Presegli ste število dovoljenih poskusov";
	public static final String STATUS_INVALID_DATA = "Podatki niso pravilni";
	public static final String STATUS_SUCCESS = "Prijava uspela";
	
	/*
	 * IZPITNI ROK
	 */
	public static final int MAX_ST_OPRAVLJANJ_LETO = 3;
	public static final int MAX_ST_OPRAVLJANJ_SKUPNO = 6;
	/*
	 * VRSTE VPISA
	 * 
	 * +----------------+----------------------+---------------------+
	 * | id_vrsta_vpisa | naziv_vrsta_vpisa    | kratica_vrsta_vpisa |
	 * +----------------+----------------------+---------------------+
	 * |              1 | prvi vpis v letnik   | V1                  |
	 * |              2 | ponavljanje letnika  | V2                  |
	 * |              3 | nadaljevanje letnika | V3                  |
	 * |              4 | absolvent            | AB                  |
	 * |              5 | evidenčno            | EV                  |
	 * +----------------+----------------------+---------------------+
	 */
	public static final int VPIS_PRVI_VPIS = 1;
	public static final int VPIS_PONAVLJANJE = 2;
	public static final int VPIS_NADALJEVANJE = 3;
	public static final int VPIS_ABSOLVENT = 4;
	
	public static final String NACIN_REDNO = "R";
	public static final String NACIN_IZREDNO = "I";
	
	/*
	 * STUDIJSKI PROGRAMI
	 * 
	 * +----------------------+------------------------------------+---------------------------+
	 * | id_studijski_program | naziv_studijski_program            | kratica_studijski_program |
 	 * +----------------------+------------------------------------+---------------------------+
	 * |                    1 | Ra?unalništvo in informatika       | RI                        |
	 * |                    2 | Ra?unalništvo in matematika        | IŠRM                      |
	 * |                    3 | Upravna informatika                | UI                        |
	 * |                    4 | Humanistika in družboslovje        | HD                        |
	 * |                    5 | Kognitivne znanosti                | KZ                        |
	 * |                    6 | Informacijski sistemi in odlo?anje | IS                        |
	 * +----------------------+------------------------------------+---------------------------+
	 */
	public static final int PROGRAM_RI = 1;
	public static final int PROGRAM_RM = 2;
	public static final int PROGRAM_UI = 3;
	public static final int PROGRAM_HD = 4;
	public static final int PROGRAM_KZ = 5;
	public static final int PROGRAM_IS = 6;
	

	/*
	 * VRSTA ŠTUDIJA
	 * 
	 * +------------------+------------------------+-----------------+-----------------------+----------+
	 * | id_vrsta_studija | naziv_vrsta_studija    | stopnja_studija | kratica_vrsta_studija | sifra_vl |
	 * +------------------+------------------------+-----------------+-----------------------+----------+
	 * |                1 | visokošolski strokovni | 1               | BVS                   | J        |
	 * |                2 | univerzitetni          | 1               | BUN                   | K        |
	 * |                3 | magistrski             | 2               | BMA                   | L        |
	 * |                4 | enovit magistrski      | 2               | BMA                   | N        |
	 * |                5 | doktorski              | 3               | BDR                   | M        |
	 * |                6 | visokošolski strokovni |                 | VS                    | B        |
	 * |                7 | univerzitetni          |                 | UN                    | C        |
	 * |                8 | specialisti?ni         |                 | SP                    | E        |
	 * |                9 | magistrski             |                 | MAG                   | F        |
	 * |               10 | doktorski              |                 | DOK                   | G        |
	 * |               11 | enovit doktorski       |                 | EDR                   | H        |
	 * +------------------+------------------------+-----------------+-----------------------+----------+
	 */
	 public static final int PROGRAM_BVS = 1;
	 public static final int PROGRAM_BUN = 2;
	 public static final int PROGRAM_BMA = 3;
	 public static final int PROGRAM_BMA2 = 4;
	 public static final int PROGRAM_BDR = 5;
	 public static final int PROGRAM_VS = 6;
	 public static final int PROGRAM_UN = 7;
	 public static final int PROGRAM_SP = 8;
	 public static final int PROGRAM_MAG = 9;
	 public static final int PROGRAM_DOK = 10;
	 public static final int PROGRAM_EDR = 11;
	 
	 /*
	  * VLOGE
	  * +----------------+-------------------+
	  * | id_vloga_oseba | naziv_vloga_oseba |
	  *	+----------------+-------------------+
	  *	|              1 | student           |
	  * |              2 | skrbnik           |
	  * |              3 | referentka        |
	  * |              4 | ucitelj           |
	  * +----------------+-------------------+
	  */
	 public static final int VLOGA_STUDENT = 1;
	 public static final int VLOGA_SKRBRNIK = 2;
	 public static final int VLOGA_REFERENTKA = 3;
	 public static final int VLOGA_UCITELJ = 4;
	 
	 
	 /*
	  * POSEBNE POTREBE
	  * 
	  * +--------------------+------------------------------+-----------------------+
	  * | id_posebne_potrebe | naziv_posebne_potrebe        | sifra_posebne_potrebe |
  	  * +--------------------+------------------------------+-----------------------+
	  * |                  1 | A - slepi/slabovidni         | A                     |
	  * |                  2 | B - gluhi/naglušni           | B                     |
	  * |                  3 | C - gibalno ovirani/invalidi | C                     |
	  * |                  4 | D - drugo                    | D                     |
	  * +--------------------+------------------------------+-----------------------+
	  */
	 public static final int POS_POTREBA_SLEPI = 1;
	 public static final int POS_POTREBA_GLUHI = 2;
	 public static final int POS_POTREBA_INVAL = 3;
	 public static final int POS_POTERBA_DRUGO = 4;
	 
	 
	 /*
	  * NAČIN OCENJEVANJA
	  * 
	  * I - SAMO IZPIT
	  * v - SAMO VAJE
	  * IV - IZPIT IN VAJE
	  * K - SAMO KONČNA
	  */
	 
	 public static final String OCENA_IZPIT = "I";
	 public static final String OCENA_VAJE = "V";
	 public static final String OCENA_IZPIT_VAJE = "IV";
	 public static final String OCENA_KONCNA = "K";

	 
}
