package si.fri.estudent.utilities;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Header;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


public class Tiskanje {
	
	String naslov;
	String dateNow;
	String path;
	String fontPath;
	public static final String LOGO = "fri.jpg";
	BaseFont bf=null;
	
	public static final String URL="www.estudent.si";
	Document dokument;
	
	public Tiskanje(HttpServletRequest hsr,HttpServletResponse response,String naslov){
		this.path=hsr.getServletContext().getRealPath("images/");
		this.fontPath=hsr.getServletContext().getRealPath("fonts/");
		this.naslov=naslov;
		dateNow=dateNow();
		System.out.println("naslov"+naslov);
		try{
			 bf= BaseFont.createFont(fontPath+"/arialuni.ttf",  BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
		}catch(Exception e){}
		
		Font fontNaslov=new Font(bf,20);
		
		try {
			 
			hsr.setCharacterEncoding( "UTF-8" ); 
			System.out.println("tiskanje ččšćž");
			String contentType= "charset=UTF-8";
			response.setContentType(contentType);
			response.setHeader("Content-Disposition", " attachment; filename=\"Izpis.pdf\"");
			
			dokument=new Document();
			PdfWriter.getInstance(dokument, response.getOutputStream());
			
			dokument.open();
			dokument.addCreationDate();
			
			
			
			
		    Paragraph p=new Paragraph(URL+"                                                                                                 "+dateNow);
		    p.setAlignment(Element.ALIGN_LEFT);		    
		    dokument.add(p);
		    
		    Font fontPodatki=new Font(bf,8);
		    
		    p=new Paragraph("Fakulteta za računalništvo in informatiko    ",fontPodatki);
		    p.setSpacingBefore(20);
		    p.setAlignment(Element.ALIGN_RIGHT);	
		    dokument.add(p);
		    
		    
		    p=new Paragraph("Tržaška cesta 25     ",fontPodatki);		    
		    p.setAlignment(Element.ALIGN_RIGHT);			    
		    dokument.add(p);
		    
		    p=new Paragraph("1000 Ljubljana    ",fontPodatki);
		    p.setAlignment(Element.ALIGN_RIGHT);	
		    dokument.add(p);
		    
		    p=new Paragraph("Slovenija    ",fontPodatki);
		    p.setAlignment(Element.ALIGN_RIGHT);	
		    dokument.add(p);
		    
			Image img = Image.getInstance(path+"/"+LOGO);
			img.setAbsolutePosition(30,680);
			img.scaleAbsolute(225f, 120f);		       
		    dokument.add(img);
		    
		    
		    dokument.add( Chunk.NEWLINE);
		    dokument.add( Chunk.NEWLINE);
		    dokument.add( Chunk.NEWLINE);
		    p=new Paragraph(naslov,fontNaslov);
			p.setAlignment(Element.ALIGN_CENTER);	
			p.setSpacingAfter(40);
			dokument.add(p);
			
			
			
			
		} catch (Exception e) {	
			System.out.println(e.getLocalizedMessage());
		}

		
	}
	public void zakljuci(){
		dokument.close();
	}
	
	public Document dobiDocument(){
		dokument.close();
		return dokument;
	}
	
	public void dodajTabelo(String[][] tabela,boolean headerjiNavpicno,boolean headerjiVodoravno){
		int stVrstice=0;
		int stStolpca=0;
	
		Font fontHeader=new Font(bf,8);
		Font fontOstalo=new Font(bf,8);
		Font font=new Font(bf,8);
		
		
		PdfPTable table=new PdfPTable(tabela[0].length);
		
		//table.setHeaderRows(1);
				
		for(String[] vrstica :tabela){			
			stStolpca=0;
			for(String polje :vrstica){
				if(stVrstice==0 ){
					font=fontHeader;
				}else if(stStolpca==0){
					font=fontHeader;
				}else{
					font=fontOstalo;
				}
				font=fontOstalo;
				
				PdfPCell c1 = new PdfPCell(new Paragraph(polje,font));
				if(stStolpca==0 && stVrstice==0 &&  (headerjiNavpicno&&headerjiVodoravno )){
					//c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
					
				}else if(stVrstice==0 && headerjiVodoravno){
					c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
					//c1.setBorderWidthBottom(1.5f);
				}else if(stStolpca==0 && headerjiNavpicno){
					c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
					//c1.setBorderWidthRight(1.5f);
				}
				
				c1.setHorizontalAlignment(Element.ALIGN_CENTER);
				c1.setPadding(5f);
				
				table.addCell(c1);
				
				stStolpca++;
			}
			stVrstice++;
		}
		table.setSpacingAfter(10);
		try {
			dokument.add(table);
		} catch (DocumentException e) {			
		}
	}	
	
	
	
	public void dodajPraznoVrstico(){
		try {
			dokument.add(Chunk.NEWLINE);
		} catch (DocumentException e) {}
	}
	
	public String dateNow(){
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter= 
				  new SimpleDateFormat("dd.MM.yyyy HH:mm");
		String dateNow = formatter.format(currentDate.getTime());
		return dateNow;
	}
	
	public void dodajBesedilo(String besedilo){
		try{
			Font font=null;
			try {
				BaseFont bf = BaseFont.createFont(fontPath+"/arialuni.ttf",  BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				
				font=new Font(bf,8);
			} catch (Exception e){
				font = new Font(bf,8);
				e.printStackTrace();
			}
			Paragraph p=new Paragraph(besedilo,font);
			p.setIndentationLeft(20);
			p.setIndentationRight(20);
			p.setSpacingAfter(20);
			dokument.add(p);
		}catch(Exception e){
			
			e.printStackTrace();
			
		}
	}
	
	public void dodajBesediloBrezProsteVrstice(String besedilo){
		try{
			Font font=null;
			try {
				BaseFont bf = BaseFont.createFont(fontPath+"/arialuni.ttf",  BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				
				font=new Font(bf,8);
			} catch (Exception e){
				System.out.println(e.getLocalizedMessage());
			}
			Paragraph p=new Paragraph(besedilo,font);
			p.setIndentationLeft(20);
			p.setIndentationRight(20);			
			dokument.add(p);
		}catch(Exception e){}
	}
	
	public void dodajTabeloAnalizaPrehodnost(String[][] tabela,String leto1,String leto2){		
	
		Font fontHeader=new Font(bf,8);
		Font fontOstalo=new Font(bf,8);
		Font font=new Font(bf,8);
		
		
		PdfPTable table=new PdfPTable(4);
		
		//prva vrstica		
		PdfPCell c1 = new PdfPCell(new Paragraph("",font));
		c1.setColspan(2);
		PdfPCell c3 = new PdfPCell(new Paragraph("ŠTEVILO VPISANIH",font));
		PdfPCell c4 = new PdfPCell(new Paragraph("ODSTOTEK",font));
		
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setPadding(5f);		
		table.addCell(c1);
		c3.setHorizontalAlignment(Element.ALIGN_CENTER);
		c3.setPadding(5f);		
		table.addCell(c3);
		c4.setHorizontalAlignment(Element.ALIGN_CENTER);
		c4.setPadding(5f);		
		table.addCell(c4);
		
		c1 = new PdfPCell(new Paragraph(leto1,font));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setPadding(5f);		
		table.addCell(c1);
		c1 = new PdfPCell(new Paragraph(tabela[0][0],font));
		
		c1.setPadding(5f);		
		table.addCell(c1);
		c1 = new PdfPCell(new Paragraph(tabela[0][1],font));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setPadding(5f);		
		table.addCell(c1);
		c1 = new PdfPCell(new Paragraph(tabela[0][2],font));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setPadding(5f);		
		table.addCell(c1);
		
		c1 = new PdfPCell(new Paragraph(leto2,font));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setRowspan(3);
		c1.setPadding(5f);		
		table.addCell(c1);
		c1 = new PdfPCell(new Paragraph(tabela[1][0],font));
		
		c1.setPadding(5f);		
		table.addCell(c1);
		c1 = new PdfPCell(new Paragraph(tabela[1][1],font));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setPadding(5f);		
		table.addCell(c1);
		c1 = new PdfPCell(new Paragraph(tabela[1][2],font));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setPadding(5f);		
		table.addCell(c1);
		
		
		
		c1 = new PdfPCell(new Paragraph(tabela[2][0],font));
		
		c1.setPadding(5f);			
		table.addCell(c1);
		c1 = new PdfPCell(new Paragraph(tabela[2][1],font));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setPadding(5f);			
		table.addCell(c1);
		c1 = new PdfPCell(new Paragraph(tabela[2][2],font));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setPadding(5f);			
		table.addCell(c1);
		
		c1 = new PdfPCell(new Paragraph(tabela[3][0],font));
		
		c1.setPadding(5f);			
		table.addCell(c1);
		c1 = new PdfPCell(new Paragraph(tabela[3][1],font));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setPadding(5f);			
		table.addCell(c1);
		c1 = new PdfPCell(new Paragraph(tabela[3][2],font));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setPadding(5f);			
		table.addCell(c1);
		
		
		
		
		table.setSpacingAfter(10);
		try {
			dokument.add(table);
		} catch (DocumentException e) {			
		}
	}	
}


