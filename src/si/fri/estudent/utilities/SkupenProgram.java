package si.fri.estudent.utilities;

import si.fri.estudent.jpa.Studijski_program;
import si.fri.estudent.jpa.Vrsta_studija;

public class SkupenProgram{
	private Studijski_program studijskiProgram;
	private Vrsta_studija vrstaStudija;
	
	public SkupenProgram(Studijski_program sp,Vrsta_studija vs){
		setStudijskiProgram(sp);
		setVrstaStudija(vs);
	}

	public Studijski_program getStudijskiProgram() {
		return studijskiProgram;
	}

	public void setStudijskiProgram(Studijski_program studijskiProgram) {
		this.studijskiProgram = studijskiProgram;
	}

	public Vrsta_studija getVrstaStudija() {
		return vrstaStudija;
	}

	public void setVrstaStudija(Vrsta_studija vrstaStudija) {
		this.vrstaStudija = vrstaStudija;
	}
	
	
	
}